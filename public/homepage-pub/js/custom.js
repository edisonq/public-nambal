
/* prettyPhoto Gallery */

jQuery(".prettyphoto").prettyPhoto({
   overlay_gallery: false, social_tools: false
});


/* Scroll to GoTop */

$(".gotop").hide();

 $(function(){
	$(window).scroll(function(){
	  if ($(this).scrollTop()>500)
	  {
		$('.gotop').fadeIn();
	  } 
	  else
	  {
		$('.gotop').fadeOut();
	  }
	});

	$('.gotop a').click(function (e) {
	  e.preventDefault();
	  $('body,html').animate({scrollTop: 0}, 1500);
	});

});

  
 /* Navbar Script */
 
$(".nav-icon a").click(function(e){
	e.preventDefault();
		var menu=$(this);
		var down=$(".navigation-bar");
	
	if(down.hasClass("open"))
		{
			down.slideUp(300);
			down.removeClass("open");
		}
	else
		{
			down.slideDown(300);
			down.addClass("open");
		}
});
  
  /* Animation */
  
  /* Heading */
  
	$('.sub-head').addClass('animation bounceIn');
	$('.phone-item-left').addClass('animation bounceIn');
	$('.phone-item-up').addClass('animation bounceIn');
	$('.phone-item-right').addClass('animation bounceIn');
  
  /* Config Block */
  
  /* 1st Block */
  
$('.config-one-item').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInDown');
}, { offset: '70%' });
	
	
  /* Portfolio */
	
$('.portfolio-item').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInUpBig');
}, { offset: '90%' });
	
 /* Features */
 
$('.feature-item').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('flipInY');
}, { offset: '70%' });
	
	
  /* Pricing-table */
  
$('.tone').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInLeft');
}, { offset: '90%' });
	
$('.ttwo').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInLeft');
}, { offset: '90%' });
	
$('.tthree').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInLeft');
}, { offset: '90%' });
	
$('.tfour').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInLeft');
}, { offset: '90%' });
	
$('.tfive').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('fadeInLeft');
}, { offset: '90%' });
	
	
 /* About */
 
$('.about-item').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('flipInY');
}, { offset: '70%' });
	
 /* Contact */
 
$('.contact-item').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('flipInY');
}, { offset: '70%' });
	
 /* Contact Social media*/
 
$('.social').waypoint(function(down) {
	$(this).addClass('animation');
	$(this).addClass('bounceIn');
}, { offset: '70%' });