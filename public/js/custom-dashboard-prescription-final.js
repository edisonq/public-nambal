jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="printandsave" class="btn btn-success" data-loading-text="Loading..." disabled="disabled">Print and Save</a><a href="javascript:void(0);" id="saveonly" class="btn btn-primary" data-loading-text="Loading...">Save only</a><button id="cancelbutton" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Finalize</span> Prescription Information</h3>');

jQuery( "#printandsave" ).on( "click", function() 
{	
	console.log('printandsave');
});
jQuery( "#saveonly" ).on( "click", function handler() 
{	
	jQuery('#show-modal-change .modal-footer #saveonly').html('Loading.. please wait');
	jQuery('#show-modal-change .modal-footer #saveonly').attr('disabled','disabled');
	jQuery('#show-modal-change .modal-footer #printandsave').attr('disabled','disabled');
	jQuery('#show-modal-change .modal-footer #cancelbutton').attr('disabled','disabled');
	jQuery('#saveonly').off('click');
	var formData = jQuery('.form-horizontal').serialize();
	jQuery.ajax({
	url: BASEURLL+"prescriptions/savePrescription?ajax=true&sessionid="+IDsafe_user+'&IDsafeprescription=',
	type: "POST",
	data: formData,
	dataType: "JSON",
	success: function (data) {
	    if (data.errorStatus == false)
	    {
	    	var IDsafeprescription = data.IDsafeprescription;
			jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
			jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#show-modal-change').modal('hide');
		}
		else
		{
			jQuery('.alert').alert('close');
			jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
			jQuery('#show-modal-change .modal-footer #saveonly').html('Continue Creating Prescription');
			jQuery('#show-modal-change .modal-footer #saveonly').removeAttr('disabled');
			jQuery('#show-modal-change .modal-footer #printandsave').removeAttr('disabled');
			jQuery('#show-modal-change .modal-footer #cancelbutton').removeAttr('disabled');
			jQuery('.alert').fadeOut(5000);
			jQuery('#saveonly').on( "click", handler);
		}
	}
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#saveonly').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".deletemedication" ).on( "click", function() 
{
	var IDsafedrug = jQuery(this).attr('data-IDsafedrug');
	var IDsafeprescription = jQuery(this).attr('data-IDsafeprescription');
	jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Medication information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this medication?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethismedicationyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button"  id="canceldeletebutton" class="btn btn-danger" aria-hidden="true">No</button>');
    jQuery("#deletethismedicationyes" ).on( "click", function() 
	{
		jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"medication/delete?ajax=true",
		type: "POST",
		data: {
		    medicationid: IDsafedrug
		},
		dataType: "JSON",
		success: function (data) {
		    // if (data.errorStatus == false)
		    // {				
				jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			// }
		}
		});
	});
	jQuery('#canceldeletebutton').on( "click", function() 
	{
		jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionFinish?IDsafeprescription='+IDsafeprescription+'&sessionid='+IDsafe_user);
	});
});
jQuery( ".modifymedication" ).on( "click", function() 
{
	var IDsafedrug = jQuery(this).attr('data-IDsafedrug');
	jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Medication</h3>');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	jQuery('#show-modal-change .modal-footer').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	jQuery('#show-modal-change .modal-body').load(BASEURLL+'medication/modificationMedicationDisplay?sessionid='+IDsafe_user+'&IDsafedrug='+IDsafedrug);
});
jQuery(".addnewmedication").on( "click", function() 
{
	var IDsafeprescription = jQuery(this).attr('data-IDsafeprescription');
	jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Medication Information</h3>');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	jQuery('#show-modal-change .modal-footer').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	
	// loading the table medication UI
	// jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionInformation?IDsafeprescription='+IDsafeprescription+"&sessionid="+IDsafe_user);
	jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionInformationTable?IDsafeprescription='+IDsafeprescription+"&sessionid="+IDsafe_user);

});
jQuery( "#includeAddress" ).on( "click", function() 
{
	jQuery('#addressareanisiya').toggle();
});
jQuery( "#includeAge" ).on( "click", function() 
{
	jQuery('#agenisiya').toggle();
});
jQuery( "#includeGender" ).on( "click", function() 
{
	jQuery('#gendernisiya').toggle();
});
jQuery( "#includeDob" ).on( "click", function() 
{
	jQuery('#dobnisiya').toggle();
});
jQuery( "#includeExpiry" ).on( "click", function() 
{
	
});
jQuery( document ).ready(function() 
{
	jQuery( '.form-horizontal' ).on("keyup", function (e) {
	 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
	  {
	    jQuery('#saveonly').click();
	    return false;  
	  }
	}); 
});  