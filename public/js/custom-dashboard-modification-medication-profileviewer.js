jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifymedicationnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel-button" aria-hidden="true">Cancel</button>');
jQuery( "#modifymedicationnow" ).on( "click", function handler() 
{	
	jQuery('#show-modal-change .modal-footer #modifymedicationnow').html('Loading.. please wait');
	jQuery('#show-modal-change .modal-footer #modifymedicationnow').attr('disabled','disabled');
	jQuery('#show-modal-change .modal-footer #cancel-button').attr('disabled','disabled');
	jQuery('#modifymedicationnow').off('click');
	var formData = jQuery('.form-horizontal').serialize();
	jQuery.ajax({
	url: BASEURLL+"medication/updateDoctor?ajax=true&sessionid="+IDsafe_user,
	type: "POST",
	data: formData,
	dataType: "JSON",
	success: function (data) {
		
	    if (data.errorStatus == false)
	    {
	    	var IDsafeprescription = data.medicationList.IDsafe_prescription;
	    	jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#journal-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			if (IDsafeprescription)
			{
				jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionFinish?IDsafeprescription='+IDsafeprescription+'&sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('#show-modal-change').modal('hide');
			}
		}
		else
		{
			jQuery('#show-modal-change .modal-footer #modifymedicationnow').html('Save Changes');
			jQuery('#show-modal-change .modal-footer #modifymedicationnow').removeAttr('disabled');
			jQuery('#show-modal-change .modal-footer #cancel-button').removeAttr('disabled');
			jQuery('.alert').alert('close');
			jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
			jQuery('.alert').fadeOut(5000);
			jQuery('#modifymedicationnow').on( "click", handler);
		}
	}
	});
});
jQuery( document ).ready(function() 
{
	jQuery( '.form-horizontal' ).on("keyup", function (e) {
	 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
	  {
	    jQuery('#modifymedicationnow').click();
	    return false;  
	  }
	}); 
});   