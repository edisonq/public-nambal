jQuery(function(){

	/*** form wizard customization ***/
	
		jQuery('.previous, .next').click(function(){
			jQuery("html, body").delay(500).animate({scrollTop: jQuery('.tabbable').offset().top });
		});
		
	/*** end form wizard customization ***/
	
	//intialize numbers only restriction
	var inputNumbers = new numbersOnly();
	
	//numbers only for this field
	inputNumbers.restrict('#number-of-doctors');
	
	var countryCodeElementId = 0;
	
	jQuery('#add-contact-number').click(function(){
	
		countryCodeElementId += 1;
	
		jQuery('#contact-number-fieldset').append('<div class="control-group"><label for="clinic-contact-number-countryCode" class="control-label">Country Code: </label><div class="controls form-group"><div class="col-xs-11 col-sm-5"><select name="countryCode[]" id="countryCode'+countryCodeElementId+'" class="chzn-select col-xs-12 col-sm-12"></select><input type="hidden" id="countryCodeSelected'+countryCodeElementId+'" value=""/></div><div >&nbsp;&nbsp;<i class="fa fa-question-circle inline" title="Country code of the clinic contact number"></i></div><script language="javascript">populateCountryCode("countryCode'+countryCodeElementId+'", "countryCodeSelected'+countryCodeElementId+'");</script></div></div><div class="control-group"><label for="clinic-contact-number-areaCode-phoneNumber" class="control-label ">Area Code - Phone Number </label><div class="controls form-group"><div class="col-xs-4 col-sm-2"><input type="text" name="areaCode[]" placeholder="Area Code" class="areaCode form-control" value=""/></div><div class="col-xs-7 col-sm-3"><input type="text" name="number[]" placeholder="Phone Number" class="number form-control" value=""/></div><div >&nbsp;&nbsp;<i class="fa fa-question-circle inline" title="Area code and Phone number of the clinic contact number"></i></div></div></div><br/>');
		
		//numbers only for this field
		inputNumbers.restrict('.areaCode, .number');
	});
	
	/****** clinic role  ******/
	
		jQuery('#clinic-role').find("input[type='checkbox']").each(function(){
			// elementId-container Id
			var thisElementId = jQuery(this).prop('id');
			
			// display this elementId-container if checked during on load
			if(jQuery(this).is(':checked')){
				jQuery('#'+thisElementId+'-container').css('display','block').find('input[type="text"]').prop('required','required');
			}
			
			//  display elementId-container when checked 
			jQuery(this).change(function(){
				var fieldContainer = jQuery('#'+thisElementId+'-container');
				var displayStatus = fieldContainer.css('display');
			
				// display this elementId-container
				if(displayStatus == 'block'){
					fieldContainer.css('display','none');
					fieldContainer.find('input[type="text"]').prop('value','').removeAttr('required');

				}else{
					fieldContainer.css('display','block');
					fieldContainer.find('input[type="text"]').prop('required','required');
				}
				
			});
		});
		
	/****** end clinic role ******/
	
	/****** create clinic information jquery/ajax process ******/
		jQuery('.createClinicInformation').click(function(){
			var isEmpty = false;
			var isEmptyRequired = false;

			// check if some required fileds is empty
			jQuery('#create-clinic-information').find('select, input').each(function(){
				if(jQuery(this).attr('required') == 'required'){
					if(jQuery(this).val() == '' || jQuery(this).val() == 0){
						isEmptyRequired = true;
					}
				}
				
			});
			
			// if all required fields are not empty
			if(isEmptyRequired == false){
				jQuery('#message').css('display','none');
			
				// check if there are clinic hours set to 00
				jQuery('.hours').each(function(){
					if(this.value == '00'){
						isEmpty = true;
					}
				});
				
				// if there are clinic hours set to 00
				if(isEmpty == true){
					jQuery('#createClinicInformationModal .modal-header').css('display','none');
					jQuery('#createClinicInformationModal .modal-footer').css('display','block');
					jQuery('#createClinicInformationModal').addClass('in').attr('aria-hidden','false').css('display','block');
					jQuery('#modal-message').append("Some of your clinic hours is not set!");
					jQuery('#dismissModalBtn').addClass('btn-primary').html("Set clinic hours");
					jQuery('#yesModalBtn').addClass('btn-success').html("Proceed anyway");
				}else{		
					jQuery('.saveCreateClinicInformation').click();
				}
			}else{
				jQuery('.previous').click();
				jQuery('#message').css('display','block');
				jQuery('#message-content').html('Please fill in required clinic information');
			}
		});
			
		// set clinic hours
		jQuery('#dismissModalBtn').click(function(){
			jQuery('#createClinicInformationModal').removeClass('in').attr('aria-hidden','true');
			
			setTimeout(function(){
				jQuery('#createClinicInformationModal').css('display','none');
				jQuery("html, body").delay(500).animate({scrollTop: jQuery('html, body').offset().top });
			},500);
		});
		
		// proceed anyway
		jQuery('#yesModalBtn').click(function(){
			jQuery('#createClinicInformationModal .modal-footer').css('display','none');
			jQuery('#warning-message').removeClass('alert alert-warning');
			jQuery('#modal-message').html("Loading...");
			jQuery('#dismissModalBtn').css('display','none');
			jQuery('#yesModalBtn').css('display','none');	

			jQuery('.saveCreateClinicInformation').click();
				
			jQuery('.errorTime').find('span').each(function(){
				jQuery(this).html('');
			});
		});

		// submit create clinic information form
		jQuery('#create-clinic-information').submit(function(){
			ajaxSubmitForm(jQuery(this));
			return false;
		});
		
		// ajax 
		var ajaxSubmitForm = function(form){
			jQuery.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				dataType: 'json',
				cache: false,
				success: function(data){
					switch(data.status){
						case 'validation error':
							jQuery('.previous').click();
							jQuery('#message').css('display','block');
							jQuery('#message-content').html(data.message);
						break;
						
						case 'create clinic':
							jQuery('#warning-message').removeClass('alert alert-warning');
							jQuery('#modal-message').html(data.message);
							
							setTimeout(function(){
								jQuery('#createClinicInformationModal').removeClass('in').attr('aria-hidden','true');
								jQuery('.previous').click();
								jQuery('#createClinicInformationModal').css('display','none');
								jQuery("html, body").delay(500).animate({scrollTop: jQuery('html, body').offset().top });
							},5000);						
						break;
						
						case 'create failed':
							jQuery("html, body").delay(500).animate({scrollTop: jQuery('html, body').offset().top });
							jQuery('#message').css('display','block');
							jQuery('#message-content').html(data.message);
						break;
						
						case 'error time':
							if(data.errorTime){
								// close modal
								jQuery('#createClinicInformationModal').removeClass('in').attr('aria-hidden','true').css('display','none');
								
								//find time error and display message
								var errorTimeFirstDay = '';
							
								for(var i=0;i < data.errorTime.length; i++){
									jQuery('#clinic-time-'+data.errorTime[i]+' span').html('Time start or Time end not set properly...');
								}
								
								switch(data.errorTime[0]){
									case 1:
										errorTimeFirstDay = 'monday';
									break;
									
									case 2:
										errorTimeFirstDay = 'tuesday';
									break;
									
									case 3:
										errorTimeFirstDay = 'wednesday';
									break;
									
									case 4:
										errorTimeFirstDay = 'thursday';
									break;
									
									case 5:
										errorTimeFirstDay = 'friday';
									break;
									
									case 6:
										errorTimeFirstDay = 'saturday';
									break;
									
									case 7:
										errorTimeFirstDay = 'sunday';
									break;
								
									default:
										errorTimeFirstDay = 'monday';
									break;
								}
								
								jQuery("html, body").delay(500).animate({scrollTop: jQuery('#control-group-'+errorTimeFirstDay).offset().top });
								
								jQuery('#message').css('display','block');
								jQuery('#message-content').html(data.message);
							}
						break;
					
						default:
						return false;
					}
				}
			});
		};
	
	/****** end create clinic information jquery/ajax process ******/
});

