/*
 * Simple State/Province Select plugin for jQuery
 * 
 * Example:
 * $(document).ready(function() {
 *    $('#country').linkToStates('#state');
 *  });
 *
 * Copyright (c) 2008 Adam Daniels
 * Licensed under the MIT License
 *
 */
jQuery(document).ready(function(){		
  var canadian_provinces = {
		'Ontario' : 'Ontario',
		'Quebec' : 'Quebec',
		'Manitoba' : 'Manitoba',
		'Alberta' : 'Alberta',
		'New Foundland and Labrador' : 'New Foundland and Labrador',
		'Nova Scotia' : 'Nova Scotia',
		'Prince Edward Island' : 'Prince Edward Island',
		'Saskatchewan' : 'Saskatchewan',
		'Nunavut' : 'Nunavut',
		'British Columbia' : 'British Columbia',
		'New Brunswick' : 'New Brunswick',
		'Yukon' : 'Yukon',
		'Northwest Territories' : 'Northwest Territories'
	}
	
	var ph_province = {
		'Abra' :	'Abra'	,
		'Agusan del Norte' :	'Agusan del Norte'	,
		'Agusan del Sur' :	'Agusan del Sur'	,
		'Aklan' :	'Aklan'	,
		'Albay' :	'Albay'	,
		'Antique' :	'Antique'	,
		'Apayao' :	'Apayao'	,
		'Aurora' :	'Aurora'	,
		'Basilan' :	'Basilan'	,
		'Bataan' :	'Bataan'	,
		'Batanes' :	'Batanes'	,
		'Batangas' :	'Batangas'	,
		'Benguet' :	'Benguet'	,
		'Biliran' :	'Biliran'	,
		'Bohol' :	'Bohol'	,
		'Bukidnon' :	'Bukidnon'	,
		'Bulacan' :	'Bulacan'	,
		'Cagayan' :	'Cagayan'	,
		'Camarines Norte' :	'Camarines Norte'	,
		'Camarines Sur' :	'Camarines Sur'	,
		'Camiguin' :	'Camiguin'	,
		'Capiz' :	'Capiz'	,
		'Catanduanes' :	'Catanduanes'	,
		'Cavite' :	'Cavite'	,
		'Cebu' :	'Cebu'	,
		'Compostela Valley' :	'Compostela Valley'	,
		'Cotabato' :	'Cotabato'	,
		'Davao del Norte' :	'Davao del Norte'	,
		'Davao del Sur' :	'Davao del Sur'	,
		'Davao Occidental' :	'Davao Occidental'	,
		'Davao Oriental' :	'Davao Oriental'	,
		'Dinagat Islands' :	'Dinagat Islands'	,
		'Eastern Samar' :	'Eastern Samar'	,
		'Guimaras' :	'Guimaras'	,
		'Ifugao' :	'Ifugao'	,
		'Ilocos Norte' :	'Ilocos Norte'	,
		'Ilocos Sur' :	'Ilocos Sur'	,
		'Iloilo' :	'Iloilo'	,
		'Isabela' :	'Isabela'	,
		'Kalinga' :	'Kalinga'	,
		'La Union' :	'La Union'	,
		'Laguna' :	'Laguna'	,
		'Lanao del Norte' :	'Lanao del Norte'	,
		'Lanao del Sur' :	'Lanao del Sur'	,
		'Leyte' :	'Leyte'	,
		'Maguindanao' :	'Maguindanao'	,
		'Marinduque' :	'Marinduque'	,
		'Masbate' :	'Masbate'	,
		'Misamis Occidental' :	'Misamis Occidental'	,
		'Misamis Oriental' :	'Misamis Oriental'	,
		'Mountain Province' :	'Mountain Province'	,
		'Negros Occidental' :	'Negros Occidental'	,
		'Negros Oriental' :	'Negros Oriental'	,
		'Northern Samar' :	'Northern Samar'	,
		'Nueva Ecija' :	'Nueva Ecija'	,
		'Nueva Vizcaya' :	'Nueva Vizcaya'	,
		'Occidental Mindoro' :	'Occidental Mindoro'	,
		'Oriental Mindoro' :	'Oriental Mindoro'	,
		'Palawan' :	'Palawan'	,
		'Pampanga' :	'Pampanga'	,
		'Pangasinan' :	'Pangasinan'	,
		'Quezon' :	'Quezon'	,
		'Quirino' :	'Quirino'	,
		'Rizal' :	'Rizal'	,
		'Romblon' :	'Romblon'	,
		'Samar' :	'Samar'	,
		'Sarangani' :	'Sarangani'	,
		'Siquijor' :	'Siquijor'	,
		'Sorsogon' :	'Sorsogon'	,
		'South Cotabato' :	'South Cotabato'	,
		'Southern Leyte' :	'Southern Leyte'	,
		'Sultan Kudarat' :	'Sultan Kudarat'	,
		'Sulu' :	'Sulu'	,
		'Surigao del Norte' :	'Surigao del Norte'	,
		'Surigao del Sur' :	'Surigao del Sur'	,
		'Tarlac' :	'Tarlac'	,
		'Tawi-Tawi' :	'Tawi-Tawi'	,
		'Zambales' :	'Zambales'	,
		'Zamboanga del Norte' :	'Zamboanga del Norte'	,
		'Zamboanga del Sur' :	'Zamboanga del Sur'	,
		'Zamboanga Sibugay' :	'Zamboanga Sibugay'	,
		'Metro Manila' :	'Metro Manila'	
	}

	var us_states = {
		'Alabama' : 'Alabama',
		'Alaska' : 'Alaska',
		'Arizona' : 'Arizona',
		'Arkansas' : 'Arkansas',
		'California' : 'California',
		'Colorado' : 'Colorado',
		'Connecticut' : 'Connecticut',
		'Delaware' : 'Delaware',							
		'Florida' : 'Florida',
		'Georgia' : 'Georgia',
		'Hawaii' : 'Hawaii',
		'Idaho' : 'Idaho',
		'Illinois' : 'Illinois',
		'Indiana' : 'Indiana',
		'Iowa' : 'Iowa',
		'Kansas' : 'Kansas',
		'Kentucky' : 'Kentucky',
		'Louisiana' : 'Louisiana',
		'Maine' : 'Maine',
		'Maryland' : 'Maryland',
		'Massachusetts' : 'Massachusetts',
		'Michigan' : 'Michigan',
		'Minnesota' : 'Minnesota',
		'Mississippi' : 'Mississippi',
		'Missouri' : 'Missouri',
		'Montana' : 'Montana',
		'Nebraska' : 'Nebraska',
		'Nevada' : 'Nevada',
		'New Hampshire' : 'New Hampshire',
		'New Jersey' : 'New Jersey',							
		'New Mexico' : 'New Mexico',
		'New York' : 'New York',
		'North Carolina' : 'North Carolina',
		'North Dakota' : 'North Dakota',
		'Ohio' : 'Ohio',
		'Oklahoma' : 'Oklahoma',
		'Oregon' : 'Oregon',
		'Pennsylvania' : 'Pennsylvania',
		'Rhode Island' : 'Rhode Island',
		'South Carolina' : 'South Carolina',
		'South Dakota' : 'South Dakota',
		'Tennessee' : 'Tennessee',
		'Texas' : 'Texas',
		'Utah' : 'Utah',
		'Vermont' : 'Vermont',
		'Virginia' : 'Virginia',
		'Washington' : 'Washington',
		'West Virginia' : 'West Virginia',
		'Wisconsin' : 'Wisconsin',
		'Wyoming' : 'Wyoming'
	}
  
  jQuery.fn.extend({
		linkToStates: function(state_select_id) {
      jQuery(this).change(function() {
        var country = jQuery('#country').val();
        jQuery(state_select_id).removeOption(/.*/);
        switch (country) {
          case 'Canada':
            jQuery(state_select_id).addOption(canadian_provinces, false);
            break;
          case 'Philippines':
            jQuery(state_select_id).addOption(ph_province, false);
            break;
          case 'United States':
            jQuery(state_select_id).addOption(us_states, false);
            break;
          default:
          	//console.log(country);
            jQuery(state_select_id).addOption({ '' : 'Please select a Country'}, false);
            break;
        }
      });
      
    }
  });
});
