jQuery(document).ready(function()
{
/* datepicker */
jQuery('.date-picker').datepicker({
    autoclose: true
});
var $btnCalendar = jQuery('#btn-select-calendar-datestarted');
$btnCalendar.datepicker({
    autoclose: true
}).on('changeDate', function(ev){
        jQuery('#select-calendar-datestarted').val($btnCalendar.data('date'));
});
$('.datepicker .datepicker-days tbody').on('click', function(){  $('.datepicker').hide() });

jQuery(".mask-int-area").mask("9?999");
jQuery(".mask-int-phone").mask("999999?999");
jQuery(".masked-date").mask("99/99/9999");
jQuery(".mask-time").mask("99:99");
/* tooltip */
jQuery('.tootipper').popover();


$( ".change-address" )
  .change(function () {
    var str = $('#street').val();
    str = str+', '+$('#city').val();
    $( "#state option:selected" ).each(function() {
        // str = $( this ).text() + ", "+str;
        str = str+ ", <br>"+$( this ).text();
    });
    $( "#country option:selected" ).each(function() {
      str = str+ ", "+$( this ).text();
    });
    str = str+' '+$('#postcode').val();
    $( "#addresspreview" ).html( str );
  })
  .change();
  $( ".change-address-text" ).keypress(function() {
        var str = $('#street').val();
        str = str+', '+$('#city').val();
        $( "#state option:selected" ).each(function() {
            // str = $( this ).text() + ", "+str;
            str = str+ ", <br>"+$( this ).text();
        });
        $( "#country option:selected" ).each(function() {
          str = str+ ", "+$( this ).text();
        });
        str = str+' '+$('#postcode').val();
        $( "#addresspreview" ).html( str );
    });
  $( ".change-barangay" )
  .change(function () {
    // --
    var formBarangay = $("#barangay option:selected").text();
    jQuery.ajax(
    {
        url: BASEURLL+"barangay/getInformation?ajax=true",
        type: "POST",
        data: {
            formBarangay: formBarangay
        },
        dataType: "JSON",
        success: function (data) 
        {
            // not yet done on this part
                // $('#postcode').val(data.postcode);
                // $('#city').val(data.city); 
                // $('#countrySelected').val(data.country);
                // $('#stateSelected').val(data.state);
                // populateCountries("country", "state", "countrySelected", "stateSelected");
        }
    });
    // --
  })
  .change();
  jQuery("#addmothersname").ready(function() {
    if ($("#addmothersname").is(':checked') == false) {
        $("#mothersnamecontainer").hide();
    } else {
        $("#mothersnamecontainer").show();
    }
  });
  jQuery("#addfathersname").ready(function() {
    if ($("#addfathersname").is(':checked') == false) {
        $("#fathersnamecontainer").hide();
    } else {
        $("#fathersnamecontainer").show();
    }
  });
  $("#addmothersname").change(function() {
    if ($("#addmothersname").is(':checked') == false) {
        $("#mothersnamecontainer").hide();
    } else {
        $("#mothersnamecontainer").show();
    }
  });
  $("#addfathersname").change(function() {
    if ($("#addfathersname").is(':checked') == false) {
        $("#fathersnamecontainer").hide();
    } else {
        $("#fathersnamecontainer").show();
    }
  });
  $("#doYouWantToBeContactInfoMother").ready(function() {
    if ($("#doYouWantToBeContactInfoMother").is(':checked') == false) {
        $("#doyouwanttobecontactmothercontainer").hide();
    } else {
        $("#doyouwanttobecontactmothercontainer").show();
    }
  });
  $("#doYouWantToBeContactInfoMother").change(function() {
    if ($("#doYouWantToBeContactInfoMother").is(':checked') == false) {
        $("#doyouwanttobecontactmothercontainer").hide();
    } else {
        $("#doyouwanttobecontactmothercontainer").show();
    }
  });
  $("#doYouWantToBeContactInfoFather").ready(function() {
    if ($("#doYouWantToBeContactInfoFather").is(':checked') == false) {
        $("#doyouwanttobecontactfathercontainer").hide();
    } else {
        $("#doyouwanttobecontactfathercontainer").show();
    }
  });
  $("#doYouWantToBeContactInfoFather").change(function() {
    if ($("#doYouWantToBeContactInfoFather").is(':checked') == false) {
        $("#doyouwanttobecontactfathercontainer").hide();
    } else {
        $("#doyouwanttobecontactfathercontainer").show();
    }
  });
});