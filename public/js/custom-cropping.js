$("#cropbox").load(function(){
	var img = document.getElementById('cropbox');
	var imgwidth = img.clientWidth;
	var imgheight = img.clientHeight;
	if (imgwidth < imgheight)
	{
		var startxy = imgwidth;
	}
	else
	{
		var startxy = imgheight;
	}
	$(function(){

    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords,
      bgFade:     true,
      bgOpacity: .2,
      setSelect: [ 0, 10, startxy, startxy ]
    });

  });


	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	};

	function checkCoords()
	{
		if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	};

	$(document).keypress(function(e) {
	    if(e.which == 13) {
	        $( "#cropform" ).submit();
	    }
	});
	
});