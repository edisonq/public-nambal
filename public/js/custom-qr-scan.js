var IDsafe_user = '<?=@$this->sessionid;?>';
jQuery( document ).ready(function() 
{
	console.log('test');
	jQuery('.tootipper').popover();
	jQuery(".masked-backupcode").mask("****-****-****-****");
	jQuery("#how-to-scan").on( "click", function() 
	{
		jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>How to scan</span> QR code in nambal card.</h3>');
		jQuery('#show-modal-change .modal-body').load(BASEURLL+'profileViewer/howtoscan');
		jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Continue scanning</button>');
	});
	jQuery("#where-to-find-qr").on( "click", function() 
	{
		jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Where to find</span> back-up code in nambal card.</h3>');
		jQuery('#show-modal-change .modal-body').load(BASEURLL+'profileViewer/wheretofindqr');
		jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Continue scanning</button>');
	});
});