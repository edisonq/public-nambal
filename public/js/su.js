$(function(){
	$('.close').click(function() {
		$('.alert').fadeOut('slow');
	});
	$('.the-male').click(function() {
		$('.the-sex').val('male');
	});

	$('.the-female').click(function() {
		$('.the-sex').val('female');
	});

	$('#create-account').submit(function(){
		var form = $(this),
	        formData = form.serialize(),
			formUrl = form.attr('action'),
			formMethod = form.attr('method'),
			msg = $('.form-message'),
			msg1 = $('.form-message1');

		$.ajax({
	        url: formUrl,
			type: formMethod,
			data: formData,
			success:function(data){
	            var responseData = jQuery.parseJSON(data);
	            switch(responseData.status){
	            	case 1:
	            		$('#user-id').val(responseData.obj);
	            		$('.a-e').hide();
	            		$('.a-s').fadeIn('slow', function() {
	            			msg.text(responseData.message);

	            			$.ajax({
	            				url: 'personal_information/signup_get_personal/'+responseData.obj,
	            				type: 'GET',
	         					success: function(data) {
	         						var json_obj = jQuery.parseJSON(data);
	         						
	         						$('#update_personal_id').val(json_obj.IDsafe_personalInfo);
	         						$('#inputCountry').val(json_obj.country);
	         						$('#inputCity').val(json_obj.city);
	         						$('#inputTown').val(json_obj.town);
	         						$('#inputStreet').val(json_obj.street);
	         						$('#inputPostcode').val(json_obj.postCode);
	         						$('#inputDate').val(json_obj.dateOfBirth);
	         						$('.the-sex').val(json_obj.sex);
	         						$('#inputBloodType').val(json_obj.bloodType);

	         						var gender = json_obj.sex;

	         						if(gender == "male") {
	         							$('.the-male').attr('class','btn btn-primary the-male active');
	         						}else if(gender == "female") {
	         							$('.the-female').attr('class','btn btn-primary the-female active');
	         						}
	         					}
	            			});

	            		});
	            		$('.tab-pinfo').attr({
	            			'href':'#tabs1-pane2',
	            			'data-toggle':'tab'
	            		});
	            		setTimeout(function(){		
					        $('#myTab a[href="#tabs1-pane2"]').tab('show');
	            			$('#myTab a[href="#tabs1-pane1"]').tab('hide');
						},2000)	
	            	break;

	            	case 0:
	            		msg.text(responseData.message);
	            		$('.a-e').fadeIn('slow', function() {
	            			msg1.text(responseData.message);
	            		});
	            	break;
	            }
	        }
	    });

		return false;
	});

	$('#personal-information').submit(function(){
		var form = $(this),
	        formData = form.serialize(),
			formUrl = form.attr('action'),
			formMethod = form.attr('method'),
			msg = $('.form-message2'),
			msg1 = $('.form-message3');

		$.ajax({
	        url: formUrl,
			type: formMethod,
			data: formData,
			success:function(data){
				
	            var responseData = jQuery.parseJSON(data);
	            switch(responseData.status){
	            	case 1:
	            		$('#user-id1').val(responseData.obj);
	            		$('#update_personal_id').val(responseData.obj);
	            		$('.a-e1').hide();
	            		$('.a-s1').fadeIn('slow', function() {
	            			msg.text(responseData.message);
	            		});
	            		$('.tab-emergency').attr({
	            			'href':'#tabs1-pane3',
	            			'data-toggle':'tab'
	            		});
	            		setTimeout(function(){
					        $('#myTab a[href="#tabs1-pane3"]').tab('show');
						},2000)	
	            	break;

	            	case 0:
	            		msg.text(responseData.message);
	            		$('.a-e1').fadeIn('slow', function() {
	            			msg1.text(responseData.message);
	            		});
	            	break;
	            }
	        }
	    });

		return false;
	});

	$('#emergency-information').submit(function(){
		var form = $(this),
	        formData = form.serialize(),
			formUrl = form.attr('action'),
			formMethod = form.attr('method'),
			msg = $('.form-message4'),
			msg1 = $('.form-message5');

		$.ajax({
	        url: formUrl,
			type: formMethod,
			data: formData,
			success:function(data){
				
	            var responseData = jQuery.parseJSON(data);
	            switch(responseData.status){
	            	case 1:
	            		$('#update_emergency_id').val(responseData.obj);
	            		$('.emer-e').hide();
	            		$('.emer-s').fadeIn('slow', function() {
	            			msg.text(responseData.message);
	            		});
	            		$('.tab-steps').attr({
	            			'href':'#tabs1-pane4',
	            			'data-toggle':'tab'
	            		});
	            		setTimeout(function(){
					        $('#myTab a[href="#tabs1-pane4"]').tab('show');
						},2000)	
	            	break;

	            	case 0:
	            		msg.text(responseData.message);
	            		$('.emer-e').fadeIn('slow', function() {
	            			msg1.text(responseData.message);
	            		});
	            	break;
	            }
	        }
	    });

		return false;
	});
});
	