jQuery.noConflict();

jQuery(document).ready(function(){
	//scroll
	//run once
  var el=jQuery('.leftmenu');
   var originalelpos=el.offset().top; // take it where it originally is on the page

    //run on scroll
     jQuery(window).scroll(function(){
        var el = jQuery('.leftmenu'); // important! (local)
        var elpos = el.offset().top; // take current situation
		var windowpos = jQuery(window).scrollTop();
       var finaldestination = windowpos+originalelpos;
	  
	   if(windowpos > originalelpos){
			el.stop().animate({'marginTop':windowpos-originalelpos},500);
		}else{
			el.stop().animate({'marginTop':0},500);
		}
	});

     //PRINT icon
    jQuery('#aPrint').hover(function(){
         jQuery('#print').css({'float':'left','background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-0px -0px','background-repeat':'no-repeat'});
         
    }).mouseout(function(){
         jQuery('#print').css({'float':'left','background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-0px -50px','background-repeat':'no-repeat' });
    });
    //WELLNESS icon
    jQuery('#aWellness').hover(function(){
         jQuery('#wellness').css({'float':'left','background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-50px -0px','background-repeat':'no-repeat'});
         
    }).mouseout(function(){
         jQuery('#wellness').css({'float':'left','background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-50px -50px','background-repeat':'no-repeat' });
    });

    //DOCTOR icon
    jQuery('#aDoc').hover(function(){
         jQuery('#doc').css({
        'background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-100px -0px','background-repeat':'no-repeat'});
         
    }).mouseout(function(){
         jQuery('#doc').css({
        'background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-100px -50px','background-repeat':'no-repeat' });
		//console.log('testing');
    });
    //PHARMACY icon
    jQuery('#aPharmacy').hover(function(){
         jQuery('#pharmacy').css({
        'background-image':"url('/public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-150px -0px','background-repeat':'no-repeat'});
         
    }).mouseout(function(){
         jQuery('#pharmacy').css({
        'background-image':"url('/public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-150px -50px','background-repeat':'no-repeat' });
    });
    //LABORATORY icon
    jQuery('#aLaboratory').hover(function(){
         jQuery('#laboratory').css({
        'background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-200px -0px','background-repeat':'no-repeat'});
         
    }).mouseout(function(){
         jQuery('#laboratory').css({
           'background-image':"url('public/img/icon-sprites.jpg')",'width':'50px','height':'50px','background-position':'-200px -50px','background-repeat':'no-repeat' });
    });
});
