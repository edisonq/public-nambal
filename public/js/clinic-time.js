function generateTime(hoursElementClass,minutesElementClass){
	var hoursElement = jQuery('.'+hoursElementClass);
	var minutesElement = jQuery('.'+minutesElementClass);
	var hours = 12;
	var minutes = 60;
	
	for(var h=0;h<=hours;h++){
		if(h < 10){
			hoursElement.append('<option value="0'+h+'"> 0'+h+' </option>');
		}else{
			hoursElement.append('<option value="'+h+'"> '+h+' </option>');
		}
		
	}
	
	for(var m=0;m<=minutes;m++){
		if(m < 10){
			minutesElement.append('<option value="0'+m+'"> 0'+m+' </option>');
		}else{
			minutesElement.append('<option value="'+m+'"> '+m+' </option>');
		}
	}
}
