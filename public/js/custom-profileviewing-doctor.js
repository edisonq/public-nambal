jQuery( document ).ready(function() 
{
  jQuery( "#modifypersonalinformation" ).on( "click", function() 
  {
    var sessionid = jQuery(this).attr('data-sessionid');
    jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Personal Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'personalinformation/modifydisplay?&IDsafe_user='+sessionid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="savechangespersonalinformation" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#savechangespersonalinformation" ).on( "click", function() 
    { 
        var formData = jQuery('.form-horizontal').serialize();
        jQuery.ajax(
        {
            url: BASEURLL+"personalinformation/updateDoctor",
            type: "POST",
            data: formData,
            dataType: "JSON",
            success: function (data) 
            {
                if (data.errorStatus == false)
                {
                    jQuery('#show-modal-change').modal('hide');
                    jQuery('#personal-information').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
                    jQuery('#personal-information').load(BASEURLL+'personalInformation/updateView?IDsafe_user='+IDsafe_user);
                }
                else
                {
                    jQuery('.alert').alert('close');
                    jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
                    jQuery('.alert').fadeOut(5000);
                }
            }
        });
    });
  });
jQuery( ".appointmentfollowup" ).on( "click", function() 
{   
    jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Set appointment</span> for follow-up</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'appointmentsDoctor/insertDisplayFollowUp');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="addfollowupnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#addfollowupnow" ).on( "click", function() 
    {
        var formData = jQuery('.form-horizontal').serialize();
        jQuery.ajax({
        url: BASEURLL+"appointmentsDoctor/insertFollowUp?ajax=true",
        type: "POST",
        data: formData,
        dataType: "JSON",
        success: function (data) {
            if (data.errorStatus == false)
            {
                jQuery('#show-modal-change').modal('hide');
                jQuery('#tests-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
                jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
            }
            else
            {
                jQuery('.alert').alert('close');
                jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
                jQuery('.alert').fadeOut(5000);
            }
        }
        });
    });
});
    jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);

    // if no load
    jQuery( ".reloadAllAreaList" ).on( "click", function() 
    {
        jQuery('#problem-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading problem list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#emergencyContactTable').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading emergency information... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#medication-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading medication information... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#allergen-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading allergens... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#procedures-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading procedures list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#immunizations-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading immunization list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#tests-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#prescriptions-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
    });
    jQuery('.tootipper').popover();
    jQuery(".masked-backupcode").mask("****-****-****-****");
    jQuery("#how-to-scan").on( "click", function() 
    {
        jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
        jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>How to scan</span> QR code in nambal card.</h3>');
        jQuery('#show-modal-change .modal-body').load(BASEURLL+'profileViewer/howtoscan');
        jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Continue scanning</button>');
    });
    jQuery("#where-to-find-qr").on( "click", function() 
    {
        jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
        jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Where to find</span> back-up code in nambal card.</h3>');
        jQuery('#show-modal-change .modal-body').load(BASEURLL+'profileViewer/wheretofindqr');
        jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Continue scanning</button>');
    });
});