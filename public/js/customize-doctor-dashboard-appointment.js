jQuery( document ).ready(function() {
  jQuery('.appointment-link').click(function(e) 
  {	
  	
  	var IDappointments = $(this).attr('data-appointment');
    var IDappnum = $(this).attr('data-appid');
    var dataString = "IDappointments="+IDappointments;
    var $modal = $("#myModal"), $modalLabel = $("#myModalLabel");
    var fullname = $(this).attr('data-fullname');
    var allday = $(this).attr('data-allday');
    var sameday = $(this).attr('data-sameday');
    var timefrom = $(this).attr('data-timefrom');
    var timeto = $(this).attr('data-timeto');
    var notes = $(this).attr('data-notes');
    var dateonly = $(this).attr('data-dateonly');
    var locationInformation = $(this).attr('data-locationInformation');


    if (notes.length < 1)
    {
      notes = 'no notes included';
    }
    $modal.modal('show');
    $modal.find(".modal-body").html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');   
  	$.ajax({
        type: 'GET',
        data: dataString,
        url: BASEURLL+"appointments/checkApprove?ajax=true",
        success: function(data, textStatus, jqXHR)
        {
          $modal.find(".modal-title").html("Appointment information");
          if (sameday == false) {
            if (data.approved == true) {
              $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');   
            }
            else {
              $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address</strong>: <br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm" data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
            }
          }
          else {
                if (timefrom == timeto) {
                  $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>To approve this you should set time<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address: </strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm" data-appointid="'+IDappnum+'">Modify the time, then approve</a>&nbsp;&nbsp;<br> </p>');
                }
                else{
                  if (data.approved == true) {
                    $modal.find(".modal-body").html('<p>Name: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');
                  } else {
                    $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
                  }
            }

            $("#approve-reservation-na").click(function(e) 
            {
              var element = '#approval-area-'+$(this).attr('data-appointid');
              dataString = "IDappointments="+IDappointments;
              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
              $modal.modal('hide');
              $.ajax({
              type: 'GET',
              data: dataString,
              url: BASEURLL+"appointments/approve?ajax=true",
              success: function(data, textStatus, jqXHR)
              {
                  if (data.approve)
                  {
                    $(element).html('Approved');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $(element).html('Please refresh');
              }
              }); 
            });

            $("#cancel-reservation-na").click(function(e) 
            {
              var element = '#approval-area-'+$(this).attr('data-appointid');
              dataString = "IDappointments="+IDappointments;
              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
              $modal.modal('hide');
              $.ajax({
              type: 'GET',
              data: dataString,
              url: BASEURLL+"appointments/cancel?ajax=true",
              success: function(data, textStatus, jqXHR)
              {
                  if (data.approve)
                  {
                    //console.log(element);
                    $(element).html('Canceled appointment');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $(element).html('Please refresh');
              }
              }); 
            });
          }
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        }
        // error : error,
        // complete : complete,
        // dataType: dataStringType
      });  
    
  });    
});