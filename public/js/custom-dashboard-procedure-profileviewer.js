jQuery( ".addprocedure" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Procedure History Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'procedures/insertDisplayDoctor');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthisprocedurenow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthisprocedurenow" ).on( "click", function handler() 
	{
		jQuery('#insertthisprocedurenow').off('click');
		jQuery('#show-modal-change .modal-footer #insertthisprocedurenow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertthisprocedurenow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"procedures/insertDoctor?ajax=true&IDsafe_user="+IDsafe_user,
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#procedures-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);  			
    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('#show-modal-change .modal-footer #insertthisprocedurenow').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #insertthisprocedurenow').removeAttr('disabled');
				jQuery('.alert').fadeOut(5000);
				jQuery('#insertthisprocedurenow').on( "click", handler);
			}
		}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#insertthisprocedurenow').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".modifythisprocedure" ).on( "click", function() 
{
	var procedureid = jQuery(this).attr('data-procedureid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Procedure History Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'procedures/insertDisplayDoctor?&IDsafe_procedure='+procedureid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisprocedureyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisprocedureyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythisprocedureyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythisprocedureyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisprocedureyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"procedures/update?ajax=true&isDoctor=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
				if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
	    			jQuery('#procedures-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);  			
	    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
					jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('#show-modal-change .modal-footer #modifythisprocedureyes').html('Continue Saving');
					jQuery('#show-modal-change .modal-footer #modifythisprocedureyes').removeAttr('disabled');
					jQuery('.alert').fadeOut(5000);
					jQuery('#modifythisprocedureyes').on( "click", handler);
				}
			}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; 
		 if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythisprocedureyes').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".deletethisprocedure" ).on( "click", function()
{
	var procedureid = jQuery(this).attr('data-procedureid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Procedure History Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this procedure?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisprocedureyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisprocedureyes" ).on( "click", function() 
	{
		jQuery('#procedures-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"procedures/deleteDoctor?ajax=true",
		type: "POST",
		data: {
		    procedureid: procedureid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#procedures-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);  			
    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#procedures-pagination-div-id a").click(function()
   {
   		jQuery('#procedures-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#procedures-table').load(theURLhere);
		   }
		});
	   return false;
   });
});