jQuery( document ).ready(function() 
{
    jQuery( "#addclinicorlocation" ).on( "click", function()
    {
    	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Clinic or location</h3>');
	    jQuery('#show-modal-change .modal-body').load(BASEURLL+'practiceSettings/displayAllAppointmentList');
	    jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    });
     jQuery( "#addappointmentschedules" ).on( "click", function()
    {
    	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Appointment Settings</h3>');
	    jQuery('#show-modal-change .modal-body').load(BASEURLL+'practiceSettings/addAppointmentSettings');
	    // jQuery('#show-modal-change .modal-body').load(BASEURLL+'practiceSettings/displayAllAppointmentList');
	    jQuery('#show-modal-change .modal-footer').html('<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    });
   
	jQuery( document ).ready(function() 
	{
		jQuery('#appointment-settings').load(BASEURLL+'practiceSettings/appointmentSettingsDisplay');
	});
	jQuery( document ).ready(function() 
	{
		jQuery('#practice-locations').load(BASEURLL+'practiceSettings/displayPracticeLocations');
	});
	// jQuery( document ).ready(function() 
	// {
	// 	jQuery('#appointment-schedules').load(BASEURLL+'practiceSettings/displayAppointmentSchedulesSettings');
	// });

});