jQuery( ".addprescriptions" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Prescription Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/insertDisplayDoctor');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthisprescription" class="btn btn-primary" data-loading-text="Loading...">Continue prescription</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthisprescription" ).on( "click", function handler() 
	{
		jQuery('#insertthisprescription').off('click');
		jQuery('#show-modal-change .modal-footer #insertthisprescription').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertthisprescription').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"prescriptions/insertPrescription?ajax=true&sessionid="+IDsafe_user,
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	var IDsafeprescription = data.IDsafeprescription;
		    	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');

		    	// loading the table medication UI
		    	jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionInformation?IDsafeprescription='+IDsafeprescription+"&sessionid="+IDsafe_user);
    			jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('#show-modal-change .modal-footer #insertthisprescription').html('Continue Creating Prescription');
				jQuery('#show-modal-change .modal-footer #insertthisprescription').removeAttr('disabled');
				jQuery('.alert').fadeOut(5000);
				jQuery('#insertthisprescription').on( "click", handler);
			}
		}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#insertthisprescription').click();
		    return false;  
		  }
		}); 
	});  
});


jQuery( ".modifythisprescription" ).on( "click", function() 
{
	var dataprescription = jQuery(this).attr('data-prescription');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Prescription Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionFinish?IDsafeprescription='+dataprescription+'&sessionid='+IDsafe_user+'&modify=TRUE');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythismedicationyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythismedicationyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythismedicationyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythismedicationyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythismedicationyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"prescription/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#prescriptions-table').load(BASEURLL+'prescription/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('#show-modal-change .modal-footer #modifythismedicationyes').html('Continue Creating Prescription');
					jQuery('#show-modal-change .modal-footer #modifythismedicationyes').removeAttr('disabled');
					jQuery('.alert').fadeOut(5000);
					jQuery('#modifythismedicationyes').on( "click", handler);
				}
			}
		});
	});
});
jQuery( ".deletethisprescription" ).on( "click", function()
{
	var medicationid = jQuery(this).attr('data-medicationid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this prescription?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethismedicationyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethismedicationyes" ).on( "click", function() 
	{
		jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"prescription/delete?ajax=true",
		type: "POST",
		data: {
		    medicationid: medicationid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#prescriptions-table').load(BASEURLL+'prescription/dashboardDisplay');
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#prescription-pagination-div-id a").click(function()
   {
   		jQuery('#prescriptions-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#prescriptions-table').load(theURLhere);
		   }
		});
	   return false;
   });
});