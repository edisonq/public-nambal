jQuery( "#addtestresult" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Test Result Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'test/insertDisplay');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="addtestresultnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#addtestresultnow" ).on( "click", function handler() 
	{
		jQuery('#addtestresultnow').off('click');
		jQuery('#show-modal-change .modal-footer #addtestresultnow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #addtestresultnow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"test/insert?ajax=true",
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#tests-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#addtestresultnow').on( "click", handler);
				jQuery('#show-modal-change .modal-footer #addtestresultnow').html('Continue Creating Prescription');
				jQuery('#show-modal-change .modal-footer #addtestresultnow').removeAttr('disabled');
			}
		}
		});
	});

	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#addtestresultnow').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".modifythistest" ).on( "click", function() 
{
	var testid = jQuery(this).attr('data-testid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Test Result Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'test/insertDisplay?&IDsafe_test='+testid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythistestyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythistestyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythistestyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythistestyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythistestyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"test/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#tests-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);
					jQuery('#modifythistestyes').on( "click", handler);
					jQuery('#show-modal-change .modal-footer #modifythistestyes').html('Continue Creating Prescription');
					jQuery('#show-modal-change .modal-footer #modifythistestyes').removeAttr('disabled');
				}
			}
		});
	});

	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythistestyes').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".deletethistest" ).on( "click", function()
{
	var testid = jQuery(this).attr('data-testid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Procedure History Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this procedure?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethistestyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethistestyes" ).on( "click", function() 
	{
		jQuery('#tests-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"test/delete?ajax=true",
		type: "POST",
		data: {
		    testid: testid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#tests-pagination-div-id a").click(function()
   {
   		jQuery('#tests-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#tests-table').load(theURLhere);
		   }
		});
	   return false;
   });
});