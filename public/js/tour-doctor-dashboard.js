// jQuery.noConflict();

jQuery('#take-a-tour .updateTour').submit(function(){
	var thisModal = jQuery('#take-a-tour');
	jQuery('#take-a-tour .skip, #take-a-tour .finish').attr('disabled','disabled');
	formAjax(jQuery(this),thisModal);
	jQuery(this).unbind('submit');

	jQuery.ajax({
	url: BASEURLL+"doctorDashboard/tour",
	type: "POST",
	data: formData,
	dataType: "JSON",
	success: function (data) {
	    location.reload();
	}
	});
	
	return false;
});
 
var formAjax = function(thisForm,thisModal){
	var formName = thisForm.attr('name');
	jQuery.ajax({
		type: thisForm.attr('method'),
		url: thisForm.attr('action'),
		dataType: 'JSON',
		data: thisForm.serialize(),
		success: function(data){
			switch(data.status)
			{
				case true:
					thisModal.removeClass('in').attr('area-hidden','false');
						
					setTimeout(function(){
						thisModal.css('display','none');
					},500);
					
					exit;
				break;
				
				case false:
					window.location = 'dashboard';
				break;
			
				default:
					window.location = 'dashboard';
				exit;
			}
		}
	});
};