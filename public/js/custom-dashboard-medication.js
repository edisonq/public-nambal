jQuery( "#addmedication" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Medication Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'medication/insertMedicationDisplay');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthismedicationnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthismedicationnow" ).on( "click", function handler() 
	{
		jQuery('#insertthismedicationnow').off('click');
		jQuery('#show-modal-change .modal-footer #insertthismedicationnow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertthismedicationnow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"medication/insert?ajax=true",
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplay');
				jQuery('#prescriptions-table').load(BASEURLL+'prescription/dashboardDisplay');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#show-modal-change .modal-footer #insertthismedicationnow').html('Continue Creating Prescription');
				jQuery('#show-modal-change .modal-footer #insertthismedicationnow').removeAttr('disabled');
				jQuery('#insertthismedicationnow').on( "click", handler);
			}
		}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#insertthismedicationnow').click();
		    return false;  
		  }
		}); 
	});  
});
jQuery( ".modifythisdrug" ).on( "click", function() 
{
	var drugid = jQuery(this).attr('data-drugid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Medication Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'medication/insertMedicationDisplay?&IDsafe_drug='+drugid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythismedicationyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythismedicationyes" ).on( "click", function() 
	{	
		jQuery('#show-modal-change .modal-footer #modifythismedicationyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythismedicationyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"medication/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplay');
					jQuery('#prescriptions-table').load(BASEURLL+'prescription/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);
					jQuery('#show-modal-change .modal-footer #modifythismedicationyes').html('Continue Creating Prescription');
					jQuery('#show-modal-change .modal-footer #modifythismedicationyes').removeAttr('disabled');
				}
			}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythismedicationyes').click();
		    return false;  
		  }
		}); 
	}); 
});
jQuery( ".deletethismedication" ).on( "click", function()
{
	var medicationid = jQuery(this).attr('data-medicationid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this medication?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethismedicationyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethismedicationyes" ).on( "click", function() 
	{
		jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"medication/delete?ajax=true",
		type: "POST",
		data: {
		    medicationid: medicationid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplay');
				jQuery('#prescriptions-table').load(BASEURLL+'prescription/dashboardDisplay');
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#medication-pagination-div-id a").click(function()
   {
   		jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#medication-table').load(theURLhere);
		   }
		});
	   return false;
   });
});