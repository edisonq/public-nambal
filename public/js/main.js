$(function(){

	$('[data-spy="scroll"]').each(function () {
  		//var $spy = $(this).scrollspy('refresh');
  	});

  	$('.gotocreate').click(function() {
		window.location = "register";
	});
	
	$('.logoutbtn').click(function() {
		window.location = "logout"; 
	});
	
	$('.gotodashboard').click(function() {
		window.location = "dashboard"; 
	});	
	
	$('#dateofbirth').datepicker({viewMode:2}).keypress(function(event) {
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 47];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		// alert(event.which); //test what keycode your key is
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		  (49 <= event.which && event.which <= 57) || // Always 1 through 9
		  (48 == event.which && $(this).attr("value")) || // No 0 first digit
		  isControlKey) { // Opera assigns values for control keys.
		return;
		} else {
		event.preventDefault();
		}
	});
	
	$('.datepicker .datepicker-days tbody').on('click', function(){  $('.datepicker').hide() });
	
	
	// By Chris Coyier & tweaked by Mathias Bynens
	// Find all YouTube videos
	var $allVideos = $("object, embed"),

	    // The element that is fluid width
	    $fluidEl = $(".embedVideo");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {

		$(this)
			.data('aspectRatio', this.height / this.width)
			
			// and remove the hard coded width/height
			.removeAttr('height')
			.removeAttr('width');

	});
	
	//check browser resolution
	var browserWidth = document.body.clientWidth;
	
	// When the window is resized
	// (You'll probably want to debounce this)
	$(window).resize(function() {
		var browserWidth = document.body.clientWidth;
	
		if(browserWidth <= 748){
			$("#card-content").css('position','relative');
		}else{
			$("#card-content").css('position','absolute');
		}
		
		var newWidth = $fluidEl.width();
		
		// Resize all videos according to their own aspect ratio
		$allVideos.each(function() {

			var $el = $(this);
			$el
				.width(newWidth)
				//modified by Belleo
				// .height(newWidth * $el.data('aspectRatio'));
				.height(newWidth );

		});

	// Kick off one resize to fix all videos on page load
	}).resize();
	
	$('#edit-email').attr('disabled','disabled');
});

function goToByScroll(element) {
   $("html,body").animate({scrollTop: $(element).offset().top},800);
}

$(document).ready(function(){
	$('a[data-toggle="tab"]').on('shown', function (e) {
	  e.target // activated tab
	  e.relatedTarget // previous tab
	});

	$('#myTab a[href="#tabs1-pane1"]').tab('show');
	/*$('#myTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});*/
	
	$('#dp5').datepicker()
  .on('changeDate', function(ev){
    if (ev.date.valueOf() < startDate.valueOf()){
    }
  });
  
  $('#socialNetworkLogin .facebook').click(function() {
	  //console.log('testinglang');
//	$('#socialNetworkLogin .facebook').popover('show');
	});

// $.getJSON("qr/getCodeOnly",function(result){
// 	    $.each(result, function(i, field){
// 	      $("#qr-codes-backup").html("<img src=\"qr/generateSelf\" alt=\"\" title=\"your QR code\" id=\"\"/><br>" + field);
// 	    });
// 	  });
  

//  $("#qr-codes-backup").click(function(){
// 	 	$.getJSON("qr/getCodeOnly",function(result){
// 	    $.each(result, function(i, field){
// 	      $("#qr-codes-backup").html("<img src=\"qr/generateSelf\" alt=\"\" title=\"your QR code\" id=\"\"/><br>" + field);
// 	    });
// 	  });
// });
	
	$('#qr-codes-backup').load(BASEURLL+'qr/getCodeOnly');

  //$('#qr-codes-backup').html('testing lang');


//	
//	$('#myTab a[href="#profile"]').tab('show'); // Select tab by name
//$('#myTab a:first').tab('show'); // Select first tab
//$('#myTab a:last').tab('show'); // Select last tab
//$('#myTab li:eq(2) a').tab('show'); // Select third tab (0-indexed)
});