jQuery( ".addjustthisjournal" ).on( "click", function() 
{
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
	jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add</span> just a journal note</h3>');
	jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertjustjournalnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
	jQuery('#show-modal-change .modal-body').load(BASEURLL+'journal/insertDisplayJournal?ajax=true&sessionid='+IDsafe_user);
	jQuery("#insertjustjournalnow" ).on( "click", function() 
	{
		jQuery('#show-modal-change .modal-footer #insertjustjournalnow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertjustjournalnow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"journal/insertDoctor?ajax=true&sessionid="+IDsafe_user,
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('#show-modal-change .modal-footer #insertjustjournalnow').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #insertjustjournalnow').removeAttr('disabled');
				jQuery('.alert').fadeOut(5000);
			}
		}
		});
	});
});
jQuery( ".modifythisjournal" ).on( "click", function() 
{
	var idemrjournal = jQuery(this).attr('data-idemrjournal');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Journal Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'journal/insertDisplay?&idemrjournal='+idemrjournal);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisjournalyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisjournalyes" ).on( "click", function() 
	{	
		jQuery('#show-modal-change .modal-footer #modifythisjournalyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisjournalyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"journal/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);
					jQuery('#show-modal-change .modal-footer #modifythisjournalyes').html('Continue Creating Prescription');
					jQuery('#show-modal-change .modal-footer #modifythisjournalyes').removeAttr('disabled');
				}
			}
		});
	});
});
jQuery( ".deletethisjournal" ).on( "click", function()
{
	var idemrjournal = jQuery(this).attr('data-idemrjournal');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Journal?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this Journal?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisimmunizationyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisimmunizationyes" ).on( "click", function() 
	{
		jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"journal/deletedoctor?ajax=true",
		type: "POST",
		data: {
		    idemrjournal: idemrjournal
		},
		dataType: "JSON",
		success: function (data) {
		    // if (data.errorStatus == false)
		    // {				
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			// }
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#journal-pagination-div-id a").click(function()
   {
   		jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');	
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#journal-table').load(theURLhere);
		   }
		});
	   return false;
   });
});
