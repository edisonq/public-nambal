jQuery( "#addimmunizations" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Immunizations Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'immunizations/insertDisplay');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthisimmunizationnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthisimmunizationnow" ).on( "click", function() 
	{
		jQuery('#show-modal-change .modal-footer #insertthisimmunizationnow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertthisimmunizationnow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"immunizations/insert?ajax=true",
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplay');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#show-modal-change .modal-footer #insertthisimmunizationnow').html('Continue Creating Prescription');
				jQuery('#show-modal-change .modal-footer #insertthisimmunizationnow').removeAttr('disabled');
			}
		}
		});
	});
});
jQuery( ".modifythisimmunization" ).on( "click", function() 
{
	var IDsafeimmunization = jQuery(this).attr('data-IDsafeimmunization');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Immunization Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'immunizations/insertDisplay?&IDsafe_immunization='+IDsafeimmunization);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisimmunizationyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisimmunizationyes" ).on( "click", function() 
	{	
		jQuery('#show-modal-change .modal-footer #modifythisimmunizationyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisimmunizationyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"immunizations/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);
					jQuery('#show-modal-change .modal-footer #modifythisimmunizationyes').html('Continue Creating Prescription');
					jQuery('#show-modal-change .modal-footer #modifythisimmunizationyes').removeAttr('disabled');
				}
			}
		});
	});
});
jQuery( ".deletethisimmunization" ).on( "click", function()
{
	var IDsafeimmunization = jQuery(this).attr('data-IDsafeimmunization');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Immunization History Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this Immunization?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisimmunizationyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisimmunizationyes" ).on( "click", function() 
	{
		jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"immunizations/delete?ajax=true",
		type: "POST",
		data: {
		    IDsafeimmunization: IDsafeimmunization
		},
		dataType: "JSON",
		success: function (data) {
		    // if (data.errorStatus == false)
		    // {				
				jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplay');
			// }
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#immunizations-pagination-div-id a").click(function()
   {
   		jQuery('#immunizations-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#immunizations-table').load(theURLhere);
		   }
		});
	   return false;
   });
});