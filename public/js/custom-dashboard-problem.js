jQuery( "#addproblem" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Problems History Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'problem/insertProblemDisplay');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthisproblemnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthisproblemnow" ).on( "click", function handler() 
	{
		jQuery('#insertthisproblemnow').off('click');
		jQuery('#show-modal-change .modal-footer #insertthisproblemnow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertthisproblemnow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"problem/insert?ajax=true",
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#problem-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplay');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#show-modal-change .modal-footer #insertthisproblemnow').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #insertthisproblemnow').removeAttr('disabled');
				jQuery('#insertthisproblemnow').on( "click", handler);
			}
		}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#insertthisproblemnow').click();
		    return false;  
		  }
		}); 
	});  
});
jQuery( ".modifythisproblem" ).on( "click", function() 
{
	var problemid = jQuery(this).attr('data-problemid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Problem History Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'problem/insertProblemDisplay?&IDsafe_problem='+problemid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisproblemyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisproblemyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythisproblemyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythisproblemyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisproblemyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"problem/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#problem-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);
					jQuery('#show-modal-change .modal-footer #modifythisproblemyes').html('Continue Saving');
					jQuery('#show-modal-change .modal-footer #modifythisproblemyes').removeAttr('disabled');
					jQuery('#modifythisproblemyes').on( "click", handler);
				}
			}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythisproblemyes').click();
		    return false;  
		  }
		}); 
	});  
});
jQuery( ".deletethisproblem" ).on( "click", function()
{
	var problemid = jQuery(this).attr('data-problemid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this problem?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisproblemyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisproblemyes" ).on( "click", function() 
	{
		jQuery('#problem-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"problem/delete?ajax=true",
		type: "POST",
		data: {
		    problemid: problemid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplay');
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#problem-pagination-div-id a").click(function()
   {
   		jQuery('#problem-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#problem-table').load(theURLhere);
		   }
		});
	   return false;
   });
});