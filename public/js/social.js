$(function(){
	
	var IDsafe_user = $('#social_safe_user').val();

	$.ajax({
	    url: 'fb_apis/incomplete_safe_user/'+IDsafe_user,
		type: 'GET',
		cache: false,
		success: function(data) {
			var user = jQuery.parseJSON(data);

			var firstname = $('#inputFirstname');
			var middlename = $('#inputMiddlename');
			var lastname = $('#inputLastname');
			var email = $('#inputEmail');
			var username = $('#inputUsername');

			$('#inputFirstname').val(user.firstname);
			$('#inputMiddlename').val(user.middlename);
			$('#inputLastname').val(user.lastname);
			$('#inputEmail').val(user.email);
			$('#inputUsername').val(user.username);

			$('.social-info').fadeIn('slow');
		}
	});

	setTimeout(function(){
		$.get("create_account/clearsession");
	},2000)		

	$("#inputFirstname, #inputMiddlename, #inputLastname, #inputEmail, #inputUsername").focusout(function() {
		//$('#inputUsername').
		alert(this.val());
	});

});