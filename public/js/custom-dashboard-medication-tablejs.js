jQuery(document).ready(function()
{
  jQuery('#olduserinterfacemedication').on( "click", function() 
  {
    var dataprescription = jQuery(this).attr('data-prescription');
    jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Medication Information</h3>');
    // jQuery('#show-modal-change .modal-body').load(BASEURLL+'medication/insertMedicationDisplayDoctor?sessionid='+IDsafe_user);
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'prescriptions/prescriptionInformation?IDsafeprescription='+dataprescription+"&sessionid="+IDsafe_user);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertthismedicationnow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertthismedicationnow" ).on( "click", function handler() 
    {
    jQuery('#insertthismedicationnow').off('click');
    jQuery('#show-modal-change .modal-footer #insertthismedicationnow').html('Loading.. please wait');
    jQuery('#show-modal-change .modal-footer #insertthismedicationnow').attr('disabled','disabled');
    var formData = jQuery('.form-horizontal').serialize();
    jQuery.ajax({
    url: BASEURLL+"medication/insertDoctor?ajax=true&sessionid="+IDsafe_user,
    type: "POST",
    data: formData,
    dataType: "JSON",
    success: function (data) {
        if (data.errorStatus == false)
        {
          // balik-ko-diri-for-create-new-prescription-task
          // mag butang of IF data.createnewprescription = true create-new-prescription true kay mo redirect sa final UI dayon.
          // else i hide na lang siya diritso
          // balikan tikaw para sa create-new-prescription

          jQuery('#show-modal-change').modal('hide');
          jQuery('#medication-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
        jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
        jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);

        jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
        jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
      }
      else
      {
        jQuery('.alert').alert('close');
        jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
        jQuery('#show-modal-change .modal-footer #insertthismedicationnow').html('Continue Creating Prescription');
        jQuery('#show-modal-change .modal-footer #insertthismedicationnow').removeAttr('disabled');
        jQuery('.alert').fadeOut(5000);
        jQuery('#insertthismedicationnow').on( "click", handler);
      }
    }
    });
    });
    jQuery( document ).ready(function() 
    {
    jQuery( '.form-horizontal' ).on("keyup", function (e) {
     var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
      {
        jQuery('#insertthismedicationnow').click();
        return false;  
      }
    }); 
    });  
  });
});