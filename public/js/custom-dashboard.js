

	
var baseUrl = window.location.origin;
jQuery("#saveEmergency").click(function()
{
	jQuery.ajax({
		url: baseUrl+"/emergencyContactInformation/insert?ajax=true",
		type: "POST",
		data: {
		    fullname: jQuery("#fullname").val(),
			email: jQuery("#email").val(),
			countryCode: jQuery("#countryCode").val(),
			areaCode: jQuery("#areaCode").val(),
			number: jQuery("#number").val(),
			fulladdress: jQuery("#fulladdress").val()
		},
	dataType: "JSON",
	success: function (data) {
	    // console.log(data.errorStatus);
	    // console.log(data[0].email);
	    // console.log(data[0].countrycode);
	    // console.log(data[0].areacode);
	    // console.log(data[0].number);
	    // console.log(data[0].fulladdress);
	    if (data.errorStatus == false)
	    {
			jQuery("#emergencyModal").modal("hide");
			// jQuery("#emergencyContactTable").append('<table class="table table-condensed"><tr><td class="table-title">Name:</td><td><div class="btn-group"><button class="btn dropdown-toggle" data-toggle="dropdown"> <i class="icon-th"></i> &nbsp; '+data[0].fullname+'</span></button><ul class="dropdown-menu"><li><a href="#under-improvement" data-toggle="modal"><i class="icon-edit"></i> &nbsp;Edit</a></li><li><a href="#under-improvement" data-toggle="modal"><i class="icon-remove"></i> &nbsp;Delete</a></li><li><a href="#under-improvement" data-toggle="modal"><i class="iconfa-lock"></i> &nbsp;Privacy Settings</a></li><li class="divider"></li><li><a href="#under-improvement" data-toggle="modal"><i class="icon-star"></i> &nbsp;Make it as Primary</a></li></ul></div></td></tr><tr><td>Address:</td><td>"+data[0].fulladdress+"</td></tr><tr><td>Phone Number:</td><td>+"+data[0].countrycode+"("+data[0].areacode+")"+data[0].number+"</td></tr><tr><td>Email:</td><td>"+data[0].email+"</td></tr></table>');
			jQuery('#emergencyContactTable').html('Loading... please wait');
			jQuery('#emergencyContactTable').load(baseUrl+'/emergencyContactInformation/forAjax');
		}
	}
	});
});

window.goBack = function (e){
    var defaultLocation = BASEURLL+'connectDoctor/search';
    var oldHash = window.location.hash;

    history.back(); // Try to go back

    var newHash = window.location.hash;

    /* If the previous page hasn't been loaded in a given time (in this case
    * 1000ms) the user is redirected to the default location given above.
    * This enables you to redirect the user to another page.
    *
    * However, you should check whether there was a referrer to the current
    * site. This is a good indicator for a previous entry in the history
    * session.
    *
    * Also you should check whether the old location differs only in the hash,
    * e.g. /index.html#top --> /index.html# shouldn't redirect to the default
    * location.
    */

    if(
        newHash === oldHash &&
        (typeof(document.referrer) !== "string" || document.referrer  === "")
    ){
        window.setTimeout(function(){
            // redirect to default location
            window.location.href = defaultLocation;
        },1000); // set timeout in ms
    }
    if(e){
        if(e.preventDefault)
            e.preventDefault();
        if(e.preventPropagation)
            e.preventPropagation();
    }
    return false; // stop event propagation and browser default event
}

jQuery( document ).ready(function() {
	// jQuery('#messages-notification').load(BASEURLL+'messages/dashboardNotification');
	jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplay');
	jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplay');
	jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplay');
	jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplay');
	jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplay');
	jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
	jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplay');
	jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
});

// if no load
jQuery( ".reloadAllAreaList" ).on( "click", function() 
{
	jQuery('#problem-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading problem list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#emergencyContactTable').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading emergency information... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#medication-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading medication information... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#allergen-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading allergens... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#procedures-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading procedures list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#immunizations-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading immunization list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#tests-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#prescriptions-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
	jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
	jQuery('#problem-table').load(BASEURLL+'problem/dashboardDisplay');
	jQuery('#medication-table').load(BASEURLL+'medication/dashboardDisplay');
	jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplay');
	jQuery('#procedures-table').load(BASEURLL+'procedures/dashboardDisplay');
	jQuery('#immunizations-table').load(BASEURLL+'immunizations/dashboardDisplay');
	jQuery('#tests-table').load(BASEURLL+'test/dashboardDisplay');
	jQuery('#prescriptions-table').load(BASEURLL+'prescriptions/dashboardDisplay');
});