$(function()
{


    var unsortableColumns = [];
    $('#msg-table').find('thead th').each(function(){
        if ($(this).hasClass( 'no-sort')){
            unsortableColumns.push({"bSortable": false});
        } else {
            unsortableColumns.push(null);
        }
    });

    $("#msg-table").dataTable({
        "aaSorting": [[ 0, "desc" ]],
        "sDom": "<'row table-top-control'<'col-md-6 hidden-xs per-page-selector'l><'col-md-12'f>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ &nbsp; "
        },
        "aoColumns": unsortableColumns
    });


    // $("#createMessageModal").modal();
    // $('#createMessageModal').modal({
    //   keyboard: false
    // });
});