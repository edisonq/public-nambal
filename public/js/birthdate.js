$(function() {

for (i = new Date().getFullYear(); i > 1900; i--)
{
    $('#years').append($('<option />').val(i).html(i));
}

for (i = 1; i < 13; i++)
{
	// original: $('#months').append($('<option />').val(i).html(i));
	
	/*** modifications by: belleo **/
		if(i == 1)
		{
			stringMonth = 'Janruary';
		}
		else if(i == 2)
		{
			stringMonth = 'February';
		}
		else if(i == 3)
		{
			stringMonth = 'March';
		}
		else if(i == 4)
		{
			stringMonth = 'April';
		}
		else if(i == 5)
		{
			stringMonth = 'May';
		}
		else if(i == 6)
		{
			stringMonth = 'June';
		}
		else if(i == 7)
		{
			stringMonth = 'July';
		}
		else if(i == 8)
		{
			stringMonth = 'August';
		}
		else if(i == 9)
		{
			stringMonth = 'September';
		}
		else if(i == 10)
		{
			stringMonth = 'October';
		}
		else if(i == 11)
		{
			stringMonth = 'November';
		}
		else if(i == 12)
		{
			stringMonth = 'December';
		}
		
		$('#months').append($('<option />').val(i).html(stringMonth));
	/** end modification **/
    
}
 updateNumberOfDays(); 

    $('#years, #months').change(function(){

        updateNumberOfDays(); 

    });
	
});

function updateNumberOfDays(){
$('#days').html('');
month=$('#months').val();
year=$('#years').val();
days=daysInMonth(month, year);

    for(i=0; i < days+1 ; i++){
			if(i == 0)
			{
				Day = 'Day';
			}
			else
			{
				Day = i;
			}
            $('#days').append($('<option />').val(i).html(Day));
        }

}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}