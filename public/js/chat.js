jQuery.fn.scrollTo = function(elem, speed) { 
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top 
    }, speed == undefined ? 1000 : speed); 
    return this; 
};

jQuery(window).load(function () 
{
    jQuery('#chat-messages').scrollTo('.chat-message:last');
});



$(function(){

    if (window.localStorage.getItem("chat-simple") != null){
        var messages = window.localStorage.getItem("chat-simple").split(",");
        for (var i = 0; i < messages.length; i++){
            localStorage.removeItem("chat-simple-" + messages[i]);
        }
    }

    var Message = Backbone.Model.extend({

        defaults: function() {
            return {
                text: "empty...",
                order: Messages.nextOrder()
            };
        },

        initialize: function() {
            if (!this.get("text")) {
                this.set({"text": this.defaults().text});
            }
        }

    });

    var MessageList = Backbone.Collection.extend({
        model: Message,
        localStorage: new Backbone.LocalStorage("chat-simple"),
        nextOrder: function() {
            if (!this.length) return 1;
            return this.last().get('order') + 1;
        },
        comparator: function(message) {
            return message.get('order');
        }

    });

    var Messages = new MessageList;

    var MessageView = Backbone.View.extend({

        className: 'chat-message',

        template: _.template($('#message-template').html()),

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
        },

        // Re-render the titles of the todo item.
        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }

    });

    var AppView = Backbone.View.extend({

        el: $("#chat"),

        events: {
            "keypress #new-message":  "createOnEnter",
            "click #new-message-btn": "createOnClick"
        },

        initialize: function() {

            this.input = this.$("#new-message");

            this.listenTo(Messages, 'add', this.addOne);
            this.listenTo(Messages, 'all', this.render);

            Messages.fetch();
        },

        addOne: function(message) {
            var view = new MessageView({model: message});
            this.$("#chat-messages").append(view.render().el);
        },

        createOnEnter: function(e) {
            if (Messages.length < 10){
                if (e.keyCode != 13) return;
                if (!this.input.val()) return;
                var messageid = Date.now();

                Messages.create({text: this.input.val(), messageID: messageid});
                var theMessage = this.input.val();
                this.input.val('');

                var view = this.$("#chat-messages")[0];
                view.scrollTop = view.scrollHeight;
                // sendMessage();
                messaging(theMessage, messageid);
            }
        },

        createOnClick: function(e) {
            if (Messages.length < 10){
                if (!this.input.val()) return;
                var messageid = Date.now();

                Messages.create({text: this.input.val(), messageID: messageid});
                var theMessage = this.input.val();
                this.input.val('');

                var view = this.$("#chat-messages")[0];
                view.scrollTop = view.scrollHeight;
                messaging(theMessage, messageid);
            }
        }

    });

    // Finally, we kick things off by creating the **App**.
    var App = new AppView;
});

$("#chat-messages").slimscroll({
    height: '440px',
    size: '5px',
    alwaysVisible: true,
    railVisible: true
});


function messaging(msg, messageid)
{
    var fromIDsafeUser = $("input#fromIDsafeUser").val();
    var toIDsafeUser = $("input#toIDsafeUser").val();
    var dataString = 'toIDsafeUser='+ toIDsafeUser + '&message=' + msg + '&fromIDsafeUser=' + fromIDsafeUser+ '&json=true' + '&messageid=' +messageid;
    // var dataString = "{fromIDsafeUser:'"+fromIDsafeUser+"',msg:'"+msg+"',json:'true',toIDsafeUser:'"+toIDsafeUser+"'}"

    $.ajax({
      type: 'POST',
      url: BASEURLL+chatAddUrl+'/create',
      data: dataString,
      success: function(data, textStatus, jqXHR)
      {
        // $('#add').val('data sent sent');
        // $('#msg').html(html);
        // alert(data);
        jQuery('#msg-table tbody').load(BASEURLL+chatAddUrl+'/inboxListOnly?sendto='+sendto);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      }

      // error : error,
      // complete : complete,
      // dataType: dataStringType
    });   
}