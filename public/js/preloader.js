jQuery(window).ready(function () {
    AnimateRotate(0);
});

jQuery(window).load(function(){
    jQuery('.preloader').hide();
});

function AnimateRotate(d){
    var elem = jQuery(".preloader img");
    d += 360;

    jQuery({deg: 0}).animate({deg: d}, {
        duration: 2000,
        step: function(now){
            elem.css({
                 transform: "rotate(" + now + "deg)"
            });
        },
        complete: function(){
            AnimateRotate(0);
        }
    });
}