jQuery( "#modifypersonalinformation" ).on( "click", function() 
{	
	var sessionid = jQuery(this).attr('data-sessionid');
    jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Personal Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'personalinformation/modifydisplay?&IDsafe_user='+sessionid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="savechangespersonalinformation" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#savechangespersonalinformation" ).on( "click", function() 
    { 
        var formData = jQuery('.form-horizontal').serialize();
        jQuery.ajax(
        {
            url: BASEURLL+"personalinformation/updateDoctor",
            type: "POST",
            data: formData,
            dataType: "JSON",
            success: function (data) 
            {
                if (data.errorStatus == false)
                {
                    jQuery('#show-modal-change').modal('hide');
                    jQuery('#personal-information').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
                    jQuery('#personal-information').load(BASEURLL+'personalInformation/updateView?IDsafe_user='+IDsafe_user);
                }
                else
                {
                    jQuery('.alert').alert('close');
                    jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
                    jQuery('.alert').fadeOut(5000);
                }
            }
        });
    });
});