$(function(){
	$('#dateofbirth').datepicker({
		viewMode:2
	}).keypress(function(event) {
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		// var controlKeys = [8, 9, 13, 35, 36, 37, 47];
		var controlKeys = [8];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		// alert(event.which); //test what keycode your key is
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		  // (49 <= event.which && event.which <= 57) || // Always 1 through 9
		  // (48 == event.which && $(this).attr("value")) || // No 0 first digit
		  isControlKey) { // Opera assigns values for control keys.
		return;
		} else {	
			event.preventDefault();
		}
	});
	
	$('.datepicker .datepicker-days tbody').on('click', function(){  $('.datepicker').hide() });

	// check email
	$('#search-patient-with-options-form').submit(function(){
		var email = $("#emailaddress").val(),
			check_email = true,
			email_checker = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		$('.error-search').html('');

		if(email.length > 0){
			check_email =  email_checker.test(email);

			if(!check_email){
				$('#error-search-email').html('Invalid email address');
				return false;
			}
		}
	});
	

});