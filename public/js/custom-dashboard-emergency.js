//jQuery.noConflict();

jQuery( ".deletethis" ).on( "click", function()
{
	var contactid = jQuery(this).attr('data-contactid');
	var personalinfoid = jQuery(this).attr('data-personalinfo');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this contact?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisyes" ).on( "click", function() 
	{
		jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change .modal-footer #deletethisyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #deletethisyes').attr('disabled','disabled');
		jQuery.ajax({
		url: BASEURLL+"emergencyContactInformation/delete?ajax=true",
		type: "POST",
		data: {
		    idsafecontactinformation: contactid,
		    personalinfoid: personalinfoid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
				jQuery('#show-modal-change').modal('hide');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'.  <br>Please select another primary contact to continue deleting this contact.</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#show-modal-change .modal-footer #deletethisyes').html('Continue Deleting');
				jQuery('#show-modal-change .modal-footer #deletethisyes').removeAttr('disabled');
				jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
			}
		}
		});
	});
});
jQuery( ".primarycontactthis" ).on( "click", function()
{
	var contactid = jQuery(this).attr('data-contactid');
	var personalinfoid = jQuery(this).attr('data-personalinfo');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Set as primary this</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to set this contact as primary?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="setprimarythiscontact" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#setprimarythiscontact" ).on( "click", function() 
	{
		jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"emergencyContactInformation/setasprimary?ajax=true",
		type: "POST",
		data: {
		    idsafecontactinformation: contactid,
		    personalinfoid: personalinfoid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
			}
		}
		});
	});
});
jQuery( ".modifythis" ).on( "click", function() 
{
	var personalinfoid = jQuery(this).attr('data-personalinfo');
	var contactid = jQuery(this).attr('data-contactid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Emergency Contact Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'emergencyContactInformation/insertContactDisplay?idsafepersonalinfo='+personalinfoid+'&IDsafe_contactinformation='+contactid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythisyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythisyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"emergencyContactInformation/update?ajax=true",
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
					jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('.alert').fadeOut(5000);		
					jQuery('#modifythisyes').on( "click", handler);			
					jQuery('#show-modal-change .modal-footer #modifythisyes').html('Continue Saving');
					jQuery('#show-modal-change .modal-footer #modifythisyes').removeAttr('disabled');
				}
			}
		});
	});
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythisyes').click();
		    return false;  
		  }
		}); 
	});   
});
jQuery( "#addemergency" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Emergency Contact Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'emergencyContactInformation/insertContactDisplay');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisyes" ).on( "click", function() 
	{
		jQuery('#modifythisyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythisyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"emergencyContactInformation/insert?ajax=true",
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
				jQuery('#emergencyContactTable').load(BASEURLL+'emergencyContactInformation/dashboardDisplay');
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('.alert').fadeOut(5000);
				jQuery('#modifythisyes').on( "click", handler);
				jQuery('#show-modal-change .modal-footer #modifythisyes').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #modifythisyes').removeAttr('disabled');
			}
		}
		});
	});

	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythisyes').click();
		    return false;  
		  }
		}); 
	});
});
//pagination code
jQuery(function()
{
   jQuery("#emergency-pagination-div-id a").click(function()
   {
   		jQuery('#emergencyContactTable').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#emergencyContactTable').load(theURLhere);
		   }
		});
	   return false;
   });
});