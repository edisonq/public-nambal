jQuery( ".addallergy" ).on( "click", function() 
{	
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Add another</span> Allergy Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'allergy/insertDisplayDoctor');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="insertallergynow" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#insertallergynow" ).on( "click", function handler() 
	{
		jQuery('#insertallergynow').off('click');
		jQuery('#show-modal-change .modal-footer #insertallergynow').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #insertallergynow').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"allergy/insertDoctor?ajax=true&sessionid="+IDsafe_user,
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
				jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('#show-modal-change .modal-footer #insertallergynow').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #insertallergynow').removeAttr('disabled');
				jQuery('.alert').fadeOut(5000);
				jQuery('#insertallergynow').on( "click", handler);
			}
		}
		});
	});

	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#insertallergynow').click();
		    return false;  
		  }
		}); 
	});   
});
jQuery( ".modifythisallergy" ).on( "click", function() 
{
	var allergyid = jQuery(this).attr('data-allergyid');
	jQuery('#show-modal-change .modal-body').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Modify</span> Allergy Information</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'allergy/insertDisplayDoctor?IDsafe_allergy='+allergyid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="modifythisallergyyes" class="btn btn-primary" data-loading-text="Loading...">Save Changes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>');
    jQuery("#modifythisallergyyes" ).on( "click", function handler() 
	{	
		jQuery('#modifythisallergyyes').off('click');
		jQuery('#show-modal-change .modal-footer #modifythisallergyyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #modifythisallergyyes').attr('disabled','disabled');
		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax(
		{
			url: BASEURLL+"allergy/update?ajax=true&isdoctor=true&sessionid="+IDsafe_user,
			type: "POST",
			data: formData,
			dataType: "JSON",
			success: function (data) 
			{
			    if (data.errorStatus == false)
			    {
			    	jQuery('#show-modal-change').modal('hide');
			    	jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
			    	jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
					jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
					jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				}
				else
				{
					jQuery('.alert').alert('close');
					jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
					jQuery('#show-modal-change .modal-footer #modifythisallergyyes').html('Continue Saving');
					jQuery('#show-modal-change .modal-footer #modifythisallergyyes').removeAttr('disabled');
					jQuery('.alert').fadeOut(5000);
					jQuery('#modifythisallergyyes').on( "click", handler);
				}
			}
		});
	});
	
	jQuery( document ).ready(function() 
	{
		jQuery( '.form-horizontal' ).on("keyup", function (e) {
		 var key = e.which; if (e.ctrlKey && e.keyCode == 13)  // the enter key code
		  {
		    jQuery('#modifythisallergyyes').click();
		    return false;  
		  }
		}); 
	});
});
jQuery( ".deletethisallergy" ).on( "click", function()
{
	var allergyid = jQuery(this).attr('data-allergyid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Delete</span> Emergency Contact Information?</h3>');
    jQuery('#show-modal-change .modal-body').html('<p>Are you sure you want to delete this allergy?</p>');
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="deletethisallergyyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#deletethisallergyyes" ).on( "click", function() 
	{
		jQuery('#problem-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change').modal('hide');
		jQuery.ajax({
		url: BASEURLL+"allergy/delete?ajax=true&sessionid="+IDsafe_user,
		type: "POST",
		data: {
		    allergyid: allergyid
		},
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {				
				jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
		}
		});
	});
});
jQuery( ".experienceitnow" ).on( "click", function()
{
	var allergyid = jQuery(this).attr('data-allergyid');
    jQuery('#show-modal-change #myModalLabel').html('<h3 id="myModalLabel"><span>Experiencing </span>it now?</h3>');
    jQuery('#show-modal-change .modal-body').load(BASEURLL+'allergy/experienceitnowview?IDsafe_allergy='+allergyid);
    jQuery('#show-modal-change .modal-footer').html('<a href="javascript:void(0);" id="experienceitnowyes" class="btn btn-primary" data-loading-text="Loading...">Yes</a><button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>');
    jQuery("#experienceitnowyes" ).on( "click", function() 
	{
		jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
		jQuery('#show-modal-change .modal-footer #experienceitnowyes').html('Loading.. please wait');
		jQuery('#show-modal-change .modal-footer #experienceitnowyes').attr('disabled','disabled');

		var formData = jQuery('.form-horizontal').serialize();
		jQuery.ajax({
		url: BASEURLL+"allergy/insertDoctor?ajax=true&sessionid="+IDsafe_user,
		type: "POST",
		data: formData,
		dataType: "JSON",
		success: function (data) {
		    if (data.errorStatus == false)
		    {
		    	jQuery('#show-modal-change').modal('hide');
    			jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
    			jQuery('#journal-table').html(' <i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading test list... please wait. <br> <a href="javascript:void(0)" onclick="location.reload();">Reload all list</a>  <br><br>');
				jQuery('#allergen-table').load(BASEURLL+'allergy/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
				jQuery('#journal-table').load(BASEURLL+'journal/dashboardDisplayProfileViewer?sessionid='+IDsafe_user);
			}
			else
			{
				jQuery('.alert').alert('close');
				jQuery('#show-modal-change .modal-body').prepend('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+data.errorMessage+'</div>');
				jQuery('#show-modal-change .modal-footer #insertallergynow').html('Continue Saving');
				jQuery('#show-modal-change .modal-footer #insertallergynow').removeAttr('disabled');
				jQuery('.alert').fadeOut(5000);
			}
		}
		});
	});
});
//pagination code
jQuery(function()
{
   jQuery("#allergen-pagination-div-id a").click(function()
   {
   		jQuery('#allergen-table').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading... please wait');
   		var theURLhere = jQuery(this).attr("href");
		jQuery.ajax(
		{
		   type: "POST",
		   url: jQuery(this).attr("href"),
		   data:"",
		   success: function(data){
		      jQuery('#allergen-table').load(theURLhere);
		   }
		});
	   return false;
   });
});