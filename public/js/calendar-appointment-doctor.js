$(function(){

    $('#external-events').find('div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        events: BASEURLL+'appointments/allAppointments',
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },

        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var $modal = $("#edit-modal"),
                $btn = $('#create-event');
            $btn.off('click');
            $btn.click(function () {
                var title = $("#event-name").val();
                if (title) {
                    calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true
                    );
                }
                calendar.fullCalendar('unselect');
            });
            $modal.modal('show');
            calendar.fullCalendar('unselect');
        },
        editable: true,
        droppable:true,

        drop: function(date, allDay) { // this function is called when something is dropped
            //console.log('test');

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            $(this).remove();

        },
        eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc)
        {
            
              // console.log(
              //       event.id + " was moved " +
              //       dayDelta + " days and " +
              //       minuteDelta + " minutes."
              //   );             
                if (!confirm("Are you sure about this change?")) {
                    revertFunc();
                }
                else
                {
                    var mStart = event.start.getMonth()+1;
                    var startDate = event.start.getFullYear()+"-"+mStart+"-"+event.start.getDate()+" "+event.start.getHours()+":"+event.start.getMinutes();
                    var dataString = "IDappointments="+event.id+'&dayDelta='+ dayDelta+'&minuteDelta='+minuteDelta+"&allDay="+allDay+"&allEventDrag=TRUE&startDate="+startDate;
                   // if (allDay) 
                   //  {
                   //      console.log("Event is now all-day");
                   //  }
                   //  else
                   //  {
                   //      console.log("Event has a time-of-day");
                   //  }
                    $.ajax({
                      type: 'POST',
                      data: dataString,
                      url: BASEURLL+"appointments/modify?ajax=true",
                      success: function(data, textStatus, jqXHR)
                      {
                          if (data.approve)
                          {
                           // console.log(data.minuteDelta);
                          }
                          else
                          {
                            revertFunc();
                          }
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          revertFunc();
                      }
                    });
                }

        },
        eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
            var dataString = "IDappointments="+event.id+'&dayDelta='+ dayDelta+'&minuteDelta='+minuteDelta;

            // console.log(
            //     "The end date of " + event.title + "has been moved " +
            //     dayDelta + " days and " +
            //     minuteDelta + " minutes."
            // );

            if (!confirm("is this okay?")) {
                revertFunc();
            }
            else
            {
                $.ajax({
                  type: 'POST',
                  data: dataString,
                  url: BASEURLL+"appointments/modify?ajax=true",
                  success: function(data, textStatus, jqXHR)
                  {
                      if (data.approve)
                      {
                      //  console.log(data.minuteDelta);
                      }
                      else
                      {
                        revertFunc();
                      }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      revertFunc();
                  }
                });
            }

        },

        // US Holidays
        

        eventClick: function(event) {
            // opens events in a popup window
            if (event.url){
                window.open(event.url, 'gcalevent', 'width=700,height=600');
                return false
            } else {
                    var $modal = $("#myModal"),
                    $modalLabel = $("#myModalLabel");
                    $modalLabel.html(event.title);
                    $modal.find(".modal-body").html(function(){
                    var dataString = "IDappointments="+event.id;
                    var IDappointments = event.id;
                    var IDappnum = event.appnum;
                    var fullname = event.firstname+" "+event.lastname;
                    var allday = event.allDay;
                    var sameday = event.sameday;
                    var timefrom  = event.timefrom;
                    var timeto = event.timeto;
                    var notes = event.note;
                    var dateonly =  event.dateonly;
                    var locationInformation = event.locationinformation;

                    if (notes.length < 1)
                    {
                      notes = 'no notes included';
                    }
                     $.ajax({
                        type: 'GET',
                        data: dataString,
                        url: BASEURLL+"appointments/checkApprove?ajax=true",
                        success: function(data, textStatus, jqXHR)
                        {
                          // ---
                      //    console.log(data.approved);
                          $modal.find(".modal-title").html("Appointment information");
                          if (sameday == false) {
                            if (data.approved == true) {
                              $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');   
                            }
                            else {
                              $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address</strong>: <br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm" data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
                            }
                          }
                          else {
                                if (timefrom == timeto) {
                                  $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>To approve this you should set time<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address: </strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm" data-appointid="'+IDappnum+'">Modify the time, then approve</a>&nbsp;&nbsp;<br> </p>');
                                }
                                else{
                                  if (data.approved == true) {
                                    $modal.find(".modal-body").html('<p>Name: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');
                                  } else {
                                    $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
                                  }
                            }

                            $("#approve-reservation-na").click(function(e) 
                            {
                              var element = '#approval-area-'+$(this).attr('data-appointid');
                              dataString = "IDappointments="+IDappointments;
                              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
                              $modal.modal('hide');
                              $.ajax({
                              type: 'GET',
                              data: dataString,
                              url: BASEURLL+"appointments/approve?ajax=true",
                              success: function(data, textStatus, jqXHR)
                              {
                                  if (data.approve)
                                  {
                                    $(element).html('Approved');
                                  }
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $(element).html('Please refresh');
                              }
                              }); 
                            });

                            $("#cancel-reservation-na").click(function(e) 
                            {
                              var element = '#approval-area-'+$(this).attr('data-appointid');
                              dataString = "IDappointments="+IDappointments;
                              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
                              $modal.modal('hide');
                              $.ajax({
                              type: 'GET',
                              data: dataString,
                              url: BASEURLL+"appointments/cancel?ajax=true",
                              success: function(data, textStatus, jqXHR)
                              {
                                  if (data.approve)
                                  {
                              //      console.log(element);
                                    $(element).html('Canceled appointment');
                                  }
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $(element).html('Please refresh');
                              }
                              }); 
                            });
                          }
                          // ---
                            
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            
                        }
                      });
                }());
                $modal.find(".modal-body").html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
                $modal.modal('show');
            }
        }

    });

    $("#calendar-switcher").find("label").click(function(){
        calendar.fullCalendar( 'changeView', $(this).find('input').val() )
    });
    $("#today").click(function(){
        calendar.fullCalendar('today');
    });
});

jQuery( document ).ready(function() {
  jQuery('.appointment-link').click(function(e) 
  { 
    
    var IDappointments = $(this).attr('data-appointment');
    var IDappnum = $(this).attr('data-appid');
    var dataString = "IDappointments="+IDappointments;
    var $modal = $("#myModal"), $modalLabel = $("#myModalLabel");
    var fullname = $(this).attr('data-fullname');
    var allday = $(this).attr('data-allday');
    var sameday = $(this).attr('data-sameday');
    var timefrom = $(this).attr('data-timefrom');
    var timeto = $(this).attr('data-timeto');
    var notes = $(this).attr('data-notes');
    var dateonly = $(this).attr('data-dateonly');
    var locationInformation = $(this).attr('data-locationInformation');


    if (notes.length < 1)
    {
      notes = 'no notes included';
    }
    $modal.modal('show');
    $modal.find(".modal-body").html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');   
    $.ajax({
        type: 'GET',
        data: dataString,
        url: BASEURLL+"appointments/checkApprove?ajax=true",
        success: function(data, textStatus, jqXHR)
        {
          $modal.find(".modal-title").html("Appointment information");
          if (sameday == false) {
            if (data.approved == true) {
              $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');   
            }
            else {
              $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>  Requested date time from:  '+timefrom+'<br>  Requested date time to: '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address</strong>: <br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm" data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
            }
          }
          else {
                if (timefrom == timeto) {
                  $modal.find(".modal-body").html('<p><strong>Name</strong>: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>To approve this you should set time<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address: </strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm" data-appointid="'+IDappnum+'">Modify the time, then approve</a>&nbsp;&nbsp;<br> </p>');
                }
                else{
                  if (data.approved == true) {
                    $modal.find(".modal-body").html('<p>Name: '+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: approved<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-danger btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Cancel reservation</a>&nbsp;&nbsp;<a href="'+BASEURLL+'profileViewer/doctorViewing?sessionid='+SESSIONIDSAUSER+'" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="cancel-reservation-na">Go to patient record</a><br> </p>');
                  } else {
                    $modal.find(".modal-body").html('<p><strong>Name: </strong>'+fullname+'<br>Requested date and time:<br>'+dateonly+'<br>'+timefrom+' to '+timeto+'<br>  Status: Waiting for approval<br></p><p>Note: <br>  '+notes+'  <br></p><p><br>  <strong>Clinic address:</strong><br>  '+locationInformation+'</p><p><a href="'+BASEURLL+'appointments/modify?appointmentID='+IDappointments+'" class="btn btn-info btn-sm">Modify the time and date</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-success btn-sm"  data-appointid="'+IDappnum+'" id="approve-reservation-na">Approve the request</a><br> </p>');
                  }
            }

            $("#approve-reservation-na").click(function(e) 
            {
              var element = '#approval-area-'+$(this).attr('data-appointid');
              dataString = "IDappointments="+IDappointments;
              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
              $modal.modal('hide');
              $.ajax({
              type: 'GET',
              data: dataString,
              url: BASEURLL+"appointments/approve?ajax=true",
              success: function(data, textStatus, jqXHR)
              {
                  if (data.approve)
                  {
                    $(element).html('Approved');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $(element).html('Please refresh');
              }
              }); 
            });

            $("#cancel-reservation-na").click(function(e) 
            {
              var element = '#approval-area-'+$(this).attr('data-appointid');
              dataString = "IDappointments="+IDappointments;
              $(element).html('loading ... <span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');
              $modal.modal('hide');
              $.ajax({
              type: 'GET',
              data: dataString,
              url: BASEURLL+"appointments/cancel?ajax=true",
              success: function(data, textStatus, jqXHR)
              {
                  if (data.approve)
                  {
                    //console.log(element);
                    $(element).html('Canceled appointment');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $(element).html('Please refresh');
              }
              }); 
            });
          }
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        }
        // error : error,
        // complete : complete,
        // dataType: dataStringType
      });  
    
  });    
});