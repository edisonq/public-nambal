jQuery.noConflict();

// jQuery('#take-a-tour .updateTour').submit(function(){
	// var thisModal = jQuery('#take-a-tour');
	// jQuery('#take-a-tour .skip').attr('disabled','disabled');
	// formAjax(jQuery(this),thisModal);
	// jQuery(this).unbind('submit');
	// return false;
// });
jQuery( document ).ready(function() {

jQuery('#take-a-tour .next').click(function(){
	jQuery('#take-a-tour')
		.removeClass('in')
		.attr('area-hidden','false');
	
	setTimeout(function(){
		jQuery('#take-a-tour').css('display','none');
		
		jQuery('#are-you-a-doctor')
			.addClass('in')
			.attr('area-hidden','true')
			.css('display','block');
	},500);
});

jQuery('#are-you-a-doctor .updateTourDoctor').submit(function(){
	var thisModal = jQuery('#are-you-a-doctor');
	formAjax(jQuery(this),thisModal);
	jQuery(this).unbind('submit');
	return false;
});

jQuery('#are-you-a-doctor .updateTour').submit(function(){
	var thisModal = jQuery('#are-you-a-doctor');
	formAjax(jQuery(this),thisModal);
	jQuery(this).unbind('submit');

	jQuery.ajax({
	url: BASEURLL+"dashboard/tour",
	type: "POST",
	data: formData,
	dataType: "JSON",
	success: function (data) {
	    location.reload();
	}
	});
	
	return false;
});

 
var formAjax = function(thisForm,thisModal){
	var formName = thisForm.attr('name');
	jQuery.ajax({
		type: thisForm.attr('method'),
		url: thisForm.attr('action'),
		dataType: 'JSON',
		data: thisForm.serialize(),
		success: function(data){
			switch(data.status)
			{
				case true:
					thisModal.removeClass('in').attr('area-hidden','false');
						
					setTimeout(function(){
						thisModal.css('display','none');
						if(formName == 'updateTourDoctor'){
							window.location = "registerDoctor";
						}
					},500);
					
					exit;
				break;
				
				case false:
					window.location = 'dashboard';
				break;
			
				default:
					window.location = 'dashboard';
				exit;
			}
		}
	});
};
});