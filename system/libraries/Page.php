<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Page {

	public static function Pre($array = array(), $die = false) {
		echo '<pre>';
		print_r($array);
		echo '</pre>';
		if ($die)
			die();
	}

	public static function vars($vars = null) {
		if ($vars == null)
			return array();

		$vars = urldecode($vars);
		$vars = rtrim($vars, ';');
		$vars = explode(';', $vars);

		$return = array();

		foreach ($vars as $var):
			$var = explode(':', $var);
			if (isset($var[0], $var[1]))
				$return[$var[0]] = $var[1];
		endforeach;

		return $return;
	}

	public static function referer() {
		return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	}

	public static function isAjax() {
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest")
			return true;

		return false;
	}

	public static function isPost() {
		if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == "POST")
			return true;

		return false;
	}

	public static function isValidEmail($email) {

		if (!@ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
			return false;
		}

		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);

		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!@ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
				return false;
			}
		}

		if (!@ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!@ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
					return false;
				}
			}
		}

		return true;
	}

}