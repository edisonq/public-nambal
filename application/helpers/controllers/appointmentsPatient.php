<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class AppointmentsPatient extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->library('form_validation');
        $this->load->model('Personalprofile_model', 'personalProfile');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Messages_model', 'messagesModel');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Appointment_model', 'appointmentsmodel');
        $this->activeMainMenu = "connect-with-professionals";
        $this->navOpen = "connect with Professionals";

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
    
        # check user's if properly logged-in
        if (empty($this->nambal_session['sessionName']))
        {
            redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        //     $fbUser = $this->facebook->getUser();
        //     if (!empty($fbUser))
        //     {
        //         if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        //         {
        //             redirect(base_url().'logout', 'refresh');
        //         }
        //     } else {
        //         redirect(base_url().'login/facebook', 'refresh');
        //     }
        // }
        
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }
        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in

        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

    public function setAppointmentFromPatientExact()
    {
        $this->to = filter_var($this->input->get('to'), FILTER_SANITIZE_STRING);
        $this->date = filter_var($this->input->get('date'), FILTER_SANITIZE_STRING);
        $this->dateCal = filter_var($this->input->get('datecal'), FILTER_SANITIZE_STRING);
        $this->dateCalTo = filter_var($this->input->get('datecalto'), FILTER_SANITIZE_STRING);

        $errorMessage = '';
        $error = FALSE;
        $success = FALSE;
        $allDay = FALSE;
        $notAppointable  = FALSE;
        $customCss = array(
            'custom-dashboard',
            'patient-dashboard',
            'custom-patient-calendar'
        );
        $customJsTop = array(
            // 'calendar',
            'fullcalendar.min'
        );
        $drName = '';
        $customJs = array(
            'tour-patient-dashboard',
            '../lib/select2',
            'custom-dashboard',
            '../lib/bootstrap-datepicker',
            '../lib/bootstrap-select/bootstrap-select',
            '../lib/wysihtml5/wysihtml5-0.3.0_rc2',
            '../lib/bootstrap-wysihtml5/bootstrap-wysihtml5'
        );

        $this->form_validation->set_rules('location-name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('timefrom', 'trim|xss_clean|required');
        $this->form_validation->set_rules('timeto', 'trim|xss_clean|required');
        $this->form_validation->set_rules('appointment-type-selected', 'trim|xss_clean|required');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
        }
        else
        {
            $error = false;
        }
        
        $locationName = filter_var($this->input->post('location-name'), FILTER_SANITIZE_STRING);
        $timefrom = filter_var($this->input->post('timefrom'), FILTER_SANITIZE_STRING);
        $timeto = filter_var($this->input->post('timeto'), FILTER_SANITIZE_STRING);
        $appointmentTypeSelected = filter_var($this->input->post('appointment-type-selected'), FILTER_SANITIZE_STRING);


        

        if (empty($this->to))
        {
            redirect(base_url().'connectDoctor/search','refresh');
        }
        if (empty($this->date))
        {
            if (!empty($this->dateCal))
            {
                $dateCal = explode(' ', $this->dateCal);
                $this->date = date("Y-m-d-H:i", strtotime($dateCal[1].' '.$dateCal[2].' '.$dateCal[3].' '.$dateCal[4]));
            }
            else
            {
                redirect(base_url().'appointmentsPatient/setAppointmentFromPatient?to='.$this->to,'refresh');   
            }
            if (!empty($this->dateCalTo))
            {
                $dateCalTo = explode(' ', $this->dateCalTo);
                $this->dateTo = date("Y-m-d-H:i", strtotime($dateCalTo[1].' '.$dateCalTo[2].' '.$dateCalTo[3].' '.$dateCalTo[4]));
            }
            if ((!empty($dateCalTo)) && (!empty($dateCal)))
            {
                if ($dateCalTo == $dateCal)
                {
                    $allday = true;
                }
            }
        }

        $to = $this->nagkamoritsing->ibalik($this->to);
        $this->doctorInfo = $this->emrUser->getDocInfoComplete($to);

        $this->doctorSpecialty = $this->emrUser->getDocSpecialty($this->doctorInfo[0]->IDemr_user);
        $this->appointmentSlot = $this->emrUser->getAppointmentSlot($this->doctorInfo[0]->IDemr_user);
        $this->getDoctorAppointments = $this->appointmentsmodel->getDoctorAppointments(
            $this->doctorInfo[0]->IDemr_user, 
            date("Y-m-d H:i:s")
        );
        $this->appointmentTypes = $this->emrUser->getAppointmentType($this->doctorInfo[0]->IDemr_user);
        $this->clinicInfo = $this->emrUser->getDocClinicInfo($to);
        

        # arrange the date programmatically
        $getDate = explode('-', $this->date);
        $getTime = explode(':', $getDate[3]);
        $getHour = $getTime[0];
        $getMinutes = $getTime[1];
        $getTime = date('H:i:s', strtotime($getHour.':'.$getMinutes));
        $getDate = $getDate[0].'-'.$getDate[1].'-'.$getDate[2];
        $getDateISO  = date('o-m-d', strtotime($getDate.' '.$getTime)); 
        $getFullDate = date('o-m-d H:i:s', strtotime($getDate.' '.$getTime));
        // $getNameofWeek = date('l', strtotime($getDateISO));
        # end of arranging the date programmtically

        # check if the slots has available appointment
        $this->scheduleAppointable = $this->appointmentsmodel->checkScheduleIfAppointable($to, $getFullDate, $getTime);



        if ($this->scheduleAppointable['available'] == TRUE)
        {
            # get queue limit
            $this->queueLeft = $this->appointmentsmodel->queueLimitCheck(
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timefrom, 
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timeto, 
                $to, 
                $getFullDate, 
                $this->scheduleAppointable['appointments_settings']->queue_limit, 
                $this->scheduleAppointable['appointments_settings']->averagetimeperpatient_min, 
                $this->scheduleAppointable['appointments_settings']->appointment_limit, 
                $this->scheduleAppointable['appointments_settings']->allowoverlap
            );
            
            # must retrive all appointments and compute time left
            # $this->queueLeft['timeTotal_before']
            # $this->queueLeft['timeTotal_appointment']
            # $this->queueLeft['minutesLeftSQL']
            # $this->queueLeft['minutesLeft']
            # $this->queueLeft['queueLeft'] 
            # $this->queueLeft['appointmentLeft']
            # check if the slot is still available for appointment
            # base on the data above
            # don't forget to restrict the appointment with:
            # - appointment limit
            # - show average time per patient
            # - compute the appointment limit
            # - check what type is chosen in time frame "$this->date".
            # - check if the the type is still appointable.
            # - $this->to should always be a doctor.
            # - allow patient to put NOTES.
            # - check if it's not today.
            # - appointment should be the day before.
            # - if $this->date = wla mo exist sa db kay mag ask sa patient if sure ba siya

            if (($this->queueLeft['queueLeft'] >= 1) && ($this->queueLeft['appointmentLeft'] >= 1) && ($this->queueLeft['minutesLeftSQL'] > 1))
            {
                # computing time left with the average time.
                if ($this->queueLeft['minutesLeftSQL'] < $this->scheduleAppointable['appointments_settings']->averagetimeperpatient_min)
                {
                    # warn user that the remaninng time is lower than the average time
                    # would they like to continue?
                    # display the remaining time
                    $errorMessage .= 'The remaining appointment time is lower than the average time the doctors can attend to each patient. ';
                }
            }
            else
            {
                # tell user to find another schedule.
                # because no more queue left available. 
                $errorMessage .= 'We recommend you to please find another schedule. It seems the doctor is not available in this selected schedule.  ';
            }

            # check patient is available
            $this->patientAvail = $this->appointmentsmodel->checkPatientIfAvailable($this->nambal_session['IDsafe_user'], $getFullDate);
            

            # create a temporary schedule if there is still time left for appointment reservation
            $this->temporarySchedule = $this->appointmentsmodel->getTemporarySchedule(
                $to, 
                $this->nambal_session['IDsafe_user'], 
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timefrom, 
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timeto, 
                $this->getDoctorAppointments,
                $this->scheduleAppointable['appointments_settings']->appointment_limit,
                $this->scheduleAppointable['appointments_settings']->averagetimeperpatient_min
            );

            $this->clinicInfoExactSchedule = $this->appointmentsmodel->getClinicInfoExactSchedule(
                $this->doctorInfo[0]->IDemr_user,
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timefrom,
                $getDateISO.' '.$this->scheduleAppointable['appointments_settings']->timeto
            );

            if (!empty($this->temporarySchedule))
            {
                if (!empty($errorMessage))
                {   
                    $errorMessage .= '  <strong>You may still set appointment but doctor may not approve.</strong>';
                }
                // print_r($this->temporarySchedule);
            }
            else
            {
                $errorMessage .= '  <strong>No more remaining time for appointment. Please select another schedule.</strong>';
            }

            if ($_POST)
            {
                if ((!empty($locationName)) && (!empty($timefrom)) && (!empty($timeto)) )
                {
                    $doctorInformation = $this->emrUser->getDocInfo($to);
                    $locationID = explode('_', $locationName);

                    // echo $locationName;
                    // echo '<br>';
                    // echo $timefrom;
                    // echo '<br>';
                    // echo $timeto;
                    $data = array(
                        'emr_user_IDemr_user' => @$doctorInformation->IDemr_user,
                        'safe_user_IDsafe_user' => $this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user']),
                        'emr_nonnambalinfo_IDemr_nonnambalinfo' => '',
                        'timefrom' => @$timefrom,
                        'timeto' => @$timeto,
                        'allday' => @$allDay,
                        'active' => true,
                        'note' => '',
                        'approved' => false,
                        'appointment_type_IDappointment_type' => @$this->scheduleAppointable['appointments_settings']->IDappointment_type,
                        'IDlocation_info' => @$locationID[0],
                        'emr_addresses_IDemr_addresses' => @$locationID[1]
                    );

                    $this->appointmentsmodel->setAppointment($data);

                   
                    $success = TRUE;
                    
                }
            }            
        }
        else
        {
            $this->patientAvail = $this->appointmentsmodel->checkPatientIfAvailable($this->nambal_session['IDsafe_user'], $getFullDate);

            # temporarily selected the clinic consultation
            # usbon ra ni pohon pero for now kini lang sa
            $appointmentSelected = 1;

            # kwaon ang tanan appointment type
            $this->allAppointmentType = $this->appointmentsmodel->getAllAppointmentType();

            #not appointable
            $timeto = explode('-', $this->dateTo);
            $timeFrom  = explode('-', $this->date);
            $this->timeto = date('o-m-d H:i:s', strtotime($timeto[0].'-'.$timeto[1].'-'.$timeto[2].' '.$timeto[3]));
            $this->timefrom = date('o-m-d H:i:s', strtotime($timeFrom[0].'-'.$timeFrom[1].'-'.$timeFrom[2].' '.$timeFrom[3]));
            $this->locationName = 

            $data = array(
                'emr_user_IDemr_user' => @$this->doctorInfo[0]->IDemr_user,
                'safe_user_IDsafe_user' => $this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user']),
                'emr_nonnambalinfo_IDemr_nonnambalinfo' => '',
                'timefrom' => @$this->timefrom,
                'timeto' => @$this->timeto,
                'allday' => @$allday,
                'active' => true,
                'note' => '',
                'approved' => false,
                'appointment_type_IDappointment_type' => $appointmentSelected
            );

            if ($_POST)
            {
                if (!$this->patientAvail)
                {
                    $success = TRUE;
                    $this->appointmentsmodel->setAppointment($data);
                }
            }
            // print_r($data);
            // print_r($this->allAppointmentType);
            $notAppointable = TRUE;
        }
        
        # end of getting the available schedules
        if ($success == TRUE)
        {
            $this->load->model('Pusher_model', 'pushery');
            $this->pushery->setAppointment(@$to);

            # recommend patient to another schedule if there are no slot left. (check average time per patient)

            # but if "allow overlap" it should allow patient to this schedule.
            # if ($this->scheduleAppointable['appointments_settings']->allowoverlap == TRUE)

            # EMAIL: 
            # - the patient - receipt of the appointment.
            # - the doctor - for approval.
            # - the staff - for approval, if doctor gave him/her access.
            # need to create an approval link (doctors side)
            # need to create a page for this appointment (shown to patient, doctor and staff only).
        }

        if ($success == TRUE)
        {        
            $this->load->view(
            //'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Set appointment to Dr. '.$drName,
            'view' => 'appointments/set-appointment-success', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'errorMessage' => $errorMessage,
            'error' => $error,
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'));
        }
        elseif(($success == FALSE) && ($notAppointable == TRUE))
        {
            $this->load->view(
            //'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Set appointment to Dr. '.$drName,
            'view' => 'appointments/set-appointment-notappointable', 
            // 'view' => 'appointments/set-appointment-success', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'errorMessage' => $errorMessage,
            'error' => $error,
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'));
        }
        else
        {
            $this->load->view(
            //'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Set appointment to Dr. '.$drName,
            'view' => 'appointments/set-appointment-exact', 
            // 'view' => 'appointments/set-appointment-success', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'errorMessage' => $errorMessage,
            'error' => $error,
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'));
        }

        //$this->output->enable_profiler(TRUE);


    }

    public function setAppointmentFromPatient()
    {
        $customCss = array(
            'custom-dashboard',
            'patient-dashboard',
            'custom-patient-calendar'
        );
        $customJsTop = array(
            // 'calendar',
            'fullcalendar.min'
        );
        $drName = '';
        $customJs = array(
            'tour-patient-dashboard',
            'custom-dashboard',
            '../lib/bootstrap-datepicker',
            '../lib/bootstrap-select/bootstrap-select',
            '../lib/wysihtml5/wysihtml5-0.3.0_rc2',
            '../lib/bootstrap-wysihtml5/bootstrap-wysihtml5'
        );

        # check if the to or doctors ID is existing
        $this->form_validation->set_rules('to','Clinic Name', 'trim|xss_clean|required');

        #run form validation
        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        } else 
        {
            $error = FALSE;
        }

        $this->to = filter_var($this->input->get('to'), FILTER_SANITIZE_STRING);
        $to = $this->nagkamoritsing->ibalik($this->to);

        if(!empty($to))
        {
            $this->doctorInfo = $this->emrUser->getDocInfoComplete($to);
            if (empty($this->doctorInfo[0]))
            {
                redirect(base_url().'connectDoctor/search','refresh');
            }
            $drName = $this->nagkamoritsing->ibalik($this->doctorInfo[0]->lastname);
            if (!empty($this->doctorInfo))
            {
                $this->doctorSpecialty = $this->emrUser->getDocSpecialty($this->doctorInfo[0]->IDemr_user);
                $this->appointmentTypes = $this->emrUser->getAppointmentType($this->doctorInfo[0]->IDemr_user);
                $this->appointmentSlot = $this->emrUser->getAppointmentSlot($this->doctorInfo[0]->IDemr_user);
                $this->getDoctorAppointments = $this->appointmentsmodel->getDoctorAppointments($this->doctorInfo[0]->IDemr_user, date("Y-m-d h:i:s"), TRUE, TRUE);
                  

                foreach ($this->appointmentSlot as $app) 
                {
                    # check if this slot is available
                    # use array_search, http://www.php.net/array_search to save space.
                    # retrieve once in db.
                    # check current date, older date will no longer be good for appointment.s

                    # tunol sa view kay:
                    # type sa appointment, avail or not, timeto, timefrom
                    
                }
            }
            else
            {
                redirect(base_url().'connectDoctor/search','refresh');
            }
            $this->clinicInfo = $this->emrUser->getDocClinicInfo($to);
            
        }
        else
        {
            redirect(base_url().'connectDoctor/search', 'refresh');
        }

        $this->load->view(
            //'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Set appointment to Dr. '.$drName,
            'view' => 'appointments/set-appointment', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'));

        //$this->output->enable_profiler(TRUE);
        return;
    }

    public function detailedInformation()
    {
        
    }

	public function index()
	{
		$customJs = array('fullcalendar.min','calendar','tables-dynamic','app');
        $customCss = array('custom-dashboard','zocial', 'custom-messaging');
        $json = '';
        $errorMessage = '';
        
        $urlAdd = 'doctorDashboard';
		$customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';        


		$this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Appointments - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/index'
        ));
	}

    
}
?>