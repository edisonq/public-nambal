<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Account', 
			'view' => 'account/index'));
		return;
	}

	public function notices(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Notices', 
			'view' => 'account/notices'));
		return;
	}

	public function activeReport(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Active Report', 
			'view' => 'account/active-report'));
		return;
	}

	public function verifyAccount(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Verify Account', 
			'view' => 'account/verify-account'));
		return;
	}

	public function progress(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Progress of your account',
				'view' => 'account/progress'));
		return;
	}

	public function changeUsername()
	{

	}

	public function signout(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Sign out now',
				'view' => 'account/sign-out'));
		return;
	}
}

?>