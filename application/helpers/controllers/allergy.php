<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Allergy extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Allergy_model', 'allergy');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('safeuserlog_model', 'safeuserlog');


		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented becaues of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index()
	{
		$this->load->view(
			'homepage.phtml', array('title' => 'Allergy Information', 
			'view' => 'allergy/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$allergyList = array();

		$this->form_validation->set_rules('allergen','Allergen', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('severity','Severity', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countreaction','countreaction', 'trim|xss_clean|required');
		$this->form_validation->set_rules('counttreatment','counttreatment', 'trim|xss_clean|required');
		$this->form_validation->set_rules('notes','Notes', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$allergen = ucwords(filter_var($this->input->post('allergen'), FILTER_SANITIZE_STRING));
			$dateexperienced = $this->input->post('dateexperienced');
			$severity = filter_var($this->input->post('severity'), FILTER_SANITIZE_STRING);
			$countreaction = filter_var($this->input->post('countreaction'), FILTER_SANITIZE_STRING);
			$counttreatment = filter_var($this->input->post('counttreatment'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexperienced);
			$dateexperienced = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			for ($treatmentCount = 1; $treatmentCount <= $counttreatment ; $treatmentCount++) 
			{ 
				$trment = @filter_var($this->input->post('medication'.$treatmentCount), FILTER_SANITIZE_STRING);
				if (!empty($trment))
					$treatment[$treatmentCount] = $trment;
			}
			for ($reactionCount = 1; $reactionCount  <= $countreaction; $reactionCount ++) 
			{ 
				$react = @filter_var($this->input->post('reaction'.$reactionCount), FILTER_SANITIZE_STRING);
				if (!empty($react))
					$reaction[$reactionCount] = $react;
			}

			$allergyList = array(
				'allergen' => $allergen,
				'dateexperienced' => $dateexperienced,
				'severity' => $severity,
				'notes' => @$notes,
				'treatmentArray' => @$treatment,
				'IDsafe_personalInfo' => @$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']),
				'reactionArray' => @$reaction
			);

			if ($error == FALSE)
				$this->allergy->insertAllergen($allergyList);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Insert Allergy Information', 
				'view' => 'allergy/insert'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}

	public function insertDoctor()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$allergyList = array();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

		$this->form_validation->set_rules('allergen','Allergen', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('severity','Severity', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countreaction','countreaction', 'trim|xss_clean|required');
		$this->form_validation->set_rules('counttreatment','counttreatment', 'trim|xss_clean|required');
		$this->form_validation->set_rules('notes','Notes', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes','Doctor Notes', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$allergen = ucwords(filter_var($this->input->post('allergen'), FILTER_SANITIZE_STRING));
			$dateexperienced = $this->input->post('dateexperienced');
			$severity = filter_var($this->input->post('severity'), FILTER_SANITIZE_STRING);
			$countreaction = filter_var($this->input->post('countreaction'), FILTER_SANITIZE_STRING);
			$counttreatment = filter_var($this->input->post('counttreatment'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);

			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo
			# get the IDsafe_personalInfo
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexperienced);
			$dateexperienced = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' 00:00:00'));


			for ($treatmentCount = 1; $treatmentCount <= $counttreatment ; $treatmentCount++) 
			{ 
				$trment = @filter_var($this->input->post('medication'.$treatmentCount), FILTER_SANITIZE_STRING);
				if (!empty($trment))
					$treatment[$treatmentCount] = $trment;
			}
			for ($reactionCount = 1; $reactionCount  <= $countreaction; $reactionCount ++) 
			{ 
				$react = @filter_var($this->input->post('reaction'.$reactionCount), FILTER_SANITIZE_STRING);
				if (!empty($react))
					$reaction[$reactionCount] = $react;
			}

			$allergyList = array(
				'allergen' => $allergen,
				'dateexperienced' => $dateexperienced,
				'severity' => $severity,
				'notes' => @$notes,
				'treatmentArray' => @$treatment,
				'IDsafe_personalInfo' => @$IDsafe_personalInfo,
				'reactionArray' => @$reaction
			);

			if ($error == FALSE)
			{
				# insert una ang sa patient side
				$IDsafe_allergen = $this->allergy->insertAllergen($allergyList);
				
				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				$allergyList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$allergyList['IDemr_user'] = $doctorInformation->IDemr_user;
				$allergyList['doctor-notes'] = $doctorNotes;
				$allergyList['IDsafe_allergen'] = $IDsafe_allergen;
				$allergyList['IDemr_patient'] = $IDemr_patient;

				$this->safejournal->insertjournalallergy($allergyList);
				$patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($IDsafe_user));
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);


				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);
				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		
				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
			}
				
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Insert Allergy Information', 
				'view' => 'allergy/insert'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}

	

	public function update()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$allergyList = array();

		// allergen		$this->allergenInfo['allergenName']
		// dateexperienced		if (!empty($this->allergenInfo['dateexperienced'])) echo date('m/d/o', strtotime($this->allergenInfo['dateexperienced'])); else echo date('m/d/o'); 
		// serverity		 echo ((@$this->allergenInfo['serverity'] == 1) ? 'checked' : ''); 
		// reaction1 ... 		@$this->allergenInfo['reaction1'];> ...
		// medication1 ... 	@$this->allergenInfo['medication1'] ...
		// countreaction	if (!empty($this->allergenInfo['countreaction'])) echo $this->allergenInfo['countreaction']; else echo 1;
		// counttreatment	if (!empty($this->allergenInfo['counttreatment'])) echo $this->allergenInfo['counttreatment']; else echo 1;
		// notes		@$this->allergenInfo['notes'];

		$this->form_validation->set_rules('allergen','Allergen', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('severity','Severity', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countreaction','countreaction', 'trim|xss_clean|required');
		$this->form_validation->set_rules('counttreatment','counttreatment', 'trim|xss_clean|required');
		$this->form_validation->set_rules('notes','Notes', 'trim|xss_clean');
		$this->form_validation->set_rules('IDsafe_allergen','IDsafe_allergen','trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}

		// ---
		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
			$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);

			$IDsafe_allergen = filter_var($this->input->post('IDsafe_allergen'), FILTER_SANITIZE_STRING);
			$allergen = ucwords(filter_var($this->input->post('allergen'), FILTER_SANITIZE_STRING));
			$dateexperienced = $this->input->post('dateexperienced');
			$severity = filter_var($this->input->post('severity'), FILTER_SANITIZE_STRING);
			$countreaction = filter_var($this->input->post('countreaction'), FILTER_SANITIZE_STRING);
			$counttreatment = filter_var($this->input->post('counttreatment'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			# para lang ni sa doctor
			$IDemr_journal = (filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING));
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING); # notes for doctor

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexperienced);
			$dateexperience = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			# retrieve reaction and treatment
			for ($treatmentCount = 1; $treatmentCount <= $counttreatment ; $treatmentCount++) 
			{ 
				$trment = @filter_var($this->input->post('medication'.$treatmentCount), FILTER_SANITIZE_STRING);
				if (!empty($trment))
					$treatment[$treatmentCount] = $trment;
			}
			for ($reactionCount = 1; $reactionCount  <= $countreaction; $reactionCount ++) 
			{ 
				$react = @filter_var($this->input->post('reaction'.$reactionCount), FILTER_SANITIZE_STRING);
				if (!empty($react))
					$reaction[$reactionCount] = $react;
			}

			$allergyList = array(
				'allergen' => $allergen,
				'dateexperienced' => $dateexperience,
				'severity' => $severity,
				'notes' => @$notes,
				'treatmentArray' => @$treatment,
				'reactionArray' => @$reaction,
				'IDsafe_allergen' => @$IDsafe_allergen
			);

			// 'IDsafe_allergen' => $IDsafe_allergen,
			// 'allergenName' => @$safe_allergen->allergenName,
			// 'dateexperienced' => @$safe_allergen->dateexperienced,
			// 'severity' => (int)@$safe_allergen->severity,
			// 'notes' => @$safe_allergen->notes,
			// 'reactions' => @$reactionArray,
			// 'treatments' => @$treatmentArray,
			// 'countreaction' => (int)@$countreaction,
			// 'counttreatment' => (int)@$counttreatment

			$dataInDb = $this->allergy->getToModify($IDsafe_allergen);
			// if ((strcasecmp($dataInDb['allergenName'], $allergen)==0) &&
			// 	(date('m/d/o',strtotime($dataInDb['dateexperienced'])) == $dateexperienced) &&
			// 	(strcasecmp($dataInDb['severity'], $severity)==0) &&
			// 	(strcasecmp($dataInDb['notes'], $notes)==0) &&
			// 	($this->allergy->isReactionChanged($IDsafe_allergen, $reaction) == FALSE) &&
			// 	($this->allergy->isTreatmentChanged($IDsafe_allergen, $treatment) == FALSE))
			// {

			// 	$error = TRUE;
			// 	$this->errorMessage = '<p>No Changes </p>';
			// 	$errorMessage = 'No Changes, please press cancel instead';
			// }


			if ($error == FALSE)
			{
				
				$this->allergy->updateSafeAllergen($allergyList);

				if (strcasecmp($isdoctor, 'true')==0)
				{
					$IDsafe_personalInfo = @$dataInDb['IDsafe_personalInfo'];
		            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
		            $IDemr_patient = @$IDemr_patient->IDemr_patient;
		            if (empty($IDemr_patient))
		                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);


		            $allergyList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
					$allergyList['IDemr_user'] = $doctorInformation->IDemr_user;
					$allergyList['doctor-notes'] = $doctorNotes;
					$allergyList['IDemr_patient'] = $IDemr_patient;
					$allergyList['IDemr_journal'] = $IDemr_journal;

		            # doctor notes connection and for sending email
		            # if empty ang IDemr_journal
		            # meaning patient input ni
		            if (!empty($IDemr_journal))
					{
						$allergyList['IDemr_journal'] = $IDemr_journal;
						$this->safejournal->updateJounal($allergyList);
					} elseif ((empty($IDemr_journal)) || ($IDemr_journal == 0)) {
		            	$this->safejournal->insertjournalallergy($allergyList);
		            } else {
		            	$this->safejournal->addjournal($allergyList,'safe_allergen');
		            }

		            $patientinfo = $this->safeuser->getSafeUserInfoUsingPersonalInfo(@$IDsafe_personalInfo);
		            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($patientinfo->IDsafe_user);
		            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

		            $emailData = array(
		                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
		                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
		            );
		            $message = $this->load->view(
		                    'for-email.phtml', array('title' => 'Notify patient', 
		                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		    
		            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
	            	$this->safeuserlog->notifypatientbyemail($patientinfo->IDsafe_user,$message);
				}
				
			}
		}
		// ---

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $allergyList)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Update Allergy Information', 
				'view' => 'allergy/update'));
		}		

		return;
	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('allergyid', 'allergyid',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$IDsafe_allergen = filter_var($this->input->post('allergyid'), FILTER_SANITIZE_STRING);
			
			$this->allergy->deleteSafeAllergy($IDsafe_allergen);

			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Allergy Information', 
			'view' => 'allergy/delete'));
		}	

		return;
	}

	public function deleteDoctor()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		# check if doctor ba jud ang ga delete

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('allergyid', 'allergyid',  'trim|required|xss_clean');

		# know who is the patient
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		
		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$IDsafe_allergen = filter_var($this->input->post('allergyid'), FILTER_SANITIZE_STRING);
			
			$returnlang = $this->allergy->deleteSafeAllergy($IDsafe_allergen, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			// $returnlang = $this->problem->deleteSafeProblem($IDsafe_problem, $this->nambal_session['IDsafe_user']);  #butangan og sakto nga doctor ID

			header('Content-Type: application/json');
			$jsonDisplay['returnlang'] = $returnlang;
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Allergy Information', 
			'view' => 'allergy/delete'));
		}	

		return;
	}

	public function dashboardDisplay()
	{
		$this->checkSession();
		$this->safeallergy = $this->allergy->displayDashboardAllergy($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		# get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);

		# for paging result 
        # will work with custom-dashboard-allergy-profileviewer.js pagination code
        # and display_allergy-information-profileviewer.phtml div allergen-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'allergy/dashboardDisplay?q=0';
        $config["total_rows"] = $this->allergy->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->allergy->displayDashboardAllergy($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		$this->load->view('allergy/display_allergy-information.phtml', $data, array());
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;
		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->safeallergy = $this->allergy->displayDashboardAllergy($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-allergy-profileviewer.js pagination code
        # and display_allergy-information-profileviewer.phtml div allergen-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'allergy/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->allergy->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->allergy->displayDashboardAllergy($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('allergy/display_allergy-information-profileviewer.phtml', $data, array());
	}

	public function displayList()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->allergy->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	public function insertDisplay()
	{
		$this->checkSession();
		$arrayGetMedication = array();

		$IDsafe_allergy = filter_var($this->input->get('IDsafe_allergy'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_allergy' => $IDsafe_allergy
		);

		$this->allergenInfo = $this->allergy->getToModify($IDsafe_allergy);
		$this->load->view('allergy/display_allergy-insert-information.phtml', array());
	}


	public function insertDisplayDoctor()
	{
		$this->checkSession();
		$arrayGetMedication = array();

		$IDsafe_allergy = filter_var($this->input->get('IDsafe_allergy'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_allergy' => $IDsafe_allergy
		);

		$this->allergenInfo = $this->allergy->getToModify($IDsafe_allergy);

		if (!empty($this->allergenInfo['IDemr_journal']))
			$this->journalinformation = $this->safejournal->getjournal($this->allergenInfo['IDemr_journal']);

		$this->allergenInfo['doctor-notes'] = @$this->journalinformation->notes;
		$this->load->view('allergy/display_allergy-insert-information-profileviewer.phtml', array());
	}

	public function experienceitnowview()
	{
		$this->checkSession();
		$arrayGetMedication = array();

		$IDsafe_allergy = filter_var($this->input->get('IDsafe_allergy'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_allergy' => $IDsafe_allergy
		);

		$this->allergenInfo = $this->allergy->getToModify($IDsafe_allergy);
		$this->allergenInfo['dateexperienced'] = date('m/d/Y'); //06/30/2015

		if (!empty($this->allergenInfo['IDemr_journal']))
			$this->journalinformation = $this->safejournal->getjournal($this->allergenInfo['IDemr_journal']);

		$this->allergenInfo['doctor-notes'] = @$this->journalinformation->notes;
		$this->load->view('allergy/display_allergy-insert-information-profileviewer.phtml', array());
	}


	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }

}
?>