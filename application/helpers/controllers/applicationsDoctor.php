<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ApplicationsDoctor extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Personalprofile_model', 'personalProfile');
		$this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	
		# redirect users to dashboard who are logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP     
        }
        # end of redirecting users to dashboard if logged in
    }

    public function index()
    {
    	$errorMessage = '';
		$personalInfoOutput = '';
		$urlAdd = 'doctorstaff';
		$customCss = array('custom-dashboard','zocial', 'custom-profile-viewer');
        $customJs = array('tables-dynamic');

        $this->load->model('Emruser_model', 'emrusermodel');
        $navOpen = 'applications and plug-ins';  

        $arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

		

        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Doctor Dashboard', 
            'customCss' => $customCss,
            'arrayGetDocInfo' => $arrayGetDocInfo,
            'personalInfoOutput' => $personalInfoOutput,
            'errorMessage' => $errorMessage,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'doctor-dashboard-staff/index'
        )); 
		return;
    }

    public function appNetwork()
    {
        $errorMessage = '';
        $personalInfoOutput = '';
        $urlAdd = 'doctorstaff';
        $customCss = array('custom-dashboard','zocial', 'custom-profile-viewer');
        $customJs = array('tables-dynamic');

        $this->load->model('Emruser_model', 'emrusermodel');
        $navOpen = 'applications and plug-ins';  

        $arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        

        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Doctor Dashboard', 
            'customCss' => $customCss,
            'arrayGetDocInfo' => $arrayGetDocInfo,
            'personalInfoOutput' => $personalInfoOutput,
            'errorMessage' => $errorMessage,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'doctor-dashboard-staff/index'
        )); 
        return;
    }

    

} 

?>