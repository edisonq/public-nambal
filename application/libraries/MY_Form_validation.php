<?php

class MY_Form_validation extends CI_Form_validation {

	function __construct($config = array()) {
		parent::__construct($config);
		// parent::CI_Form_validation();
	}

	/**
	 * Error Array
	 *
	 * Returns the error messages as an array
	 *
	 * @return  array
	 */

	public function valid_date($str)
    {
        $arr = explode("/", $str);    // splitting the array
        $yyyy = $arr[2];            // year
        $mm = $arr[0];              // month
        $dd = $arr[1];              // day
        if(is_numeric($yyyy) && is_numeric($mm) && is_numeric($dd))
        {
        	if ((checkdate($mm, $dd, $yyyy)) == FALSE)
        	{
        		$mythis =& get_instance();
             	$mythis->form_validation->set_message('valid_date', 'The %s field must be in format: mm/dd/yyyy.');
             	return FALSE;
        	}
            return TRUE;
        } 
        else 
        {
             $mythis =& get_instance();
             $mythis->form_validation->set_message('valid_date', 'The %s field must be in format: mm/dd/yyyy.');
                return FALSE;
        }
    }
    public function validate_time($str)
	{
		# dont care if empty
		if (empty($str))
			return TRUE;

		//Assume $str SHOULD be entered as HH:MM
		list($hh, $mm) = explode(':', $str);

		if (!is_numeric($hh) || !is_numeric($mm))
		{
			$mythis =& get_instance();
		    $mythis->form_validation->set_message('validate_time', 'The %s field must be in format: hh:mm. In military time.');
		    return FALSE;
		}
		else if ((int) $hh > 24 || (int) $mm > 59)
		{
			$mythis =& get_instance();
		    $mythis->form_validation->set_message('validate_time', 'The %s field must be in format: hh:mm. In military time.');
		    return FALSE;
		}
		else if (mktime((int) $hh, (int) $mm) === FALSE)
		{
			$mythis =& get_instance();
		    $mythis->form_validation->set_message('validate_time', 'The %s field must be in format: hh:mm. In military time.');
		    return FALSE;
		}

		return TRUE;
	}

	public function error_array() {
		if (count($this->_error_array) === 0) {
			return FALSE;
		}
		else
			return $this->_error_array;
	}

	public function unset_field_data() {
		unset($_POST);
		unset($this->_field_data);
	}

	public function should_select($str)
	{
		if (empty($str))
		{
			$mythis =& get_instance();
		    $mythis->form_validation->set_message('should_select', 'Please select options in %s field.');
		    return FALSE;
		}
		else
		{
			if (strcasecmp($str, 'Please select...')==0)
			{
				$mythis =& get_instance();
			    $mythis->form_validation->set_message('should_select', 'Please select options in %s field.');
			    return FALSE;
			}
			elseif (strcasecmp($str, "select...")==0)
			{
				$mythis =& get_instance();
			    $mythis->form_validation->set_message('should_select', 'Please select options in %s field.');
			    return FALSE;
			}
			elseif (strcasecmp($str, "DKY")==0)
			{
				$mythis =& get_instance();
			    $mythis->form_validation->set_message('should_select', 'Please select options in %s field.');
			    return FALSE;
			}
			else
			{
		    	return TRUE;
			}
		}
	}

}
?>