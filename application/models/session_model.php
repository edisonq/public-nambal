<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->session_logger = "session_logger";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->currentDate = date('Y-m-d H:i:s');
		$this->currentIP = $_SERVER['REMOTE_ADDR'];
	}

	public function setSession($data)
	{
		$dataUpdate = array(
			'active' => false
		);
		$this->db->where('IDsafe_user', $this->nagkamoritsing->ibalik($data['IDsafe_user']));
		$this->db->where('active', true);
		$this->db->update($this->session_logger,$dataUpdate);

		$data = array(
			'timeSet' => $this->currentDate,
			'ipAddress' => $this->nagkamoritsing->ibalik($data['sessionAddress']),
			'IDsafe_user' => $this->nagkamoritsing->ibalik($data['IDsafe_user']),
			'active' => true
		);
		$this->personalInfo = $this->db->insert($this->session_logger, $data);
	}

	public function unsetSession($data)
	{
		$dataUpdate = array(
			'active' => false
		);
		$this->db->where('IDsafe_user', $data['IDsafe_user']);
		$this->db->where('active', true);
		$this->db->update($this->session_logger,$dataUpdate);
	}

	public function checklogin($data)
	{
		$this->db->where('');
		
	}

	public function compareSessionToDatabase($ipAddress, $IDsafe_user)
	{
		// $return = FALSE;
		// $ipAddress = $this->nagkamoritsing->ibalik($ipAddress);
		// $IDsafe_user = $this->nagkamoritsing->ibalik($IDsafe_user);

		// $this->db->where('ipAddress', $ipAddress);
		// $this->db->where('IDsafe_user', $IDsafe_user);
		// $this->db->where('active',true);
		// $res = $this->db->get($this->session_logger);
		// if ($res->num_rows() >= 1)
		// {
		// 	$return = TRUE;
		// }

		// return $return;
		return TRUE;
	}

	public function checkSessionIP($ipAddress)
	{
		// $return = FALSE;
		// $ipAddress = $this->nagkamoritsing->ibalik($ipAddress);
		// if (strcmp($ipAddress, $this->currentIP)==0)
		// {
		// 	$return = TRUE;
		// }

		// return $return;
		return TRUE;
	}
}
?>