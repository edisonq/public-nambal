<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appointment_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Emruser_model', 'emrUser');
		$this->emrActualSchedule = 'emr_actual_schedule';
		$this->appointments = 'appointments';
		$this->appointmentlocation = 'appointment_location';
	}

	public function getAllAppointmentType()
	{
		$this->db->from('appointment_type');
		$this->db->where('active', TRUE);
		$this->db->where('approved', TRUE);
		return $this->db->get()->result();
	}

	public function getDoctorAppointmentType($IDappointments_settings)
	{
		$this->db->from('appointments_prioritization AS prioritization');
		$this->db->join('appointment_type AS type', 'type.IDappointment_type = prioritization.type_IDappointment_type');
		$this->db->where('appointments_settings_IDappointments_settings', $IDappointments_settings);
		return $this->db->get()->result();
	}

	public function getDoctorAppointmentsStartEnd($IDemr_user, $start, $end)
	{

	}

	public function addAppointmentType($data)
	{
		$this->db->like('typeName', $data['typename'], 'none');
		$this->db->where('active', TRUE);
		$result = $this->db->get('appointment_type')->row();

		if (empty($result))
		{
			$insertArray = array(
				'typeName' => $data['typename'],
				'description' => $data['typedescription'],
				'approved' => FALSE,
				'active' => TRUE,
				'useradded' => TRUE
			);

			$this->db->insert('appointment_type', $insertArray);
			$IDappointment_type = $this->db->insert_id();
		}
		else
		{
			$IDappointment_type = $result->IDappointment_type;
		}

		return $IDappointment_type;
	}

	public function checkIfOwnerSettings($IDappointments_settings, $IDemr_user)
	{
		$this->db->where('IDappointments_settings', $IDappointments_settings);
		$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$res = $this->db->get('appointments_settings');

		return $res->num_rows();
	}

	public function addAppointmentTypePriority($data)
	{
		$this->db->where('appointments_settings_IDappointments_settings', @$data['IDappointments_settings']);
		$this->db->delete('appointments_prioritization');

		$count = 0;
		foreach ($data['appointmenttypes'] as $appointmenttype) 
		{
			$count++;

			$dataInsert = array(
				'level' => $count,
				'type_IDappointment_type' => $appointmenttype,
				'appointments_settings_IDappointments_settings' => @$data['IDappointments_settings']
			);
			$this->db->insert('appointments_prioritization', $dataInsert);
			
		}

		return ;
	}

	public function getDoctorAppointments($IDemr_user, $retDate = NULL, $approveOnly = FALSE, $showPublicOnly = FALSE, $withPatientNambalSafe = FALSE, $includeLocation = FALSE, $start = NULL, $end = NULL)
	{
		$docAppointments = array();

		$this->db->select('
			IDappointments,
		    emr_user_IDemr_user,
		    safe_user_IDsafe_user,
		    emr_nonnambalinfo_IDemr_nonnambalinfo,
		    timefrom,
		    timeto,
		    allday,
		    appointments.active,
		    note,
		    appointments.approved,
		    forced,
		    public,
		    daterequested,
		    dateapproved, 
		    appointment_type_IDappointment_type,
		');

		if ($withPatientNambalSafe == TRUE)
		{
			$this->db->select('
			IDsafe_user,
		    username,
		    firstname,
		    middlename,
		    lastname,
		    password,
		    verified,
		    firsttime,
		    IDappointment_type,
		    typeName,
		    description');
		}

		if ($includeLocation == TRUE)
		{
			$this->db->select('
				emr_addresses_IDemr_addresses,
				location_info_IDlocation_info
			');
		}
		
		$this->db->from('appointments');
				
		$retDate = date("Y-m-d H:i:s", strtotime($retDate - 1));

		$this->db->where('emr_user_IDemr_user', $IDemr_user);

	 	// if (!empty($retDate))
	 	// {
	 	// 	$this->db->where('timefrom >=',$retDate);
	 	// }
	
	 	if ($approveOnly == TRUE)
	 	{
	 		$this->db->where('appointments.approved', TRUE);
	 	}
	 	
	 	if ($showPublicOnly == TRUE)
	 	{
	 		$this->db->where('appointments.public', TRUE);
	 	}

	 	if ($withPatientNambalSafe == TRUE)
		{
			$this->db->join('safe_user as patient','patient.IDsafe_user = appointments.safe_user_IDsafe_user');
		}

		if ($includeLocation == TRUE)
		{
			$this->db->join('appointment_location as location', 'location.appointments_IDappointments = appointments.IDappointments');
		}

		if ((!empty($start)) && (!empty($end)))
		{
			$timefrom = date('o-m-d H:i:s', strtotime($start));
			$timeto = date('o-m-d H:i:s', strtotime($end));
			$dateRange = "timeto BETWEEN '$timefrom' AND '$timeto'";
			$this->db->where($dateRange, NULL, FALSE);
		}

		$this->db->join('appointment_type as type', 'type.IDappointment_type = appointment_type_IDappointment_type');
		 	
	 	$res = $this->db->get();

	 	if($res->num_rows() >= 1)
		{
			$docAppointments = $res->result();
	 	}
		return $docAppointments;	
	}

	public function searchSlotDay($slots, $searchKey)
	{
		$array = array();
		foreach ($slots as $key => $slot) 
		{
			if (strcasecmp($slot->day, $searchKey)==0)
			{
				array_push($array, $slot);
			}
		}
		return $array;
	}

	public function setAppointment($data) 
	{
		$return = FALSE;


		// echo 'test: '.$data['IDlocation_info'];
		// echo $data['safe_user_IDsafe_user'];

		if ((!empty($data['emr_user_IDemr_user'])) && ((!empty($data['IDlocation_info']))))
		{

			# you must check if the doctor already set emr_actual_schedule and valid location
			$doc_IDsafe_user = $this->emrUser->getDocIDsafeUser($data['emr_user_IDemr_user']);
			$actualSchedule = $this->checkEASSet($doc_IDsafe_user->IDsafe_user);
			$validlocationReturn = $this->checkDoctorValidLocation($data['IDlocation_info'], $data['emr_user_IDemr_user']);
			if (($actualSchedule['error'] == false) && ($validlocationReturn['error'] == false))
            {
            	$dataInsert = array(
                    'emr_user_IDemr_user' => $data['emr_user_IDemr_user'],
                    'timefrom' => $data['timefrom'],
                    'timeto' => $data['timeto'],
                    'allday' => $data['allday'],
                    'active' => true,
                    'note' => $data['note'],
                    'approved' => false,
                    'appointment_type_IDappointment_type' => $data['appointment_type_IDappointment_type']
                );
                if (!empty($data['daterequested']))
                {
                	$dataInsert['daterequested'] = $data['daterequested'];
                }
                else
                {
                	$dataInsert['daterequested'] = date('o-m-d H:i:s');
                }
            	if (!empty($data['safe_user_IDsafe_user']))
            	{
	            	$dataInsert['safe_user_IDsafe_user'] = $data['safe_user_IDsafe_user'];
	            }
	            elseif(!empty($data['emr_nonnambalinfo_IDemr_nonnambalinfo']))
	            {
	            	$dataInsert['emr_nonnambalinfo_IDemr_nonnambalinfo'] = $data['emr_nonnambalinfo_IDemr_nonnambalinfo'];
	            }
	            
                $idReturn = $this->db->insert($this->appointments,$dataInsert);
				$IDappointments = $this->db->insert_id();

				# location
				$dataInsert = array(
					'active' => true,
					'appointments_IDappointments' => $IDappointments,
					'location_info_IDlocation_info' => $data['IDlocation_info']
				);
				if (!empty($data['emr_addresses_IDemr_addresses']))
                {
                	$dataInsert['emr_addresses_IDemr_addresses'] = $data['emr_addresses_IDemr_addresses'];
                }
				$this->db->insert($this->appointmentlocation,$dataInsert);
            }
            
		}
		else
        {
        	$dataInsert = array(
                'safe_user_IDsafe_user' => @$data['safe_user_IDsafe_user'],
                'note' => @$data['note'],
                'emr_user_IDemr_user' => $data['emr_user_IDemr_user'],
                'timefrom' => $data['timefrom'],
                'timeto' => $data['timeto'],
                'allday' => $data['allday'],
                'active' => true,
                'approved' => false,
                'appointment_type_IDappointment_type' => $data['appointment_type_IDappointment_type']
            );
        	if (!empty($data['daterequested']))
            {
            	$dataInsert['daterequested'] = $data['daterequested'];
            }
            else
            {
            	$dataInsert['daterequested'] = date('o-m-d H:i:s');
            }
            if(!empty($data['emr_nonnambalinfo_IDemr_nonnambalinfo']))
            {
            	$dataInsert['emr_nonnambalinfo_IDemr_nonnambalinfo'] = $data['emr_nonnambalinfo_IDemr_nonnambalinfo'];
            }
            $idReturn = $this->db->insert($this->appointments,$dataInsert);
            $IDappointments = $this->db->insert_id();

            # location
			$dataInsert = array(
				'active' => true,
				'appointments_IDappointments' => $IDappointments,
			);
			if (!empty($data['emr_addresses_IDemr_addresses']))
            {
            	$dataInsert['emr_addresses_IDemr_addresses'] = $data['emr_addresses_IDemr_addresses'];
            }
			$this->db->insert($this->appointmentlocation,$dataInsert);
        }
		return $return;
	}

	public function checkOverlap($IDemr_user, $timefrom, $timeto, $location)
	{
		# need to work on this too		
	}

	public function checkEASSet($IDsafe_user)
	{
		$this->db
		->select('IDemr_user, timefrom, timeto, day, averagetimeperpatient_min, appointment_limit, queue_limit, typeName, IDappointment_type')
  		->from('emr_actual_schedule')
  		->join('appointments_settings as appointSettings', 'appointSettings.IDappointments_settings = emr_actual_schedule.appointments_settings_IDappointments_settings')
  		->join('emr_user AS emruser', 'emruser.IDemr_user = appointSettings.emr_user_IDemr_user')
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user')
  		->join('appointment_type AS appointType', 'appointType.IDappointment_type = emr_actual_schedule.appointment_type_IDappointment_type');
  		$this->db->where('emr_actual_schedule.active', TRUE);
  		$this->db->where('appointType.active', TRUE);
  		$this->db->where('emruser.IDsafe_user', $IDsafe_user);

  		$theresult = $this->db->get();

  		if ($theresult->num_rows() >= 1)
  		{
  			$return = array(
  				'error' => false,
  				'theResult' => $theresult->result()
  			);
  			return $return;
  		}
  		else
  		{
  			$return = array(
  				'error' => true,
  				'theResult' => '',
  			);
  		}

  		return $return;
	}

	public function getLocationInfo($IDsafe_user)
	{
		$this->db
		->select('locationname, website, numberOfDoctors, locationtype')
		->from('location_info')
		->join('appointments_settings AS appsettings', 'appsettings.IDappointments_settings = location_info.appointments_settings_IDappointments_settings')
		->join('emr_user AS emruser', 'emruser.IDemr_user = appsettings.emr_user_IDemr_user')
		->join('location_hours AS locationhours', 'locationhours.location_info_IDlocation_info = location_info.IDlocation_info')
		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user');
		$this->db->where('safeuser.IDsafe_user', $IDsafe_user);
		$this->db->where('location_info.active', true);

		$theResult = $this->db->get()->result();

		if (!empty($theResult[0]->locationname))
		{
			$return = array(
				'error' => false,
				'theResult' => $theResult
			);
			return $return;
		}
		else
		{
			$return = array(
				'error' => true,
				'theResult' => ''
			);
		}

		return $return;
	}

	public function checkDoctorValidLocation($IDlocation_info, $IDemr_user)
	{
		$this->db
		->select('IDlocation_info, locationname')
		->from('location_info')
		->join('appointments_settings AS appointSettings', 'appointSettings.IDappointments_settings = location_info.appointments_settings_IDappointments_settings')
		->join('emr_user AS emruser', 'emruser.IDemr_user = appointSettings.emr_user_IDemr_user');
		$this->db->where('appointSettings.emr_user_IDemr_user', $IDemr_user);
		$this->db->where('location_info.IDlocation_info', $IDlocation_info);

		$theResult = $this->db->get()->row();

		if (!empty($theResult->locationname))
		{
			$return = array(
				'error' => false,
				'theResult' => $theResult
			);
			return $return;
		}
		else
		{
			$return = array(
				'error' => true,
				'theResult' => ''
			);
		}

		return $return;
	}

	public function checkScheduleIfAppointable($IDemr_user, $fullDate, $timeto = NULL)
	{
		$return = array(
			'available' => FALSE,
			'reason' => "It's not an appointable schedule",
			'appointments_settings' => array()
		);
		$getNameOfTheWeek = strtolower(date('l', strtotime($fullDate)));

		if ((!empty($IDemr_user)) && (!empty($fullDate)))
		{
			$this->db->select('timefrom, timeto, day, averagetimeperpatient_min, appointment_limit, queue_limit, allowoverlap, typeName, IDappointment_type')
			->from('appointments_settings AS settings')
			->join('emr_actual_schedule AS actual', 'actual.appointments_settings_IDappointments_settings = settings.IDappointments_settings')
			->join('appointment_type AS type', 'type.IDappointment_type = actual.appointment_type_IDappointment_type')
			->join('emr_user AS doctor', 'doctor.IDemr_user = settings.emr_user_IDemr_user')
			->join('safe_user AS user', 'user.IDsafe_user = doctor.IDsafe_user');

			$this->db->where('user.IDsafe_user', $IDemr_user);
			$this->db->where('actual.day', $getNameOfTheWeek);
			$this->db->where('appointable', TRUE);
			$this->db->where('timeto >=', $timeto);
			$this->db->where('timefrom <=', $timeto);

			$result = $this->db->get();

			if ($result->num_rows >= 1)
			{
				$result = $result->row();
				$return = array(
					'available' => TRUE,
					'reason' => '',
					'appointments_settings' => $result
				);
			}
		}

		# return false if not available
		return $return;
	}

	public function queueLimitCheck($timefrom, $timeto, $IDsafe_user, $getFullDate, $queue_limit, $averagetimeperpatient_min, $appointment_limit, $allowoverlap)
	{
		$getNameOfTheWeek = strtolower(date('l', strtotime($getFullDate)));
		if ((!empty($IDsafe_user)) && (!empty($getFullDate)) && (!empty($queue_limit)) && (!empty($averagetimeperpatient_min)) && (!empty($appointment_limit)) && (!empty($allowoverlap)))
		{
			$time = explode(' ', $getFullDate);

			# get and count all the appointments of this doctor
			$this->db->from('appointments')
			->join('emr_user AS doctor', 'doctor.IDemr_user = appointments.emr_user_IDemr_user')
			->join('safe_user AS doctorInfo', 'doctorInfo.IDsafe_user = doctor.IDsafe_user')
			->where('doctor.IDsafe_user', $IDsafe_user)
			->where('approved', TRUE);
			$dateRange = "timefrom BETWEEN '$timefrom' AND '$timeto'";
			$this->db->where($dateRange, NULL, FALSE); 

			$dateRange = "timeto BETWEEN '$timefrom' AND '$timeto'";
			$this->db->or_where($dateRange, NULL, FALSE);
			// ->where('timeto <=', $timeto)
			// ->where('timefrom >=', $timefrom);
			$appointmentCount = $this->db->count_all_results();
			$totalMinutes = round(abs(strtotime($timeto) - strtotime($timefrom)) / 60, 2);

			if ($appointmentCount >= 1)
			{
				$queueLeft = $queue_limit - $appointmentCount;
				$minutesLeft = $totalMinutes - ($averagetimeperpatient_min * $appointmentCount);
				$appointmentLeft = $appointment_limit - $appointmentCount;
			} 
			else
			{
				$queueLeft = $queue_limit - 0;
				$minutesLeft = $totalMinutes - ($averagetimeperpatient_min * 0);
				$appointmentLeft = $appointment_limit - 0;
			}
				

				# minutesLeftSQL
				$this->db
				->select('timefrom, timeto')
				->from('appointments')
				->join('emr_user AS doctor', 'doctor.IDemr_user = appointments.emr_user_IDemr_user')
				->join('safe_user AS doctorInfo', 'doctorInfo.IDsafe_user = doctor.IDsafe_user')
				->where('doctor.IDsafe_user', $IDsafe_user)
				->where('approved', TRUE);
				$dateRange = "timefrom BETWEEN '$timefrom' AND '$timeto'";
				$this->db->where($dateRange, NULL, FALSE); 

				$dateRange = "timeto BETWEEN '$timefrom' AND '$timeto'";
				$this->db->or_where($dateRange, NULL, FALSE);
				// ->where('timeto <=', $timeto)
				// ->where('timefrom >=', $timefrom);

				$appointmentTotalTime = $this->db->get()->result();
				$timeTotal_min = 0;
				foreach ($appointmentTotalTime as $key) 
				{
					$timeLoop = round(abs(strtotime($key->timeto) - strtotime($key->timefrom)) / 60, 2);
					$timeTotal_min = $timeTotal_min + $timeLoop;
				}

				$minutesLeftSQL = $totalMinutes - $timeTotal_min;
				# end of minutesLeftSQL
			
			# end getting and counting all the appointment of this doctor
		}

		$return = array(
			'timeTotal_before' => @$totalMinutes,
			'timeTotal_appointment' => @$timeTotal_min,
			'minutesLeftSQL' => @$minutesLeftSQL,
			'minutesLeft' => @$minutesLeft,
			'queueLeft' => @$queueLeft,
			'appointmentLeft' => @$appointmentLeft
		);

		return $return;
	}

	public function getTemporarySchedule($doctorIDemr_user, $IDsafe_user, $getFullDateFrom, $getFullDateTo, $doctorAppointments, $appointment_limit, $avgTimePatient)
	{
		# get doctor appointments
		# dont forget:
		# when setting temporary schedule
		# dont forget to compute the time 
		# the time should be less than or equal to the timeto(limit of the slots)
		# ALSo, don't forget to check current time especially in putting appointment in same day
		$timeIncrease = new DateTime($getFullDateFrom);
		$count = 0;
		$flagFound = false;
		$returnArray = array();

		# start fetching temporary schedule
		while($count < $appointment_limit)
		{
			$interval = 'PT'.$avgTimePatient.'M';
			$loopTimefrom = $timeIncrease->format('Y-m-d H:i:s');
			$timeIncrease->add(new DateInterval($interval));
			$loopTimeto = $timeIncrease->format('Y-m-d H:i:s');
			
			# CHECK if the schedule is not available by searching to $doctorAppointments array
			foreach ($doctorAppointments as $key => $val) 
			{

				if (($val->timefrom >= $loopTimefrom) && ($val->timefrom <= $loopTimeto))
				{
					$timeIncrease->modify($val->timeto);
					$flagFound = true;
			   		break;
			    }
			    
			    if (($val->timeto > $loopTimefrom)) 
			    {
			    	// $valCompare = explode(' ', $val->timeto);
			    	// $loopCompare = explode(' ', $loopTimefrom);
			    	// if ($valCompare[0] == $loopCompare[0])
			    	// {
			       		if (($val->timefrom <= $loopTimefrom))
						{
							$timeIncrease->modify($val->timeto);
							$flagFound = true;	
					   		break;
					    }
			       	// }
			    }
			   
			}
			# end of CHECKing if the schedule is not available by searching to $doctorAppointments array

			# also, cut the loop if over the timeto slot
			if ($loopTimeto >= $getFullDateTo)
			{
				break;
			}
			# end of cutting the loop if over the timeto slot
			$loopTimeto = $timeIncrease->format('Y-m-d H:i:s');
			if ($flagFound == false)
			{
				$dataArray['timefrom'] = $loopTimefrom;
				$dataArray['timeto'] = $loopTimeto;
				array_push($returnArray, $dataArray);
				// echo 'timefrom: '.$loopTimefrom;
				// echo 'timeto: '.$loopTimeto;
				// echo '<br>';
			}
			$flagFound = false;
			$count++;
		}
		# end of fetching possible temporary schedule.	
		return $returnArray;
	}

	public function getClinicInfoExactSchedule($IDemr_user, $dateFrom, $dateTo)
	{
		$returnArray = array();
		$getNameofWeekFrom = date('l', strtotime($dateFrom));
		$getNameofWeekTo = date('l', strtotime($dateTo));
		$timeOnlyFrom = date('H:i:s', strtotime($dateFrom));
		$timeOnlyTo = date('H:i:s', strtotime($dateTo));

		$this->db
		->select('IDlocation_info, location.locationname, website, numberofdoctors,locationtype, address.IDemr_addresses, street, state, city, country, postcode')
		->from('location_info AS location')
		->join('appointments_settings AS settings','settings.IDappointments_settings = location.appointments_settings_IDappointments_settings')
		->join('clinic_info_has_emr_addresses AS addresses', 'addresses.location_info_IDlocation_info = location.IDlocation_info')
		->join('emr_addresses AS address', 'address.IDemr_addresses = addresses.emr_addresses_IDemr_addresses')
		->join('emr_user AS doctor', 'doctor.IDemr_user = settings.emr_user_IDemr_user')
		->join('emr_actual_schedule AS schedule', 'schedule.appointments_settings_IDappointments_settings = settings.IDappointments_settings')
		->join('appointment_type AS type', 'type.IDappointment_type = schedule.appointment_type_IDappointment_type')
		->where('doctor.IDemr_user', $IDemr_user)
		->where('address.active', TRUE)
		->where('schedule.active', TRUE)
		->where('type.active', TRUE)
		->where('schedule.timefrom', $timeOnlyFrom)
		->where('schedule.timeto', $timeOnlyTo)
		->where('schedule.day', $getNameofWeekFrom)
		->where('location.active', TRUE);

		$returnArray = $this->db->get()->result();

		return $returnArray;
	}

	public function checkPatientIfAvailable($IDsafe_user, $fullDate)
	{
		$anotherDate = explode(' ', $fullDate);
		$return = FALSE;
		if ((!empty($IDsafe_user)) && (!empty($fullDate)))
		{

			$this->db
			->from('appointments')
			->where('safe_user_IDsafe_user', $IDsafe_user)
			->or_where('timefrom', $fullDate)
			->or_where('timeto', $fullDate)
			->or_where('timeto', $anotherDate[0])
			->or_where('timefrom', $anotherDate[0]);

			$ret = $this->db->get();

			if($ret->num_rows() >= 1)
			{
				$return = TRUE;
			}
		}
		# return false if not available
		return $return;
	}

	public function getSpecificAppointment($IDappointments, $select = NULL)
	{
		if (!empty($select))
		{
			$this->db->select($select);
		}
		$appointInfo = array();
		$this->db
		->from('appointments')
		->join('safe_user AS patient', 'patient.IDsafe_user = appointments.safe_user_IDsafe_user')
		->where('IDappointments', $IDappointments);

		$appointInfo = $this->db->get()->row();

		return $appointInfo;
	}

	public function approveAppointment($IDappointments)
	{
		$changes = array(
			'approved' => TRUE
		);
		$this->db->where('IDappointments',$IDappointments);
 		$this->db->update($this->appointments, $changes);
	}

	public function cancelAppointment($IDappointments)
	{
		$changes = array(
			'approved' => FALSE
		);
		$this->db->where('IDappointments',$IDappointments);
 		$this->db->update($this->appointments, $changes);
	}

	public function checkApprove($IDappointments)
	{
		$return = FALSE;
		$this->db->select('approved');
		$this->db->where('IDappointments',$IDappointments);
		$ret = $this->db->get($this->appointments);
		if ($ret->num_rows() >= 1)
		{
			$return = $ret->row()->approved;
		}
		return $return;
	}

	public function modifyAppointment($IDappointments, $timefrom, $timeto, $allday = NULL)
	{
		if ((!empty($timefrom)) && (!empty($timeto)) && (!empty($IDappointments)))
		{
			$change = array(
				'timefrom' => $timefrom,
				'timeto' => $timeto
			);

			$this->db->where('IDappointments',$IDappointments);
	 		$this->db->update($this->appointments, $change);

	 		if (!empty($allday))
	 		{
	 			$change = array(
				'allday' => $allday
				);
				
				$this->db->where('IDappointments',$IDappointments);
		 		$this->db->update($this->appointments, $change);
	 		}
	 		return TRUE;
	 	}
	 	else
	 	{
	 		return FALSE;
	 	}
	}

	public function appointmentSettings($data)
	{
		$dataInsert = array(
			'settingsname' => $data['settingsname'],
	        'averagetimeperpatient_min' => $data['averageTimePerPatient'],
	        'appointment_limit' => $data['appointmentLimit'],
	        'queue_limit' => $data['queueLimit'],
	        'allowoverlap' => (boolean)$data['allowOverlap'],
	        'emr_user_IDemr_user' => $data['IDemr_user']
		);
		$this->db->insert('appointments_settings', $dataInsert);
		$IDappointments_settings = $this->db->insert_id();

		return $IDappointments_settings;
	}

	public function updateAppointmentSettings($data)
	{
		$dataInsert = array(
			'settingsname' => $data['settingsname'],
	        'averagetimeperpatient_min' => $data['averageTimePerPatient'],
	        'appointment_limit' => $data['appointmentLimit'],
	        'queue_limit' => $data['queueLimit'],
	        'allowoverlap' => (boolean)$data['allowOverlap'],
	        'emr_user_IDemr_user' => $data['IDemr_user']
		);
		$this->db->where('IDappointments_settings',$data['IDappointments_settings']);
 		$this->db->update('appointments_settings', $dataInsert);

		return $data['IDappointments_settings'];
	}

	public function getAppointmentSettings($IDemr_user)
	{
		$this->db->from('appointments_settings');
		$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$return = $this->db->get()->result();

		return $return;
	}

	public function getAppointmentSettingsDetail($IDappointments_settings)
	{
		$this->db->from('appointments_settings');
		$this->db->where('IDappointments_settings', $IDappointments_settings);
		return $this->db->get()->row();
	}

	public function getAppointmentActualSchedules($IDappointments_settings)
	{
		$this->db->select('IDemr_actual_schedule, timefrom, timeto, day, actual.description, typeName, IDappointment_type');
		$this->db->from('emr_actual_schedule AS actual');
		$this->db->join('appointment_type AS type', 'type.IDappointment_type = actual.appointment_type_IDappointment_type');
		$this->db->where('appointments_settings_IDappointments_settings', $IDappointments_settings);

		return $this->db->get()->result();
	}

	public function deleteAppointmentSettings($IDappointments_settings)
	{
		# delete EMR actual schedule connected
		$this->db->where('appointments_settings_IDappointments_settings', $IDappointments_settings);
		$this->db->delete('emr_actual_schedule');

		# delete the appointment type connected
		$this->db->where('appointments_settings_IDappointments_settings', $IDappointments_settings);
		$this->db->delete('appointments_prioritization');

		# delete appointment setting 
		$this->db->where('IDappointments_settings', $IDappointments_settings);
		$this->db->delete('appointments_settings');
		

		return;
	}

	public function getPatientAppointments($IDsafe_user)
	{
		$return = FALSE;

		

		return $return;
	}

	public function locationAndActualTimeMatchWith($requestTime)
	{

	}
}
?>