<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recommendation_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->emruser = "emr_user";
		$this->safeuser = "safe_user";
		$this->recommend = "emr_recommendation";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->IDsafe_user = '';
		$this->IDemr_user = "";
		
	}

	public function getPublicRecommendation_doctors($IDemr_user = NULL)
	{
		$return = '';
		if (empty($IDemr_user))
		{
			if (!($this->IDemr_user))
			{
				$IDemr_user = $this->IDemr_user;
			}
		}

		# retrieve all the recommendation for this doctor
	 	$this->db
	 	->select('notes, patient.firstname, patient.middlename, patient.lastname, patient.username ')
	 	->from('emr_recommendation AS recommendation')
	 	->join ('safe_user as patient', 'recommendation.by_IDsafe_user = patient.IDsafe_user')
	 	->join ('emr_user as doctor', 'recommendation.IDemr_user = doctor.IDemr_user');
	 	$this->db->where('recommendation.IDemr_user', $IDemr_user);
		$this->db->where('recommendation.active', TRUE);
		$this->db->where('recommendation.approved', TRUE);
		$this->db->where('recommendation.recommend', TRUE);
		$res = $this->db->get();

		if($res->num_rows() >= 1)
		{
			$return = $res->result();
	 	}

		return $return;

	}

	public function recommendDoctor($patient, $IDemr_user, $notes, $recommend = NULL)
	{
		if ($recommend == null)
		{
			$recommend = TRUE;
		}
		if ((!empty($patient)) && (!empty($IDemr_user)))
		{
			$dateRecommend = array(
					'recommend' => $recommend,
					'IDemr_user' => $IDemr_user,
					'by_IDsafe_user' => $patient,
					'notes' => $notes,
					'active' => TRUE,
					'approved'=> FALSE
				);
			$this->db->insert($this->recommend,$dateRecommend);
		}
	}

}
?>