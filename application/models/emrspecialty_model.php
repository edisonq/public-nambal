<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emrspecialty_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->emrspecialty = "emr_specialties";

	}

	public function getAll()
	{
		$ret = $this->db->get($this->emrspecialty);
		if ($ret->num_rows() >= 1)
		{
			return $ret->result();
		}
		else
		{
			return '';
		}
	}
}