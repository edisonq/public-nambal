<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qr_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_qr = "safe_qr";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->safe_userID = '';
		$this->personalInfoID = '';
		$this->qrInfo = '';

		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}
	public function insertQr($filename) 
	{
		$return = FALSE;
			
		if (!empty($filename))
		{
			
		}
		return $return;
	}
	public function retrieveCurrent($IDsafe_personalInfo)
	{
		$found = FALSE;
		$this->personalInfoID = $IDsafe_personalInfo;
		$this->db->where('IDsafe_personalInfo', $this->personalInfoID);
		$this->db->where('current', TRUE);
		$qrInfo = $this->db->get($this->safe_qr);
		if($qrInfo->num_rows() == 1)
	 	{
			$this->qrInfo = $qrInfo->row();
	 		$found = TRUE;
	 	}
	 	return $found;
	}
	public function saveGenerated($filename, $IDsafe_personalInfo)
	{
		$loop = 1;
		$code = '';
		$this->personalInfoID = $IDsafe_personalInfo;
		$this->db->where('IDsafe_personalInfo', $this->personalInfoID);
		$this->db->where('current', TRUE);
		$qrInfo = $this->db->get($this->safe_qr);
		if($qrInfo->num_rows() == 1)
	 	{
			$this->loginInformation = $qrInfo->row();
	 		$found = TRUE;
	 	}

	 	# generate the textcode
	 	// xxxx-xxxx-xxxx-xxxx
	 	do{
			$code .= substr(str_shuffle("23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ"), 0, 4);
			$loop++;

			if ($loop != 5)
			{
				$code .= '-';
			}
		}while($loop <=4);
	 	# end of generating the textcode

 		$qr_modification_data = array(
			'current' => false
		);
		$this->db->where('IDsafe_personalInfo', $this->personalInfoID);
		$this->db->update($this->safe_qr,$qr_modification_data);

		$qrData = array(
			'filename' => $filename,
			'current' => true,
			'dateCreated' => $this->currentDate,
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'textcode' => $code
		);

		try
		{
			$idReturn = $this->db->insert($this->safe_qr,$qrData);
		}
		catch (Exception $e)
		{

		}
	}

	public function getCodeOnly($IDsafe_personalInfo)
	{
		$found = FALSE;
		$this->personalInfoID = $IDsafe_personalInfo;

		$this->db->where('IDsafe_personalInfo', $this->personalInfoID);
		$this->db->where('current', TRUE);
		$qrInfo = $this->db->get($this->safe_qr);
		if($qrInfo->num_rows() == 1)
	 	{
			$this->loginInformation = $qrInfo->row();
	 		$found = TRUE;
	 	}
	 	$returnArray = array ('result' => @$found, 'textcode' => @$this->loginInformation->textcode);
	 	return $returnArray;
	}

	public function inputprinter($IDsafe_user, $IDemr_user)
	{
		$insertData = array(
			'dateprinted' => $this->currentDate,
			'printed_IDsafe_user' => $IDsafe_user,
			'doctor_IDemr_user' => $IDemr_user
		);
		$this->db->insert('safe_print_card', $insertData);
		$IDsafe_print_card = $this->db->insert_id();

		return $IDsafe_print_card;
	}

	public function manualRetrieve($filename)
	{

	}
}
?>