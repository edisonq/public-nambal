<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personalprofile_model extends CI_Model 
{
	private $safe_user = '';
	private $safe_personalinfo = '';
	private $safe_racelist = '';
	private $safe_addresses = '';
	private $safe_useremails = '';
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_user = "safe_user";
		$this->safe_personalinfo = "safe_personalinfo";
		$this->safe_racelist = "safe_racelist";
		$this->safe_addresses = "safe_addresses";
		$this->safe_useremails = "safe_useremails";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function addAnotherProfile($data)
	{
		$return = false;
		$safe_personalinfo = array(
			'fullname' => $data['fullname'],
			'default' => false,
			'sex' => $data['sex'],
			'dateofbirth' => $data['dateofbirth'],
			'bloodtype' => $data['bloodtype'],
			'IDsafe_user' => $data['IDsafe_user']
		);
		$return = $this->db->insert($this->safe_personalinfo, $safe_personalinfo);

		$safe_racelist = array(
			'IDraceList' => $data[''],
			'IDsafe_personalInfo' => ''
		);
		$return = $this->db->insert($this->safe_racelist, $safe_racelist);

		$safe_address = array(
			'IDsafe_personalInfo' => '',
			'street' => '',
			'state' => '',
			'city' => '',
			'country' => '',
			'postcode' => '',
			'default' => '',
			'active' => ''
		);
		$return = $this->db->insert($this->safe_addresses, $safe_address);

		return $return;
	}
	public function checkComplete($IDsafe_user){
		$return = FALSE;
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('dateofbirth <> "0000-00-00"');
		$this->db->where('sex IS NOT NULL');
		$this->db->where('bloodtype IS NOT NULL');
        $res = $this->db->get($this->safe_personalinfo); 
		if($res->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}
	public function getIDSafePersonalInfo($IDsafe_user)
	{
		$return = 0;
		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$res = $this->db->get($this->safe_personalinfo);
	 	if($res->num_rows() >= 1)
		{
			$return = $res->row();
	 		$return = $return->IDsafe_personalInfo;
	 		
	 	}
	 	return $return;
	}

	public function getPersonalInfo($IDsafe_user)
	{
		$return = 0;
		$this->db->where('IDsafe_user', $IDsafe_user);	
	 	$res = $this->db->get($this->safe_personalinfo)->row();
	 	return $res;
	}

	public function update($data)
	{
		// safe_personalinfo
		$fullname = $data['firstname'].' '.$data['middlename'].' '.$data['lastname'];
		$birthday = date('o-m-d H:i:s', strtotime($data['birthday']));
		$updateArray = array(
			'fullname' => $this->nagkamoritsing->bungkag($fullname),
			'sex' => $data['gender'],
			'dateofbirth' => $birthday,
			'bloodtype' => $data['bloodtype'],
			'active' => TRUE
		);
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$this->db->where('IDsafe_user', $data['IDsafe_user']);
		$this->db->update('safe_personalinfo', $updateArray);

		// safe_user
		$updateArray = array(
			'firstname' => $this->nagkamoritsing->bungkag($data['firstname']),
			'middlename' => $this->nagkamoritsing->bungkag($data['middlename']),
			'lastname' => $this->nagkamoritsing->bungkag($data['lastname'])
		);
		$this->db->where('IDsafe_user', $data['IDsafe_user']);
		$this->db->update('safe_user', $updateArray);

		// safe_addresses
		$updateArray = array(
			'street' => $data['street'],
			'state' => $data['state'],
			'city' => $data['city'],
			'country' => $data['country'],
			'postcode' => $data['postcode']
		);
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$this->db->update('safe_addresses', $updateArray);
	}
}
?>