<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safeuseremail_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_useremails = "safe_useremails";
		$this->safe_user = "safe_user";
		$this->safe_useremailsInfo = '';
	}

	public function getUseremailInfo_using_IDsafe_user($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		$ret = $this->db->get($this->safe_useremails);
		$this->safe_useremailsInfo = $ret->row();
		return $this->safe_useremailsInfo;
	}
	public function getuseremaildefault($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('primary', TRUE);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->safe_useremails);
		$this->safe_useremailsInfo = $ret->row();
		return $this->safe_useremailsInfo;
	}
}
?>