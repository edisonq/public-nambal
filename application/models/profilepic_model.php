<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profilepic_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->defaultSecretKey = "GODrules";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->profilepic = "safe_profilepic";
		$this->files = "safe_files";
	}
	public function uploadprofile($data)
	{
		# if profile set change it
		$this->deactivateprofilepic($data['IDsafe_user']);
		# end of changin profile pic

		$dataInsert = array(
			'safe_user_IDsafe_user' => $data['IDsafe_user'],
			'filename' => $data['filename'],
			'type' => 'profile-pic-ec2',
			'active' => TRUE,
			'dateuploaded' => $this->currentDate,
		);
		$this->db->insert($this->files,$dataInsert);
		$dataInsert = array(
			'main' => TRUE,
			'safe_files_idsafe_files' => $this->db->insert_id(),
			'dateset' => $this->currentDate
		);
		$this->db->insert($this->profilepic,$dataInsert);
		
		return;
	}	

	public function deactivateprofilepic($IDsafe_user)
	{
		$this->db->select('idsafe_files');
		$this->db->from('safe_files AS files');
		$this->db->join('safe_profilepic AS profile', 'profile.safe_files_idsafe_files = files.idsafe_files');
		$this->db->where('safe_user_IDsafe_user', $IDsafe_user);
		$this->db->where('main', TRUE);
		$theresult = $this->db->get()->result();


		foreach ($theresult as $key) 
		{
			$dataModify = array(
				'main' => FALSE,
				'dateupdated' => $this->currentDate
			);
			$this->db->where ('safe_files_idsafe_files', $key->idsafe_files);	
			$this->db->where ('main', TRUE);
			$this->db->update($this->profilepic, $dataModify);
		}
		

		return;
	}

	public function getprofilepicinfo($IDsafe_user)
	{
		$this->db->select('filename,type')
		->from('safe_files AS files')
		->join('safe_profilepic AS profile','files.idsafe_files = profile.safe_files_idsafe_files')
		->where('files.safe_user_IDsafe_user', $IDsafe_user)
		->where('active', TRUE)
		->where('main', TRUE);

		$theresult = $this->db->get()->row();

		return  $theresult;
	}

	public function transfertos3($IDsafe_user, $filename)
	{
		$this->db->where('safe_user_IDsafe_user', $IDsafe_user);
		$this->db->where('filename', $filename);
		$this->db->where('type', 'profile-pic-ec2');
		$dataModify = array(
			'type' => 'profile-pic-s3'
		);

		$this->db->update($this->files, $dataModify);
	}

	public function checkifavail($IDsafe_user)
	{

	}
}

?>