<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->messages = "messages";
		$this->message_log = "message_log";
		$this->message_people = "message_people";
		$this->safe_user = "safe_user";
		$this->message_backup = "message_backup";
		$this->message_report_spam = "message_report_spam";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function createMessage($to, $from, $msg, $datecreated = NULL)
	{
		if (empty($datecreated))
		{
			$datecreated = $this->currentDate;
		}
		$data = array(
			'message' => $msg,
			'active' => true,
			'sent' => true,
			'nabasa' => false
		);
		$this->db->insert($this->messages, $data);


		$data = array(
			'datesent' => $this->currentDate,
			'datecreated' => $datecreated,
			'messages_from_IDmsg_from' => $this->db->insert_id()
		);
		$this->db->insert($this->message_log, $data);
	
		$data = array(
			'from_IDsafe_user' => $from,
			'to_IDsafe_user' => $to,
			'message_log_IDmessage_log' => $this->db->insert_id(),
			'hidefrom' => false,
			'hideto' => false,
			'active' => true,
			'nabasa' => false
		);
		$this->db->insert($this->message_people, $data);
	}


	public function getListSender($IDsafe_user)
	{
		# get the Messages_model
		$this->db
		->select('messagepeople.from_IDsafe_user, messagepeople.to_IDsafe_user')
  		->from('message_log AS messagelog')
  		->join('message_people as messagepeople', 'messagepeople.message_log_IDmessage_log = messagelog.IDmessage_log'	)
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = messagepeople.from_IDsafe_user')
  		->join('messages AS messages', 'messages.IDmsg_from = messagelog.messages_from_IDmsg_from');
  		$this->db->where('messages.active', TRUE);
  		$this->db->where('messages.sent', TRUE);
  		$this->db->where('messagepeople.hideto', false);
  		$this->db->where('messagepeople.active', true);
 		$this->db->where("(messagepeople.to_IDsafe_user = '$IDsafe_user' OR messagepeople.from_IDsafe_user = '$IDsafe_user')");
  		$this->db->order_by('messagelog.datesent DESC');
  		$this->db->distinct();

  		$theresult = $this->db->get()->result();


  		if (!empty($theresult))
  		{
	  		foreach ($theresult as $res) 
	  		{
	  			// array_push($returnArray, $this->nagkamoritsing->ibalik($inbox->firstname));
	  			$usersID[] = $res->from_IDsafe_user;
	  			$usersID[] = $res->to_IDsafe_user;
	  		}

	  		$usersID = array_unique($usersID);
	  		$statement = '';

	  		$arrayCount = count($usersID);
	  	
	  		
	  		# get the latest message using the $usersID
	  		
			foreach ($usersID as $userID) 
		  	{
		  		$this->db
				// ->select('messagelog.datesent, messagepeople.to_IDsafe_user, messagepeople.from_IDsafe_user')
				->select('messagelog.datesent, 
					messagepeople.to_IDsafe_user, 
					messagepeople.from_IDsafe_user, 
					safeuserto.firstname, 
					safeuserfrom.firstname as fromfirstname,
					safeuserto.lastname, 					
					safeuserfrom.lastname as fromlastname,
					messages.message, 
					messages.nabasa,
					messagepeople.nabasa AS nabasapeople')
		  		->from('message_log AS messagelog')
		  		->join('message_people as messagepeople', 'messagepeople.message_log_IDmessage_log = messagelog.IDmessage_log', 'left');

		  		$this->db->join('safe_user AS safeuserto', 'safeuserto.IDsafe_user = messagepeople.to_IDsafe_user');
		  		$this->db->join('safe_user AS safeuserfrom', 'safeuserfrom.IDsafe_user = messagepeople.from_IDsafe_user');

		  		$this->db
		  		->join('messages AS messages', 'messages.IDmsg_from = messagelog.messages_from_IDmsg_from');
		  		$this->db->where('messages.active', TRUE);
		  		$this->db->where('messages.sent', TRUE);
		  		$this->db->where('messagepeople.hideto', false);
		  		$this->db->where('messagepeople.active', true);
		  		$this->db->order_by('messagelog.datesent DESC');
		  		$this->db->distinct();

	  			$statement .= "(messagepeople.to_IDsafe_user = '$IDsafe_user' AND messagepeople.from_IDsafe_user = '$userID') OR";
	  			$statement .= "(messagepeople.to_IDsafe_user = '$userID' AND messagepeople.from_IDsafe_user = '$IDsafe_user')";
		  		
		  		$this->db->where("($statement)");
		  		$theresult = $this->db->get()->row();
		  		$arraytheresult[] = $theresult;
		  		$statement = '';

		  	}
		  	foreach ($arraytheresult as $key => $row) 
		  	{
			    $datesent[$key]  = @$row->datesent;
			    $to_IDsafe_user[$key] = @$row->to_IDsafe_user;
			    $from_IDsafe_user[$key] = @$row->from_IDsafe_user;
			}

			array_multisort($datesent, SORT_DESC, $arraytheresult);
			// print_r($arraytheresult);

	  	}
	  	else
	  	{
	  		$arraytheresult = array();
	  	}


		return $arraytheresult;
	}

	public function getmessages($from_IDsafe_user, $IDsafe_user, $limit = 20)
	{
		# count how many row return
		$statement = '';
		$this->db
		->select('messagepeople.to_IDsafe_user')
  		->from('message_log AS messagelog')
  		->join('message_people as messagepeople', 'messagepeople.message_log_IDmessage_log = messagelog.IDmessage_log', 'left')
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = messagepeople.from_IDsafe_user')
  		->join('messages AS messages', 'messages.IDmsg_from = messagelog.messages_from_IDmsg_from');
  		$this->db->where('messages.active', TRUE);
  		$this->db->where('messages.sent', TRUE);
  		$this->db->where('messagepeople.hideto', false);
  		$this->db->where('messagepeople.active', true);
		$statement .= "(messagepeople.to_IDsafe_user = '$IDsafe_user' AND messagepeople.from_IDsafe_user = '$from_IDsafe_user') OR";
		$statement .= "(messagepeople.to_IDsafe_user = '$from_IDsafe_user' AND messagepeople.from_IDsafe_user = '$IDsafe_user')";
		$this->db->where("($statement)");
  		$this->db->order_by('messagelog.datesent 	');
  		$numrows = $this->db->count_all_results();
  		if ($numrows > $limit)
  		{
  			$end = $numrows;
  			$start = $numrows-$limit;
  		}
  		else
  		{
  			$start = 0;
  			$end = $limit;
  		}
		# end of counting how many row return

		# retrieve messages with limit 20 message will be shown only. OR unread messages only.

		# end retrieving messages with limit 20 messages OR unread messages only.
		$statement = '';
		$this->db
		->select('messagepeople.to_IDsafe_user, messagepeople.from_IDsafe_user, safeuser.username, safeuser.firstname, safeuser.lastname, messages.message, messagelog.datesent, messages.nabasa')
  		->from('message_log AS messagelog')
  		->join('message_people as messagepeople', 'messagepeople.message_log_IDmessage_log = messagelog.IDmessage_log', 'left')
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = messagepeople.from_IDsafe_user')
  		->join('messages AS messages', 'messages.IDmsg_from = messagelog.messages_from_IDmsg_from');
  		$this->db->where('messages.active', TRUE);
  		$this->db->where('messages.sent', TRUE);
  		$this->db->where('messagepeople.hideto', false);
  		$this->db->where('messagepeople.active', true);
		$statement .= "(messagepeople.to_IDsafe_user = '$IDsafe_user' AND messagepeople.from_IDsafe_user = '$from_IDsafe_user') OR";
		$statement .= "(messagepeople.to_IDsafe_user = '$from_IDsafe_user' AND messagepeople.from_IDsafe_user = '$IDsafe_user')";
		$this->db->where("($statement)");
  		$this->db->limit($end,$start);
  		$this->db->order_by('messagelog.datesent');

  		$message = $this->db->get()->result();

  		$this->db
		->select('messages.IDmsg_from, messagelog.IDmessage_log')
  		->from('message_log AS messagelog')
  		->join('message_people as messagepeople', 'messagepeople.message_log_IDmessage_log = messagelog.IDmessage_log', 'left')
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = messagepeople.from_IDsafe_user')
  		->join('messages AS messages', 'messages.IDmsg_from = messagelog.messages_from_IDmsg_from');
  		$this->db->where('messages.active', TRUE);
  		$this->db->where('messages.sent', TRUE);
  		$this->db->where('messagepeople.hideto', false);
  		$this->db->where('messagepeople.active', true);
		$this->db->where('messagepeople.to_IDsafe_user', $IDsafe_user);
		$this->db->where('messagepeople.from_IDsafe_user', $from_IDsafe_user);

  		$IDmsg_from = $this->db->get()->result();

  		$dataforupdate = array(
  			'nabasa' => true
  		);

  		foreach ($IDmsg_from as $value) 
  		{
  			$this->db->where('IDmsg_from',$value->IDmsg_from);
  			$this->db->where('nabasa', false);
	 		$this->db->update($this->messages, $dataforupdate);

	 		$this->db->where('message_log_IDmessage_log', $value->IDmessage_log);
	 		$this->db->where('nabasa', false);
	 		$this->db->update($this->message_people, $dataforupdate);
  		}
  		

  		// if (!isset($message))
  		// {
  			return $message;
  		// }
  		// else
  		// {
  		// 	return 0;
  		// }
	}
	public function countUnreadMessage($to_IDsafe_user)
    {
        $this->db->select('IDmessage_people');
        $this->db->from('message_people');
        $this->db->where('to_IDsafe_user', $to_IDsafe_user);
        $this->db->where('nabasa', FALSE);
        $this->db->where('hideto', FALSE);
        $this->db->where('active', TRUE);
        return $this->db->count_all_results();
    }
	
}
?>