<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safeverification_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_useremails = "safe_useremails";
		$this->safe_user = "safe_user";
		$this->safe_verification = "safe_verification";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		
		$this->IDsafe_user = '';
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function requestNewCode_byemail($email)
	{
		$return = array(
			'errorMessage' => ''
		);

		if (!empty($email))
		{
			$found = false;
			$findThis = $this->nagkamoritsing->useForSearch($email);
		 	
		 	#get the IDsafe_user
		 	foreach ($findThis as $find) 
		 	{	 		
			 	if ($found == false)
			 	{
			 		$this->db->like('emailAddress', $find);
			 		$res = $this->db->get($this->safe_useremails);
		 		 	if($res->num_rows() >= 1)
				 	{
				 		$this->IDsafe_user = $res->row()->IDsafe_user;
				 		$found = true;
				 	}
			 	}
		 	}
			
			if (empty($this->IDsafe_user))
			{
				$return = array(
				'errorMessage' => 'Email is not existing',
				'vcode' => ''
				);
				return $return;
			}
			# end of the IDsafe_user

			# check if verified
			$this->db->where('IDsafe_user', $this->IDsafe_user);
			$this->db->where('verified', FALSE);
			$res = $this->db->get($this->safe_user);
			if ($res->num_rows() >= 1)
			{
				$safeUserInformation = $res->row();
				$this->IDsafe_user = $res->row()->IDsafe_user;
			}
			else
			{
				$return = array(
				'errorMessage' => 'Email is already verified',
				'vcode' => ''
				);
				return $return;
			}
			# end of checking if verified

			# deactivate existing verification code is active
			$verification_modification_data = array(
				'active' => false
			);
			$this->db->update($this->safe_verification,$verification_modification_data);
			# end of checkinf if the existing verification code is active

			# generate the code
			$validationCode = sha1(md5($email).'edisonguapo'.$this->currentDate);
			$userVerification = array(
				'IDsafe_user' => $this->IDsafe_user,
				'vcode' => $validationCode,
				'active' => true,
				'datetime_generated' => $this->currentDate
			);
			$this->db->insert($this->safe_verification, $userVerification);

			$return = array(
				'errorMessage' => '',
				'firstname' => $safeUserInformation->firstname,
				'lastname' => $safeUserInformation->lastname,
				'email' => $email,
				'vcode' => $validationCode
			);
			#end of generating the code		
		} 	
		return $return;
	}
	
}
?>