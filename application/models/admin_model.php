<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->requestip = $_SERVER['REMOTE_ADDR'];
	}
}