<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Passwordchecker_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function checkValidPassword($password, $confirmPassword)
	{
		$return = false;
		$strength = array("Blank","Very Weak","Weak","Medium","Strong","Very Strong");
		$score = 1;

		if (strcmp($password, $confirmPassword)==0)
		{
			if (strlen($password) < 1)
			{
				return $strength[0]; 
			}
			if (strlen($password) < 4)
			{
				return $strength[1]; 
			}

			if (strlen($password) >= 8)
			{
				$score++; 
			}
			if (strlen($password) >= 10)
			{
				$score++; 
			}

			if (preg_match("/[a-z]/", $password) && preg_match("/[A-Z]/", $password)) 
			{
				$score++; 
			}
			if (preg_match("/[0-9]/", $password)) 
			{
				$score++; 
			}
			if (preg_match("/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", $password)) 
			{
				$score++; 
			}
			$return = $strength[$score];
		}
		else
		{
			$return = 'not matched';
		}
		return $return;
	}
}