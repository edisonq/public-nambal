<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procedures_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->procedurelist = "procedurelist";
		$this->safe_procedure = "safe_procedure";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}
	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('procedurelist');
		$this->db->where('approved', TRUE);
		$this->db->where('active', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDprocedureList');
			$this->db->like('procedurename', $keyword, 'none');
		}
		else
		{
			$this->db->select('procedurename');
			$this->db->like('procedurename', $keyword, 'both');
			$this->db->like('proceduredescription', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->procedurename);
			}
		}
		else
		{
			$arrayReturn = $result->row();
			$arrayReturn = @$arrayReturn->IDprocedureList;
		}
		
		return $arrayReturn;
	}

	public function insertProcedure($IDsafe_personalInfo, $data)
	{
		// 'procedures' => @$procedures,
		// 	'datest' => @$dateexecuted,
		// 	'notes' => @$notes
		$IDprocedureList = $this->searchlist($data['procedures'], TRUE);

		if (empty($IDprocedureList))
			$IDprocedureList = 0;

		if ($IDprocedureList == 0)
		{
			$dataInsert = array(
				'proceduredescription' => $data['procedures'],
				'procedurename' => $data['procedures'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->procedurelist, $dataInsert);

			$IDprocedureList = $this->db->insert_id();			
		}

		$dataInsert = array(
			'IDprocedureList' => $IDprocedureList,
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'dateexecuted' => $data['dateexecuted'],
			'notes' => $data['notes'],
			'active' => TRUE
		);
		$this->db->insert($this->safe_procedure, $dataInsert);
		$IDsafe_procedure = $this->db->insert_id();

		return $IDsafe_procedure;
	}

	public function displayDashboard($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);
		
		$this->db->select('IDsafe_procedure, proced.IDprocedureList, dateexecuted, notes, procedurename, proceduredescription');
		$this->db->from('safe_procedure AS proced');
		$this->db->join('procedurelist AS list','list.IDprocedureList = proced.IDprocedureList');
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('proced.active', TRUE);
		$this->db->order_by('dateexecuted','DESC');
		$return = $this->db->get()->result();
		$returnArray = array();

		foreach ($return as $ret) 
		{
			$this->db->select('doctor.IDemr_user, lastname');
			$this->db->from('emr_procedure_safe AS safe');
			$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = safe.emr_user_IDemr_user');
			$this->db->join('safe_user AS doctorInfo', 'doctorInfo.IDsafe_user = doctor.IDsafe_user');
			$this->db->where('safe_procedure_IDsafe_procedure', $ret->IDsafe_procedure);
			$this->db->where('safe.active', TRUE);
			$returnDoctor = $this->db->get()->result();

			if (count($returnDoctor) != 0)
			{
				foreach ($returnDoctor as $dr) 
				{
					$rtrn = array(
						'IDsafe_procedure' => $ret->IDsafe_procedure,
						'dateexecuted' => $ret->dateexecuted,
						'procedurename' => $ret->procedurename,
						'notes' => $ret->notes,
						'proceduredescription' => $ret->proceduredescription,
						'doctor' => $dr->lastname, 
						'IDemr_user' => $dr->IDemr_user
					);
					array_push($returnArray, $rtrn);
				}
			}			
			else
			{
				$rtrn = array(
					'IDsafe_procedure' => $ret->IDsafe_procedure,
					'dateexecuted' => $ret->dateexecuted,
					'procedurename' => $ret->procedurename,
					'notes' => $ret->notes,
					'proceduredescription' => $ret->proceduredescription,
					'doctor' => '', 
					'IDemr_user' => ''
				);
				array_push($returnArray, $rtrn);
			}

			
		}
		return $returnArray;
	}

	public function getToModify($IDsafe_procedure)
	{
		$this->db->select('procedurename, IDsafe_personalInfo, dateexecuted, notes, IDemr_journal');
		$this->db->from('safe_procedure AS safep');
		$this->db->join('procedurelist AS list', 'list.IDprocedureList = safep.IDprocedureList');
		$this->db->where('IDsafe_procedure', $IDsafe_procedure);
		$row = $this->db->get()->row();

		// needed for display man gud
		$return = array(
			'IDsafe_procedure' => @$IDsafe_procedure,
			'procedure' => @$row->procedurename,
			'dateexperienced' => @$row->dateexecuted,
			'notes' => @$row->notes,
			'IDemr_journal' => @$row->IDemr_journal,
			'IDsafe_personalInfo' => @$row->IDsafe_personalInfo
		);
		return $return;
	}

	public function getToModifyB($IDsafe_procedure)
	{
		$this->db->select('procedurename, IDsafe_personalInfo, dateexecuted, safep.notes AS notes, safep.IDemr_journal, journal.notes AS doctornotes');
		$this->db->from('safe_procedure AS safep');
		$this->db->join('procedurelist AS list', 'list.IDprocedureList = safep.IDprocedureList');
		$this->db->join('emr_journal AS journal', 'journal.IDemr_journal = safep.IDemr_journal');
		$this->db->where('IDsafe_procedure', $IDsafe_procedure);
		$row = $this->db->get()->row();

		// needed for display man gud
		return $row;
	}

	public function isModified($IDsafe_procedure, $newData, $variableName = 'procedurename')
	{
		if (strcasecmp($variableName, 'procedurename')==0)
		{
			$IDprocedureList = $this->searchlist($newData, TRUE);
			if (empty($IDprocedureList))
				return TRUE;

			$this->db->from('safe_procedure');
			$this->db->where('IDsafe_procedure', $IDsafe_procedure);
			$this->db->where('IDprocedureList', $IDprocedureList);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif(strcasecmp($variableName, 'dateexecuted')==0)
		{
			$this->db->from('safe_procedure');
			$this->db->where('IDsafe_procedure', $IDsafe_procedure);
			$this->db->where('dateexecuted', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif (strcasecmp($variableName, 'notes')==0)
		{
			$this->db->from('safe_procedure');
			$this->db->where('IDsafe_procedure', $IDsafe_procedure);
			$this->db->where('notes', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		return FALSE;
	}

	public function update($data)
	{
		// 'IDsafe_procedure' => $IDsafe_procedure,
		// 'procedures' => @$procedures,
		// 'dateexecuted' => @$dateexecuted,
		// 'notes' => @$notes
		if ($this->isModified($data['IDsafe_procedure'], $data['procedures'], 'procedurename'))
		{
			$IDprocedureList = $this->searchlist($data['procedures'], TRUE);

			if (empty($IDprocedureList))
				$IDprocedureList = 0;

			if ($IDprocedureList == 0)
			{
				$dataInsert = array(
					'proceduredescription' => $data['procedures'],
					'procedurename' => $data['procedures'],
					'userAdded' => TRUE,
					'approved' => FALSE
				);
				$this->db->insert($this->procedurelist, $dataInsert);

				$IDprocedureList = $this->db->insert_id();			
			}

			$arrayUpdate['IDprocedureList'] = $IDprocedureList;
		}

		if ($this->isModified($data['IDsafe_procedure'], $data['dateexecuted'], 'dateexecuted'))
			$arrayUpdate['dateexecuted'] = $data['dateexecuted'];

		if ($this->isModified($data['IDsafe_procedure'], $data['notes'], 'notes'))
			$arrayUpdate['notes'] = $data['notes'];

		if (!empty($arrayUpdate))
		{
			$this->db->where('IDsafe_procedure', $data['IDsafe_procedure']);
			$this->db->update($this->safe_procedure, $arrayUpdate);
		}
	}

	public function deleteSafeProcedure($IDsafe_procedure)
	{
		$updateArray = array(
			'active' => FALSE
		);
		$this->db->where('IDsafe_procedure', $IDsafe_procedure);
		$this->db->update($this->safe_procedure, $updateArray);
	}
	public function record_count($IDsafe_personalInfo)
    {
		$this->db->select('IDsafe_procedure');
		$this->db->from('safe_procedure AS proced');
		$this->db->where('proced.active', TRUE);
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->order_by('dateexecuted','DESC');
		return $this->db->get()->num_rows();
    }
}