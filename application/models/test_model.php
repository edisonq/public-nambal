<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->testlist = "testlist";
		$this->safe_test = "safe_test";
		$this->safe_test_flag = "safe_test_flag";
		$this->safe_test_range = "safe_test_range";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}
	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('testlist');
		$this->db->where('approved', TRUE);
		$this->db->where('active', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDtestList');
			$this->db->like('testName', $keyword, 'none');
		}
		else
		{
			$this->db->select('testName');
			$this->db->like('testName', $keyword, 'both');
			$this->db->like('testDescription', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->testName);
			}
		}
		else
		{
			$arrayReturn = $result->row();
			$arrayReturn = @$arrayReturn->IDtestList;
		}
		
		return $arrayReturn;
	}

	public function insertSafe($testlist)
	{
		// 'testname' => $testname,
		// 'dateoftest' => $dateoftest,
		// 'flag' => $flag,
		// 'result' => $result,
		// 'units' => $units,
		// 'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']),
		// 'minimumrange' => @$minimumrange,
		// 'maximumrange' => @$maximumrange,
		// 'textrange' => @$textrange
		if (!empty($testlist['IDsafe_personalInfo']))
		{
			$IDtestList = $this->searchlist($testlist['testname'], TRUE);

			if (empty($IDtestList))
				$IDtestList = 0;

			if ($IDtestList == 0)
			{
				$dataInsert = array(
					'testdescription' => $testlist['testname'],
					'testname' => $testlist['testname'],
					'userAdded' => TRUE,
					'approved' => FALSE,
					'active' => TRUE
				);
				$this->db->insert($this->testlist, $dataInsert);

				$IDtestList = $this->db->insert_id();			
			}
			
			$insertArray = array(
				'safe_personalinfo_ID' => $testlist['IDsafe_personalInfo'],
				'IDtestList' => $IDtestList,
				'result' => @$testlist['result'],
				'units' => @$testlist['units'],
				'dateoftest' => $testlist['dateoftest'],
				'notes' => $testlist['notes'],
				'active' => TRUE
			);
			$this->db->insert($this->safe_test, $insertArray);
			$IDsafe_test = $this->db->insert_id();

			if (!empty($testlist['flag']))
			{
				$insertArray = array(
					'testflag' => $testlist['flag'],
					'safe_test_IDsafe_test' => $IDsafe_test,
					'optionaldate' => $this->currentDate,
					'active' => TRUE
				);
				$this->db->insert($this->safe_test_flag, $insertArray);
			}

			if (!empty($testlist['safe_test_range']))
			{
				foreach ($testlist['safe_test_range'] as $safe_test_range)
				{
					$insertArray = array(
						'rangemin' => $safe_test_range['minimumrange'],
						'rangemax' => $safe_test_range['maximumrange'],
						'rangetext' => $safe_test_range['rangetext'],
						'safe_test_IDsafe_test' => $IDsafe_test,
						'optionaldate' => $this->currentDate
					);

					$this->db->insert($this->safe_test_range, $insertArray);
				}
			}
		}

		return $IDsafe_test; 
	}

	public function displayDashboard($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);
		
		$this->db->select('safetest.IDsafe_test, 
			safe_test_group_IDsafe_test_groupcol, 
			result, notes, units, dateoftest, dateresult, testname, testdescription, testflag');
		$this->db->from('safe_test AS safetest');
		$this->db->join('safe_test_flag AS flagsafe', 'flagsafe.safe_test_IDsafe_test = safetest.IDsafe_test');
		$this->db->join('testlist AS testsafe','testsafe.IDtestList = safetest.IDtestList');
		$this->db->where('safe_personalinfo_ID', $IDsafe_personalInfo);
		$this->db->where('safetest.active', TRUE);
		$this->db->where('flagsafe.active', TRUE);
		$this->db->order_by('dateoftest', 'DESC');
		$safetest = $this->db->get()->result();
		$returnArray = array();

		if (!empty($safetest))
		{
			foreach ($safetest as $test) 
			{
				// Unknown column 'grouptest.emr_lab_request_IDemr_lab_request' in 'on clause'
				$this->db->select('IDsafe_test_groupcol, IDemr_lab_request, doctor.IDemr_user, lastname');
				$this->db->from('safe_test_group AS grouptest');
				$this->db->join('emr_lab_request AS requesttest', 'requesttest.IDemr_lab_request = grouptest.emr_lab_request_IDemr_lab_request');
				$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = requesttest.emr_user_IDemr_user');
				$this->db->join('safe_user AS infotest', 'infotest.IDsafe_user = doctor.IDsafe_user');
				$this->db->where('grouptest.IDsafe_test_groupcol', $test->safe_test_group_IDsafe_test_groupcol);
				$testgroup = $this->db->get()->row();


				if (!empty($testgroup))
				{
					$array = array(
						'IDsafe_test' => $test->IDsafe_test,
						'result' => $test->result,
						'unit' => $test->units,
						'dateoftest' => $test->dateoftest,
						'dateofresult' => @$test->dateresult,
						'testname' => $test->testname,
						'testdescription' => $test->testdescription,
						'flag' => $test->testflag,
						'IDsafe_test_groupcol' => $testgroup->IDsafe_test_groupcol,
						'IDemr_lab_request' => $testgroup->IDemr_lab_request,
						'doctor' => $testgroup->lastname,
						'IDemr_user' => $testgroup->IDemr_user
					);
				}
				else
				{
					$array = array(
						'IDsafe_test' => $test->IDsafe_test,
						'result' => $test->result,
						'unit' => $test->units,
						'dateoftest' => $test->dateoftest,
						'dateofresult' => @$test->dateresult,
						'testname' => $test->testname,
						'testdescription' => $test->testdescription,
						'flag' => $test->testflag,
						'IDsafe_test_groupcol' => '',
						'IDemr_lab_request' => '',
						'doctor' => '',
						'IDemr_user' => ''
					);	
				}

				if (empty($testgroup))
				{
					$this->db->select('lastname, doctor.IDsafe_user, doctor.IDemr_user');
					$this->db->from('emr_safe_test_direct AS direct');
					$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = direct.emr_user_IDemr_user');
					$this->db->join('safe_user AS name', 'name.IDsafe_user = doctor.IDsafe_user');
					$this->db->where('safe_test_IDsafe_test', $test->IDsafe_test);
					$this->db->where('direct.active', TRUE);
					$emrdirect = $this->db->get()->row();

					$array['IDemr_lab_request'] = @$emrdirect->IDemr_user;
					$array['doctor'] = @$emrdirect->lastname;
					$array['IDemr_user'] = @$emrdirect->IDemr_user;
				}
				
				array_push($returnArray, $array);
			}
		}
		return $returnArray;
	}

	public function getToModify($IDsafe_test)
	{
		$this->db->from('safe_test AS safe');
		$this->db->join('testlist AS list', 'list.IDtestList = safe.IDtestList');
		$this->db->where('IDsafe_test', $IDsafe_test);
		$safetest = $this->db->get()->row();
		$returnArray = array();

		if (!empty($safetest))
		{
			$this->db->from('safe_test_flag');
			$this->db->where('active', TRUE);
			$this->db->where('safe_test_IDsafe_test', $safetest->IDsafe_test);
			$safetestflag = $this->db->get()->result();

			$this->db->from('safe_test_range');
			$this->db->where('active', TRUE);
			$this->db->where('safe_test_IDsafe_test', $safetest->IDsafe_test);
			$safetestrange = $this->db->get()->result();

			$rangecount = count($safetestrange);

			$returnArray = array(
				'IDsafe_test' => $safetest->IDsafe_test,
				'testname' => $safetest->testname,
				'dateoftest' => $safetest->dateoftest,
				'flag' => $safetestflag,
				'result' => $safetest->result,
				'notes' => $safetest->notes,
				'units' => $safetest->units,
				'safetestrange' => $safetestrange,
				'rangecount' => $rangecount,
				'IDemr_journal' => $safetest->IDemr_journal,
				'IDsafe_personalInfo' => $safetest->safe_personalinfo_ID
			);
		}

		return $returnArray;
	}

	public function isModified($IDsafe_test, $newData, $variableName = 'testname')
	{
		if (strcasecmp($variableName, 'testname')==0)
		{
			$IDtestList = $this->searchlist($newData, TRUE);
			if (empty($IDprocedureList))
				return TRUE;

			$this->db->from('safe_test');
			$this->db->where('IDsafe_test', $IDsafe_test);
			$this->db->where('IDtestList', $IDtestList);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif(strcasecmp($variableName, 'result')==0)
		{
			$this->db->from('safe_test');
			$this->db->where('IDsafe_test', $IDsafe_test);
			$this->db->where('result', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif (strcasecmp($variableName, 'units')==0)
		{
			$this->db->from('safe_test');
			$this->db->where('IDsafe_test', $IDsafe_test);
			$this->db->where('units', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif (strcasecmp($variableName, 'dateoftest')==0)
		{
			$this->db->from('safe_test');
			$this->db->where('IDsafe_test', $IDsafe_test);
			$this->db->where('dateoftest', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		elseif (strcasecmp($variableName, 'dateresult')==0)
		{
			$this->db->from('safe_test');
			$this->db->where('IDsafe_test', $IDsafe_test);
			$this->db->where('dateresult', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}

		return FALSE;
	}

	public function updateSafe($testLister)
	{
		// 'IDsafe_test' => $IDsafe_test,
		// 'testname' => $testname,
		// 'dateoftest' => $dateoftest,
		// 'flag' => $flag,
		// 'result' => $result,
		// 'units' => $units,
		// 'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']),
		// 'safe_test_range' => @$safe_test_range,
		// echo 'IDsafe_personalInfo'.$testList['IDsafe_personalInfo'].'<br>';
		// echo 'IDsafe_test'.$testList['IDsafe_test'];
		if ((!empty($testLister['IDsafe_personalInfo'])) && (!empty($testLister['IDsafe_test'])) )
		{

			$IDtestList = $this->searchlist($testLister['testname'], TRUE);

			if (empty($IDtestList))
				$IDtestList = 0;

			if ($IDtestList == 0)
			{
				$dataInsert = array(
					'testdescription' => $testLister['testname'],
					'testname' => $testLister['testname'],
					'userAdded' => TRUE,
					'approved' => FALSE,
					'active' => TRUE
				);
				$this->db->insert($this->testlist, $dataInsert);
				$IDtestList = $this->db->insert_id();			
			}

			$updateArray = array(
				'IDtestList' => $IDtestList,
				'result' => $testLister['result'],
				'units' => $testLister['units'],
				'notes' => $testLister['notes'],
				'dateoftest' => $testLister['dateoftest']
			);
			$this->db->where('IDsafe_test', $testLister['IDsafe_test']);
			$this->db->update($this->safe_test, $updateArray);

			$updateArray = array(
				'active' => FALSE
			);
			$this->db->where('safe_test_IDsafe_test', $testLister['IDsafe_test']);
			$this->db->update($this->safe_test_flag, $updateArray);

			$updateArray = array(
				'testflag' => $testLister['flag'],
				'safe_test_IDsafe_test' => $testLister['IDsafe_test'],
				'optionaldate' => $this->currentDate,
				'active' => TRUE
			);
			$this->db->insert($this->safe_test_flag, $updateArray);

			$updateArray = array(
				'active' => FALSE
			);
			$this->db->where('safe_test_IDsafe_test', $testLister['IDsafe_test']);
			$this->db->update($this->safe_test_range, $updateArray);

			if (!empty($testLister['safe_test_range']))
			{
				foreach ($testLister['safe_test_range'] as $safe_test_range)
				{
					$insertArray = array(
						'rangemin' => $safe_test_range['minimumrange'],
						'rangemax' => $safe_test_range['maximumrange'],
						'rangetext' => $safe_test_range['rangetext'],
						'safe_test_IDsafe_test' => $testLister['IDsafe_test'],
						'optionaldate' => $this->currentDate
					);

					$this->db->insert($this->safe_test_range, $insertArray);
				}
			}
		}
	}

	public function deleteSafe($IDsafe_test)
	{


		$updateArray = array(
			'active' => FALSE
		);
		$this->db->where('IDsafe_test', $IDsafe_test);
		$this->db->update($this->safe_test, $updateArray);

		return TRUE;
	}
	public function record_count($IDsafe_personalInfo)
    {
		$this->db->select('IDsafe_test');
		$this->db->from('safe_test AS safetest');
		$this->db->where('safe_personalinfo_ID', $IDsafe_personalInfo);
		$this->db->where('safetest.active', TRUE);
		return $this->db->get()->num_rows();
    }
}