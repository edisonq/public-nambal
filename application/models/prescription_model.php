<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prescription_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->safe_drug = "safe_drug";
		$this->safe_prescription = "safe_prescription";
		$this->safe_prescription_dispense = "safe_prescription_dispense";
		$this->druglist = "druglist";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->currentTime = date('H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function prescriptionInformation($IDsafe_prescription)
	{
		$this->db->from('safe_prescription AS prescription');
		$this->db->where('IDsafe_prescription', $IDsafe_prescription);

		# i need to retrive doctor information with prescription
		$this->db->join('emr_prescription_safe AS doctorprescription','doctorprescription.safe_prescription_IDsafe_prescription = prescription.IDsafe_prescription');
		$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = doctorprescription.IDemr_user');
		$this->db->join('safe_personalinfo AS personal', 'personal.IDsafe_personalInfo = prescription.safe_personalinfo_IDsafe_personalInfo');
		$this->db->join('safe_user AS patient','patient.IDsafe_user = personal.IDsafe_user');
		$prescriptionInformation = $this->db->get()->row();

		return $prescriptionInformation;
	}

	public function updatePrescriptionInformation($data)
	{
		if (!empty($data['prescriptiondate']))
			$updateArray['prescriptiondate'] = $data['prescriptiondate'];
        
        if (!empty($data['prescriptionend']))    
        	$updateArray['prescriptionend'] = $data['prescriptionend'];
        
        if (!empty($data['dr_name']))    
        	$updateArray['dr_name'] = $data['dr_name'];
        
        if (!empty($data['dr_rx']))
        	$updateArray['dr_rx'] = $data['dr_rx'];
            
        if (!empty($data['dr_email']))
        	$updateArray['dr_email'] = $data['dr_email'];
        
        if (!empty($data['approve']))
        	$updateArray['approve'] = $data['approve'];
            
        if (!empty($data['active']))
        	$updateArray['active'] = $data['active'];
        
        if (!empty($data['safe_personalinfo_IDsafe_personalInfo']))
        	$updateArray['safe_personalinfo_IDsafe_personalInfo'] = $data['IDsafe_personalInfo'];
            
        // if (!empty((boolean)$data['includeaddress']))
        	$updateArray['includeaddress'] = (boolean)$data['includeaddress'];
        
        // if (!empty((boolean)$data['includeage']))
        	$updateArray['includeage'] = (boolean)$data['includeage'];
        
        // if (!empty((boolean)$data['includegender']))
        	$updateArray['includegender'] = (boolean)$data['includegender'];
        
        // if (!empty((boolean)$data['includedob']))
        	$updateArray['includedob'] = (boolean)$data['includedob'];
        
        // if (!empty((boolean)$data['includeexpiry']))
        	// $updateArray['includeexpiry'] = (boolean)$data['includeexpiry'];

        if (!empty($data['notes']))
        	$updateArray['notes'] = $data['notes'];

        // print_r($updateArray);
        
		$this->db->where('safe_personalinfo_IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$this->db->where('IDsafe_prescription', $data['IDsafe_prescription']);
		if (!empty($updateArray))
			$this->db->update('safe_prescription', $updateArray);
		else
			$this->db->get('safe_prescription')->row();

		return;
	}

	public function getAllPrescription($IDsafe_personalInfo)
	{
		$this->db->where('safe_personalinfo_IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->from('safe_prescription');
		$resultPrecription = $this->db->get()->result();
		return $resultPrecription;
	}

	public function getAllActivePrescription($IDsafe_personalInfo, $IDemr_user = NULL)
	{
		$this->db->from('safe_prescription AS prescription');
		$this->db->join('emr_prescription_safe AS emrpres','emrpres.safe_prescription_IDsafe_prescription = prescription.IDsafe_prescription');
		$this->db->where('safe_personalinfo_IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('prescription.active', TRUE);
		if (!empty($IDemr_user))
			$this->db->where('IDemr_user', @$IDemr_user);			
		
		$resultPrecription = $this->db->get()->result();
		return $resultPrecription;
	}

	public function createPrescription($data)
	{
		$dataInsert = array(
			'prescriptiondate' => @$data['prescriptiondate'].' '.$this->currentTime,
			'prescriptionend' => @$data['prescriptionend'].' '.$this->currentTime,
			'dr_name' => @$data['dr_name'],
			'dr_rx' => @$data['dr_rx'],
			'dr_email' => @$data['dr_email'],
			'approve' => @$data['approve'],
			'active' => TRUE,
			'safe_personalinfo_IDsafe_personalInfo' => $data['safe_personalinfo_IDsafe_personalInfo'],
			'includeaddress' => @$data['includeaddress'],
			'includeage' => @$data['includeage'],
			'includegender' => @$data['includegender'],
			'includedob' => @$data['includedob'],
			'includeexpiry' => @$data['includeexpiry']
		);
		$this->db->insert($this->safe_prescription, $dataInsert);
		$IDsafe_prescription = $this->db->insert_id();

		return $IDsafe_prescription;
	}

	public function connectPrescriptionDoctor($data)
	{
		$dataInsert = array(
			'IDemr_user' => $data['IDemr_user'],
			'safe_prescription_IDsafe_prescription' => $data['safe_prescription_IDsafe_prescription'],
			'active' => TRUE
		);
		$this->db->insert('emr_prescription_safe', $dataInsert);

		return $this->db->insert_id();
	}

	public function displayDashboardPrescription($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);

		$arrayReturn = array();
		$this->db->from('safe_prescription AS prescription');
		$this->db->where('safe_personalinfo_IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('active', TRUE);
		$return = $this->db->get()->result();

		foreach ($return as $prescription) 
		{
			# identify the prescription made by the doctors
			$this->db->select('lastname, doctor.IDemr_user, licenseNumber');
			$this->db->from('emr_prescription_safe AS prescription');
			$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = prescription.IDemr_user');
			$this->db->join('safe_user AS name', 'name.IDsafe_user = doctor.IDsafe_user');
			$this->db->where('safe_prescription_IDsafe_prescription', $prescription->IDsafe_prescription);
			$this->db->where('prescription.active', TRUE);
			$result = $this->db->get()->row();

			if (!empty($result))
			{
				$prescription->lastname = @$result->lastname;
				$prescription->IDemr_user = @$result->IDemr_user;
				$prescription->licenseNumber = @$result->licenseNumber;
			}
			else
			{
				$prescription->lastname = '';
				$prescription->IDemr_user = '';
				$prescription->licenseNumber = '';
			}
			array_push($arrayReturn, $prescription);
		}		

		return $arrayReturn;
	}
	public function record_count($IDsafe_personalInfo)
    {       
		$this->db->select('IDsafe_prescription');
		$this->db->from('safe_prescription AS prescription');
		$this->db->where('safe_personalinfo_IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('active', TRUE);
		return $this->db->get()->num_rows();
    }
}
?>