<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Immunization_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->immunizationlist = "immunizationlist";
		$this->safe_immunization = "safe_immunization";
		$this->safe_immunization_date = "safe_immunization_date";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('immunizationlist');
		$this->db->where('approved', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDimmunizationList');
			$this->db->like('immunizationName', $keyword, 'none');
		}
		else
		{
			$this->db->select('immunizationName');
			$this->db->like('immunizationName', $keyword, 'both');
			$this->db->like('immunizationDescription', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    array_push($arrayReturn, $row->immunizationName);
			}
		}
		else
		{
			$arrayReturn = $result->row();
			$arrayReturn = @$arrayReturn->IDimmunizationList;
		}
		
		return $arrayReturn;
	}

	public function insert($IDsafe_personalInfo, $immunizationList)
	{	
		$immunization_userlog = array();
		$immunization_userlog['IDimmunizationList'] = $IDimmunizationList = $this->searchlist($immunizationList['immunization'], TRUE);

		if (empty($IDimmunizationList))
			$IDimmunizationList = 0;

		if ($IDimmunizationList == 0)
		{
			$dataInsert = array(
				'immunizationName' => $immunizationList['immunization'],
				'immunizationDescription' => $immunizationList['immunization'],
				'userAdded' => TRUE,
				'approved' => FALSE,
				'active' => TRUE
			);

			$this->db->insert($this->immunizationlist, $dataInsert);
			$immunization_userlog['IDimmunizationList'] = $IDimmunizationList = $this->db->insert_id();			
		}

		$dataInsert = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'IDimmunizationList' => $IDimmunizationList,
			'dateImmuned' => $immunizationList['datereceived'],
			'boosterdate' => $immunizationList['boosterdate'],
			'notes' => $immunizationList['notes']
		);
	
		$this->db->insert($this->safe_immunization, $dataInsert);
		$immunization_userlog['IDsafe_immunization'] = $IDsafe_immunization = $this->db->insert_id();

		# add date received in immunization_date
		$immunizationDate = array(	
			'safe_immunization_IDsafe_immunization' => $IDsafe_immunization,
			'datestaken' => $immunizationList['datereceived'],
			'taken' => true
		);

		$this->db->insert($this->safe_immunization_date, $immunizationDate);
		$immunization_userlog['IDsafe_immunization_dateImmuned'] = $this->db->insert_id();

		# add booster date in immunization_date
		if(!empty($immunizationList['boosterdate']) || $immunizationList['boosterdate'] != NULL)
		{
			$immunizationDate = array(	
				'safe_immunization_IDsafe_immunization' => $IDsafe_immunization,
				'datestaken' => $immunizationList['boosterdate'],
				'taken' => false
			);

			$this->db->insert($this->safe_immunization_date, $immunizationDate);
			$immunization_userlog['IDsafe_immunization_booster'] = $this->db->insert_id();
			$immunization_userlog['boosterdate'] = $immunizationList['boosterdate'];
		}

		# array data is set by this, because there will be instances that boosterdate's informations are NULL
		$immunization_userlog['immunizationName'] = $immunizationList['immunization'];
		$immunization_userlog['dateImmuned'] = $immunizationList['datereceived'];
		$immunization_userlog['notes'] = $immunizationList['notes'];

		return $immunization_userlog;
	}

	public function dashboardDisplay($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);

		$this->db->select('IDsafe_immunization, immunizationName, immunizationDescription, dateImmuned, notes, boosterdate');
		$this->db->from('safe_immunization AS safe');
		$this->db->join('immunizationlist AS list','list.IDimmunizationList = safe.IDimmunizationList');
		$this->db->where('safe.active', TRUE);
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->order_by('dateImmuned','DESC');
		$safeimmuness = $this->db->get()->result();

		$returnArray = array();

		foreach ($safeimmuness as $immune) 
		{

			$this->db->select('doctor.IDemr_user, info.lastname, IDemr_immunization_safe');
			$this->db->from('emr_immunization_safe AS safe');
			$this->db->join('emr_user AS doctor','safe.emr_user_IDemr_user = doctor.IDemr_user');
			$this->db->join('safe_user AS info','info.IDsafe_user = doctor.IDsafe_user');
			$this->db->where('safe_immunization_IDsafe_immunization', $immune->IDsafe_immunization);
			$this->db->where('safe.active', TRUE);
			$IDemruser = $this->db->get()->result();

			if (!empty($IDemruser))
			{
				foreach ($IDemruser as $doctor) 
				{
					$arrayToPush = array(
						'IDsafe_immunization' => $immune->IDsafe_immunization,
						'immunizationName' => $immune->immunizationName, 
						'immunizationDescription' => $immune->immunizationDescription, 
						'dateImmuned' =>$immune->dateImmuned, 
						'notes' => $immune->notes,
						'IDemr_user' => $doctor->IDemr_user,
						'doctor' => $doctor->lastname,
						'datebooster' => $immune->boosterdate,
						'IDemr_immunization_safe' => $doctor->IDemr_immunization_safe
					);
					array_push($returnArray, $arrayToPush);
				}
			}
			else
			{
				$arrayToPush = array(
						'IDsafe_immunization' => $immune->IDsafe_immunization,
						'immunizationName' => $immune->immunizationName, 
						'immunizationDescription' => $immune->immunizationDescription, 
						'dateImmuned' =>$immune->dateImmuned, 
						'notes' => $immune->notes,
						'IDemr_user' => '',
						'doctor' => '',
						'datebooster' => $immune->boosterdate,
						'IDemr_immunization_safe' => ''
					);
				array_push($returnArray, $arrayToPush);
			}
		}

		return $returnArray;
	}

	public function check_overdueBoosterdate($sessionid)
	{
		$this->db->select('boosterdate');
		$this->db->from($this->safe_immunization);
		$this->db->where('IDsafe_personalInfo', $sessionid);
		$this->db->where('active', true);
		return $this->db->get()->result();
	}

	private function getSafeImmunizationDate($immunization = array(), $isDateReceived = null)
	{
		$this->db->from($this->safe_immunization_date);
		$this->db->where('safe_immunization_IDsafe_immunization', $immunization['IDsafe_immunization']);
		$this->db->where('datestaken', $immunization['date']);

		if($isDateReceived)
		{
			$this->db->where('taken', true);
		}
		else if(!$isDateReceived)
		{
			$this->db->where('taken', false);
		}

		return $this->db->get()->row();
	}

	public function getToModify($IDsafe_immunization)
	{
		$this->db->select('IDsafe_immunization, immunizationName, immunizationDescription, dateImmuned, boosterdate, notes');
		$this->db->from('safe_immunization AS safe');
		$this->db->join('immunizationlist AS list', 'list.IDimmunizationList = safe.IDimmunizationList');
		$this->db->where('IDsafe_immunization', $IDsafe_immunization);
		return $this->db->get()->row();
	}

	public function isModified($IDsafe_immunization, $newData, $variableName = 'dateImmuned', $isDateReceived = null)
	{
		if (strcasecmp($variableName, 'immunizationName')==0)
		{
			$IDimmunizationList = $this->searchlist($newData, TRUE);
			if (empty($IDimmunizationList))
				return TRUE;

			$this->db->from('safe_immunization');
			$this->db->where('IDsafe_immunization', $IDsafe_immunization);
			$this->db->where('IDimmunizationList', $IDimmunizationList);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		else if (strcasecmp($variableName, 'dateImmuned')==0)
		{
			$this->db->from('safe_immunization');
			$this->db->where('IDsafe_immunization', $IDsafe_immunization);
			$this->db->where('dateImmuned', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		else if (strcasecmp($variableName, 'boosterdate')==0)
		{
			$this->db->from('safe_immunization');
			$this->db->where('IDsafe_immunization', $IDsafe_immunization);
			$this->db->where('boosterdate', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		else if (strcasecmp($variableName, 'datestaken')==0)
		{
			# check if date already exist regardless if it is date received or booster date
			$this->db->from('safe_immunization_date');
			$this->db->where('safe_immunization_IDsafe_immunization', $IDsafe_immunization);
			$this->db->where('datestaken', $newData);

			if($isDateReceived)
			{
				$this->db->where('taken', true);
				$this->db->where('active', false);
			}
			else if(!$isDateReceived)
			{
				$this->db->where('taken', false);
				$this->db->where('active', false);
			}

			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
		else if (strcasecmp($variableName, 'notes')==0)
		{
			$this->db->from('safe_immunization');
			$this->db->where('IDsafe_immunization', $IDsafe_immunization);
			$this->db->where('notes', $newData);
			if ($this->db->count_all_results() < 1)
				return TRUE;
		}
	}

	public function update($immunizationList = array())
	{	
		# get the original immunization informations
		$originalImmunizationInfomation = $this->getToModify($immunizationList['IDsafe_immunization']);

		# immunization
		if ($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['immunization'], 'immunizationName'))
		{
			$IDimmunizationList = $this->searchlist($immunizationList['immunization'], TRUE);

			if (empty($IDimmunizationList))
				$IDimmunizationList = 0;

			if ($IDimmunizationList == 0)
			{
				$dataInsert = array(
					'immunizationName' => $immunizationList['immunization'],
					'immunizationDescription' => $immunizationList['immunization'],
					'userAdded' => TRUE,
					'approved' => FALSE
				);
				$this->db->insert($this->immunizationlist, $dataInsert);

				$IDimmunizationList = $this->db->insert_id();			
			}

			$arrayUpdate['IDimmunizationList'] = $IDimmunizationList;
		}


		# date immuned or date received
		if ($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['datereceived'], 'dateImmuned'))
		{
			$arrayUpdate['dateImmuned'] = $immunizationList['datereceived'];

			$originalDateInformation = array(
				'IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
				'date' => $originalImmunizationInfomation->dateImmuned // original date immuned data
			); 

			$originalDateInfo = $this->getSafeImmunizationDate($originalDateInformation, true);

			# update / deactivate date received 
			$this->db->where('IDsafe_immunization_booster', $originalDateInfo->IDsafe_immunization_booster);
			$this->db->update($this->safe_immunization_date, array('active' => false));

			if($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['datereceived'], 'datestaken', true))
			{
				# if date does not exists
				# add new date received 
				$immunizationDate = array(	
					'safe_immunization_IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
					'datestaken' => $immunizationList['datereceived'],
					'taken' => true
				);

				$this->db->insert($this->safe_immunization_date, $immunizationDate);
			}
			else
			{
				$originalDateInformation = array(
					'IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
					'date' => $immunizationList['datereceived']
				); 

				$originalDateInfo = $this->getSafeImmunizationDate($originalDateInformation, true);

				# activate date if date is a deactivated date 
				$this->db->where('IDsafe_immunization_booster', $originalDateInfo->IDsafe_immunization_booster);
				$this->db->update($this->safe_immunization_date, array('active' => true));
			}
		}

		# get original booster date data
		if(!empty($originalImmunizationInfomation->boosterdate))
		{
			$originalDateInformation = array(
				'IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
				'date' => $originalImmunizationInfomation->boosterdate // original booster date data
			); 

			$originalDateInfo = $this->getSafeImmunizationDate($originalDateInformation, false);
		}
		else
		{
			$originalDateInfo = array();
		}

			# start update booster date
			if(!empty($immunizationList['boosterdate']))
			{
				if ($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['boosterdate'], 'boosterdate'))
				{
					$arrayUpdate['boosterdate'] = $immunizationList['boosterdate'];

					if(!empty($originalDateInfo))
					{
						# update / deactivate booster date
						$this->db->where('IDsafe_immunization_booster', $originalDateInfo->IDsafe_immunization_booster);
						$this->db->update($this->safe_immunization_date, array('active' => false));
					}

					if($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['boosterdate'], 'datestaken', false))
					{
						# if date does not exists
						# add new booster date 
						$immunizationDate = array(	
							'safe_immunization_IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
							'datestaken' => $immunizationList['boosterdate'],
							'taken' => false
						);

						$this->db->insert($this->safe_immunization_date, $immunizationDate);
					}
					else
					{
						$originalDateInformation = array(
							'IDsafe_immunization' => $immunizationList['IDsafe_immunization'],
							'date' => $immunizationList['boosterdate']
						); 

						$originalDateInfo = $this->getSafeImmunizationDate($originalDateInformation, false);

						# activate date if date is a deactivated date 
						$this->db->where('IDsafe_immunization_booster', $originalDateInfo->IDsafe_immunization_booster);
						$this->db->update($this->safe_immunization_date, array('active' => true));
					}
				}
			}
			else
			{
				# deactivate orignal booster date
				if(!empty($originalDateInfo))
				{
					$arrayUpdate['boosterdate'] = NULL;

					$this->db->where('IDsafe_immunization_booster', $originalDateInfo->IDsafe_immunization_booster);
					$this->db->update($this->safe_immunization_date, array('active' => false));
				}
			}


		# notes
		if ($this->isModified($immunizationList['IDsafe_immunization'], $immunizationList['notes'], 'notes'))
			$arrayUpdate['notes'] = $immunizationList['notes'];

		# update safe_immunization table
		if (!empty($arrayUpdate)) 
		{
			$this->db->where('IDsafe_immunization', $immunizationList['IDsafe_immunization']);
			$this->db->update($this->safe_immunization, $arrayUpdate);
		}
	}

	public function deleteSafe($IDsafe_immunization)
	{
		$updateArray = array(
			'active' => FALSE
		);
		$this->db->where('IDsafe_immunization', $IDsafe_immunization);
		$this->db->update($this->safe_immunization, $updateArray);
	}
	public function record_count($IDsafe_personalInfo)
    {
		$this->db->select('IDsafe_immunization');
		$this->db->from('safe_immunization AS safe');
		$this->db->where('safe.active', TRUE);
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		return $this->db->get()->num_rows();
    }
}

?>