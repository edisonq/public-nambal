<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Allergy_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->safe_allergen = "safe_allergen";
		$this->allergenlist = "allergenlist";
		$this->druglist = "druglist";
		$this->problemlist = "problemlist";
		$this->safe_allergen_reaction = "safe_allergen_reaction";
		$this->safe_allergen_treatment = "safe_allergen_treatment";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('session');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Problem_model','problem');
		$this->load->model('Medication_model', 'medication');
		$this->load->model('emruser_model', 'emruser');		
		$this->nambal_session = $this->session->userdata('logged_in');
	}

	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('allergenlist');
		$this->db->where('approved', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDallergenList');
			$this->db->like('allergenName', $keyword, 'none');
		}
		else
		{
			$this->db->select('allergenName');
			$this->db->like('allergenName', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->allergenName);
			}
		}
		else
		{
			$arrayReturn = $result->row();
			$arrayReturn = @$arrayReturn->IDallergenList;
		}
		
		return $arrayReturn;
	}

	public function insertAllergen($data)
	{
		// 'allergen' => $allergen,
		// 'dateexperienced' => $dateexperienced,
		// 'serverity' => $serverity,
		// 'notes' => @$notes,
		// 'treatmentArray' => @$treatment,
		// 'IDsafe_personalInfo' => @$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);
		// 'reactionArray' => @$reaction
		$IDallergenList = $this->searchlist($data['allergen'], TRUE);

		if (empty($IDallergenList))
			$IDallergenList = 0;

		if ($IDallergenList == 0)
		{
			$dataInsert = array(
				'allergenDescription' => $data['allergen'],
				'allergenName' => $data['allergen'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->allergenlist, $dataInsert);

			$IDallergenList = $this->db->insert_id();		
		}

		$dataInsert = array(
			'IDallergenList' => $IDallergenList,
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'dateexperienced' => $data['dateexperienced'],
			'severity' => $data['severity'],
			'notes' => $data['notes']
		);
		$this->db->insert($this->safe_allergen, $dataInsert);
		$IDsafe_allergen = $this->db->insert_id();

		if (!empty($data['treatmentArray']))
		{
			foreach ($data['treatmentArray'] as $treatment) 
			{
				$IDsafe_allergen_treatment = $this->medication->searchlist($treatment, TRUE);

				if (empty($IDsafe_allergen_treatment))
					$IDsafe_allergen_treatment = 0;

				if ($IDsafe_allergen_treatment == 0)
				{
					$dataInsert = array(
						'manufacturer' => $treatment,
						'brandName' => $treatment,
						'genericName' => $treatment,
						'userAdded' => TRUE,
						'approved' => FALSE
					);
					$this->db->insert($this->druglist, $dataInsert);

					$IDsafe_allergen_treatment = $this->db->insert_id();			
				}

				$dataInsert = array(
					'druglist_IDdrugList' => $IDsafe_allergen_treatment,
					'safe_allergen_IDsafe_allergen' => $IDsafe_allergen
				);

				$this->db->insert($this->safe_allergen_treatment, $dataInsert);
			}
		}

		if (!empty($data['reactionArray']))
		{
			foreach ($data['reactionArray'] as $reaction) 
			{
				$IDsafe_allergen_reaction = $this->problem->searchlist($reaction, TRUE);

				if (empty($IDsafe_allergen_reaction))
					$IDsafe_allergen_reaction = 0;

				if ($IDsafe_allergen_reaction == 0)
				{
					$dataInsert = array(
						'problemDescription' => $reaction,
						'problemName' => $reaction,
						'userAdded' => TRUE,
						'approved' => FALSE
					);
					$this->db->insert($this->problemlist, $dataInsert);

					$IDsafe_allergen_reaction = $this->db->insert_id();			
				}

				$dataInsert = array(
					'problemlist_IDproblemList' => $IDsafe_allergen_reaction,
					'safe_allergen_IDsafe_allergen' => $IDsafe_allergen
				);

				$this->db->insert($this->safe_allergen_reaction, $dataInsert);
			}
		}

		return $IDsafe_allergen;
	}

	public function displayDashboardAllergy($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);
		
		$allergenArray = array();
		$this->db->select('allergy.IDsafe_allergen, list.allergenName, dateexperienced, severity, notes');
		$this->db->from('safe_allergen AS allergy');
		$this->db->join('allergenlist AS list','list.IDallergenList = allergy.IDallergenList');
		$this->db->where('allergy.active', TRUE);
		$this->db->where('allergy.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->order_by("dateexperienced", "desc");
		$return = $this->db->get()->result();

		foreach ($return as $ret) 
		{
			$this->db->select('doctorinfo.lastname, doctor.IDemr_user');
			$this->db->from('emr_allergen_safe AS allergy');
			$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = allergy.IDemr_user');
			$this->db->join('safe_user AS doctorinfo','doctorinfo.IDsafe_user = doctor.IDsafe_user');
			$this->db->where('IDsafe_allergen', $ret->IDsafe_allergen);
			$result = $this->db->get()->row();

			if (!empty($result->lastname))
			{
				$retArray = array(
					'IDsafe_allergen' => $ret->IDsafe_allergen,
					'allergenName' => $ret->allergenName,
					'dateexperienced' => $ret->dateexperienced,
					'severity' => $ret->severity,
					'notes' => $ret->notes,
					'lastname' => $result->lastname,
					'IDemr_user' => $result->IDemr_user
				);
			}
			else
			{
				$retArray = array(
					'IDsafe_allergen' => $ret->IDsafe_allergen,
					'allergenName' => $ret->allergenName,
					'dateexperienced' => $ret->dateexperienced,
					'severity' => $ret->severity,
					'notes' => $ret->notes,
					'lastname' => ''
				);
			}
			array_push($allergenArray, $retArray);
		}
		

		return $allergenArray;
	}

	public function getToModify($IDsafe_allergen)
	{
		$reactionArray = array();
		$treatmentArray = array();
		$countreaction = 0;
		$counttreatment = 0;
		$this->db->select('allergenName, dateexperienced, severity, notes, IDemr_journal, IDsafe_personalInfo');
		$this->db->from('safe_allergen AS allergy');
		$this->db->join('allergenlist AS list','allergy.IDallergenList = list.IDallergenList');
		$this->db->where('allergy.IDsafe_allergen', $IDsafe_allergen);
		$this->db->where('allergy.active', TRUE);
		$safe_allergen = $this->db->get()->row();


		if (!empty($safe_allergen))
		{
			$this->db->select('problemName');
			$this->db->from('safe_allergen_reaction AS reaction');
			$this->db->join('problemlist AS list','list.IDproblemList = reaction.problemlist_IDproblemList');
			$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
			$reactions = $this->db->get()->result();


			if (!empty($reactions))
			{
				foreach ($reactions as $react) 
				{
					array_push($reactionArray, $react->problemName);
					$countreaction++;
				}
			}

			$this->db->select('brandName');
			$this->db->from('safe_allergen_treatment AS treatment');
			$this->db->join('druglist AS list', 'list.IDdrugList = treatment.druglist_IDdrugList');
			$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
			$treatments = $this->db->get()->result();

			if (!empty($treatments))
			{
				foreach ($treatments as $treat) 
				{
					array_push($treatmentArray, $treat->brandName);
					$counttreatment++;
				}
			}
		}

		$safeallergen = array(
			'IDsafe_allergen' => $IDsafe_allergen,
			'allergenName' => @$safe_allergen->allergenName,
			'dateexperienced' => @$safe_allergen->dateexperienced,
			'severity' => (int)@$safe_allergen->severity,
			'notes' => @$safe_allergen->notes,
			'reactions' => @$reactionArray,
			'treatments' => @$treatmentArray,
			'countreaction' => (int)@$countreaction,
			'IDemr_journal' => @$safe_allergen->IDemr_journal,
			'counttreatment' => (int)@$counttreatment,
			'IDsafe_personalInfo' => (int)@$safe_allergen->IDsafe_personalInfo
		);

		return $safeallergen;
	}

	public function isReactionChanged($IDsafe_allergen, $reactionArray)
	{
		$this->db->from('safe_allergen_reaction');
		$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
		if ($this->db->count_all_results() != count($reactionArray))
			return TRUE;

		foreach ($reactionArray as $react) 
		{
			$IDproblemList = $this->problem->searchlist($react, TRUE);
			if (empty($IDproblemList))
				return TRUE;

			$this->db->from('safe_allergen_reaction');
			$this->db->where('problemlist_IDproblemList', $IDproblemList);
			$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
			if ($this->db->count_all_results() == 0)
			{
				return TRUE;
			}
		}
		
		return FALSE;
	}

	public function isTreatmentChanged($IDsafe_allergen, $treatmentArray)
	{
		$this->db->from('safe_allergen_treatment');
		$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
		if ($this->db->count_all_results() != count($treatmentArray))
			return TRUE;
		
		foreach ($treatmentArray as $treat) 
		{
			$IDdrugList = $this->medication->searchlist($treat, TRUE);
			if (empty($IDdrugList))
				return TRUE;

			$this->db->from('safe_allergen_treatment');
			$this->db->where('druglist_IDdrugList', $IDdrugList);
			$this->db->where('safe_allergen_IDsafe_allergen', $IDsafe_allergen);
			if ($this->db->count_all_results() == 0)
			{
				return TRUE;
			}
		}
		
		return FALSE;
	}

	public function updateSafeAllergen($data)
	{
		if ($this->isReactionChanged($data['IDsafe_allergen'], $data['reactionArray']) == TRUE)
		{
			$this->db->where('safe_allergen_IDsafe_allergen', $data['IDsafe_allergen']);
			$this->db->delete($this->safe_allergen_reaction);
			foreach ($data['reactionArray'] as $react) 
			{
				$IDproblemList = $this->problem->searchlist($react, TRUE);
				if (empty($IDproblemList))
				$IDproblemList = 0;

				if ($IDproblemList == 0)
				{
					$dataInsert = array(
						'problemDescription' => $react,
						'problemName' => $react,
						'userAdded' => TRUE,
						'approved' => FALSE
					);
					$this->db->insert($this->problemlist, $dataInsert);

					$IDproblemList = $this->db->insert_id();			
				}

				$dataInsert = array(
					'problemlist_IDproblemList' => $IDproblemList,
					'safe_allergen_IDsafe_allergen' =>  $data['IDsafe_allergen']
				);
				$this->db->insert($this->safe_allergen_reaction, $dataInsert);
			}
		}
		if ($this->isTreatmentChanged($data['IDsafe_allergen'], $data['treatmentArray']) == TRUE)
		{
			$this->db->where('safe_allergen_IDsafe_allergen', $data['IDsafe_allergen']);
			$this->db->delete($this->safe_allergen_treatment);
			foreach ($data['treatmentArray'] as $treat) 
			{
				$IDdrugList = $this->medication->searchlist($treat, TRUE);

				if (empty($IDdrugList))
					$IDdrugList = 0;

				if ($IDdrugList == 0)
				{
					$dataInsert = array(
						'manufacturer' => $treat,
						'brandName' => $treat,
						'genericName' => $treat,
						'userAdded' => TRUE,
						'approved' => FALSE
					);
					$this->db->insert($this->druglist, $dataInsert);

					$IDdrugList = $this->db->insert_id();			
				}

				$dataInsert = array(
					'druglist_IDdrugList' => $IDdrugList,
					'safe_allergen_IDsafe_allergen' => $data['IDsafe_allergen']
				);
				$this->db->insert($this->safe_allergen_treatment, $dataInsert);
			}
		}

		$IDallergenList = $this->searchlist($data['allergen'], TRUE);

		if (empty($IDallergenList))
			$IDallergenList = 0;

		if ($IDallergenList == 0)
		{
			$dataInsert = array(
				'allergenDescription' => $data['allergen'],
				'allergenName' => $data['allergen'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->allergenlist, $dataInsert);

			$IDallergenList = $this->db->insert_id();		
		}

		$dataUpdate = array(
			'IDallergenList' => $IDallergenList,
			'dateexperienced' => $data['dateexperienced'],
			'severity' => $data['severity'],
			'notes' => $data['notes']
		);
		$this->db->where('IDsafe_allergen', $data['IDsafe_allergen']);
		$this->db->update($this->safe_allergen, $dataUpdate);
	}

	public function deleteSafeAllergy($IDsafe_allergen, $deleter_IDsafe_user = NULL)
	{
		// $deleteArray = array(
		// 	'active' => FALSE
		// );
		// $this->db->where('IDsafe_allergen', $IDsafe_allergen);
		// $this->db->update($this->safe_allergen, $deleteArray);

		$error = FALSE;
		if (empty($deleter_IDsafe_user))
		{
			# this will help us prevent deletion from other user
			$deleter_IDsafe_user = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']);
			
		}
		if (!$this->checkIfAuthorized($deleter_IDsafe_user, $IDsafe_allergen))
			$error = TRUE;
		// return $this->checkIfAuthorized($deleter_IDsafe_user, $IDsafe_allergen);
		// return $IDsafe_allergen;

		if ($error == FALSE)
		{
			$deleteArray = array(
				'active' => FALSE
			);
			$this->db->where('IDsafe_allergen', $IDsafe_allergen);
			$this->db->update($this->safe_allergen, $deleteArray);

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function checkIfAuthorized($IDsafe_user, $IDsafe_allergen = NULL)
	{
		# check if owner of the problem
		if (!empty($IDsafe_allergen))
		{
			$this->db->select('personal.IDsafe_personalInfo');
			$this->db->from('safe_personalinfo AS personal');
			$this->db->join('safe_allergen AS allergy', 'allergy.IDsafe_personalInfo = personal.IDsafe_personalInfo');
			$this->db->join('safe_user AS user', 'user.IDsafe_user = personal.IDsafe_user');
			$this->db->where('user.IDsafe_user', $IDsafe_user);
			$this->db->where('allergy.IDsafe_allergen', $IDsafe_allergen);
			$return = $this->db->get('safe_problem')->row();

			if (empty($return->IDsafe_personalInfo))
				$error = TRUE;
			else
				$error = FALSE;
		}


		if ($error == TRUE)
		{	

			# if not try to check if doctor
			if ($this->emruser->checkIfDoctor($IDsafe_user))
				$error = FALSE;
			else
				$error = TRUE;

			# butangan pa unta kung iyaha ba ni input pero nag kuwang og oras
			# need to balik this area
		}

		if ($error == FALSE)
			return TRUE;
		else
			return FALSE;
	}
	public function record_count($IDsafe_personalInfo)
    {
		$this->db->select('IDsafe_allergen');
		$this->db->from('safe_allergen AS allergy');
		$this->db->where('allergy.active', TRUE);
		$this->db->where('allergy.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->order_by("dateexperienced", "desc");
		return $this->db->get()->num_rows();
    }
}
?>