<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pusher_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->library('pusher');
		$this->channel = '';
		$this->channel2 = '';
		$this->datesent = date("M d, Y (h:i A e)");
		$this->array = array();
		$this->notify_user = 'notify_user';
	}

	public function set_channel($toIDsafeUser)
	{
		$this->channel = sha1($toIDsafeUser.'GOD=>gugma=>nambal!23');
	}
	public function set_channel2($fromIDsafeUser)
	{
		$this->channel2 = sha1($fromIDsafeUser.'GOD=>gugma=>nambal!23');
	}

	public function sendChat($message, $fromIDsafeUser, $toIDsafeUser,  $messageid, $event = 'notify_user', $channel = NULL, $channel2 = NULL, $datesent = NULL)
	{
		if (empty($channel))
		{
			$this->set_channel($toIDsafeUser);
			$channel = $this->channel;
		}
		if (empty($channel2))
		{
			$this->set_channel2($fromIDsafeUser);
			$channel2 = $this->channel2;
		}
		
		if (empty($datesent))
		{
			$datesent = $this->datesent;
		}
		$this->pusher->trigger($channel, $event, array('message' => $message,'noti'=> $fromIDsafeUser, 'fy' => '', 'datesent' => $datesent));
        $this->pusher->trigger($channel2, $event, array('message' => $message,'noti'=> $fromIDsafeUser, 'fy' => $toIDsafeUser, 'datesent' => $datesent, 'messageid' => $messageid));

        return 0;
	}

	public function setAppointment($toIDsafeUser, $event = 'notify_user')
	{
		if (empty($channel))
		{
			$this->set_channel($toIDsafeUser);
			$channel = $this->channel;
		}
		$this->pusher->trigger($channel, $event, array('appointment' => 'appointmentReceived','message' => '','noti'=> $toIDsafeUser, 'fy' => '', 'datesent' => $this->datesent, 'messageid' => ''));
		return 0;
	}

	public function appointmentApproveByDoctor($toIDsafeUser, $event = 'notify_user')
	{
		if (empty($channel))
		{
			$this->set_channel($toIDsafeUser);
			$channel = $this->channel;
		}
		$this->pusher->trigger($channel, $event, array('appointment' => 'approvedByDoctor','message' => '','noti'=> $toIDsafeUser, 'fy' => '', 'datesent' => $this->datesent, 'messageid' => ''));
		return 0;
	}

	public function cancelAppointment($toIDsafeUser, $event = 'notify_user')
	{
		if (empty($channel))
		{
			$this->set_channel($toIDsafeUser);
			$channel = $this->channel;
		}
		$this->pusher->trigger($channel, $event, array('appointment' => 'appointmentCanceled','message' => '','noti'=> $toIDsafeUser, 'fy' => '', 'datesent' => $this->datesent, 'messageid' => ''));
		return 0;
	}

	
}
?>