<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patientsearch_model extends CI_Model 
{
	private $safe_user =  '';
	private $safe_personalinfo = '';
	private $safe_racelist = '';
	private $safe_addresses = '';
	private $safe_useremails = '';
	private $emr_user = '';
	private $emr_patient = '';
	private $nambal_session = array();
	private $verfiedAccountsId = array();
	private $myPatientsId = array();
	private $dateOfBirth = array();
	private $pageCount = '';
	
	public function __construct() 
	{
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
		$this->currentDate = date('Y-m-d H:i:s');
		$this->safe_user = "safe_user";
		$this->safe_personalinfo = "safe_personalinfo";
		$this->safe_addresses = "safe_addresses";
		$this->safe_useremails = "safe_useremails";
		$this->emr_user  = "emr_user";
		$this->emr_patient  = "emr_patient";
		$this->contactinformation = "safe_contactinformation";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->verfiedAccountsId = $this->getVerifiedAccountsId();
		$this->myPatientsId = $this->getMyPatientsId();
		$this->safesersearchinfo = array();
		$this->patientArrayCount = array();
	}

	public function backupCodeSearch($backupcode)
	{
		$this->db->from('safe_qr as qr');
		$this->db->join('safe_personalinfo AS personal','personal.IDsafe_personalInfo = qr.IDsafe_personalInfo');
		$this->db->join('safe_user AS user','user.IDsafe_user = personal.IDsafe_user');
		$this->db->join('safe_useremails AS email','email.IDsafe_user = user.IDsafe_user');
		
		$this->db->like('textcode',$backupcode);
		$this->db->where('qr.current', TRUE);
		$this->db->where('email.primary', TRUE);
		$return = $this->db->get()->result();

		return $return;
	}

	# get verified accounts id
	private function getVerifiedAccountsId()
	{
		$verfiedIds = array();
		
		$this->db->where('verified', TRUE);
		$this->db->where('active', TRUE);
		$result = $this->db->get($this->safe_user)->result();
		
		# select patient active and verified user accounts 
		foreach($result as $r)
		{
			array_push($verfiedIds, $r->IDsafe_user);
		}
		
		return $verfiedIds;
	}

	# get my patients account id 
	private function getMyPatientsId()
	{
		$myPatientIds = array();
		
		$this->db->from($this->emr_patient);
		$this->db->join($this->emr_user, $this->emr_user.'.IDemr_user = '.$this->emr_patient.'.IDemr_user', 'left');
		$this->db->where($this->emr_user.'.IDsafe_user', $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->db->where($this->emr_user.'.approve', TRUE);
		$this->db->where($this->emr_user.'.active', TRUE);
		$result = $this->db->get()->result();
		
		# select patient active and verified user accounts 
		foreach($result as $r)
		{
			array_push($myPatientIds, $r->IDsafe_personalInfo);
		}

		return $myPatientIds;
	}
	
	# search date of birth
	public function searchPatient_safeuser($keyword = array(), $toSearch = '', $myPatientsOnly = '')
	{
		$searchName = array();
		if(!empty($myPatientsOnly) && strcmp($myPatientsOnly, 'on') == 0)
		{
			$accountIds = $this->myPatientsId;
		}
		else
		{
			$accountIds = $this->verfiedAccountsId;
		}
		
		foreach($keyword as $kword => $kValue)
		{
			foreach($kValue as $k)
			{
				$this->db->like($toSearch, $k, 'both');
				// $this->db->where('verified', TRUE);
				$this->db->where('active', TRUE);
				$result = $this->db->get($this->safe_user)->result();
				
				foreach($result as $r)
				{	
					if(in_array($r->IDsafe_user, $accountIds))
					{
						array_push($searchName, $r->IDsafe_user);
					}
				}
			}		
		}

		return $searchName;
	}
	
	#  search emails
	public function searchPatient_useremails($keyword = array(), $toSearch = '', $myPatientsOnly = '')
	{
		$searchEmails = array();
		if(!empty($myPatientsOnly) && strcmp($myPatientsOnly, 'on') == 0)
		{
			$accountIds = $this->myPatientsId;
		}
		else
		{
			$accountIds = $this->verfiedAccountsId;
		}
		
		foreach($keyword as $kword => $kValue)
		{
			foreach($kValue as $k)
			{
				$this->db->from($this->safe_user);
				$this->db->join($this->safe_useremails, $this->safe_useremails.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
				$this->db->like($toSearch, $k, 'both');
				$this->db->where($this->safe_user.'.verified', TRUE);
				$this->db->where($this->safe_user.'.active', TRUE);
				$this->db->where($this->safe_useremails.'.active', TRUE);
				$result = $this->db->get()->result();
				
				foreach($result as $r)
				{	
					if(in_array($r->IDsafe_user, $accountIds))
					{
						array_push($searchEmails, $r->IDsafe_user);
					}
				}
			}		
		}
		
		return $searchEmails;
	}
	
	# search personal info
	public function searchPatient_personalinfo($keyword = '', $toSearch = '', $myPatientsOnly = '')
	{
		$searchPersonalInfo = array();
		if(!empty($myPatientsOnly) && strcmp($myPatientsOnly, 'on') == 0)
		{
			$accountIds = $this->myPatientsId;
		}
		else
		{
			$accountIds = $this->verfiedAccountsId;
		}
		
		$this->db->from($this->safe_user);
		$this->db->join($this->safe_personalinfo, $this->safe_personalinfo.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
		$this->db->like($toSearch, $keyword, 'both');
		$this->db->where($this->safe_user.'.verified', TRUE);
		$this->db->where($this->safe_user.'.active', TRUE);
		$result = $this->db->get()->result();
		
		foreach($result as $r)
		{		
			if(in_array($r->IDsafe_user, $accountIds))
			{
				array_push($searchPersonalInfo, $r->IDsafe_user);
			}
		}
		
		return $searchPersonalInfo;
	}
	
	# search personal info for dateofbirth
	public function searchPatient_personalinfo_dateofbirth($keyword = '', $myPatientsOnly = '')
	{
		$searchPersonalInfo_dateofbirth = $explode_dateofbirth = array();
		if(!empty($myPatientsOnly) && strcmp($myPatientsOnly, 'on') == 0)
		{
			$accountIds = $this->myPatientsId;
		}
		else
		{
			$accountIds = $this->verfiedAccountsId;
		}
		
		# split string by spaces(" "), commas(,) , forward slash(/) and dash (-) in accordance with common date string format
		$explode_keyword = preg_split( "/[\s,\/-]+/", $keyword); 

		$this->db->where_in('IDsafe_user', $accountIds);
		$result = $this->db->get($this->safe_personalinfo)->result();
		
		foreach($result as $r)
		{
			$explode_dateofbirth = explode(' ', date('F d Y', strtotime($r->dateofbirth)));
			foreach($explode_keyword as $ek)
			{
				foreach($explode_dateofbirth as $ed)
				{
					if(strcmp(strtolower($ek), strtolower($ed)) == 0)
				    {
				    	array_push($searchPersonalInfo_dateofbirth, $r->IDsafe_user);
				    }
				}
				
			}
		}
		
		return $searchPersonalInfo_dateofbirth;
	}
	
	# search addresses
	public function searchPatient_addresses($keyword = '', $toSearch = '', $myPatientsOnly = '')
	{
		$searchAddresses = array();
		if(!empty($myPatientsOnly) && strcmp($myPatientsOnly, 'on') == 0)
		{
			$accountIds = $this->myPatientsId;
		}
		else
		{
			$accountIds = $this->verfiedAccountsId;
		}
		
		$this->db->from($this->safe_user);
		$this->db->join($this->safe_personalinfo, $this->safe_personalinfo.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
		$this->db->join($this->safe_addresses, $this->safe_addresses.'.IDsafe_personalInfo = '.$this->safe_personalinfo.'.IDsafe_personalInfo', 'left');
		$this->db->like($toSearch, $keyword, 'both');
		$this->db->where($this->safe_user.'.verified', TRUE);
		$this->db->where($this->safe_user.'.active', TRUE);
		$this->db->where($this->safe_addresses.'.active', TRUE);
		$result = $this->db->get()->result();
		
		foreach($result as $r)
		{	
			if(in_array($r->IDsafe_user, $accountIds))
			{
				array_push($searchAddresses, $r->IDsafe_user);
			}
		}
		return $searchAddresses;
	}
	
	# get search patient information 
	public function getSearchPatientInfo($sId)
	{
		$this->db->from($this->safe_user);
		$this->db->join($this->safe_personalinfo, $this->safe_personalinfo.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
		$this->db->join($this->safe_useremails, $this->safe_useremails.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
		$this->db->where($this->safe_user.'.IDsafe_user', $sId);
		$this->db->where($this->safe_user.'.verified', TRUE);
		$this->db->where($this->safe_user.'.active', TRUE);
		$this->db->where($this->safe_useremails.'.active', TRUE);
		$this->db->where($this->safe_useremails.'.primary', TRUE);
		return $this->db->get()->result();
	}

	/* para revision lang */
	/* added by edison */
	public function getSearchPatientInformation($searchText, $ispatient = FALSE, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		

		$this->safesersearchinfo = array();
		$explode_keywordbyword = array();
		
		array_push($explode_keywordbyword, $searchText);
		array_push($explode_keywordbyword, ucwords($searchText));
		array_push($explode_keywordbyword, strtolower($searchText));
		array_push($explode_keywordbyword, strtoupper($searchText));
		$explode_keyword = preg_split( "/[\s,\/-]+/", $searchText); 
		
		foreach ($explode_keyword as $keyword) 
		{
			// $keyword = ucwords($keyword);
			array_push($explode_keywordbyword, ucwords($keyword));
			array_push($explode_keywordbyword, strtolower($keyword));
			array_push($explode_keywordbyword, strtoupper($keyword));
		}
		// print_r($explode_keywordbyword);
		// echo '<br><br>';
			foreach($explode_keywordbyword as $ek)
			{
				$findThis = $this->nagkamoritsing->useForSearch($ek);
				foreach ($findThis as $find) 
			 	{		 		
					$this->db->from($this->safe_user);
					$this->db->join($this->safe_personalinfo, $this->safe_personalinfo.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
					$this->db->join($this->safe_addresses, $this->safe_addresses.'.IDsafe_personalInfo = '.$this->safe_personalinfo.'.IDsafe_personalInfo', 'left');
			 		$this->db->or_like('firstname',$find);
			 		$this->db->or_like('lastname',$find);
			 		$this->db->or_like('middlename',$find);
			 		$this->db->or_like('username',$find);
			 		$this->db->order_by("username", "asc");
			 		$this->db->order_by($this->safe_personalinfo.'.IDsafe_user', "asc"); 
			 		$this->db->where($this->safe_user.'.active', true);
			 		$res = $this->db->get();

		 		 	if($res->num_rows() >= 1)
				 	{
				 		$result = $res->result();
				 		$this->puttoarray($result);
				 	}					
			 	}
		 		$this->db->from($this->safe_user);
				$this->db->join($this->safe_personalinfo, $this->safe_personalinfo.'.IDsafe_user = '.$this->safe_user.'.IDsafe_user', 'left');
				$this->db->join($this->safe_addresses, $this->safe_addresses.'.IDsafe_personalInfo = '.$this->safe_personalinfo.'.IDsafe_personalInfo', 'left');
		 		$this->db->or_like('street',$ek);
		 		$this->db->or_like('state',$ek);
		 		$this->db->or_like('city',$ek);
		 		$this->db->or_like('country',$ek);
		 		$this->db->or_like('bloodtype',$ek);
		 		$this->db->order_by("username", "asc");
		 		$this->db->order_by($this->safe_personalinfo.'.IDsafe_user', "asc"); 
		 		$this->db->where($this->safe_user.'.active', true);
		 		$res = $this->db->get();
	 		 	if($res->num_rows() >= 1)
			 	{
			 		// $this->safesersearchinfo = $res->row();
			 		// array_push($this->safesersearchinfo, $res->result());
			 		// $found = true;
			 		$result = $res->result();
			 		$this->puttoarray($result);
			 	}
			 } 
		# check if patient ba sa doctor

	 	# kailangan pud ma remove ang duplicate
		$this->safesersearchinfo = $this->unique_multidim_array($this->safesersearchinfo,'username');
		$this->patientArrayCount = count($this->safesersearchinfo);
		
		# paging
		$this->safesersearchinfo = array_slice( $this->safesersearchinfo, $start, $limit); 
		# WHEN SHOWING NEXT 10
		# $menuItems = array_slice( $menuItems, 10, 10 );
		
		return $this->safesersearchinfo;
	}

	public function getPatientCount()
	{
		return $this->patientArrayCount;
	}

	public function getPageCount()
	{
		return $this->$pageCount;
	}

	private function unique_multidim_array($array, $key) 
	{ 
	    $temp_array = array(); 
	    $i = 0; 
	    $key_array = array(); 
	    
	    foreach($array as $val) { 
	        if (!in_array($val[$key], $key_array)) { 
	            $key_array[$i] = $val[$key]; 
	            $temp_array[$i] = $val; 
	        } 
	        $i++; 
	    } 
	    return $temp_array; 
	} 

	private function puttoarray($result)
	{
		foreach ($result as $ress) 
	 	{
	 		$patientInformationArray = array(
				'IDsafe_user' => $ress->IDsafe_user,
				'username' => $this->nagkamoritsing->ibalik($ress->username),
				'firstname' => $this->nagkamoritsing->ibalik($ress->firstname),
				'middlename' => $this->nagkamoritsing->ibalik($ress->middlename),
				'lastname' => $this->nagkamoritsing->ibalik($ress->lastname),
				'verified' => (boolean)$ress->verified,
				'active' => (boolean)$ress->active,
				'firsttime' => $this->nagkamoritsing->ibalik($ress->firsttime),
				'donesettings' => $ress->donesettings,
				'IDsafe_personalInfo' => $ress->IDsafe_personalInfo,
				'fullname' => $this->nagkamoritsing->ibalik($ress->fullname),
				'default' => (boolean)$ress->default, 
				'sex' => $ress->sex,
				'dateofbirth' => $ress->dateofbirth,
				'bloodtype' => $ress->bloodtype,
				'IDsafe_addresses' => $ress->IDsafe_addresses,
				'street' => $ress->street,
				'state' => $ress->state,
				'city' => $ress->city,
				'country' => $ress->country,
				'postcode' => $ress->postcode 
			);
			array_push($this->safesersearchinfo, $patientInformationArray);
	 	}

	 	return;
	}


	
}

?>