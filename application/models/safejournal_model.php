<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safejournal_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->requestip = $_SERVER['REMOTE_ADDR'];
	}

	public function getjournal($IDemr_journal)
	{
		$this->db->where('IDemr_journal', $IDemr_journal);
		return $this->db->get('emr_journal')->row();
	}

	public function updateJounal($data)
	{
		$insertData = array(
			'notes' => $data['doctor-notes'],
			'dateinputed' => $this->currentDate
		);
		$this->db->where('IDemr_journal', $data['IDemr_journal']);
		$this->db->update('emr_journal',$insertData);

		return;
	}

	public function deleteJournal($IDemr_journal)
	{
		$updateData = array(
			'active' => FALSE
		);
		$this->db->where('IDemr_journal', $IDemr_journal);
		$this->db->update('emr_journal', $updateData);

		return;
	}

	public function insertJournalOnly($data)
	{
		$dataInsert = array(
			'notes' => $data['notes'],
			'dateinputed' => $this->currentDate,
			'emr_user_IDemr_user' => $data['emr_user_IDemr_user'],
			'emr_patient_IDemr_patient' => $data['emr_patient_IDemr_patient'],
			'active' => TRUE
		);
		$this->db->insert('emr_journal', $dataInsert);

		return $this->db->insert_id();
	}
	
	public function insertjournalmedication($data)
	{
		
		# emr_prescription_safe
		# emr_safe_drug
		$dataInsert = array(
			'safe_drug_IDsafe_drug' => $data['IDsafe_drug'],
			'emr_user_IDemr_user' => $data['IDemr_user']
		);
		$this->db->insert('emr_safe_drug', $dataInsert);
		$IDemr_safe_drug = $this->db->insert_id();

		if (!empty($data['doctor-notes']))
			return $this->addjournal($data,'safe_drug');
		else
			return;
	}

	public function insertjournalprescription($data)
	{
		$dataInsert = array(
			'IDemr_user' => $data['IDemr_user'],
			'safe_prescription_IDsafe_prescription' => $data['safe_prescription_IDsafe_prescription'],
			'active' => TRUE
		);
		$this->db->insert('emr_prescription_safe', $dataInsert);

		$IDemr_prescription_safe = $this->db->insert_id();

		return @$IDemr_prescription_safe;

		# no need for this line kay buwag man ang pag save sa prescription
		# og journal, hinoon tungod ni sa arranggement sa UI
		// if (!empty($data['doctor-notes']))
		// 	return $this->addjournal($data);
		// else
		// 	return;		
	}

	public function insertjournalproblem($data)
	{
		$insertData = array(
			'IDemr_user' => $data['IDemr_user'],
			'IDsafe_problem' => $data['IDsafe_problem'],
			'active' => TRUE
		);
		$idReturn = $this->db->insert('emr_problem_safe',$insertData);

		$data['IDemr_problem_safe'] = $this->db->insert_id();

		
		if (!empty($data['doctor-notes']))
			return $this->addjournal($data,'safe_problem');
		else
			return;
	}

	public function insertjournalallergy($data)
	{
		$insertData = array(
			'IDemr_user' => $data['IDemr_user'],
			'IDsafe_allergen' => $data['IDsafe_allergen'],
			'active' => TRUE
		);
		$idReturn = $this->db->insert('emr_allergen_safe',$insertData);

		$IDemr_journal = $this->db->insert_id();	

		if (!empty($data['doctor-notes']))
			return $this->addjournal($data,'safe_allergen');
		else
			return;
		
	}

	public function inserjournalprocedure($data)
	{
		# tiwasa ni
		$insertData = array(
			'emr_user_IDemr_user' => $data['IDemr_user'],
			'safe_procedure_IDsafe_procedure' => $data['IDsafe_procedure'],
			'active' => TRUE
		);
		$idReturn = $this->db->insert('emr_procedure_safe', $insertData);

		$IDemr_journal = $this->db->insert_id();

		if (!empty($data['doctor-notes']))
			return $this->addjournal($data, 'safe_procedure');
		else
			return;
	}

	public function insertjournaltest($data)
	{
		$insertData = array(
			'safe_test_IDsafe_test' => $data['IDsafe_test'],
			'emr_user_IDemr_user' => $data['IDemr_user'],
			'dateinput' => $this->currentDate,
			'active' => TRUE
		);

		$idReturn = $this->db->insert('emr_safe_test_direct', $insertData);

		$IDemr_journal = $this->db->insert_id();

		if (!empty($data['doctor-notes']))
			return $this->addjournal($data, 'safe_test');
		else
			return;
	}

	public function insertjournalimmunization($data)
	{
		$dataInsert = array(
			'safe_immunization_IDsafe_immunization' => $data['IDsafe_immunization'],
			'emr_user_IDemr_user' => $data['IDemr_user']
		);
		
		$this->db->insert('emr_immunization_safe', $dataInsert);
		$this->db->insert_id();

		if (!empty($data['doctor-notes']))
			return $this->addjournal($data,'safe_immunization');
		else
			return;
	}

	public function getpatientofdoctor($IDsafe_personalInfo, $IDemr_user)
	{
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('IDemr_user', $IDemr_user);
		$result = $this->db->get('emr_patient')->row();

		return $result;
	}

	public function addthispatient($IDsafe_personalInfo, $IDemr_user)
	{
		$insertData = array(
			'IDemr_user' => $IDemr_user,
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'datetimeadded' => $this->currentDate
		);
		$this->db->insert('emr_patient', $insertData);

		return $this->db->insert_id();
	}

	public function noteChanges($IDemr_journal, $note)
	{
		$this->db->where('IDemr_journal', $IDemr_journal);
		$this->db->where("notes", $note);
		$ret = $this->db->get('emr_journal');

		if ($ret->num_rows > 0)
			return false;
		else
			return true;
	}

	public function addjournal($data, $whatTable = NULL)
	{
		$hasquery = TRUE; # just to avoid unecessary errors
		if (empty($data['IDemr_journal']))
			$data['IDemr_journal'] = 0;
		
		if (!empty($data['doctor-notes']))
		{
			if ($this->noteChanges($data['IDemr_journal'], $data['doctor-notes']))
			{
				
				$insertData = array(
					'notes' => $data['doctor-notes'],
					'dateinputed' => $this->currentDate,
					'emr_user_IDemr_user' => $data['IDemr_user'],
					'emr_patient_IDemr_patient' => $data['IDemr_patient']
				);

				$idReturn = $this->db->insert('emr_journal',$insertData);
				$IDemr_journal = $this->db->insert_id();

				# this will help us identify if asa ang journal na belong
				$updateArray = array(
					'IDemr_journal' => $IDemr_journal, 
				);

				if (strcasecmp($whatTable, 'safe_problem')==0)
					$this->db->where('IDsafe_problem', $data['IDsafe_problem']);
				elseif (strcasecmp($whatTable, 'safe_allergen')==0)
					$this->db->where('IDsafe_allergen', $data['IDsafe_allergen']);
				elseif (strcasecmp($whatTable,'safe_drug')==0)
					$this->db->where('IDsafe_drug',$data['IDsafe_drug']);
				elseif (strcasecmp($whatTable, 'safe_prescription')==0)
					$this->db->where('IDsafe_prescription', $data['IDsafe_prescription']);
				elseif (strcasecmp($whatTable, 'safe_procedure')==0)
					$this->db->where('IDsafe_procedure', $data['IDsafe_procedure']);
				elseif (strcasecmp($whatTable, 'safe_test')==0) 
					$this->db->where('IDsafe_test', $data['IDsafe_test']);
				elseif (strcasecmp($whatTable, 'safe_contactinformation')==0) 
					$this->db->where('IDcontactinfo_address', $data['IDsafe_contactinformation']);
				elseif (strcasecmp($whatTable, 'safe_immunization')==0)
					$this->db->where('IDsafe_immunization', $data['IDsafe_immunization']);
				else
					$hasquery = FALSE;

				if ($hasquery == TRUE)
					$this->db->update($whatTable, $updateArray);
				# this will help us identify if asa ang journal na belong
				# or unsa journal connected ang certain safe_* data
			}
			else
				$IDemr_journal = $data['IDemr_journal'];

			return $IDemr_journal;
		}
		else
			return;

		
	}
}
?>