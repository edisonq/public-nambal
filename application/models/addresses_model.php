<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addresses_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_addresses = "safe_addresses";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->safe_userID = '';
		$this->personalInfoID = '';
		$this->safe_addressesID = '';
	}
	public function checkComplete($IDsafe_personalInfo) 
	{
		if (empty($IDsafe_personalInfo))
		{
			$IDsafe_personalInfo = $this->personalInfoID;
		}
		
		$return = FALSE;
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('street IS NOT NULL');
		$this->db->where('state IS NOT NULL');
		$this->db->where('city IS NOT NULL');
		$this->db->where('country IS NOT NULL');
		$this->db->where('default', TRUE);
		$this->db->where('active', TRUE);
        $res = $this->db->get($this->safe_addresses); 
		if($res->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}

	public function getPersonalInfoAddress($IDsafe_personalInfo)
	{
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('active', TRUE);
		$this->db->from('safe_addresses');
		return $this->db->get()->row();
	}
}
?>