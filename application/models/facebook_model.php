<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_user = "safe_user";
		$this->safe_personalinfo = "safe_personalinfo";
		$this->safe_facebook = "safe_facebook";

		$this->safe_useremails = "safe_useremails";
		$this->safe_addresses = "safe_addresses";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->personalInfoData = array(
			'IDsafe_personalInfo' => '',
			'IDsafe_user' => '',
			'fullname' => '',
			'default' => '',
			'sex' => '',
			'dateofbirth' => '',
			'bloodtype' => ''
		);
		$this->safe_userInfoData = array(
			'username' => '',
			'firstname' => '',
			'middlename' => '',
			'lastname' => '',
			'password' => ''
		);
		$this->usernameNew = '';
		$this->safe_userID = '';
		$this->personalInfoID = '';
	}

	public function createNewFB($data)
	{
		$justForUsername = str_replace(' ', '', $this->currentDate);
		$this->usernameNew = $data['username'].'-'.$justForUsername.'-'.$data['id'];
		$user = array(
			'username' => $this->nagkamoritsing->bungkag($this->usernameNew),
			'firstname' => $this->nagkamoritsing->bungkag($data['first_name']),
			'middlename' => $this->nagkamoritsing->bungkag(@$data['middlename']),
			'lastname' => $this->nagkamoritsing->bungkag($data['last_name']),
			'password' => '',
			'verified' => 1,
			'active' => TRUE
		);
		try
		{
			$idReturn = $this->db->insert($this->safe_user,$user);
		}
		catch (Exception $e)
		{

		}
		if (empty($data['birthday']))
			$data['birthday'] = '00/00/0000';
		$formattedBday = explode('/',$data['birthday']);
		$formattedBday = $formattedBday[2].'-'.$formattedBday[0].'-'.$formattedBday[1];

		if($idReturn)
		{
			$IDsafe_user = $this->db->insert_id();
			$this->safe_userID = $IDsafe_user;
			$userEmails = array(
				'emailAddress' => $this->nagkamoritsing->bungkag($data['email']),
				'primary' => 1,
				'active' => TRUE,
				'IDsafe_user' => $IDsafe_user
			);
			try
			{
				$idReturn = $this->db->insert($this->safe_useremails, $userEmails);
			}
			catch (Exception $e)
			{
				
			}

			$personalInfo = array (
				'IDsafe_user' => $IDsafe_user,
				'fullname' => $this->nagkamoritsing->bungkag($data['last_name'].', '.$data['first_name']),
				'default' => true,
				'sex' => $data['gender'],
				'dateofbirth' => $formattedBday
			);
			$this->db->insert($this->safe_personalinfo, $personalInfo);
			$this->personalInfoID = $this->db->insert_id();

			$facebookInfo = array(
				'fb_username' =>  $this->nagkamoritsing->bungkag($data['username']),
				'fb_id' => $data['id'],
				'active' => true,
				'registereddate' => $this->currentDate,
				'IDsafe_user' => $IDsafe_user
			);
			$this->facebookInfo = $this->db->insert($this->safe_facebook, $facebookInfo);

			return $IDsafe_user;
		}
		else
			return false;
	}

	public function checkID($fb_id)
	{
		$this->db->where('fb_id', $fb_id);
	 	$res = $this->db->get($this->safe_facebook);
	 	if($res->num_rows() >= 1)
		{
	 		return 1;
	 	}
	 	else
	 	{
	 		return 0;
	 	}
	}


	public function getUsername()
	{
		return $this->usernameNew;
	}

	public function getIDSafeUser($fb_id)
	{
		$return = 0;
		$this->db->where('fb_id', $fb_id);
	 	$res = $this->db->get($this->safe_facebook);
	 	if($res->num_rows() >= 1)
		{
			$return = $res->row();
	 		$return = $return->IDsafe_user;
	 		$this->safe_userID = $return;
	 	}
	 	return $return;
	}

	public function getPersonalInfoData($IDsafe_user = NULL)
	{
		$return = array();

		if (empty($IDsafe_user))
		{
			$IDsafe_user = $this->safe_userID;
		}

		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('default', true);
		$res = $this->db->get($this->safe_personalinfo);
		if($res->num_rows() >= 1)
		{
			$return = $res->row();
			$this->personalInfoData = $return;
			$this->personalInfoID = $return->IDsafe_personalInfo;
		}

		return $return;
	}


	public function getSafeUserInfo($IDsafe_user = NULL)
	{
		$return = array();

		if (empty($IDsafe_user))
		{
			$IDsafe_user = $this->safe_userID;
		}

		$this->db->where('IDsafe_user', $IDsafe_user);
		$res = $this->db->get($this->safe_user);
		if($res->num_rows() >= 1)
		{
			$safeUserInfo = $res->row();
			$return = array(
				'username' => $this->nagkamoritsing->ibalik($safeUserInfo->username),
				'firstname' => $this->nagkamoritsing->ibalik($safeUserInfo->firstname),
				'middlename' => $this->nagkamoritsing->ibalik($safeUserInfo->lastname),
				'lastname' => $this->nagkamoritsing->ibalik($safeUserInfo->lastname),
				'password' => ''
			);
			$this->safe_userInfoData = $return;
		}

		return $return;
	}

	public function getSafeAddressInfo($IDsafe_personalInfo = NULL)
	{
		$return = array();

		if (empty($IDsafe_personalInfo))
		{
			$IDsafe_personalInfo = $this->personalInfoID;
		}

		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$res = $this->db->get($this->safe_addresses);

		if($res->num_rows() >= 1)
		{
			$return = $res->row();
			$this->safe_userInfoData = $return;
		}

		return $return;
	}

}