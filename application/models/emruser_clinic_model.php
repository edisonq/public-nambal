<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emruser_Clinic_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		
		$this->emruser = 'emr_user';
		$this->emraddresses = 'emr_addresses';
		$this->clinicinfohasemruser = 'clinic_info_has_emr_user';
		$this->clinicinfohasemraddress = 'clinic_info_has_emr_address';
		$this->clinicinfo = 'clinic_info';
		$this->cliniccontactnumber = 'clinic_contact_number';
		$this->cliniccontactemail = 'clinic_contact_email';
		$this->clinichours = 'clinic_hours';
		$this->clinicstaff = 'clinic_staff';
		$this->safeuser = 'safe_user';
	}
		
	#create clinic	
	public function createClinicInformation($createClinicInformation = array())
	{
		$return = false;
		
		////add clinic info
		$clinic_info = array('clinicname' => $createClinicInformation['clinicname'],
							 'active' => 1,
							 'numberOfDoctors' => $createClinicInformation['numberOfDoctors']);
		
		$return = $this->db->insert($this->clinicinfo, $clinic_info);
		
		// //get doctor's recent added clinic id
		// $IDclinic_info = $this->db->insert_id();
		
		// //get doctor id
		// $IDemr_user = $this->getDoctorId($createClinicInformation['IDsafe_user']);
		
		// /////emr user added this clinic
		// $IDclinic_has_emr_user = array('clinic_info_IDclinic_info' => $IDclinic_info,
									   // 'emr_user_IDemr_user' => $IDemr_user,
									   // 'active' => 1);
		// $return = $this->db->insert($this->clinicinfohasemruser, $IDclinic_has_emr_user);
		
		// ///add emr address
		// $emr_addresses = array('country' => $createClinicInformation['country'],
							   // 'state' => $createClinicInformation['state'],
							   // 'street' => $createClinicInformation['street'],
							   // 'city' => $createClinicInformation['city'],
							   // 'postcode' => $createClinicInformation['postcode'],
							   // 'IDemr_user' => $IDemr_user,
							   // 'active' => 1);
		// $return = $this->db->insert($this->emraddresses, $emr_addresses);
		
		// //get recently added address' id
		// $IDemr_addresses = $this->db->insert_id();
		
		// ///// add clinic contact number
		// if(!empty($createClinicInformation['countrycode']) || 
		   // !empty($createClinicInformation['areacode']) || 
		   // !empty($createClinicInformation['phonenumber'])
		   // )
		// {
			// for($i = 0; $i < sizeof($createClinicInformation['countrycode']); $i++)
			// {
				// if($i === 0)
				// {
					// $primary = 1;
				// }
				// else
				// {
					// $primary = 0;
				// }
			
				// $clinic_contact_number = array('clinic_info_IDclinic_info' => $IDclinic_info,
											   // 'countrycode' => $createClinicInformation['countrycode'][$i],
											   // 'areacode' => $createClinicInformation['areacode'][$i],
											   // 'phonenumber' => $createClinicInformation['phonenumber'][$i],
											   // 'primary' => $primary,
											   // 'active' => 1);
											   
				// $return = $this->db->insert($this->cliniccontactnumberm, $clinic_contact_number);
			// }
		// }
		
		// ///// add clinic contact email
		// $clinic_contact_email = array('clinic_info_IDclinic_info' => $IDclinic_info,
									  // 'email' => $createClinicInformation['email'],
									  // 'primary' => 1,
									  // 'active' => 1);
		// $return = $this->db->insert($this->cliniccontactemail, $clinic_contact_email);
		
		// ///// add clinic hours
		// for($d = 0; $d < 7; $d++)
		// {
			// // day
			// switch($d)
			// {
				// case 1:
					// $day = 'tuesday';
				// break;
				
				// case 2:
					// $day = 'wednesday';
				// break;
				
				// case 3:
					// $day = 'thursday';
				// break;
				
				// case 4:
					// $day = 'friday';
				// break;
				
				// case 5:
					// $day = 'saturday';
				// break;
				
				// case 6:
					// $day = 'sunday';
				// break;
				
				// default:
					// $day = 'monday';
				// break;
			// }
			
			// // from hours
			// $date_time_from = $fromHour[$d].':'.$fromMinute[$d].': '.$meridianFrom[$d];
			// $date_from = DateTime::createFromFormat('H:i A', $date_time_from);
			// $timeFrom = $date_from->format('H:i');
			
			// // to hours
			// $date_time_to = $fromHour[$d].':'.$fromMinute[$d].': '.$meridianFrom[$d];
			// $date_to = DateTime::createFromFormat('H:i A', $date_time_to);
			// $timeTo = $date_to->format('H:i');
		
			// $clinic_hours = array('clinic_info_IDclinic_info' => $IDclinic_info,
								   // 'timefrom' => $timeFrom,
								   // 'timeto' => $timeTo,
								   // 'day' => ucfirst($day),
								   // 'active' => 1,
								   // 'description' => '');
								   
			// $return = $this->db->insert($this->clinichours, $clinic_hours);
		// }
		
		return $return;
	}
	
	public function addClinicStaff($addClinicStaff)
	{
		$return = true;
			
		return $return;
	}
	
	public function getDoctorId($IDsafe_user = 0)
	{
		$docId = 0;

		$this->db->select('IDemr_user');
		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$docId = $this->db->get($this->emruser);
	 	
		return $docId;
	}
}

?>