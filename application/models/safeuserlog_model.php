<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SafeuserLog_model extends CI_Model 
{
	
	public function __construct() 
	{
		$this->load->library('email');
		parent::__construct();
		$this->userlog = "safe_userlog";
		$this->immunization_userlog = "safe_immunization_userlog";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Safeuser_model', 'safeuser');
		$this->currentDate = date('Y-m-d H:i:s');
		$this->ipaddress = @$_SERVER['REMOTE_ADDR'];
		
		// getSafeUserInfo
	}

	public function logUser($logdata, $tableName = '')
	{
		/*
		 * To avoid conflicts between the previous version and the present one of this module, the declaration of the following data should be on its related controller :
		 * 	- IDsafe_user
		 * 	- activity 
		 * 	- activityDescription
		 *  
		 * $tableName by default is empty to determine whether the userlog informations passed are for the mirror log data (detailed) or not and also to avoid conflicts between the codes that uses the old version rather than this one.
		 */

		if($tableName == 'immunization')
		{
			$tableName = $this->immunization_userlog;
		}
		else
		{
			$logdata['activityDate'] = $this->currentDate;
			$logdata['IpAddress'] = $this->ipaddress;
			$tableName = $this->userlog;
		}

		$this->db->insert($tableName, $logdata);
		return $this->db->insert_id();
	}

	public function getLogUser($IDsafe_user, $activity = NULL, $activitydate = NULL)
	{
		if (!empty($activitydate))
			$this->db->like('activityDate', $activitydate);
		if (!empty($activity))
			$this->db->like('activity', $activity);
		$this->db->where('IDsafe_user', $IDsafe_user);
		$return = $this->db->get('safe_userlog')->result();

		return $return;
	}

	public function notifypatientbyemail($IDsafe_user,$message)
	{
		# get the email of the patient
		$patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($IDsafe_user));
		$patientinfoEmail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);

		# check if patient already receive notification within this day
		# don't send notification if already received notification this day
		# 1 notification per day
		$loguser = $this->getLogUser($IDsafe_user, 'sent email notification for patient', date('Y-m-d'));
		if (!empty($loguser))
			return;


		if (!empty($patientinfoEmail))
		{
			$this->email->from('no-reply@nambal.com', 'Nambal Information');
			$this->email->to($this->nagkamoritsing->ibalik(@$patientinfoEmail->emailAddress));
			$footeremail = $patientinfoEmail->emailAddress; 
			// $this->email->cc('edisonq@yahoo.com');
			// $this->email->bcc('edisonq@hotmail.com');
			 
			$this->email->subject('Changes in your nambal safe');
			$emailData = array(
				'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
				'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
			);

			$this->email->message($message);

			# --------------[ NEED ATTENTION ]-------------
			# un-comment this when launching
			$this->email->send();
			$dataInsert = array(
				'IDsafe_user' => $IDsafe_user,
				'activity' => 'sent email notification for patient',
				'activityDescription' => 'sent email notification for patient'
			);
			$this->logUser($dataInsert);
		}

		return;
	}
}
?>