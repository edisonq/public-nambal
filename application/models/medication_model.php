<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medication_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->safe_drug = "safe_drug";
		$this->safe_prescription = "safe_prescription";
		$this->safe_prescription_dispense = "safe_prescription_dispense";
		$this->druglist = "druglist";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('druglist');
		$this->db->where('approved', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDdrugList');
			$this->db->like('manufacturer', $keyword, 'none');
			$this->db->like('brandName', $keyword, 'none');
			$this->db->like('genericName', $keyword, 'none');
		}
		else
		{
			$this->db->select('brandName');
			$this->db->like('manufacturer', $keyword, 'both');
			$this->db->like('brandName', $keyword, 'both');
			$this->db->like('genericName', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->brandName);
			}
		}
		else
		{
			$arrayReturn = @$result->row();
			$arrayReturn = @$arrayReturn->IDdrugList;
		}
		
		return $arrayReturn;
	}

	public function getAllMedicationInPrescription($IDsafe_prescription)
	{
		$this->db->where('IDsafe_prescription', $IDsafe_prescription);
		$this->db->where('drug.active', TRUE);
		$this->db->from('safe_drug AS drug');
		$this->db->join('druglist AS list', 'list.IDdrugList = drug.IDdrugList');
		return $this->db->get()->result();
	}

	public function insertMedication($IDsafe_personalInfo, $data)
	{
		// $medicationList = array(
		// 	'medication' =>$medication
		// 	'now' => $now
		// 	'datetimeStart' => $datetimeStart		
		// 	'datetimeEnd' => $datetimeEnd
		// 	'route' => $route
		// 	'strength' => $strength
		// 	'strengthunit' => $strengthunit
		// 	'dosage' => $dosage
		// 	'dosagesunit' => $dosagesunit
		// 	'writtenon' => $writtenon
		// 	'expirydate' => $expirydate
		// 	'doctorsname' => $doctorsname
	 	//	'rxid' => $rxid
		// 	'dremail' => $dremail
		// 	'dispensedon1' => $dispensedon1
		// 	'dispensedqty1' => $dispensedqty1
		// 	'notes' => $notes
		// );
		$IDdrugList = $this->searchlist($data['medication'], TRUE);

		if (empty($IDdrugList))
			$IDdrugList = 0;

		if ($IDdrugList == 0)
		{
			$dataInsert = array(
				'manufacturer' => $data['medication'],
				'brandName' => $data['medication'],
				'genericName' => $data['medication'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->druglist, $dataInsert);

			$IDdrugList = $this->db->insert_id();			
		}

		if (empty($data['IDsafe_prescription']))
		{
			if ((!empty(@$data['doctorsname'])) && (!empty(@$data['writtenon'])))
			{
				$dataInsert = array(
					'prescriptiondate' => @$data['writtenon'],
					'prescriptionend' => @$data['expirydate'],
					'dr_name' => @$data['doctorsname'],
					'dr_rx' => @$data['rxid'],
					'dr_email' => @$data['dremail'],
					'approve' => FALSE,
					'active' => TRUE,
					'safe_personalinfo_IDsafe_personalInfo' => $IDsafe_personalInfo
				);
				$this->db->insert($this->safe_prescription, $dataInsert);
				$IDsafe_prescription = $this->db->insert_id();
			}
		}
		else
			$IDsafe_prescription = $data['IDsafe_prescription'];


		$dataInsert = array(
			'IDdrugList' => $IDdrugList,
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'now' => $data['now'],
			'dateStart' => @$data['datetimeStart'],
			'dateEnd' => @$data['datetimeEnd'],
			'route' => @$data['route'],
			'strength' => @$data['strength'],
			'strengthtype' => @$data['strengthunit'],
			'dosage' => @$data['dosage'],
			'dosagetype' => @$data['dosagesunit'],
			'frequency' => @$data['frequency'],
			'notes' => @$data['notes'],
			'IDsafe_prescription' => @$IDsafe_prescription,
			'active' => TRUE
		);
		$this->db->insert($this->safe_drug, $dataInsert);
		$IDsafe_drug = $this->db->insert_id();
		
		if ((!empty($data['dispensedon1'])) && (!empty($data['dispensedqty1'])) && (!empty($IDsafe_drug)) && (!empty($IDsafe_prescription)))
		{
			$dataInsert = array(
				'safe_prescription_IDsafe_prescription' => $IDsafe_prescription,
				'dispensedate' => $data['dispensedon1'],
				'quantity' => $data['dispensedqty1']
			);
			$this->db->insert($this->safe_prescription_dispense, $dataInsert);
		}


		return $IDsafe_drug;
	}

	public function displayDashboardMedication($IDsafe_personalInfo,  $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);
		
		# get the problem added
		$medicationArray = array();
		$this->db->select('medicine.IDdrugList, IDsafe_drug, now, dateStart, dateEnd, manufacturer, brandName, genericName, medicine.notes,IDsafe_prescription');
		$this->db->from('safe_drug AS medicine');
		$this->db->join('druglist AS drug', 'drug.IDdrugList = medicine.IDdrugList');
		$this->db->where('medicine.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('medicine.active', TRUE);
		$this->db->order_by("dateStart", "desc"); 
		$drugs = $this->db->get()->result();

		foreach ($drugs as $ret) 
		{
			if (!empty($ret->IDsafe_prescription))
			{
				$this->db->select('lastname, doctor.IDemr_user');
				$this->db->from('emr_prescription_safe AS fromdoctor');
				$this->db->join('safe_prescription AS prescription','fromdoctor.safe_prescription_IDsafe_prescription = prescription.IDsafe_prescription');
				$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = fromdoctor.IDemr_user');
				$this->db->join('safe_user AS doctorName','doctorName.IDsafe_user = doctor.IDsafe_user');
				$this->db->where('prescription.IDsafe_prescription', $ret->IDsafe_prescription);
				$return = $this->db->get()->row();

				if (!empty($return->IDemr_user))
				{
					$arrayMedicationInfo = array(
						'IDdrugList' => $ret->IDdrugList,
						'IDsafe_drug' => $ret->IDsafe_drug,
						'manufacturer' => $ret->manufacturer,
						'brandName' => $ret->brandName,
						'genericName' => $ret->genericName,
						'havingitnow' => $ret->now,
						'dateStart' => $ret->dateStart,
						'dateEnd' => $ret->dateEnd,
						'notes' => $ret->notes,
						'IDsafe_prescription' => $ret->IDsafe_prescription,
						'doctor' => $return->lastname,
						'IDemr_user' => $return->IDemr_user
					);
				}
				else
				{
					$arrayMedicationInfo = array(
						'IDdrugList' => $ret->IDdrugList,
						'IDsafe_drug' => $ret->IDsafe_drug,
						'manufacturer' => $ret->manufacturer,
						'brandName' => $ret->brandName,
						'genericName' => $ret->genericName,
						'havingitnow' => $ret->now,
						'dateStart' => $ret->dateStart,
						'dateEnd' => $ret->dateEnd,
						'notes' => $ret->notes,
						'IDsafe_prescription' => $ret->IDsafe_prescription,
						'doctor' => '',
						'IDemr_user' => ''
					);
				}
			}
			else
			{
				$arrayMedicationInfo = array(
					'IDdrugList' => $ret->IDdrugList,
					'IDsafe_drug' => $ret->IDsafe_drug,
					'manufacturer' => $ret->manufacturer,
					'brandName' => $ret->brandName,
					'genericName' => $ret->genericName,
					'havingitnow' => $ret->now,
					'dateStart' => $ret->dateStart,
					'dateEnd' => $ret->dateEnd,
					'notes' => $ret->notes,
					'IDsafe_prescription' => $ret->IDsafe_prescription,
					'doctor' => '',
					'IDemr_user' => ''
				);
			}

			$this->db->select('lastname,emr_user_IDemr_user');
			$this->db->from('emr_safe_drug AS directdoctor');
			$this->db->join('emr_user AS doctor', 'doctor.IDemr_user = directdoctor.emr_user_IDemr_user');
			$this->db->join('safe_user AS doctorName','doctorName.IDsafe_user = doctor.IDsafe_user');
			$this->db->where('directdoctor.safe_drug_IDsafe_drug', $ret->IDsafe_drug);
			$doctorAddedDirectJournal = $this->db->get()->row();
			
			if (!empty($doctorAddedDirectJournal))
			{
				$arrayMedicationInfo['doctor'] = @$doctorAddedDirectJournal->lastname;
				$arrayMedicationInfo['IDemr_user'] = @$doctorAddedDirectJournal->emr_user_IDemr_user;
			}

			array_push($medicationArray, $arrayMedicationInfo);
		}

		return $medicationArray;
	}
	public function getToModifyMedication($IDsafe_drug)
	{
		$this->db->select('medicine.IDdrugList, IDsafe_drug, now, dateStart, dateEnd, manufacturer, brandName, genericName, route, strength, strengthtype, dosage, dosagetype, frequency,  medicine.notes,IDsafe_prescription, IDemr_journal');
		$this->db->from('safe_drug AS medicine');
		$this->db->join('druglist AS drug', 'drug.IDdrugList = medicine.IDdrugList');
		$this->db->where('medicine.IDsafe_drug', $IDsafe_drug);
		$this->db->where('medicine.active', TRUE);

		return $this->db->get()->row(); 
	}
	public function getPrescriptionID($IDsafe_drug)
	{
		$this->db->select('IDsafe_prescription');
		$this->db->from('safe_drug AS medicine');
		$this->db->join('druglist AS drug', 'drug.IDdrugList = medicine.IDdrugList');
		$this->db->where('medicine.IDsafe_drug', $IDsafe_drug);
		$this->db->where('medicine.active', TRUE);

		return $this->db->get()->row(); 
	}
	public function getPrescription($IDsafe_prescription)
	{
		$this->db->from('safe_prescription AS prescription');
		$this->db->where('prescription.IDsafe_prescription', $IDsafe_prescription);
		$this->db->where('prescription.active', TRUE);

		return $this->db->get()->row();
	}
	public function getDispense($IDsafe_prescription)
	{
		$this->db->from('safe_prescription_dispense');
		$this->db->where('safe_prescription_IDsafe_prescription', $IDsafe_prescription);

		return $this->db->get()->result();
	}
	public function deleteSafeDrug($IDsafe_drug)
	{
		$updateArray = array(
			'active' => FALSE
		);
		$this->db->where('IDsafe_drug', $IDsafe_drug);
		$this->db->where('active', TRUE);
		$this->db->update($this->safe_drug, $updateArray);
	}
	public function updateMedication($data)
	{
		$IDdrugList = $this->searchlist($data['medication'], TRUE);

		if (empty($IDdrugList))
			$IDdrugList = 0;

		if ($IDdrugList == 0)
		{
			$dataInsert = array(
				'manufacturer' => $data['medication'],
				'brandName' => $data['medication'],
				'genericName' => $data['medication'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->druglist, $dataInsert);

			$IDdrugList = $this->db->insert_id();			
		}

		$getPrescriptionID = $this->getPrescriptionID(@$data['IDsafe_drug']);
		$IDsafe_prescription = 	@$getPrescriptionID->IDsafe_prescription;
		$getDispense = $this->getDispense($IDsafe_prescription);

		if ((!empty(@$data['doctorsname'])) && (!empty(@$data['writtenon'])))
		{
			
			$dataUpdate = array(
				'prescriptiondate' => @$data['writtenon'],
				'prescriptionend' => @$data['expirydate'],
				'dr_name' => @$data['doctorsname'],
				'dr_rx' => @$data['rxid'],
				'dr_email' => @$data['dremail'],
				'approve' => FALSE,
				'active' => TRUE,
				'safe_personalinfo_IDsafe_personalInfo' => $data['IDsafe_personalInfo']
			);

			if (!empty($getPrescriptionID))
			{
				$this->db->where('IDsafe_prescription', $IDsafe_prescription);
				$this->db->update($this->safe_prescription, $dataUpdate);
			}
			else
			{
				$this->db->insert($this->safe_prescription, $dataUpdate);
				$IDsafe_prescription = $this->db->insert_id();
			}			
		}

		if (!empty($data['IDsafe_drug']))
		{
			$dataUpdate = array(
				'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
				'IDdrugList' => $IDdrugList,
				'now' => $data['now'],
				'dateStart' => @$data['datetimeStart'],
				'dateEnd' => @$data['datetimeEnd'],
				'route' => @$data['route'],
				'strength' => @$data['strength'],
				'strengthtype' => @$data['strengthunit'],
				'dosage' => @$data['dosage'],
				'dosagetype' => @$data['dosagesunit'],
				'frequency' => @$data['frequency'],
				'notes' => @$data['notes'],
				'IDsafe_prescription' => $IDsafe_prescription,
				'active' => TRUE
			);
			$this->db->where('IDsafe_drug',$data['IDsafe_drug']);
			$this->db->update($this->safe_drug, $dataUpdate);
		}
		$IDsafe_drug = $data['IDsafe_drug'];
		
		if ((!empty($data['dispensedon1'])) && (!empty($data['dispensedqty1'])) && (!empty($IDsafe_drug)) && (!empty($IDsafe_prescription)))
		{
			$dataInsert = array(
				'safe_prescription_IDsafe_prescription' => $IDsafe_prescription,
				'dispensedate' => $data['dispensedon1'],
				'quantity' => $data['dispensedqty1']
			);
			if (!empty($getDispense[0]->IDsafe_prescription_dispense))
			{
				$this->db->where('IDsafe_prescription_dispense',$getDispense[0]->IDsafe_prescription_dispense);
				$this->db->update($this->safe_prescription_dispense, $dataInsert);
			}
			else
			{
				$this->db->insert($this->safe_prescription_dispense, $dataInsert);
			}
		}	
	}
	public function record_count($IDsafe_personalInfo)
    {       
		$this->db->select('IDsafe_drug');
		$this->db->from('safe_drug AS medicine');
		$this->db->where('medicine.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('medicine.active', TRUE);
		$this->db->order_by("dateStart", "desc"); 
		return $this->db->get()->num_rows();
    }
	
}
?>