<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_useremails = "safe_useremails";
		$this->safe_user = "safe_user";
		$this->safe_verification = "safe_verification";
		$this->safe_addresses = "safe_addresses";
		$this->safe_personalinfo = "safe_personalinfo";
		$this->safe_contactinformation = "safe_contactinformation";
		$this->contactinfo_email = "contactinfo_email";
		$this->contactinfo_address = "contactinfo_address";
		$this->contactinfo_numbers = "contactinfo_numbers";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');

		$this->currentDate = date('Y-m-d H:i:s');
		$this->personalInfo = '';
		$this->errorMessage = '';
		$this->IDsafe_user = '';

		$this->defaultSecretKey = "GODrules";
	}

	public function firstRegistration($data)
	{
		# auto generate the password here
		$password = sha1(md5(NULL."".config_item('secret_key_nambal')));
		# end of auto generation of the password

		$user = array(
			'username' => $this->nagkamoritsing->bungkag($data['username']),
			'firstname' => $this->nagkamoritsing->bungkag($data['firstname']),
			'middlename' => $this->nagkamoritsing->bungkag($data['middlename']),
			'lastname' => $this->nagkamoritsing->bungkag($data['lastname']),
			'password' => $password,
			'verified' => 0,
			'active' => TRUE
		);
		try
		{
			$idReturn = $this->db->insert($this->safe_user,$user);
		}
		catch (Exception $e)
		{

		}

		if($idReturn){
			$userEmails = array(
				'emailAddress' => $this->nagkamoritsing->bungkag($data['email']),
				'primary' => 1,
				'active' => true,
				'IDsafe_user' => $this->db->insert_id()
			);
			$IDsafe_user = $this->db->insert_id();
			$this->IDsafe_user = $IDsafe_user;
			try
			{
				$idReturn = $this->db->insert($this->safe_useremails, $userEmails);
			}
			catch (Exception $e)
			{
				
			}

			$validationCode = sha1(md5($data['email']).'edisonguapo'.$this->currentDate);
			$userVerification = array(
				'IDsafe_user' => $IDsafe_user,
				'vcode' => $validationCode,
				'datetime_generated' => $this->currentDate
			);
			$idReturn = $this->db->insert($this->safe_verification, $userVerification);

			$personalInfo = array (
				'IDsafe_user' => $IDsafe_user,
				'fullname' => $this->nagkamoritsing->bungkag($data['firstname'].' '.$data['middlename'].' '.$data['lastname']),
				'default' => true
			);
			$this->personalInfo = $this->db->insert($this->safe_personalinfo, $personalInfo);


			return $IDsafe_user;
		}
		else
			return false;
	}

	public function secondRegistration ($data)
	{
		if (!empty($data['IDsafe_user']))
		{
			$this->IDsafe_user = $data['IDsafe_user'];
		}
		$return = false;
		$personalInfo = array(
			'fullname' => $this->nagkamoritsing->bungkag(@$data['fullname']),
			'sex' => @$data['sex'],
			'dateofbirth' => @$data['dateofbirth'],
			'bloodtype' => @$data['bloodtype']
		);
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$return = $this->db->update($this->safe_personalinfo, $personalInfo);

		if ($return)
		{
			$addressInfo = array(
				'IDsafe_personalInfo' => @$data['IDsafe_personalInfo'],
				'street' => @$data['street'],
				'state' => @$data['state'],
				'city' => @$data['city'],
				'country' => @$data['country'],
				'postcode' => @$data['postcode'],
				'default' => true,
				'active' => true
			);
			$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
			$return = $this->db->get($this->safe_addresses);
			if($return->num_rows() == 1)
			{
				$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
				$return = $this->db->update($this->safe_addresses, $addressInfo);
			}
			else
			{
				$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
				$return = $this->db->insert($this->safe_addresses,$addressInfo);
			}			
		}
		return $return;
	}

	public function thirdRegistration ($data)
	{
		if (!empty($data['IDsafe_user']))
		{
			$this->IDsafe_user = $data['IDsafe_user'];
		}
		
		$return = false;

		# check if the contact information is existing in this profile.
		$deactivateContact = array(
			'active' => false,
			'primary' => false
		);
		$this->db->where('fullname', $data['fullname']);
		$this->db->where('active', true);
		$this->db->where('IDsafe_personalInfo',  $data['IDsafe_personalInfo']);
		$this->db->update($this->safe_contactinformation, $deactivateContact);
		# end of checking if the contact information is existing in this profile.

		# check if there is already a primary contact
		$deactivateContact = array(
			'primary' => false
		);
		$this->db->where('active', true);
		$this->db->where('primary', true);
		$this->db->where('IDsafe_personalInfo',  $data['IDsafe_personalInfo']);
		$this->db->update($this->safe_contactinformation, $deactivateContact);
		# end of checking if there is a primary contact

		$contactInfo = array(
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'fullname' => $data['fullname'],
			'active' => true,
			'primary' => true
		);

		$return = $this->db->insert($this->safe_contactinformation, $contactInfo);
		$IDsafe_contactinformation = $this->db->insert_id();
		$contactInfo_email = array(
			'email' => $data['email'],
			'current' => true,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);
		$return = $this->db->insert($this->contactinfo_email,$contactInfo_email);
		$contactinfo_address = array(
			'IDsafe_contactinformation' => $IDsafe_contactinformation,
			'fulladdress' => $data['fulladdress'],
			'current' => true,
		);
		$return = $this->db->insert($this->contactinfo_address,$contactinfo_address);

		$contactinfo_numbers = array(
			'phonenumber' => $data['number'],
			'areacode' => $data['areacode'],
			'countrycode' => $data['countrycode'],
			'current' => true,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);
		$return = $this->db->insert($this->contactinfo_numbers, $contactinfo_numbers);


		return $return;
	}
	

	public function check_email($email) 
	{
		$found = false;
		$findThis = $this->nagkamoritsing->useForSearch($email);
	 	
	 	foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == false)
		 	{
		 		$this->db->like('emailAddress', $find);
		 		$res = $this->db->get($this->safe_useremails);
	 		 	if($res->num_rows() == 1)
			 	{
			 		$found = true;
			 	}
		 	}
	 	}
	 	return $found;
	}

	public function check_username($username) 
	{
		$found = FALSE;
		$findThis = $this->nagkamoritsing->useForSearch($username);

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == FALSE)
		 	{
		 		$this->db->like('username', $find);
				$res = $this->db->get($this->safe_user);
				if($res->num_rows() == 1)
			 	{
			 		$found = TRUE;
			 	}
			}
		}
		return $found;
	}

	public function getValidationCodeByID($IDsafe_user)
	{
		# get the validation code
		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$res = $this->db->get($this->safe_verification);
	 	return $res->row();
		# end of getting the validation code		
	}

	public function check_verification($vcode,$email){
		$this->db->where('vcode', $vcode);
		$this->db->where('active', TRUE);
	 	$vcode_IDsafe_user = $this->db->get($this->safe_verification);

	 	if (!empty($vcode_IDsafe_user->row()->IDsafe_user))
	 	{
	 		$this->db->where('IDsafe_user', $vcode_IDsafe_user->row()->IDsafe_user);
	 		$email_IDsafe_user = $this->db->get($this->safe_useremails);
	 	}
	 	else
	 	{
	 		return false;
	 	}
		
		if (empty($this->IDsafe_user))
		{
			$this->IDsafe_user = $email_IDsafe_user->row()->IDsafe_user;
		}

		if (strcmp($this->nagkamoritsing->ibalik($email_IDsafe_user->row()->emailAddress), $this->nagkamoritsing->ibalik($email))!=0)
		{
			return false;
		} else {
			if (isset($email_IDsafe_user->row()->IDsafe_user) && isset($vcode_IDsafe_user->row()->IDsafe_user))
			{
		 		if ($email_IDsafe_user->row()->IDsafe_user == $vcode_IDsafe_user->row()->IDsafe_user)
		 		{
		 			# update the database here
		 			# update the safe_user.verified to 1 or true
		 			$dataVerification = array(
		 				'verified' => true
		 			);
		 			$this->db->where('IDsafe_user', $email_IDsafe_user->row()->IDsafe_user);
		 			$this->db->update($this->safe_user, $dataVerification);
		 			# end updating the database.
		 			
		 			# delete the verification code
		 			//$this->db->where ('IDsafe_user', $email_IDsafe_user->row()->IDsafe_user);
		 			//$this->db->delete($this->safe_verification);
		 			# end of deleting the verification code

		 			# update the verification code status to
		 			$verificationMod = array(
		 				'active' => false
		 			);
		 			$this->db->where ('IDsafe_user', $email_IDsafe_user->row()->IDsafe_user);
		 			$this->db->update($this->safe_verification, $verificationMod);
		 			# end of update the verification code status

		 			return true;
		 		}
		 		else
		 		{
		 			return false;
		 		}
		 	}
	 	}
	}

	public function setNewPassword($password, $username, $secret_key = NULL)
	{
		$found = FALSE;
		$findThis = $this->nagkamoritsing->useForSearch($username);

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == FALSE)
		 	{
		 		$this->db->like('username', $find);
				$res = $this->db->get($this->safe_user);
				if($res->num_rows() == 1)
			 	{
			 		$emailToFind = $res;
			 		$found = TRUE;
			 	}
			}
		}


		if (empty($secret_key))
		{
			$secret_key = $this->defaultSecretKey;
		}
		$password = sha1(md5($password."".$secret_key));
		$newPasswordData = array(
	 		'password' => $password
		);
	 	$this->db->where('IDsafe_user', $emailToFind->row()->IDsafe_user);
	 	$this->db->update($this->safe_user, $newPasswordData);
	}

	public function getIDsafe_user()
	{
		return $this->IDsafe_user;
	}
	
	public function editRegistration($data)
	{
		if (!empty($data['IDsafe_user']))
		{
			$this->IDsafe_user = $data['IDsafe_user'];
		}
		
		$return = false;
		$contactInfo_id = 0;
		
		$user = array(
			'firstname' => ($data['firstname']),
			'middlename' => ($data['middlename']),
			'lastname' => ($data['lastname'])
		);
		
		$this->db->where('IDsafe_user', $data['IDsafe_user']);
		$return = $this->db->update($this->safe_user,$user);
		
		$personalInfo = array (
			'IDsafe_user' => $data['IDsafe_user'],
			'fullname' => $data['fullname'],
			'sex' => @$data['sex'],
			'dateofbirth' => @$data['dateofbirth'],
			'bloodtype' => @$data['bloodtype']
		);
		
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$return = $this->db->update($this->safe_personalinfo, $personalInfo);
		
		$addressInfo = array(
			'IDsafe_personalInfo' => @$data['IDsafe_personalInfo'],
			'street' => @$data['street'],
			'state' => @$data['state'],
			'city' => @$data['city'],
			'country' => @$data['country'],
			'postcode' => @$data['postcode']
		);
		
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$return = $this->db->update($this->safe_addresses, $addressInfo);
		
		# check if the contact information is existing in this profile.
		$deactivateContact = array(
			'active' => false,
			'primary' => false
		);
		
		$this->db->where('fullname', $data['fullname']);
		$this->db->where('active', true);
		$this->db->where('IDsafe_personalInfo',  $data['IDsafe_personalInfo']);
		$this->db->update($this->safe_contactinformation, $deactivateContact);
		# end of checking if the contact information is existing in this profile.
		
		# check if there is already a primary contact
		$deactivateContact = array(
			'primary' => false
		);
		
		$this->db->where('active', true);
		$this->db->where('primary', true);
		$this->db->where('IDsafe_personalInfo',  $data['IDsafe_personalInfo']);
		$this->db->update($this->safe_contactinformation, $deactivateContact);
		# end of checking if there is a primary contact
		
		$contactInfo = array(
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'fullname' => $data['emergencyfullname'],
			'active' => true,
			'primary' => true
		);
		
		// $this->db->where('IDsafe_personalInfo',  $data['IDsafe_personalInfo']);
		// // $return = $this->db->update($this->safe_contactinformation, $contactInfo);
		// $this->db->insert($this->safe_contactinformation, $contactInfo);

		$return = $this->db->insert($this->safe_contactinformation, $contactInfo);
		$IDsafe_contactinformation = $this->db->insert_id();
		$contactInfo_email = array(
			'email' => $data['email'],
			'current' => true,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);
		$return = $this->db->insert($this->contactinfo_email,$contactInfo_email);
		$contactinfo_address = array(
			'IDsafe_contactinformation' => $IDsafe_contactinformation,
			'fulladdress' => $data['fulladdress']
		);
		$return = $this->db->insert($this->contactinfo_address,$contactinfo_address);

		$contactinfo_numbers = array(
			'phonenumber' => $data['number'],
			'areacode' => $data['areacode'],
			'countrycode' => $data['countrycode'],
			'current' => true,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);
		$return = $this->db->insert($this->contactinfo_numbers, $contactinfo_numbers);
			
		return $return;
	}


	// public function safe_user($data) {
	// 	$secret_key = "nambalizers";
	// 	$salt = md5($data->password."".$secret_key);
	// 	$user = array(
	// 		'username' => $data->username,
	// 		'firstname' => $data->firstname,
	// 		'middlename' => $data->middlename,
	// 		'lastname' => $data->lastname,
	// 		'password' => $salt
	// 	);
	// 	$res = $this->db->insert($this->safe_user,$user);
	// 	if($res)
	// 		return $this->db->insert_id();
	// 	else
	// 		return false;
	// }


	// public function get_account_info($safe_user_id) {
	// 	$this->db->where('IDsafe_user', $safe_user_id);
	// 	$res = $this->db->get($this->safe_user);
	// 	return $res->row();
	// }

	// public function safe_personal_info($data) {
	// 	$r = $this->get_account_info($data->safe_user_id);
	// 	$personal = array(
	// 		'fullname' => $r->firstname." ".$r->middlename." ".$r->lastname,
	// 		'dateOfBirth' => $data->datebirth,
	// 		'bloodType' => $data->bloodtype,
	// 		'sex' => $data->sex,
	// 		'IDsafe_user' => $data->safe_user_id
	// 	);
	// 	$res = $this->db->insert('safe_personalinfo', $personal);
	// 	return $res;
	// }

	// public function safe_addresses($data) {
	// 	$address = array(
	// 		'street' => $data->street,
	// 		'town' => $data->town,
	// 		'city' => $data->city,
	// 		'country' => $data->country,
	// 		'postCode' => $data->postcode,
	// 		'IDsafe_user' => $data->safe_user_id
	// 	);
	// 	$res = $this->db->insert('safe_addresses', $address);
	// 	return $res;
	// }

}

?>