<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safeuser_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->safe_user = "safe_user";
		$this->safe_addresses = "safe_addresses";
		$this->safe_useremails = "safe_useremails";
		$this->changePassReq = "change_password_request";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->safe_userID = '';
		$this->personalInfoID = '';
		$this->errorMessage = array();
		$this->safe_useremailsInfo = array();
		$this->safe_userInfo = array();
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->requestip = $_SERVER['REMOTE_ADDR'];
	}

	public function forgotPassword($email)
	{
		$code = substr(sha1(rand()),0,10);

		$found = false;
		$findThis = $this->nagkamoritsing->useForSearch($email);
		$password = '';
		$return = array(
			'username' => '',
	 		'code' => '',
	 		'requestdate' => '',
	 		'requestip' => '',
	 		'firstname' => '',
			'middlename' => '',
			'lastname' => '',
			'verified' => ''
		);

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == false)
		 	{
		 		$this->db->like('emailAddress', $find);
		 		$this->db->where('primary', true);
		 		$this->db->where('active', true);
		 		$res = $this->db->get($this->safe_useremails);
	 		 	if($res->num_rows() == 1)
			 	{
			 		$this->safe_useremailsInfo = $res->row();
			 		$found = true;
			 	}
		 	}
	 	}
	 	if (!empty($this->safe_useremailsInfo->IDsafe_userEmails))
	 	{
	 		$this->db->where('IDsafe_user', $this->safe_useremailsInfo->IDsafe_user);
		 	$this->safe_userInfo = $this->db->get($this->safe_user);
		 	$this->safe_userInfo = $this->safe_userInfo->row();

		 	$return = array(
		 		'username' => $this->safe_userInfo->username,
		 		'code' => $code,
		 		'requestdate' => $this->currentDate,
		 		'requestip' => $this->requestip,
		 		'firstname' => @$this->safe_userInfo->firstname,
				'middlename' => @$this->safe_userInfo->middlename,
				'lastname' => @$this->safe_userInfo->lastname,
				'verified' => @$this->safe_userInfo->verified,
				'IDsafe_user' =>$this->safe_useremailsInfo->IDsafe_user
		 	);

		 	$setChangeData = array(
		 		'active' => false
		 	);

		 	# below restrics un-verified users
		 	if ($this->safe_userInfo->verified == true)
		 	{
		 		$this->db->where('IDsafe_user',$this->safe_useremailsInfo->IDsafe_user);
		 		$this->db->where('active', true);
		 		$this->db->update($this->changePassReq, $setChangeData);


			 	$setChangeData = array(
			 		'active' => true,
			 		'requestdate' => $this->currentDate,
			 		'requestip' => $this->requestip,
			 		'code' => $code,
			 		'IDsafe_user' => $this->safe_useremailsInfo->IDsafe_user
			 	);

			 	$this->db->insert($this->changePassReq,$setChangeData);
		 	}
		 	# above restrics un-verified users
	 	}

	 	return $return;
	}

	public function changePassword($IDsafe_user, $newPassword, $code)
	{
		// if (!empty($this->safe_useremailsInfo->IDsafe_userEmails))
	 // 	{
	 // 		if (empty($secret_key))
		// 	{
		// 		$secret_key = "GODrules!";
		// 	}
		// 	$password = sha1(md5($password."".$secret_key));
		// 	$newPasswordData = array(
		//  		'password' => $password
		// 	);
		//  	$this->db->where('IDsafe_user', $this->safe_useremailsInfo->IDsafe_userEmails);
		//  	$this->db->update($this->safe_user, $newPasswordData);
	 // 	}

	 	
	}

	public function getSafeUserEmailsInfo($IDsafe_user = NULL)
	{	
		$return = '';
		if (!empty($this->safe_useremailsInfo->IDsafe_user))
		{
			$return = $this->safe_useremailsInfo;
		} else
		{
			if (empty($IDsafe_user))
			{
				$IDsafe_user = $this->safe_userID;
			}

			$this->db->where('IDsafe_user', $IDsafe_user);
			$this->db->where('active', true);
			$this->db->where('primary', true);
			$res = $this->db->get($this->safe_useremails);

			if ($res->num_rows() == 1)
			{
				$this->safe_useremailsInfo = $res->row();
				$return = $this->safe_useremailsInfo;
			}
		}
		return $return;
	}

	public function searchUsingEmailUsername($emailOrUsername)
	{
		$found = false;
		$this->db
		->select('safeuser.IDsafe_user as IDsafe_user, safeuser.firstname as firstname, safeuser.username as username, safeuser.lastname as lastname, safeuser.middlename as middlename, safeuseremails.emailAddress as emailAddress')
  		->from('safe_user AS safeuser')
  		->join('safe_useremails AS safeuseremails', 'safeuseremails.IDsafe_user = safeuser.IDsafe_user', 'LEFT');
  		$this->db->group_by('safeuser.IDsafe_user');
  		
  		$findThis = $this->nagkamoritsing->useForSearch($emailOrUsername);
		foreach ($findThis as $find) 
		{
			if ($found == false)
		 	{
		 		$this->db->or_like('safeuseremails.emailAddress', $find);
  				$this->db->or_like('safeuser.username', $find);
		 	}
		}
		$dbResult = $this->db->get()->result();		

  		return $dbResult;
	}

	public function getSafeUserInfo($IDsafe_user)
	{
		$return = false;
		if (!empty($IDsafe_user))
		{
			$IDsafe_user = $this->nagkamoritsing->ibalik($IDsafe_user);
			$this->db->from("safe_user");
			$this->db->where('IDsafe_user', $IDsafe_user);
			$res = $this->db->get();
			if ($res->num_rows() >= 1 )
			{
				$return = $res->row();
			}
		}

		return $return;
	}

	public function getSafeUserInfoUsingPersonalInfo($IDsafe_personalInfo)
	{
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->from('safe_personalinfo AS personal');
		$this->db->join('safe_user AS safeuser', 'safeuser.IDsafe_user = personal.IDsafe_user');
		$result = $this->db->get()->row();

		return $result;
	}

	public function checkPasswordSet($IDsafe_user)
	{
		$where = "(password='0c2e40f18b1065e67a09cb4077ffe91eb3290432' OR password='')";
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where($where);
		$this->db->from('safe_user');
		$passwordCheck = $this->db->get();

		if ($passwordCheck->num_rows() >= 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function showAllUsers($beginLimit = 0, $endLimt = 1000)
	{
		$this->db->select('user.IDsafe_user, user.username, user.firstname, user.lastname, user.verified, user.active, emails.emailAddress');
		$this->db->from('safe_user AS user');
		$this->db->join('safe_useremails AS emails', 'emails.IDsafe_user = user.IDsafe_user');
		$this->db->where('emails.active', TRUE);
		$this->db->where('emails.primary', TRUE);
		$this->db->limit($endLimt, $beginLimit);
		$result = $this->db->get()->result();

		return $result;
	}

	public function disableuser($IDsafe_user)
	{
		$updateData = array(
			'active' => FALSE
		);
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->update('safe_user',$updateData);
	}

	public function activateuser($IDsafe_user)
	{

		$updateData = array(
			'active' => TRUE
		);
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->update('safe_user',$updateData);
	}

	public function checkIfVerified($email)
	{

	}
}

?>