<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NonNambal_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('safeuser_model','safeusermodel');
		$this->load->model('signup_model','signupmodel');
	}
	public function addPatient($data) 
	{
		# check email exist, if exist don't continue adding
		# or find way first
		$insertArray = array(
			'username' => $this->nagkamoritsing->bungkag(@$data['username']),
			'firstname' => $this->nagkamoritsing->bungkag(@$data['firstname']),
			'middlename' => $this->nagkamoritsing->bungkag(@$data['middlename']),
			'lastname' => $this->nagkamoritsing->bungkag(@$data['lastname']),
			'password' => '',
			'verified' => FALSE,
			'active' => TRUE,
			'firsttime' => TRUE,
			'donesettings' => FALSE
		);

		try
		{
			$this->db->insert('safe_user', $insertArray);
		}
		catch (Exception $e)
		{
			
		}

		$IDsafe_user = $this->db->insert_id();

		$fullname = $data['firstname'].' '.$data['middlename'].' '.$data['lastname'];

		$insertArray = array(
			'IDsafe_user' => $IDsafe_user,
			'fullname' => $fullname,
			'default' => TRUE,
			'sex' => $data['sex'],
			'dateofbirth' => $data['dateofbirth'],
			'bloodtype' => $data['bloodtype'],
			'active' => TRUE
		);
		$this->db->insert('safe_personalinfo', $insertArray);
		$IDsafe_personalInfo = $this->db->insert_id();

		$insertArray = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'street' => $data['street'],
			'state' => $data['state'],
			'city' => $data['city'],
			'country' => $data['country'],
			'postcode' => $data['postcode'],
			'default' => TRUE,
			'active' => TRUE
		);
		$this->db->insert('safe_addresses', $insertArray);

		$validationCode = sha1(md5(@$data['emailaddress']).'edisonguapo'.$this->currentDate);
		$insertArray = array(
			'vcode' => $validationCode,
			'datetime_generated' => $this->currentDate,
			'active' => TRUE,
			'IDsafe_user' => $IDsafe_user
		);
		$this->db->insert('safe_verification', $insertArray);

		// return IDsafe_personalInfo, vcode, IDsafe_user
		$returnArray = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'vcode' => $validationCode,
			'IDsafe_user' => $IDsafe_user
		);

		return $returnArray;
	}

	public function checkEmailExist($email)
	{
		$searchUsingEmailUsername = $this->safeusermodel->searchUsingEmailUsername($email);
		if (!empty($searchUsingEmailUsername))
			return TRUE;
		else
		{
			$searchUsingEmailUsername = $this->signupmodel->check_email($email);
			if (!empty($searchUsingEmailUsername))
				return TRUE;
			else
				return FALSE;
		}

	}

	public function addPatientEmail($data)
	{
		$insertArray = array(
			'emailAddress' => $this->nagkamoritsing->bungkag(@$data['emailaddress']),
			'IDsafe_user' => $data['IDsafe_user'],
			'active' => TRUE,
			'primary' => TRUE
		);
		$this->db->insert('safe_useremails', $insertArray);

		return;
	}

	public function addPatientQr($data)
	{
		$insertArray = array(
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'filename' => $data['filename'],
			'current' => $data['current'],
			'dateCreated' => $data['dateCreated'],
			'textcode' => $data['textcode']
		);
		$this->db->insert('safe_qr', $insertArray);

		return;
	}

	public function addContact($data)
	{
		$insertArray = array (
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'fullname' => $data['contactfullname'],
			'active' => TRUE,
			'primary' => @$data['primaryni']
		);
		$this->db->insert('safe_contactinformation', $insertArray);
		$IDsafe_contactinformation = $this->db->insert_id();
		$dataSent['IDsafe_contactinformation'] = $IDsafe_contactinformation;

		if (!empty($data['contactemail']))
		{
			$dataSent['contactemail'] = $data['contactemail'];
			$IDcontactinfo_email = $this->addContactEmail($dataSent);
		}
		if (!empty($data['contactfulladdress']))
		{
			$dataSent['fulladdress'] = $data['contactfulladdress'];
			$IDcontactinfo_address = $this->addContactAddress($dataSent);
		}
		if (!empty($data['contactnumbers']))
		{
			$dataSent['phonenumber'] = $data['contactnumbers'];
			$dataSent['areacode'] =  $data['contactAreacode'];
			$dataSent['countrycode'] = $data['countrycode'];
			$contactNumber = $this->addContactNumbers($dataSent);
		}

		$returnArray = array(
			'IDsafe_contactinformation' => $IDsafe_contactinformation,
			'IDcontactinfo_email' => @$IDcontactinfo_email,
			'IDcontactinfo_address' => @$IDcontactinfo_address,
			'IDcontactinfo_numbers' => @$contactNumber
		);

		return $returnArray;
	}

	public function addContactEmail($data)
	{
		$insertArray = array(
			'email' => $data['contactemail'],
			'current' => TRUE,
			'IDsafe_contactinformation' => $data['IDsafe_contactinformation']
		);
		$this->db->insert('contactinfo_email', $insertArray);


		return $this->db->insert_id();
	}

	public function addContactAddress($data)
	{
		$insertArray = array(
			'IDsafe_contactinformation' => $data['IDsafe_contactinformation'],
			'fulladdress' => $data['fulladdress'],
			'current' => TRUE
		);
		$this->db->insert('contactinfo_address', $insertArray);

		return $this->db->insert_id();
	}

	public function addContactNumbers($data)
	{
		$insertArray = array (
			'phonenumber' => $data['phonenumber'],
			'areacode' => $data['areacode'],
			'countrycode' => $data['countrycode'],
			'IDsafe_contactinformation' => $data['IDsafe_contactinformation'],
			'current' => TRUE,
			'active' => TRUE
		);
		$this->db->insert('contactinfo_numbers', $insertArray);

		return $this->db->insert_id();
	}

	public function addBarangay($data)
	{
		if (!empty($data['idbarangaylist']))
		{
			$insertArray = array(
				'safe_personalinfo_IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
				'barangaylist_idbarangaylist' => $data['idbarangaylist'],
				'active' => TRUE
			);
			$this->db->insert('safe_barangay', $insertArray);

			return $this->db->insert_id();
		}
		else
			return;
	}

	public function addParent($data)
	{
		$insertArray = array(
			'gender' => @$data['parentGender'],
			'fullname' => @$data['parentName'],
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo'],
			'parents_IDsafe_user' => @$data['parentSafeID']
		);
		$this->db->insert('safe_parents', $insertArray);

		return $this->db->insert_id();
	}

	public function doctorAdder($data)
	{
		$insertArray = array(
			'IDemr_user' => $data['IDemr_user'],
			'IDsafe_personalInfo' => $data['IDsafe_personalInfo']
		);
		$this->db->insert('emr_patient', $insertArray);

		return $this->db->insert_id();
	}
}
?>