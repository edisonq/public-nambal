<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emruser_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->emruser = "emr_user";
		$this->emrspecialtieshasemruser = "emr_specialties_has_emr_user";
		$this->emrspecialties = 'emr_specialties';
		$this->recommendation = 'emr_recommendation';
		$this->emraddresses = 'emr_addresses';
		$this->emrapprover = 'emr_approver';
		$this->emrclinichours = 'emr_clinichours';
		$this->safeuseremails = ' safe_useremails';
		$this->safeuser = 'safe_user';
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function getDocInfo($IDsafe_user)
	{
		$docInfo = array();

		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$res = $this->db->get($this->emruser);
	 	if($res->num_rows() >= 1)
		{
			$docInfo = $res->row();
	 		
	 	}

		return $docInfo;
	}
	
	public function getDoctorId($IDsafe_user = 0)
	{
		$docId = 0;

		$this->db->select('IDemr_user');
		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$docId = $this->db->get($this->emruser);
	 	
		return $docId->row();
	}

	public function checkIfDoctor($IDsafe_user)
	{
		$return = FALSE;
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('approve', TRUE);
		$this->db->where("active", TRUE);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}

	public function checkIfDoctorDisabled($IDsafe_user)
	{
		$return = FALSE;
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where("active", FALSE);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}

	public function insertDocInfo($data)
	{
		if (!empty($data))
		{
			# check if the user is already registered as doctor but not active
			if (!$this->checkIfDoctorDisabled($data['IDsafe_user']))
			{
				$dataEmrUser = array(
					'rxId' => $data['rxId'],
					'licenseNumber' => $data['licenseNumber'],
					'IDsafe_user' => $data['IDsafe_user'],
					'approve' => FALSE,
					'active' => TRUE
				);
				$idReturn = $this->db->insert($this->emruser,$dataEmrUser);
				$IDemr_user = $this->db->insert_id();
				$specialty = $data['specialty'];

				foreach ($specialty as $spec) 
				{
					$dataEmrSpecialty = array(
						'specialty_id' => $spec,
						'IDemr_user' => $IDemr_user
					);
					$idReturn = $this->db->insert($this->emrspecialtieshasemruser,$dataEmrSpecialty);
				}
				return $IDemr_user;
			}
			else
			{
				return FALSE;
			}
			# end of checking if the user is already registered as doctor but not active (disabled)
		}	
		else
		{
			return FALSE;
		}
	}

	public function addDoctorApprover($IDemr_user, $email)
	{
		$IDsafe_user = '';
		$return = false;
		$found = false;

		$findThis = $this->nagkamoritsing->useForSearch($email);
	 	
	 	foreach ($findThis as $find) 
	 	{
	 		if ($found == false)
		 	{
		 		$this->db->like('emailAddress', $find);
		 		$this->db->where('active', TRUE);
		 		$res = $this->db->get($this->safeuseremails);
	 		 	if($res->num_rows() == 1)
			 	{
			 		$found = true;
			 		$return = TRUE;
			 		$IDsafe_user = $res->row()->IDsafe_user;
			 	}
		 	}	
	 	}

		$dataApprove = array(
				'IDsafe_user' => $IDsafe_user,
				'IDemr_user' => $IDemr_user,
				'approved' => FALSE,
				'dateRequest' => $this->currentDate,
				'emailAddress' => $email
		);

		$this->db->insert($this->emrapprover, $dataApprove);
	}

	public function approveDoctor($emr_user)
	{
		$this->db->where('IDemr_user', $emr_user);
		$dataChange = array (
			'approve' => TRUE
		);
		$this->db->update($this->emruser,$dataChange);
		return;
	}

	public function getMainEmail ($IDsafe_user)
	{
		$return = '';
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('active', TRUE);
		$this->db->where('primary', TRUE);
		$ret = $this->db->get($this->safeuseremails);
		if ($ret->num_rows() >= 1)
		{
			$return = $ret->row()->emailAddress;
		}
		return $return;
	}

	public function isAlreadyRequesting($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		// $this->db->where('approve', FALSE);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->emruser);
		if($ret->num_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function isAlreadyApprove($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('approve', TRUE);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->emruser);
		if($ret->num_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function search($specialty, $country, $state, $countFirst = NULL, $countLast = NULL)
	{
		$array = array();
		$arrayRes = array();
		$arrayGen = array();
		$arraySpecialty = array();
		$arrayAddress = array();
		$arrayCount = 0;

		$this->db
		->select('safeuser.IDsafe_user, safeuser.firstname, safeuser.lastname, safeuser.middlename, emruser.rxId, emruser.licenseNumber, emruser.IDemr_user')
  		->from('emr_user AS emruser')
  		->join('emr_specialties_has_emr_user AS specialhasemr', 'specialhasemr.IDemr_user = emruser.IDemr_user')
  		->join('emr_specialties AS emrspecialties', 'emrspecialties.specialty_id = specialhasemr.specialty_id')
  		->join('safe_personalinfo AS safepersonalinfo', 'safepersonalinfo.IDsafe_user = emruser.IDsafe_user')
  		->join('safe_addresses AS safeaddress', 'safeaddress.IDsafe_personalInfo = safepersonalinfo.IDsafe_personalInfo')
  		->join('emr_addresses_has_emr_user AS emrAddHemrUser', 'emrAddHemrUser.emr_user_IDemr_user = emruser.IDemr_user')
  		->join('emr_addresses AS emraddress', 'emraddress.IDemr_addresses = emrAddHemrUser.emr_addresses_IDemr_addresses')
  		// ->join('emr_addresses AS emraddress', 'emraddress.IDemr_user = emruser.IDemr_user', 'LEFT')
  		// ->join('emr_recommendation AS emrrecommendation', 'emraddress.IDemr_user = emruser.IDemr_user')
  		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user');
  		$this->db->group_by('safeuser.IDsafe_user');
  		$this->db->where('emruser.approve', TRUE);
  		$this->db->where('emruser.active', TRUE);
  		// $this->db->like('emruser.approve', TRUE);
  		// $this->db->like('emruser.active', TRUE);
  		# sort by feedback/points/recommendation -- next sprint
  		// $this->db->order_by('count(emrrecommendation.IDemr_user) DESC');
  		

  		if (!empty($country))
  		{
  			$anotherSql= '(emraddress.country="'.$country.'" OR safeaddress.country="'.$country.'")';
  			$this->db->where($anotherSql);
  		}
  		if (!empty($state))
  		{
  			$anotherSql = '(emraddress.state="'.$state.'" OR safeaddress.state="'.$state.'")';
  			$this->db->where($anotherSql);
  		}

  		if (!empty($specialty))
  		{
	  		$arrayCount = count($specialty);
	  		$anotherSql = '(';
	  		foreach ($specialty as $specs) 
	  		{
	  			$anotherSql .= 'specialhasemr.specialty_id='.$specs;
	  			$arrayCount--;
	  			if ($arrayCount == 0)
	  			{
	  				$anotherSql .= ')';
	  			}
	  			else
	  			{
	  				$anotherSql .= ' OR ';
	  			}
	  		}
	  		$this->db->where($anotherSql);
	  	}

	  	$arrayRes = $this->db->get()->result();

	  	
	  	foreach($arrayRes as $res)
	  	{
	  		# getting the clinic and personal address
	  		// $this->db->where('IDemr_user', $res->IDemr_user);
	  		
		  	// if (!empty($country))
		  	// {
		  	// 	$this->db->where('country', $country);
		  	// }
		  	// if (!empty($state))
		  	// {
		  	// 	$this->db->where('state', $state);
		  	// }
		  	// $arrayAddress = $this->db->get($this->emraddresses)->result();
		  	# end of getting the clinic and personal address

		  	# getting the specialty
	  		$this->db->select('emrspecialties.specialtyName')
	  		->from('emr_user AS emruser')
	  		->join('emr_specialties_has_emr_user AS specialhasemr','specialhasemr.IDemr_user = emruser.IDemr_user')
	  		->join('emr_specialties AS emrspecialties','emrspecialties.specialty_id = specialhasemr.specialty_id')
	  		->where('emruser.IDemr_user',$res->IDemr_user);
	  		$arraySpecialty = $this->db->get()->result();
	  		# end of getting the specialty

	  		# getting how many recommended
	  		$this->db->where('IDemr_user', $res->IDemr_user);
	  		$this->db->where('recommend', TRUE);
	  		$countRecommend = $this->db->get($this->recommendation)->num_rows();
	  		# end of getting how many recommended

		  	$array = array(
		  		'doctorInfo' => $res,
		  		'clinicAddress' => $arrayAddress,
		  		'specialty' => $arraySpecialty,
		  		'countRecommend' => $countRecommend
		  		);
		  	array_push($arrayGen, $array);
	  	}
	  	
  		# output the SQL statement
  		

		return $arrayGen;
	}
	
	public function checkFirstTime($IDsafe_user = NULL)
	{
		$return = false;
		
		if(!empty($IDsafe_user))
		{
			$this->db->where('IDsafe_user', $IDsafe_user);
			$this->db->where('firsttime', TRUE);
			$res = $this->db->get($this->emruser);
			if($res->num_rows() == 1)
			{
				$return = true;
			}
		}
		
		return $return;
	}
	
	public function updateAfterTour($IDsafe_user = 0)
	{
		$return = false;
		
		#chek if doctor
		if($this->checkIfDoctor($IDsafe_user))
		{
			$data = array(
				'firsttime' => 0
			);
			$this->db->where('IDsafe_user', $IDsafe_user);
			if($this->db->update($this->emruser,$data))
			{
				$return = true;
			}
		}
		return $return;
	}

	public function getDocIDsafeUser($IDemr_user)
	{
		$this->db->select('IDsafe_user');
		$this->db->where('IDemr_user', $IDemr_user);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			return $ret->row();
		}
		else
		{
			return false;
		}
	}

	public function createClinic($IDsafe_user,$data)
	{
		
	}
	
	public function doctorHasClinic($IDemr_user)
	{
		// $return = false;
			
		// $doctorId = $this->getDoctorId($IDsafe_user);
		// $this->db->where('IDsafe_user', $IDsafe_user);
		// $this->db->where('approve', TRUE);
		// $this->db->where("active", TRUE);
		// $ret = $this->db->get($this->emruser);
		// if ($ret->num_rows() >= 1)
		// {
			// $return = TRUE;
		// }
		// return $return;
	}
	
	public function getStaffAdministrationList($IDsafe_user = 0)
	{
		// $return = array();

		// #chek if doctor
		// if($this->checkIfDoctor($IDsafe_user))
		// {
			
		// }
	}
}
?>