<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barangaylist_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_barangay = "safe_barangay";
		$this->barangaylist = "barangaylist";
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function getAllBarangay()
	{
		$this->db->from($this->barangaylist);
		return $this->db->get()->result();
	}

	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('barangaylist');
		$this->db->where('active', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('idbarangaylist');
			$this->db->like('barangayName', $keyword, 'none');
			// $this->db->like('street', $keyword, 'none');
			// $this->db->like('city', $keyword, 'none');
			// $this->db->like('country', $keyword, 'none');
			// $this->db->like('postcode', $keyword, 'none');
			// $this->db->like('notes', $keyword, 'none');
		}
		else
		{
			$this->db->select('barangayName');
			$this->db->like('barangayName', $keyword, 'both');
			// $this->db->like('street', $keyword, 'both');
			// $this->db->like('city', $keyword, 'both');
			// $this->db->like('country', $keyword, 'both');
			// $this->db->like('postcode', $keyword, 'both');
			// $this->db->like('notes', $keyword, 'both');
			// $this->db->or_where('city',$keyword, 'both');

		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->barangayName);
			}
		}
		else
		{
			$arrayReturn = @$result->row();
			$arrayReturn = @$arrayReturn->IDdrugList;
		}
		
		return $arrayReturn;
	}

	public function getBarangayInfoString($string)
	{
		$this->db->from('barangaylist');
		$this->db->where('active', TRUE);
		$this->db->like('barangayName', $string, 'none');
		return $this->db->get()->row();
	}
}

?>