<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emruser_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->emruser = "emr_user";
		$this->emrspecialtieshasemruser = "emr_specialties_has_emr_user";
		$this->emraddresshasemruser = "emr_addresses_has_emr_user";
		$this->emrspecialties = 'emr_specialties';
		$this->recommendation = 'emr_recommendation';
		$this->emraddresses = 'emr_addresses';
		$this->emrapprover = 'emr_approver';
		$this->emrclinichours = 'emr_clinichours';
		$this->safeuseremails = 'safe_useremails';
		$this->licenseinfo = 'licenseinfo';
		$this->safeuser = 'safe_user';
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function getDocInfo($IDsafe_user)
	{
		$docInfo = array();

		$this->db->where('IDsafe_user', $IDsafe_user);
		
	 	$res = $this->db->get($this->emruser);
	 	if($res->num_rows() >= 1)
		{
			$docInfo = $res->row();
	 		
	 	}

		return $docInfo;
	}

	public function showAllDoctors($beginLimit = 0, $endLimt = 1000)
	{
		$this->db->select('IDemr_user, doctorname.IDsafe_user, licenseNumber, rxId, username, firstname, lastname, doctor.approve, doctor.active, doctor.firsttime');
		$this->db->from('emr_user AS doctor');
		$this->db->join('safe_user AS doctorname', 'doctorname.IDsafe_user = doctor.IDsafe_user');
		$this->db->limit($endLimt, $beginLimit);
		$result = $this->db->get()->result();

		return $result;
	}

	public function disabledoctor($IDemr_user)
	{
		$updateData = array(
			'approve' => FALSE
		);
		$this->db->where('IDemr_user', $IDemr_user);
		$this->db->update('emr_user',$updateData);
	}

	public function activatedoctor($IDemr_user)
	{

		$updateData = array(
			'approve' => TRUE
		);
		$this->db->where('IDemr_user', $IDemr_user);
		$this->db->update('emr_user',$updateData);
	}

	public function getLicense($IDemr_user)
	{
		$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$this->db->where('active', TRUE);
		$license = $this->db->get($this->licenseinfo);

		return $license->row();
	}

	public function getDocInfoComplete($IDsafe_user)
	{
		$docInfo = array();

		$this->db
			->select('IDemr_user,rxId, licenseNumber, approve, emruser.active, emruser.firsttime, firstname, lastname, middlename, verified, emruser.active, ')
			->from('emr_user AS emruser')
			->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user');

		$this->db->where('emruser.IDsafe_user', $IDsafe_user);
		$this->db->where('emruser.approve', TRUE);
		$this->db->where('emruser.active', TRUE);
		$docInfo = $this->db->get()->result();


		return $docInfo;
	}

	public function getDocSpecialty($IDemr_user)
	{
		$specialtyInfo = array();

		$this->db
		->select('emrspecialties.specialtyName, emrspecialties.specialty_id')
		->from('emr_specialties_has_emr_user AS emrspecialtieshasemruser')
		->join('emr_user AS emruser','emruser.IDemr_user = emrspecialtieshasemruser.IDemr_user')
		->join('emr_specialties AS emrspecialties', 'emrspecialties.specialty_id = emrspecialtieshasemruser.specialty_id')
		->where('emruser.IDemr_user', $IDemr_user);
		$specialtyInfo = $this->db->get()->result();

		return $specialtyInfo;
	}

	public function getAppointmentType($IDemr_user)
	{
		$typeAppointmentInfo = array();

		$this->db
		->select('typeName, IDappointment_type')
		->from('emr_actual_schedule AS emractualSked')
		->join('appointments_settings AS appSettings', 'appSettings.IDappointments_settings = emractualSked.appointments_settings_IDappointments_settings')
		->join('appointment_type AS appType', 'appType.IDappointment_type = emractualSked.appointment_type_IDappointment_type')
		->where('appSettings.emr_user_IDemr_user', $IDemr_user)
		->where('appType.active', TRUE)
		->where('emractualSked.active', TRUE)
		->where('appointable', TRUE)
		->distinct();

		$typeAppointmentInfo = $this->db->get()->result();


		return $typeAppointmentInfo;
	}

	public function getAppointmentSlot($IDemr_user)
	{
		$appointmentInfo = array();

		$this->db
		->select('timefrom, timeto, day, appointable,typeName, averagetimeperpatient_min, appointment_limit, queue_limit, emr_user_IDemr_user AS IDemr_user, IDappointment_type,IDlocation_info,locationname, locationtype')
		->from('emr_actual_schedule as actual')
		->join('appointment_type AS type', 'type.IDappointment_type = actual.appointment_type_IDappointment_type')
		->join('appointments_settings AS settings', 'settings.IDappointments_settings = appointments_settings_IDappointments_settings')
		->join('location_info AS location','location.appointments_settings_IDappointments_settings = settings.IDappointments_settings')
		->where('settings.emr_user_IDemr_user', $IDemr_user)
		->where('actual.active', TRUE)
		->where('location.active', TRUE)
		->where('actual.appointable', TRUE)
		->where('type.approved', TRUE)
		->where('type.active', TRUE);

		$appointmentInfo = $this->db->get()->result();

		if (empty($appointmentInfo))
		{
			$this->db
			->select('timefrom, timeto, day, appointable,typeName, averagetimeperpatient_min, appointment_limit, queue_limit, emr_user_IDemr_user AS IDemr_user, IDappointment_type')
			->from('emr_actual_schedule as actual')
			->join('appointment_type AS type', 'type.IDappointment_type = actual.appointment_type_IDappointment_type')
			->join('appointments_settings AS settings', 'settings.IDappointments_settings = appointments_settings_IDappointments_settings')
			->where('settings.emr_user_IDemr_user', $IDemr_user)
			->where('actual.active', TRUE)
			->where('actual.appointable', TRUE)
			->where('type.approved', TRUE)
			->where('type.active', TRUE);

			$appointmentInfo = $this->db->get()->result();
		}

		return $appointmentInfo;
	}
	
	public function getDoctorId($IDsafe_user = 0)
	{
		$docId = 0;

		$this->db->select('IDemr_user');
		$this->db->where('IDsafe_user', $IDsafe_user);
	 	$docId = $this->db->get($this->emruser);
	 	
		return $docId->row();
	}

	public function checkIfDoctor($IDsafe_user)
	{
		$return = FALSE;
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('approve', TRUE);
		$this->db->where("active", TRUE);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}

	public function checkIfDoctorDisabled($IDsafe_user)
	{
		$return = FALSE;
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where("active", FALSE);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			$return = TRUE;
		}
		return $return;
	}

	public function insertDocInfo($data)
	{
		if (!empty($data))
		{
			# check if the user is already registered as doctor but not active
			if (!$this->checkIfDoctorDisabled($data['IDsafe_user']))
			{
				$dataEmrUser = array(
					'rxId' => $data['rxId'],
					'licenseNumber' => $data['licenseNumber'],
					'IDsafe_user' => $data['IDsafe_user'],
					'approve' => FALSE,
					'active' => TRUE
				);
				$idReturn = $this->db->insert($this->emruser,$dataEmrUser);
				$IDemr_user = $this->db->insert_id();
				$specialty = $data['specialty'];

				foreach ($specialty as $spec) 
				{
					$dataEmrSpecialty = array(
						'specialty_id' => $spec,
						'IDemr_user' => $IDemr_user
					);
					$idReturn = $this->db->insert($this->emrspecialtieshasemruser,$dataEmrSpecialty);
				}
				return $IDemr_user;
			}
			else
			{
				return FALSE;
			}
			# end of checking if the user is already registered as doctor but not active (disabled)
		}	
		else
		{
			return FALSE;
		}
	}

	public function insertSpecialty($data)
	{
		foreach ($data['specialty_id'] as $spec) 
		{
			# check if existing
			$this->db->from('emr_specialties_has_emr_user');
			$this->db->where('specialty_id', $spec);
			$this->db->where('IDemr_user', $data['IDemr_user']);
			$result = $this->db->get()->num_rows();
			
			if ($result <= 0)
			{
			# end of checking if existing
				$dataEmrSpecialty = array(
					'specialty_id' => $spec,
					'IDemr_user' => $data['IDemr_user']
				);
				$idReturn = $this->db->insert($this->emrspecialtieshasemruser,$dataEmrSpecialty);
			}
		}
	}

	public function addDoctorApprover($IDemr_user, $email)
	{
		$IDsafe_user = '';
		$return = false;
		$found = false;

		$findThis = $this->nagkamoritsing->useForSearch($email);
	 	
	 	foreach ($findThis as $find) 
	 	{
	 		if ($found == false)
		 	{
		 		$this->db->like('emailAddress', $find);
		 		$this->db->where('active', TRUE);
		 		$res = $this->db->get($this->safeuseremails);
	 		 	if($res->num_rows() == 1)
			 	{
			 		$found = true;
			 		$return = TRUE;
			 		$IDsafe_user = $res->row()->IDsafe_user;
			 	}
		 	}	
	 	}

		$dataApprove = array(
				'IDsafe_user' => $IDsafe_user,
				'IDemr_user' => $IDemr_user,
				'approved' => FALSE,
				'dateRequest' => $this->currentDate,
				'emailAddress' => $email
		);

		$this->db->insert($this->emrapprover, $dataApprove);
	}

	public function approveDoctor($emr_user)
	{
		$this->db->where('IDemr_user', $emr_user);
		$dataChange = array (
			'approve' => TRUE
		);
		$this->db->update($this->emruser,$dataChange);
		return;
	}

	public function getMainEmail ($IDsafe_user)
	{
		$return = '';
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('active', TRUE);
		$this->db->where('primary', TRUE);
		$ret = $this->db->get($this->safeuseremails);
		if ($ret->num_rows() >= 1)
		{
			$return = $ret->row()->emailAddress;
		}
		return $return;
	}

	public function isAlreadyRequesting($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		// $this->db->where('approve', FALSE);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->emruser);
		if($ret->num_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function isAlreadyApprove($IDsafe_user)
	{
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->where('approve', TRUE);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->emruser);
		if($ret->num_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function search($specialty, $country, $state, $sortRecommendation = NULL, $sortPopularChat = NULL, $setPopularAppointment = NULL, $page = NULL)
	{
		$array = array();
		$arrayRes = array();
		$arrayGen = array();
		$arraySpecialty = array();
		$arrayAddress = array();
		$arrayCount = 0;

		# problem need to be solve
		# if the doctor has many clinic address or personal address, the sorting is inaccurate giving doctor having more than 1 address an advantage
		# if the doctor has many personal information , the sorting is inaccuarate
		# if the doctor has many specialty, the sorting is inaccurate giving advatange to doctors who have many.

		$this->db
		->select('safeuser.IDsafe_user, safeuser.firstname, safeuser.lastname, safeuser.middlename, emruser.rxId, emruser.licenseNumber, emruser.IDemr_user')
  		->from('emr_user AS emruser');

  		if (!empty($specialty))
  		{
			$this->db
			->join('emr_specialties_has_emr_user AS specialhasemr', 'specialhasemr.IDemr_user = emruser.IDemr_user', 'right')
			->join('emr_specialties AS emrspecialties', 'emrspecialties.specialty_id = specialhasemr.specialty_id');
  		}

  		$this->db
  		->join('safe_personalinfo AS safepersonalinfo', 'safepersonalinfo.IDsafe_user = emruser.IDsafe_user');

  		if (!empty($country) || !empty($state))
  		{
  			$this->db
  			->join('emr_addresses_has_emr_user AS emrAddHemrUser', 'emrAddHemrUser.emr_user_IDemr_user = emruser.IDemr_user')
  			->join('emr_addresses AS emraddress', 'emraddress.IDemr_addresses = emrAddHemrUser.emr_addresses_IDemr_addresses')
  			->join('safe_addresses AS safeaddress', 'safeaddress.IDsafe_personalInfo = safepersonalinfo.IDsafe_personalInfo');
  		}

  		# sort by feedback/points/recommendation 
  		if ((!empty($sortRecommendation)) && ($sortRecommendation == true))
  		{
  			$this->db->join('emr_recommendation AS emrrecommendation', 'emrrecommendation.IDemr_user = emruser.IDemr_user');
  		}

  		if ((!empty($sortPopularChat)) && ($sortPopularChat == true))
  		{
  			$this->db->join('message_people AS messages', 'messages.from_IDsafe_user = emruser.IDsafe_user');
  		}

  		if ((!empty($setPopularAppointment)) && ($setPopularAppointment == true))
  		{
  			$this->db->join('appointments', 'appointments.emr_user_IDemr_user = emruser.IDemr_user');
  			// $this->db->join('appointment_location as appLocation', 'appLocation.appointments_IDappointments = appointments.IDappointments');
  		}

  		$this->db->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user');
  		$this->db->group_by('safeuser.IDsafe_user');
  		$this->db->where('emruser.approve', TRUE);
  		$this->db->where('emruser.active', TRUE);

  		if ((!empty($sortRecommendation)) && ($sortRecommendation == true))
  		{
  			$this->db->order_by('count(emrrecommendation.IDemr_user) DESC');
  			$this->db->where('emrrecommendation.active', TRUE);
  			$this->db->where('emrrecommendation.approved', TRUE);
  		}

  		if ((!empty($setPopularAppointment)) && ($setPopularAppointment == true))
  		{
  			$this->db->order_by('count(appointments.emr_user_IDemr_user) DESC');
  			$this->db->where('appointments.approved', TRUE);
  			$this->db->where('appointments.active', TRUE);
  		}

  		if ((!empty($sortPopularChat)) && ($sortPopularChat == true))
  		{
  			$this->db->order_by('count(messages.IDmessage_people) DESC');
  			$this->db->where('messages.active', TRUE);
  		}

  		# sort by ID is the least prioritization
  		$this->db->order_by('safeuser.lastname ASC');
  		$this->db->order_by('safeuser.IDsafe_user ASC');
  		

  		#paging here
  		// $this->db->limit($end,$start);
  		# end paging display
  		

  		if (!empty($country))
  		{
  			$anotherSql= '(emraddress.country="'.$country.'" OR safeaddress.country="'.$country.'")';
  			$this->db->where($anotherSql);
  		}
  		if (!empty($state))
  		{
  			$anotherSql = '(emraddress.state="'.$state.'" OR safeaddress.state="'.$state.'")';
  			$this->db->where($anotherSql);
  		}

  		if (!empty($specialty))
  		{
	  		$arrayCount = count($specialty);
	  		$anotherSql = '(';
	  		foreach ($specialty as $specs) 
	  		{
	  			$anotherSql .= 'specialhasemr.specialty_id='.$specs;
	  			$arrayCount--;
	  			if ($arrayCount == 0)
	  			{
	  				$anotherSql .= ')';
	  			}
	  			else
	  			{
	  				$anotherSql .= ' OR ';
	  			}
	  		}
	  		$this->db->where($anotherSql);
	  	}

	  	$arrayRes = $this->db->get()->result();

	  	
	  	foreach($arrayRes as $res)
	  	{
	  		# getting the clinic address
		  	$this->db->select('IDemr_addresses')
		  	->from ('emr_addresses AS emraddresses')
		  	->join ('emr_addresses_has_emr_user AS emrhasuser','emrhasuser.emr_addresses_IDemr_addresses = emraddresses.IDemr_addresses');
		  	$this->db->where('emrhasuser.emr_user_IDemr_user', $res->IDemr_user);

		  	if (!empty($country))
		  	{
		  		$this->db->where('country', $country);
		  	}
		  	if (!empty($state))
		  	{
		  		$this->db->where('state', $state);
		  	}
		  	$addressCount = $this->db->count_all_results();
		  	# end of getting the clinic and personal address

		  	# atotz oi dili k kahibaw unsaon pag kuha sa return and count at same time
		  	# please revise this later
		  	$this->db->select('street, state, city, country, postcode')
		  	->from ('emr_addresses AS emraddresses')
		  	->join ('emr_addresses_has_emr_user AS emrhasuser','emrhasuser.emr_addresses_IDemr_addresses = emraddresses.IDemr_addresses');
		  	$this->db->where('emrhasuser.emr_user_IDemr_user', $res->IDemr_user);

		  	if (!empty($country))
		  	{
		  		$this->db->where('country', $country);
		  	}
		  	if (!empty($state))
		  	{
		  		$this->db->where('state', $state);
		  	}
		  	$AddressRow = $this->db->get()->result();
		  	# end of balik2x nga sql

		  	# getting the specialty
	  		$this->db->select('emrspecialties.specialtyName, emrspecialties.specialty_id')
	  		->from('emr_user AS emruser')
	  		->join('emr_specialties_has_emr_user AS specialhasemr','specialhasemr.IDemr_user = emruser.IDemr_user')
	  		->join('emr_specialties AS emrspecialties','emrspecialties.specialty_id = specialhasemr.specialty_id')
	  		->where('emruser.IDemr_user',$res->IDemr_user);
	  		$arraySpecialty = $this->db->get()->result();
	  		# end of getting the specialty

	  		# getting how many recommended
	  		$this->db->where('IDemr_user', $res->IDemr_user);
	  		$this->db->where('recommend', TRUE);
	  		$countRecommend = $this->db->get($this->recommendation)->num_rows();
	  		# end of getting how many recommended

		  	$array = array(
		  		'doctorInfo' => $res,
		  		'clinicAddress' => $AddressRow,
		  		'clinicCount' => $addressCount,
		  		'specialty' => $arraySpecialty,
		  		'countRecommend' => $countRecommend
		  		);
		  	array_push($arrayGen, $array);
	  	}
	  	
  		# output the SQL statement
  		

		return $arrayGen;
	}
	
	public function checkFirstTime($IDsafe_user = NULL)
	{
		$return = false;
		
		if(!empty($IDsafe_user))
		{
			$this->db->where('IDsafe_user', $IDsafe_user);
			$this->db->where('firsttime', TRUE);
			$res = $this->db->get($this->emruser);
			if($res->num_rows() == 1)
			{
				$return = true;
			}
		}
		
		return $return;
	}
	
	public function updateAfterTour($IDsafe_user = 0)
	{
		$return = false;
		
		#chek if doctor
		if($this->checkIfDoctor($IDsafe_user))
		{
			$data = array(
				'firsttime' => 0
			);
			$this->db->where('IDsafe_user', $IDsafe_user);
			if($this->db->update($this->emruser,$data))
			{
				$return = true;
			}
		}
		return $return;
	}

	public function getDocIDsafeUser($IDemr_user)
	{
		$this->db->select('IDsafe_user');
		$this->db->where('IDemr_user', $IDemr_user);
		$ret = $this->db->get($this->emruser);
		if ($ret->num_rows() >= 1)
		{
			return $ret->row();
		}
		else
		{
			return false;
		}
	}

	public function getDocClinicInfo($IDsafe_user)
	{
		$return = '';
		$returnArray = array();
		$this->db->select('firstname, lastname, street, state, city, country, postcode, location.locationname, website, numberofdoctors, locationtype')
		// ->from('emr_addresses_has_emr_user AS emraddresses_hasemr')
		->from('emr_addresses AS emraddresses')
		// ->join('emr_addresses AS emraddresses', 'emraddresses.IDemr_addresses = emraddresses.emr_addresses_IDemr_addresses')
		->join('clinic_info_has_emr_addresses AS clinicinfohasemraddresses', 'clinicinfohasemraddresses.emr_addresses_IDemr_addresses = emraddresses.IDemr_addresses')
		->join('location_info AS location', 'location.IDlocation_info  = clinicinfohasemraddresses.location_info_IDlocation_info')
		->join('appointments_settings AS settings', 'settings.IDappointments_settings = location.appointments_settings_IDappointments_settings')
		->join('emr_user AS emruser', 'emruser.IDemr_user = settings.emr_user_IDemr_user')
		->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user')
		->where('safeuser.IDsafe_user', $IDsafe_user)
		->where('emraddresses.active', TRUE)
		->where('location.active', TRUE);

		$return = $this->db->get()->result();
		if (!empty($return))
		{
			foreach ($return as $key) 
			{
				$ret = array(
					'street' => $key->street,
					'city' => $key->city,
					'country' => $key->country,
					'postcode' => $key->postcode,
					'locationname' => $key->locationname,
					'website' => $key->website,
					'numberofdoctors' => $key->numberofdoctors,
					'state' => $key->state,
					'locationtype' => $key->locationtype
				);

				array_push($returnArray, $ret);
			}
		}
		else
		{
			$this->db->select('firstname, lastname, street, state, city, country, postcode')
			->from('emr_addresses_has_emr_user AS emraddresses_hasemr')
			->join('emr_addresses AS emraddresses', 'emraddresses.IDemr_addresses = emraddresses_hasemr.emr_addresses_IDemr_addresses')
			->join('emr_user AS emruser', 'emruser.IDemr_user = emraddresses_hasemr.emr_user_IDemr_user')
			->join('safe_user AS safeuser', 'safeuser.IDsafe_user = emruser.IDsafe_user')
			->where('safeuser.IDsafe_user', $IDsafe_user)
			->where('emraddresses.active', TRUE);
			$return = $this->db->get()->result();
			if (!empty($return))
			{
				foreach ($return as $key) 
				{
					$ret = array(
						'street' => $key->street,
						'city' => $key->city,
						'country' => $key->country,
						'postcode' => $key->postcode,
						'locationname' => '',
						'website' => '',
						'numberofdoctors' => '',
						'state' => $key->state,
						'locationtype' => ''
					);

					array_push($returnArray, $ret);
				}
			}
		}

		return $returnArray;
	}

	public function deleteSpecialty($IDspecialty, $IDemr_user)
	{
		$this->db->where('specialty_id', $IDspecialty);
		$this->db->where('IDemr_user', $IDemr_user);
		$this->db->delete($this->emrspecialtieshasemruser);
	}

	public function updateLicense($data)
	{
		$this->db->from('licenseinfo');
		$this->db->where('emr_user_IDemr_user', $data['IDemr_user']);
		$retnumrow = $this->db->get()->num_rows();

		$updateArray = array(
			'licensenumber' => $data['licensenumber'],
			'dateregistered' => $data['dateregistered'],
			'dateexpire' => $data['dateexpire'],
			'active' => TRUE
		);

		if ($retnumrow < 1)
		{
			$updateArray['emr_user_IDemr_user'] = $data['IDemr_user'];
			$this->db->insert($this->licenseinfo, $updateArray);
		}
		else
		{
			$this->db->where('emr_user_IDemr_user', $data['IDemr_user']);
			$this->db->update($this->licenseinfo, $updateArray);
		}
	}

	public function createClinic($IDsafe_user,$data)
	{
		
	}

	public function insertDoctorAddress($data)
	{
		$insertArray = array(
			'street' => $data['street'],
			'state' => $data['state'],
			'city' => $data['city'],
			'country' => $data['country'],
			'postcode' => $data['postcode'],
			'active' => TRUE
		);
		$this->db->insert('emr_addresses', $insertArray);
		$IDemr_addresses = $this->db->insert_id();

		$insertArray = array(
			'emr_addresses_IDemr_addresses' => $IDemr_addresses,
			'emr_user_IDemr_user' => $data['IDemr_user'],
			'active' => TRUE
		);
		$this->db->insert('emr_addresses_has_emr_user', $insertArray);

		return $IDemr_addresses;
	}

	public function updateDoctorAddress($data)
	{
		$updateArray = array(
			'street' => $data['street'],
			'state' => $data['state'],
			'city' => $data['city'],
			'country' => $data['country'],
			'postcode' => $data['postcode'],
		);
		$this->db->where('IDemr_addresses', $data['IDemr_addresses']);
		$this->db->update('emr_addresses', $updateArray);

		return;
	}

	public function getDefaultNumber($IDemr_user)
	{
		// appointments_settings
		// location_info
		// clinic_contact_number
		$this->db->select('*');
		$this->db->from('appointments_settings AS appsettings');
		$this->db->join('location_info AS locationinfo', 'locationinfo.appointments_settings_IDappointments_settings = appsettings.IDappointments_settings');
		$this->db->join('clinic_contact_number AS cliniccontact', 'cliniccontact.location_info_IDlocation_info = locationinfo.IDlocation_info');
		$this->db->where('appsettings.emr_user_IDemr_user', $IDemr_user);
		return $this->db->get()->result();
	}

	public function doctorHasClinic($IDemr_user)
	{
		// $return = false;
			
		// $doctorId = $this->getDoctorId($IDsafe_user);
		// $this->db->where('IDsafe_user', $IDsafe_user);
		// $this->db->where('approve', TRUE);
		// $this->db->where("active", TRUE);
		// $ret = $this->db->get($this->emruser);
		// if ($ret->num_rows() >= 1)
		// {
			// $return = TRUE;
		// }
		// return $return;
	}
	
	public function getStaffAdministrationList($IDsafe_user = 0)
	{
		// $return = array();

		// #chek if doctor
		// if($this->checkIfDoctor($IDsafe_user))
		// {
			
		// }
	}
}
?>