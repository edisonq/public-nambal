<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->promocode = "promocode";
		$this->errorMessage = '';
	}
	public function checkPromoCode($promoCode)
	{
		$return = FALSE;
		$this->db->where('promocode', $promoCode);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->promocode);
		
		if($ret->num_rows() >= 1)
	 	{
	 		# check if the promo code is not expired
	 		if ($this->currentDate <= $ret->row()->expirationdate)
	 		{
	 			# check if it has not yet reached the limit
	 			if ($ret->row()->limit >= 1)
	 			{
	 				$return = TRUE;
	 			}
	 			else
	 			{
	 				$this->errorMessage .= 'Promo Code has reached the limit.';
	 			}
	 			# end of checking if it has not yet reached the limit
	 		}
	 		else
	 		{
	 			$this->errorMessage .= 'Promo has expired';
	 		}
            # check if the promo code is in limit
	 	}
	 	else
	 	{
	 		$this->errorMessage .= "The promo code is not valid.  Please make sure it is the right Promo code.";
	 	}
	 	return $return;
	 	// return '';
	}

	public function reducePromoLimit($promoCode, $reduce = 1)
	{
		$limit = 0;
		$return = FALSE;
		$this->db->where('promocode', $promoCode);
		$this->db->where('active', TRUE);
		$ret = $this->db->get($this->promocode);

		if ($ret->num_rows() >= 1)
		{
			$limit = $ret->row()->limit;
			$limit = $limit-$reduce;
			$return = TRUE;
		}

		if ($return = TRUE)
		{
			$this->db->where('promocode', $promoCode);
			$this->db->where('active', TRUE);
			$dataChange = array(
				'limit' => $limit
			);
			$this->db->update($this->promocode, $dataChange);
		}
		return $return;
	}

	public function linktouser($promoCode, $user)
	{
		$this->db->where('promocode', $promoCode);
		$this->db->where('active', TRUE);
		$this->db->from('promocode');
		$ret = $this->db->get()->row();

		if (!empty($ret))
		{
			$insertData = array(
				'IDpromo' => $ret->IDpromo,
				'IDsafe_user' => $user
			);
			$this->db->insert('used_promocode', $insertData);

			return TRUE;
		}
		return FALSE;
	}
}