<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->appointments = 'appointments';
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function getDoctorLocations($emr_user_IDemr_user)
	{
		$allLocation = array();
		$this->db
		->from('location_info AS location')
		->join('appointments_settings AS settings', 'settings.IDappointments_settings = location.appointments_settings_IDappointments_settings')
		->join('clinic_info_has_emr_addresses AS clinicinfo', 'clinicinfo.location_info_IDlocation_info = location.IDlocation_info')
		->join('emr_addresses AS address', 'address.IDemr_addresses = clinicinfo.emr_addresses_IDemr_addresses')
		->join('emr_user AS doctor', 'doctor.IDemr_user = settings.emr_user_IDemr_user')
		->where('doctor.IDsafe_user', $emr_user_IDemr_user);
		$allLocation = $this->db->get()->result();

		if (empty($allLocation))
		{
			$this->db
			->from('emr_addresses_has_emr_user AS hasdoctor')
			->join('emr_addresses AS address','address.IDemr_addresses = hasdoctor.emr_addresses_IDemr_addresses')
			->join('emr_user AS doctor','doctor.IDemr_user = hasdoctor.emr_user_IDemr_user')
			->where('doctor.IDemr_user', $emr_user_IDemr_user);
			$allLocation = $this->db->get()->result();
		}

		return $allLocation;
	}

	public function getAllLocationType()
	{
		$this->db->select('locationtype');
		$this->db->from('location_info');
		$this->db->where('active', TRUE);
		$this->db->distinct();
		$this->db->order_by('locationtype');
		return $this->db->get()->result();
	}

	public function insertlocation($data)
	{
		$insertArray = array(
			'locationname' => $data['locationname'],
			'active' => TRUE,
			'website' => $data['website'],
			'numberofdoctors' => $data['numberofdoctors'],
			'locationtype' => $data['locationtype'],
			'appointments_settings_IDappointments_settings' => $data['IDappointments_settings']
		);
		$this->db->insert('location_info', $insertArray);
		$IDlocation_info = $this->db->insert_id();

		return $IDlocation_info;
	}

	public function updatelocation($data)
	{
		$updateArray = array (
			'locationname' => $data['locationname'],
			'website' => $data['website'],
			'numberofdoctors' => $data['numberofdoctors'],
			'locationtype' => $data['locationtype'],
			'appointments_settings_IDappointments_settings' => $data['IDappointments_settings']
		);
		$this->db->where('IDlocation_info', $data['IDlocation_info']);
		$this->db->update('location_info', $updateArray);

		return;
	}

	public function getAllLocation($IDemr_user)
	{
		$this->db->select('IDlocation_info,locationname, website, numberofdoctors, locationtype, setting.settingsname, setting.IDappointments_settings');
		$this->db->from('location_info AS location');
		$this->db->join('appointments_settings AS setting', 'setting.IDappointments_settings = location.appointments_settings_IDappointments_settings');
		$this->db->join('emr_user AS doctor','doctor.IDemr_user = setting.emr_user_IDemr_user');
		$this->db->where('doctor.IDemr_user', $IDemr_user);
		$this->db->where('location.active', TRUE);

		return $this->db->get()->result();
	}

	public function getLocationDetail($IDlocation_info)
	{
		$this->db->select('IDlocation_info,locationname, website, numberofdoctors, locationtype, setting.settingsname, setting.IDappointments_settings, street, state, city, country, postcode');
		$this->db->from('location_info AS location');
		$this->db->join('appointments_settings AS setting', 'setting.IDappointments_settings = location.appointments_settings_IDappointments_settings');
		$this->db->join('emr_user AS doctor','doctor.IDemr_user = setting.emr_user_IDemr_user');
		$this->db->join('clinic_info_has_emr_addresses AS address','address.location_info_IDlocation_info = location.IDlocation_info');
		$this->db->join('emr_addresses AS adddetail', 'adddetail.IDemr_addresses = address.emr_addresses_IDemr_addresses');
		$this->db->where('IDlocation_info', $IDlocation_info);
		// $this->db->where('location.active', TRUE);
		$arrayReturn = $this->db->get()->row();
		$emailArray = $this->getLocationEmail($IDlocation_info);
		$phoneArray = $this->getLocationPhone($IDlocation_info);
		$arrayReturn->getLocationEmail = $emailArray;
		$arrayReturn->getLocationPhone = $phoneArray;

		return $arrayReturn;
	}

	public function getLocationEmail($IDlocation_info)
	{
		$this->db->where('location_info_IDlocation_info', $IDlocation_info);
		$this->db->from('clinic_contact_email');
		return $this->db->get()->result();
	}

	public function getLocationPhone($IDlocation_info)
	{
		$this->db->where('location_info_IDlocation_info', $IDlocation_info);
		$this->db->from('clinic_contact_number');
		return $this->db->get()->result();
	}
}

?>