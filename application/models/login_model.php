<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// copyright 2013 and 2014
// Author Edison Quinones

class Login_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_user = "safe_user";
		$this->safe_personalinfo = "safe_personalinfo";
		$this->safe_addresses = "safe_addresses";
		$this->safe_contactinformation = 'safe_contactinformation';
		$this->currentDate = date('Y-m-d H:i:s');
		$this->loginInformation = array(
			'username' => '',
			'firstname' => '',
			'middlename' => '',
			'lastname' => '',
			'password' => '',
			'verified' => ''
		);
		$this->personalInfoData = array(
			'IDsafe_personalInfo' => '',
			'IDsafe_user' => '',
			'fullname' => '',
			'default' => '',
			'sex' => '',
			'dateofbirth' => '',
			'bloodtype' => ''
		);
		$this->safeAddressData = array();
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->IDsafe_user = ''; 
		$this->errorMessage = array();
	}

	public function login($data, $secret_key = NULL)
	{
		$found = FALSE;
		$findThis = $this->nagkamoritsing->useForSearch($data['username']);
		if (empty($secret_key))
		{
			$secret_key = "GODrules";
		}	

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == FALSE)
		 	{
		 		$password = sha1(md5($data['password']."".$secret_key));
		 		$this->db->like('username', $find);
		 		$this->db->where('password', $password);
				$res = $this->db->get($this->safe_user);
				if($res->num_rows() == 1)
			 	{
					$this->loginInformation = $res->row();
			 		$found = TRUE;
			 	}
			}
		}	

	 	if (!empty($this->loginInformation->IDsafe_user))
	 	{
	 		# check if the user was verified
	 		if ($this->loginInformation->verified != true)
	 		{
	 			array_push($this->errorMessage, 'User has not yet verified');
	 			return false;
	 		}
	 		# above is checking if the user was verified

	 		$this->IDsafe_user = $this->loginInformation->IDsafe_user;
		 	$this->db->where('IDsafe_user', $this->loginInformation->IDsafe_user);
		 	$this->db->where('default', 1);
		 	$personalInfoData = $this->db->get($this->safe_personalinfo);
		 	$this->personalInfoData = $personalInfoData->row();

		 	$this->db->where('IDsafe_personalInfo', $this->personalInfoData->IDsafe_personalInfo);
		 	$personalInfoData = $this->db->get($this->safe_addresses);
		 	$this->safeAddressData = $personalInfoData->row();
	 	}	 	

	 	if($res->num_rows() == 1)
	 	{
	 		return true;
	 	}
	 	else
	 	{
	 		array_push($this->errorMessage, 'User does not exist, password is incorrect or user not verified. To verify please check your email.');
	 		return false;
	 	}
	 }

	# this should be used tarong
	# security breach is very dool
	public function notVerifiedLogin($data, $secret_key = NULL)
	{
		$found = FALSE;
		$findThis = $this->nagkamoritsing->useForSearch($data['username']);
		if (empty($secret_key))
		{
			$secret_key = "GODrules";
		}	

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == FALSE)
		 	{
		 		$password = sha1(md5($data['password']."".$secret_key));
		 		$this->db->like('username', $find);
		 		$this->db->where('password', $password);
				$res = $this->db->get($this->safe_user);
				if($res->num_rows() == 1)
			 	{
					$this->loginInformation = $res->row();
			 		$found = TRUE;
			 	}
			}
		}	

	 	if (!empty($this->loginInformation->IDsafe_user))
	 	{
	 		
	 		$this->IDsafe_user = $this->loginInformation->IDsafe_user;
		 	$this->db->where('IDsafe_user', $this->loginInformation->IDsafe_user);
		 	$this->db->where('default', 1);
		 	$personalInfoData = $this->db->get($this->safe_personalinfo);
		 	$this->personalInfoData = $personalInfoData->row();

		 	$this->db->where('IDsafe_personalInfo', $this->personalInfoData->IDsafe_personalInfo);
		 	$personalInfoData = $this->db->get($this->safe_addresses);
		 	$this->safeAddressData = $personalInfoData->row();
	 	}	 	

	 	if($res->num_rows() == 1)
	 	{
	 		return true;
	 	}
	 	else
	 	{
	 		array_push($this->errorMessage, 'User is not existing or password is incorrect');
	 		return false;
	 	}
	}

	 public function getData()
	 {
	 	return $this->loginInformation;
	 }

	 public function getPersonalInfoData()
	 {
	 	return $this->personalInfoData;
	 }

	public function getSafeAddressData()
	{
		return $this->safeAddressData;
	}

	public function checkVerified($IDsafe_user = NULL)
	{
		$return = false;
		if(empty($IDsafe_user))
		{
			$IDsafe_user = $this->IDsafe_user;
		}

		if(!empty($IDsafe_user))
		{
			$this->db->where('IDsafe_user', $IDsafe_user);
			$this->db->where('verified', TRUE);
			$res = $this->db->get($this->safe_user);
			if($res->num_rows() == 1)
			{
				$return = true;
			}
		}
		return $return;
	}
	public function checkFirstTime($IDsafe_user = NULL)
	{
		$return = false;
		if(empty($IDsafe_user))
		{
			$IDsafe_user = $this->IDsafe_user;
		}

		if(!empty($IDsafe_user))
		{
			$this->db->where('IDsafe_user', $IDsafe_user);
			$this->db->where('firsttime', TRUE);
			$res = $this->db->get($this->safe_user);
			if($res->num_rows() == 1)
			{
				$return = true;
			}
		}
		return $return;
	}

	public function checkPrimaryContact($IDsafe_personalInfo = NULL)
	{
		$return = false;

		if (empty($IDsafe_personalInfo))
		{
			$IDsafe_personalInfo = $this->personalInfoData->IDsafe_personalInfo;
		}

		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('active', TRUE);
		$this->db->where('primary', TRUE);
		$res = $this->db->get($this->safe_contactinformation);
		if ($res->num_rows() == 1)
		{
			$return = true;
		}	

		return $return;
	}

	public function getErrors()
    {
        return $this->errorMessage;
    }

    public function checkIfSessionIsReal($username, $IDsafe_user, $IDsafe_personalInfo)
    {
    	$return = FALSE;
    	$found = false;
    	$IDsafe_user = $this->nagkamoritsing->ibalik($IDsafe_user);
		$findThis = $this->nagkamoritsing->useForSearch($this->nagkamoritsing->ibalik($username));
	 	
	 	foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == false)
		 	{
		 		$this->db->like('username', $find);
		 		$this->db->where('IDsafe_user', $IDsafe_user);
		 		$res = $this->db->get($this->safe_user);
	 		 	if($res->num_rows() == 1)
			 	{
			 		$theData = $res->row();
			 		$found = true;
			 	}
		 	}
	 	}
    	
    	if ($found)
    	{    		
    		$this->db->where('IDsafe_user', $IDsafe_user);
    		$this->db->where('IDsafe_personalInfo', $this->nagkamoritsing->ibalik($IDsafe_personalInfo));
    		$res = $this->db->get($this->safe_personalinfo);
    		if ($res->num_rows() == 1)
    		{
    			$return = TRUE;
    		}
    	}
    	return $return;
    }
	
	public function updateAfterTour($IDsafe_user = 0)
	{
		$return = false;
		$data = array(
			'firsttime' => 0
		);
		$this->db->where('IDsafe_user', $IDsafe_user);
		if($this->db->update($this->safe_user,$data))
		{
			$return = true;
		}

		return $return;
	}
	
}
?>