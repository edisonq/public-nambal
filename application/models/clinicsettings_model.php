<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clinicsettings_model extends CI_Model 
{
	public function __construct() 
	{
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Location_model','locationmodel');
		$this->load->model('emruser_model', 'emruser');
	}

	public function insertStep3($data)
	{
		$IDlocation_info = $this->locationmodel->insertlocation($data);
		$data['IDlocation_info'] = $IDlocation_info;
		$this->insertclinicemail($data['emailaddress'], $data['emaildescription'], $IDlocation_info);
		$this->insertclinicnumber($data['countryCode'], $data['areaCode'], $data['phoneNumber'], $IDlocation_info);
		$this->insertclinicaddress($data);
		$this->addClinicDoctor($data);
		$data['clinicposition'] = 'doctor';
		$this->addClinicStaff($data);

		for ($count=0; $count<$data['countschedule']; $count++)
		{
			$this->insertActualSchedule(
				$data['timefrom'][$count], 
				$data['timeto'][$count],
				$data['dayoftheweek'][$count], 
				$data['appointmentdescription'][$count], 
				$data['appointmenttype'][$count], 
				$data['IDappointments_settings'], 
				$appointable = TRUE
			);
		}

		return $IDlocation_info;
	}

	public function updatestep3($data)
	{
		$this->locationmodel->updatelocation($data);
		$this->deleteClinicEmail($data['IDlocation_info']);
		$this->insertclinicemail($data['emailaddress'], $data['emaildescription'], $data['IDlocation_info']);
		$this->deleteClinicNumber($data['IDlocation_info']);
		$this->insertclinicnumber($data['countryCode'], $data['areaCode'], $data['phoneNumber'], $data['IDlocation_info']);
		$this->updateClinicAddress($data);

		$this->deleteActualSchedule($data);

		for ($count=0; $count<$data['countschedule']; $count++)
		{
			$this->insertActualSchedule(
				$data['timefrom'][$count], 
				$data['timeto'][$count],
				$data['dayoftheweek'][$count], 
				$data['appointmentdescription'][$count], 
				$data['appointmenttype'][$count], 
				$data['IDappointments_settings'], 
				$appointable = TRUE
			);
		}

		return;
	}

	public function deleteActualSchedule($data)
	{
		$this->db->where('appointments_settings_IDappointments_settings', $data['IDappointments_settings']);
		$this->db->delete('emr_actual_schedule');

		return;
	}

	public function insertclinicemail($emailArray, $emailDescriptionArray, $IDlocation_info)
	{
		$count = 0;
		foreach ($emailArray as $email) 
		{
			if (!empty($email))
			{
				$insertArray = array(
					'location_info_IDlocation_info' => $IDlocation_info,
					'active' => TRUE,
					'primary' => TRUE,
					'emailaddress' => $email,
					'description' =>  @$emailDescriptionArray[$count]
				);
				$this->db->insert('clinic_contact_email', $insertArray);
			}
			$count++;
		}

		return;
	}

	public function deleteClinicEmail($IDlocation_info)
	{
		$this->db->where('location_info_IDlocation_info', $IDlocation_info);
		$this->db->delete('clinic_contact_email');

		return;
	}

	public function insertclinicnumber($countryCodeArray, $areaCodeArray, $phoneArray, $IDlocation_info)
	{
		$count = 0;
		foreach ($phoneArray as $phone) 
		{
			if (!empty($phone))
			{
				if ($count ==0)
					$primary = TRUE;
				else
					$primary = FALSE;
				$insertArray = array(
					'location_info_IDlocation_info' => $IDlocation_info,
					'areacode' => $areaCodeArray[$count],
					'countrycode' => $countryCodeArray[$count],
					'phonenumber' => $phone,
					'primary' => $primary,
					'active' => TRUE,
					'description' => ''
				);
				$this->db->insert('clinic_contact_number', $insertArray);
			}
			$count++;
		}
	}

	public function deleteClinicNumber($IDlocation_info)
	{
		$this->db->where('location_info_IDlocation_info', $IDlocation_info);
		$this->db->delete('clinic_contact_number');

		return;
	}

	public function insertclinicaddress($data)
	{
		$IDemr_addresses = $this->emruser->insertDoctorAddress($data);
		$insertArray = array(
			'location_info_IDlocation_info' => $data['IDlocation_info'],
			'emr_addresses_IDemr_addresses' => $IDemr_addresses,
			'active' => TRUE,
			'primary' => TRUE
		);
		$this->db->insert('clinic_info_has_emr_addresses', $insertArray);

		return;
	}

	public function updateClinicAddress($data)
	{
		$this->db->from('clinic_info_has_emr_addresses');
		$this->db->select('location_info_IDlocation_info,emr_addresses_IDemr_addresses');
		$this->db->where('location_info_IDlocation_info', $data['IDlocation_info']);
		$result = $this->db->get()->row();
		$data['IDemr_addresses'] = $result->emr_addresses_IDemr_addresses;
		$this->emruser->updateDoctorAddress($data);

		return;
	}

	public function insertActualSchedule(
		$timefrom, 
		$timeto, 
		$day, 
		$description, 
		$appointmentType, 
		$appointmentSettings, 
		$appointable = TRUE)
	{
		$dataInsert = array(
			'timefrom' => $timefrom,
			'timeto' => $timeto,
			'day' => $day,
			'active' => TRUE,
			'description' => $description,
			'appointment_type_IDappointment_type' => $appointmentType,
			'appointments_settings_IDappointments_settings' => $appointmentSettings,
			'appointable' => $appointable
		);
		$this->db->insert('emr_actual_schedule', $dataInsert);

		return $this->db->insert_id();
	}

	public function addClinicDoctor($data)
	{
		# check if existing
		$this->db->select('IDclinic_has_emr_user');
		$this->db->from('clinic_info_has_emr_user');
		$this->db->where('location_info_IDlocation_info', $data['IDlocation_info']);
		$this->db->where('emr_user_IDemr_user', $data['IDemr_user']);
		$this->db->where('active', TRUE);
		$result = $this->db->get()->row();

		if (!empty($result))
		{
			return $result->IDclinic_has_emr_user;
		}
		# return ID if existing

		$dataInsert = array(
			'location_info_IDlocation_info' => $data['IDlocation_info'],
			'emr_user_IDemr_user' => $data['IDemr_user'],
			'active' => TRUE
		);
		$this->db->insert('clinic_info_has_emr_user', $dataInsert);

		return $this->db->insert_id();
	}

	public function addClinicStaff($data)
	{
		# check if existing
		$this->db->select('IDclinic_staff');
		$this->db->from('clinic_staff');
		$this->db->where('safe_user_IDsafe_user', $data['IDsafe_user']);
		$this->db->where('location_info_IDlocation_info', $data['IDlocation_info']);
		$this->db->where('active', TRUE);
		$this->db->where('position', $data['clinicposition']);
		$result = $this->db->get()->row();

		if (!empty($result))
			return $result->IDclinic_staff;

		$dataInsert = array(
			'safe_user_IDsafe_user' => $data['IDsafe_user'],
			'location_info_IDlocation_info' => $data['IDlocation_info'],
			'active' => TRUE,
			'admin' => TRUE,
			'position' => $data['clinicposition']
		);
		$this->db->insert('clinic_staff', $dataInsert);

		return $this->db->insert_id();
	}

	public function getLocationsFromAppointment($IDappointments_settings)
	{
		$this->db->from('location_info');
		$this->db->where('appointments_settings_IDappointments_settings', $IDappointments_settings);
		$this->db->where('active', TRUE);

		return $this->db->get()->row();
	}

	public function getDoctorActualSchedule($IDemr_user)
	{
		$this->db->select('timefrom, timeto, day, actual.active, actual.description, type.typeName, settings.settingsname');
		$this->db->from('emr_actual_schedule AS actual');
		$this->db->join('appointment_type AS type', 'type.IDappointment_type = actual.appointment_type_IDappointment_type');
		$this->db->join('appointments_settings AS settings', 'settings.IDappointments_settings = actual.appointments_settings_IDappointments_settings');
		$this->db->join('emr_user AS doc', 'doc.IDemr_user = settings.emr_user_IDemr_user');
		$this->db->where('actual.active', TRUE);
		$this->db->where('settings.emr_user_IDemr_user', $IDemr_user);

		return $this->db->get()->result();
	}
	
}
?>