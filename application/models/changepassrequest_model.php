<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Changepassrequest_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->changePasswordRequest = "change_password_request";
		$this->safe_useremails = "safe_useremails";
		$this->safe_user = "safe_user";
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->currentDate = date('Y-m-d H:i:s');
		$this->safe_useremailsInfo = '';
		$this->safe_userInfo = '';
	}

	public function changePasswordApproval($code, $sessionName)
	{
		$return = 0;
		$found = false;
		
		$findThis = $this->nagkamoritsing->useForSearch($sessionName);		

		foreach ($findThis as $find) 
	 	{	 		
		 	if ($found == false)
		 	{
		 		$this->db->like('username', $find);
		 		$this->db->where('verified', true);
		 		$res = $this->db->get($this->safe_user);
	 		 	if($res->num_rows() >= 1)
			 	{
			 		$this->safe_userInfo = $res->row();
			 		$found = true;
			 	}
		 	}
	 	}
		if ($found)
		{
			$this->db->where('active', true);
			$this->db->where('code', $code);
			$this->db->where('IDsafe_user', $this->safe_userInfo->IDsafe_user);
			$ret = $this->db->get($this->changePasswordRequest);
			if ($ret->num_rows() >= 1)
			{
				$return = $res->row()->IDsafe_user;
			}
		}
		


		return $return;
	}

	public function afterChangePass($code, $IDsafe_user)
	{

		$changePassDisable = array(
			'active' => false
		);
		$this->db->where('active', true);
		$this->db->where('code', $code);
		$this->db->where('IDsafe_user', $IDsafe_user);
		$this->db->update($this->changePasswordRequest, $changePassDisable);
		
	}

	public function getSafe_userInfo()
	{
		return $this->safe_userInfo;
	}

	


}
?>