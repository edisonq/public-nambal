<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactinformation_model extends CI_Model 
{	
	public function __construct() 
	{
		parent::__construct();
		$this->safe_contactinformation = "safe_contactinformation";
		$this->contactinfo_email = "contactinfo_email";
		$this->contactinfo_address = "contactinfo_address";
		$this->contactinfo_numbers = "contactinfo_numbers";
	}
	public function getContact($data, $IDsafe_contactinformation = NULL)
	{
		$return = array();
		if (empty($IDsafe_contactinformation))
		{
			$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
			$this->db->where('active', true);
			$this->db->where('primary', true);
			$return = $this->db->get($this->safe_contactinformation);
			$contactInformation = $return->row();
		}
		else
		{
			$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
			$this->db->where('active', true);
			$return = $this->db->get($this->safe_contactinformation);
			$contactInformation = $return->row();
		}

		$this->db->where('IDsafe_contactinformation', @$contactInformation->IDsafe_contactinformation);
		$this->db->where('current', TRUE);
		$return = $this->db->get($this->contactinfo_email);
		$contactEmail = $return->row();

		$this->db->where('IDsafe_contactinformation', @$contactInformation->IDsafe_contactinformation);
		$this->db->where('current', TRUE);
		$return = $this->db->get($this->contactinfo_address);
		$contactAddress = $return->row();

		$this->db->where('IDsafe_contactinformation', @$contactInformation->IDsafe_contactinformation);
		$this->db->where('current', TRUE);
		$return = $this->db->get($this->contactinfo_numbers);
		$contactNumbers = $return->row();

		$return = array(
			'IDsafe_contactinformation' => @$contactInformation->IDsafe_contactinformation,
			'IDsafe_personalInfo' => @$contactInformation->IDsafe_personalInfo,
			'fullname' => @$contactInformation->fullname,
			'email' => @$contactEmail->email,
			'fulladdress' => @$contactAddress->fulladdress,
			'countrycode' => @$contactNumbers->countrycode,
			'areacode' => @$contactNumbers->areacode,
			'number' => @$contactNumbers->phonenumber,
			'primary' => @$contactInformation->primary
		);

		return $return;
	}
	public function getAllContact($data, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);
		
		$return = array();
		$this->db->where('IDsafe_personalInfo', $data['IDsafe_personalInfo']);
		$this->db->where('active', true);
		$theDb = $this->db->get($this->safe_contactinformation);
		//$contactInformation = $return->rows();

		foreach ($theDb->result() as $ret) 
		{
			$this->db->where('IDsafe_contactinformation', @$ret->IDsafe_contactinformation);
			$this->db->where('current', TRUE);
			$theDb = $this->db->get($this->contactinfo_email);
			$contactEmail = $theDb->row();

			$this->db->where('IDsafe_contactinformation', @$ret->IDsafe_contactinformation);
			$this->db->where('current', TRUE);
			$theDb = $this->db->get($this->contactinfo_address);
			$contactAddress = $theDb->row();

			$this->db->where('IDsafe_contactinformation', @$ret->IDsafe_contactinformation);
			$this->db->where('current', TRUE);
			$theDb = $this->db->get($this->contactinfo_numbers);
			$contactNumbers = $theDb->row();

			$contactArray = array(
				'IDsafe_contactinformation' => @$ret->IDsafe_contactinformation,
				'IDsafe_personalInfo' => @$ret->IDsafe_personalInfo,
				'fullname' => @$ret->fullname,
				'email' => @$contactEmail->email,
				'fulladdress' => @$contactAddress->fulladdress,
				'countrycode' => @$contactNumbers->countrycode,
				'areacode' => @$contactNumbers->areacode,
				'number' => @$contactNumbers->phonenumber,
				'primary' => $ret->primary
			);

			array_push($return, $contactArray);

		}

		return $return;
	}
	public function checkComplete($IDsafe_user) {
		$return = FALSE;
		$this->db->where('IDsafe_personalInfo', $IDsafe_user);
		$this->db->where('fullname IS NOT NULL');
		$this->db->where('primary IS NOT NULL');
		$this->db->where('active IS NOT NULL');
        $res = $this->db->get($this->safe_contactinformation); 
		if($res->num_rows() >= 1)
		{
			$return = TRUE;
		}


		return $return;
	}

	public function addContact($IDsafe_personalInfo, $fullname, $email, $countryCode, $areaCode, $number, $fulladdress)
	{
		$contactInfo = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'fullname' => $fullname,
			'active' => TRUE,
			'primary' => FALSE
		);
		
		$idReturn = $this->db->insert($this->safe_contactinformation,$contactInfo);
		$contactInfoId = $this->db->insert_id();

		$contactInfoNumbers = array(
			'phonenumber' => $number,
			'IDsafe_contactinformation' => $contactInfoId,
			'areacode' => $areaCode,
			'countrycode' => $countryCode,
			'current' => TRUE
		);

		$this->db->insert($this->contactinfo_numbers, $contactInfoNumbers);

		$contactInfoEmail = array(
			'email' => $email,
			'current' => TRUE,
			'IDsafe_contactinformation' => $contactInfoId
		);

		$this->db->insert($this->contactinfo_email,$contactInfoEmail);

		$contactInfoAddress = array(
			'IDsafe_contactinformation' => $contactInfoId,
			'fulladdress' => $fulladdress,
			'current' => TRUE
		);

		$this->db->insert($this->contactinfo_address,$contactInfoAddress);
		
		return;
	}

	public function modifyContact($IDsafe_contactinformation, $safeContact)
	{
		// 'fullname' => $fullname,
		// 'email' => $email,
		// 'countrycode' => $countryCode,
		// 'areacode' => $areaCode,
		// 'number' => $number,
		// 'fulladdress' => $fulladdress,
		// 'IDsafe_contactinformation' => $IDsafe_contactinformation,
		// 'IDsafe_personalInfo' => $IDsafe_personalInfo
		$dataUpdate = array(
			'current' => FALSE
		);
		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->update($this->contactinfo_address, $dataUpdate); 

		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->update($this->contactinfo_email, $dataUpdate);

		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->update($this->contactinfo_numbers, $dataUpdate);

		$dataUpdate = array(
			'fullname' => $safeContact['fullname']
		);
		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->update($this->safe_contactinformation, $dataUpdate);

		$dataInsert = array(
			'IDsafe_contactinformation' => $IDsafe_contactinformation,
			'fulladdress' => $safeContact['fulladdress'],
			'current' => TRUE
		);
		$this->db->insert($this->contactinfo_address, $dataInsert);

		$dataInsert = array(
			'email' => $safeContact['email'],
			'current' => TRUE,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);
		$this->db->insert($this->contactinfo_email,$dataInsert);

		$dataInsert = array(
			'phonenumber' => $safeContact['number'],
			'IDsafe_contactinformation' => $IDsafe_contactinformation,
			'areacode' => $safeContact['areacode'],
			'countrycode' => $safeContact['countrycode'],
			'current' => TRUE
		);
		$this->db->insert($this->contactinfo_numbers, $dataInsert);
	}

	public function setasprimary($IDsafe_contactinformation, $personalinfoid = NULL)
	{
		$dataUpdate = array(
			'primary' => FALSE
		);
		$this->db->where('IDsafe_personalInfo', $personalinfoid);
		$this->db->where('active', TRUE);
		$this->db->where('primary', TRUE);
		$this->db->update($this->safe_contactinformation, $dataUpdate);

		$dataUpdate = array(
			'primary' => TRUE
		);
		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->update($this->safe_contactinformation, $dataUpdate);
	}

	public function checkifprimary($IDsafe_contactinformation, $personalinfoid = NULL)
	{
		$this->db->where('IDsafe_contactinformation', $IDsafe_contactinformation);
		$this->db->where('active', TRUE);
		$this->db->where('primary', TRUE);
		return $this->db->get('safe_contactinformation')->num_rows();
	}

	public function privacySettings()
	{

	}

	public function deleteContact($idsafecontactinformation, $personalinfoid = NULL)
	{
		$dataUpdate = array(
			'active' => FALSE,
			'primary' => FALSE
		);
		$this->db->where('IDsafe_contactinformation', $idsafecontactinformation);
		$this->db->update($this->safe_contactinformation, $dataUpdate);

		


		// $this->db->where('IDsafe_contactinformation', $idsafecontactinformation);
		// $this->db->delete($this->contactinfo_email);

		// $this->db->where('IDsafe_contactinformation', $idsafecontactinformation);
		// $this->db->delete($this->contactinfo_address);

		// $this->db->where('IDsafe_contactinformation', $idsafecontactinformation);
		// $this->db->delete($this->contactinfo_numbers);

		// $this->db->where('IDsafe_contactinformation', $idsafecontactinformation);
		// $this->db->delete($this->safe_contactinformation);
	}
	public function record_count($IDsafe_personalInfo)
    {
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('active', true);
		$this->db->from('safe_contactinformation');
		return $this->db->get()->num_rows();
    }
}
?>