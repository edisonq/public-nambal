<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Problem_model extends CI_Model 
{
	
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->problemlist = "problemlist";
		$this->safeproblem = "safe_problem";
		$this->load->library('session');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->nambal_session = $this->session->userdata('logged_in');
	}

	public function searchlist($keyword, $retTheId = FALSE)
	{
		$arrayReturn = array();
			
		$this->db->from('problemlist');
		$this->db->where('approved', TRUE);
		$this->db->where('active', TRUE);
		if ($retTheId == TRUE)
		{
			$this->db->select('IDproblemList');
			$this->db->like('problemName', $keyword, 'none');
		}
		else
		{
			$this->db->select('problemName');
			$this->db->like('problemName', $keyword, 'both');
		}
		$result = $this->db->get();

		if ($retTheId == FALSE)
		{
			foreach ($result->result() as $row)
			{
			    
			    array_push($arrayReturn, $row->problemName);
			}
		}
		else
		{
			$arrayReturn = $result->row();
			$arrayReturn = @$arrayReturn->IDproblemList;
		}
		
		return $arrayReturn;
	}

	public function insertProblem($IDsafe_personalInfo, $data)
	{
		// 'conditionOrSymptom' => $conditionOrSymptom,
		// 'problemnow' => $problemnow,
		// 'datetimestarted' => @$datetimeStart,
		// 'datetimeended' => @$datetimeEnd,
		// 'notes' => @$notes
		
		// # find the IDproblemList
		$IDproblemList = $this->searchlist($data['conditionOrSymptom'], TRUE);

		if (empty($IDproblemList))
			$IDproblemList = 0;

		if ($IDproblemList == 0)
		{
			$dataInsert = array(
				'problemDescription' => $data['conditionOrSymptom'],
				'problemName' => $data['conditionOrSymptom'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->problemlist, $dataInsert);

			$IDproblemList = $this->db->insert_id();			
		}

		$dataInsert = array(
			'IDproblemList' => $IDproblemList,
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'havingitnow' => $data['problemnow'],
			'dateStart' => $data['datetimestarted'],
			'dateEnd' => $data['datetimeended'],
			'notes' => $data['notes']
		);
		$this->db->insert($this->safeproblem, $dataInsert);

		return $this->db->insert_id();
	}

	public function getDoctorsInput($IDsafe_personalInfo)
	{
		$this->select('IDsafe_problem');
		$this->db->from('safe_problem AS problem');
		$this->db->join('emr_problem_safe AS doctor', 'doctor.IDsafe_problem = problem.IDsafe_problem');
		$this->db->where('problem.active', TRUE);
		$this->db->where('problem.IDsafe_personalInfo', $IDsafe_personalInfo);
		$doctorsInput = $this->db->get()->result();

		return $doctorsInput;
	}

	public function displayDashboardProblem($IDsafe_personalInfo, $limit = 10, $whatpage = 0)
	{
		# just talking about limit
		$start = ($limit * $whatpage) - $limit;
		if ($start < 0)
			$start = 0;
		$this->db->limit($limit, $start);

		# get the problem added
		$problemArray = array();
		$this->db->select('problem.IDproblemList, IDsafe_problem, havingitnow, dateStart, dateEnd, problemName, notes');
		$this->db->from('safe_problem AS problem');
		$this->db->join('problemlist AS list', 'problem.IDproblemList = list.IDproblemList');
		$this->db->where('problem.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('problem.active', TRUE);
		$this->db->order_by("dateStart", "desc"); 
		$problems = $this->db->get()->result();

		foreach ($problems as $ret) 
		{
			$this->db->select('problem.IDsafe_problem, lastname, info.IDemr_user');
			$this->db->from('emr_problem_safe AS doctor');
			$this->db->join('emr_user AS info', 'info.IDemr_user = doctor.IDemr_user');
			$this->db->join('safe_user AS doctorName', 'doctorName.IDsafe_user = info.IDsafe_user');
			$this->db->join('safe_problem AS problem','problem.IDsafe_problem = doctor.IDsafe_problem');
			$this->db->where('problem.IDsafe_problem', $ret->IDsafe_problem);
			$this->db->where('problem.active', TRUE);
			$return = $this->db->get()->row();

			if (!empty($return->IDsafe_problem))
			{
				$arrayProblemInfo = array(
					'problemName' => $ret->problemName,
					'IDemr_user' =>$return->IDemr_user,
					'lastname' => $return->lastname,
					'IDsafe_problem' => $ret->IDsafe_problem,
					'dateStart' => $ret->dateStart,
					'dateEnd' => $ret->dateEnd,
					'notes' => $ret->notes,
					'havingitnow' => $ret->havingitnow
				);
			}
			else
			{
				$arrayProblemInfo = array(
					'problemName' => $ret->problemName,
					'IDemr_user' => '',
					'lastname' => '',
					'IDsafe_problem' => $ret->IDsafe_problem,
					'dateStart' => $ret->dateStart,
					'dateEnd' => $ret->dateEnd,
					'notes' => $ret->notes,
					'havingitnow' => $ret->havingitnow
				);
			}
			array_push($problemArray, $arrayProblemInfo);
		}

		return $problemArray;
	}

	public function getToModifyProblem($IDsafe_problem)
	{
		$this->db->select('problem.IDproblemList, IDsafe_personalInfo, IDsafe_problem, havingitnow, dateStart, dateEnd, problemName, notes, IDemr_journal');
		$this->db->from('safe_problem AS problem');
		$this->db->join('problemlist AS list', 'problem.IDproblemList = list.IDproblemList');
		$this->db->where('problem.IDsafe_problem', $IDsafe_problem);
		$this->db->where('problem.active', TRUE);
		return $this->db->get()->row(); 
	}

	public function checkDateStartEnd($dateStart, $dateEnd, $autocorrect = FALSE)
	{
		# if $dateEnd is older than datestart
		# return FALSE or 0
		# if autocorrect SET to true
		# display #dateEnd the current date

		if (strtotime($dateStart > strtotime($dateEnd)))
		{
			if ($autocorrect == FALSE)
				return FALSE;
			else
				return $this->currentDate;
		}
		else
		{
			if ($autocorrect == FALSE)
				return TRUE;
			else 
				return $dateEnd;
		}
	}

	public function updateSafeProblem($data)
	{
		// 'conditionOrSymptom' => $conditionOrSymptom,
		// 'problemnow' => (boolean)$problemnow,
		// 'datetimestarted' => @$datetimeStart,
		// 'datetimeended' => @$datetimeEnd,
		// 'notes' => @$notes,
		// 'IDsafe_problem' => $IDsafe_problem
		# check problemList
		$IDproblemList = $this->searchlist($data['conditionOrSymptom'], TRUE);

		if (empty($IDproblemList))
			$IDproblemList = 0;

		if ($IDproblemList == 0)
		{
			$dataInsert = array(
				'problemDescription' => $data['conditionOrSymptom'],
				'problemName' => $data['conditionOrSymptom'],
				'userAdded' => TRUE,
				'approved' => FALSE
			);
			$this->db->insert($this->problemlist, $dataInsert);

			$IDproblemList = $this->db->insert_id();			
		}

		$dataUpdate = array(
			'IDproblemList' => $IDproblemList,
			'havingitnow' => $data['problemnow'],
			'dateStart' => $data['datetimestarted'],
			'dateEnd' => $data['datetimeended'],
			'notes' => $data['notes']
		);

		$this->db->where('IDsafe_problem', $data['IDsafe_problem']);
		$this->db->update($this->safeproblem, $dataUpdate);
	}

	public function deleteSafeProblem($IDsafe_problem, $deleter_IDsafe_user = NULL) 
	{
		$error = FALSE;
		if (empty($deleter_IDsafe_user))
		{
			# this will help us prevent deletion from other user
			$deleter_IDsafe_user = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']);
		}
		if (!$this->checkIfAuthorized($deleter_IDsafe_user, $IDsafe_problem))
			$error = TRUE;
		// return $this->checkIfAuthorized($deleter_IDsafe_user, $IDsafe_problem);
		if ($error == FALSE)
		{
			$deleteArray = array(
				'active' => FALSE
			);
			$this->db->where('IDsafe_problem', $IDsafe_problem);
			$this->db->update($this->safeproblem, $deleteArray);
		}

		return $error;//$this->balikdelete;
	}

	public function checkIfAuthorized($IDsafe_user, $IDsafe_problem = NULL)
	{
		# check if owner of the problem
		if (!empty($IDsafe_problem))
		{
			$this->db->select('personal.IDsafe_personalInfo');
			$this->db->from('safe_personalinfo AS personal');
			$this->db->join('safe_problem AS problem', 'problem.IDsafe_personalInfo = personal.IDsafe_personalInfo');
			$this->db->join('safe_user AS user', 'user.IDsafe_user = personal.IDsafe_user');
			$this->db->where('user.IDsafe_user', $IDsafe_user);
			$this->db->where('problem.IDsafe_problem', $IDsafe_problem);
			$return = $this->db->get('safe_problem')->row();

			if (empty($return->IDsafe_personalInfo))
				$error = TRUE;
			else
				$error = FALSE;
		}

		if ($error == TRUE)
		{
			# if not try to check if doctor
			if ($this->emruser->checkIfDoctor($IDsafe_user))
				$error = FALSE;
			else
				$error = TRUE;
			# butangan pa unta kung iyaha ba ni input pero nag kuwang og oras
		}
		

		if ($error == FALSE)
			return TRUE;
		else
			return FALSE;
	}

	public function setResolveProblem($IDsafe_problem, $value = FALSE)
	{
		$updateArray = array(
			'havingitnow' =>  $value,
			'dateEnd' => $this->currentDate
		);
		$this->db->where('IDsafe_problem', $IDsafe_problem);
		$this->db->update($this->safeproblem, $updateArray);

		return;
	}

	public function testingsasession()
	{
		return $this->nambal_session;
	}

	public function justanothertest($IDsafe_user, $IDsafe_problem)
	{
		$this->db->from('safe_personalinfo AS personal');
		$this->db->group_by('problem.IDsafe_problem');
		$this->db->join('safe_problem AS problem', 'problem.IDsafe_personalInfo = personal.IDsafe_personalInfo');
		$this->db->join('safe_user AS user', 'user.IDsafe_user = personal.IDsafe_user');
		$this->db->where('user.IDsafe_user', $IDsafe_user);
		$this->db->where('problem.IDsafe_problem', $IDsafe_problem);
		$return = $this->db->get('safe_problem')->result();

		return $return;
	}

	public function record_count($IDsafe_personalInfo)
    {       
        $this->db->select('problem.IDproblemList');
		$this->db->from('safe_problem AS problem');
		$this->db->where('problem.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('problem.active', TRUE);
		$this->db->order_by("dateStart", "desc"); 
		return $this->db->get()->num_rows();
    }

	
}
?>