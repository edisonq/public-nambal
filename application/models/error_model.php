<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class error_model extends CI_Model 
{
	
	public function __construct() 
	{
	}

	public function identifyError($errorNumber)
	{
		$returnArray = array(
			'name' => '',
			'userdisplay' => '',
			'userdisplayhtml' => '',
			'resolution' => ''
		);

		if ($errorNumber == 2001)
		{
			$returnArray = array(
				'description' => 'user is not a doctor',
				'userdisplaytext' => 'Thanks for your interest in using this functionality but I\'m sorry to inform you that you are not yet registered as a doctor in nambal or your doctor\'s access was temporarily disabled',
				'userdisplayhtml' => '<p>Thanks for your interest in using this functionality but I&rsquo;m sorry to inform you that you are not yet registered as a doctor in nambal or your doctor&rsquo;s access was temporarily disabled</p>',
				'resolutiontext' => 'Please register first as a doctor to continue or please contact us.',
				'resolutionhtml'=> '<p>Please register first as a doctor to continue or please contact us.</p>'
			); 

		}
		elseif ($errorNumber == 2002)
		{
			$returnArray = array(
				'description' => 'user is not a doctor',
				'userdisplaytext' => 'I\'m sorry to inform you that your doctor access was temporarily disabled',
				'userdisplayhtml' => '',
				'resolutiontext' => 'Please check your email or you may contact us for more information',
				'resolutionhtml'=> ''
			); 
		}
		elseif ($errorNumber == 1000) 
		{
			$returnArray = array(
				'description' => 'Session ID not found',
				'userdisplaytext' => 'Patient not found using that session ID.',
				'userdisplayhtml' => '',
				'resolutiontext' => 'Please search again',
				'resolutionhtml' => ''
			);
		}
		elseif ($errorNumber == 3001) 
		{
			$returnArray = array(
				'description' => 'Session expired',
				'userdisplaytext' => 'Your session expired.',
				'userdisplayhtml' => '<p>Your session expired.</p>',
				'resolutiontext' => 'Please login again',
				'resolutionhtml' => '<p>Please <a href="'.base_url().'login" target="_blank">login</a> again</p>'
			);
		}
		elseif ($errorNumber == 3002) 
		{
			$returnArray = array(
				'description' => 'Registration not complete',
				'userdisplaytext' => 'Registration not complete.',
				'userdisplayhtml' => '<p>Registration not complete.</p>',
				'resolutiontext' => 'Complete your registration',
				'resolutionhtml' => '<p>Complete your registration</p>'
			);
		}
		elseif ($errorNumber == 3003) 
		{
			$returnArray = array(
				'description' => 'Registration not complete',
				'userdisplaytext' => 'Registration not complete.',
				'userdisplayhtml' => '<p>Registration not complete.</p>',
				'resolutiontext' => 'Please complete your registration',
				'resolutionhtml' => '<p>Please complete your registration</p>'
			);
		}

		return $returnArray;
	}
}
?>