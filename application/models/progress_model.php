<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Progress_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
	}

	public function checkUserDoctorDone($IDemr_user)
	{
		$this->db->select('donesettings');
		$this->db->from('emr_user');
		$this->db->where('IDemr_user', $IDemr_user);
		$this->db->get()->row();
	}

	public function setUserDone($IDemr_user)
	{

	}

	public function checkUserVerified($IDsafe_user)
	{

	}

	public function checkUserDoctorLicense($IDemr_user)
	{

	}

	public function checkUserDoctorClinic($IDemr_user)
	{

	}

	public function checkUserActualSchedule($IDemr_user)
	{

	}

	public function checkUserLocation($IDemr_user)
	{

	}

	/* we need the user to scan their printed ID */
	public function checkUserPrintCard()
	{

	}
}

?>