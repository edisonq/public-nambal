<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emrjournal_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function createJournal($data)
	{
		
	}

	public function identifywhattableused($data)
	{
		# list the primary ID of each of health record table
        $checkTableArray = 
        array(
            'IDsafe_allergen' => 'safe_allergen',
            'IDsafe_drug' => 'safe_drug',
            'idsafe_files' => 'safe_files',
            'IDsafe_immunization' => 'safe_immunization',
            'IDsafe_prescription' => 'safe_prescription',
            'IDsafe_problem' => 'safe_problem',
            'IDsafe_procedure' => 'safe_procedure',
            'IDsafe_test' => 'safe_test'
        );

        # identify what table used
        if (!empty($data))
        {
	        foreach ($data as $key => $value) 
	        {
	            foreach ($checkTableArray as $idtable => $tablename) 
	            {
	                if(strcasecmp($key, $idtable)==0)
	                {
	                	$return = array(
	                		'tableId' => $key,
	                		'tableName' => $tablename
	                	);
	                	return $return;
	                }
	            }
	        }
	    }

        return;
	}

	public function whoOwnsTheJournal($IDemr_journal)
	{
		$arrayOfTables = array(
			'safe_problem',
			'safe_drug',
			'safe_prescription',
			'safe_allergen',
			'safe_procedure',
			'safe_immunization',
			'safe_test',
			'safe_files'
		);

		foreach ($arrayOfTables as $table) 
		{
			if (@$return->IDemr_journal == 0 || (empty($return)) )
			{
				$this->db->from($table);
				$this->db->where('IDemr_journal', $IDemr_journal);
				$return = $this->db->get()->row();
			}
			else
			{
				return $return;
			}
		}		
	}

	public function doctorOwnsJournal($IDemr_journal, $IDemr_user)
	{
		$this->db->from('emr_journal');
		$this->db->where('IDemr_journal', $IDemr_journal);
		$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$rowCount = $this->db->get()->num_rows();

		if ($rowCount >= 1)
			return true;
		else
			return false;
	}

	public function update($data)
	{
		$updateArray = array(
			'notes' => $data['notes'],
			'dateinputed' => $data['datestarted']
		);
		$this->db->where('emr_user_IDemr_user', $data['IDemr_user']);
		$this->db->where('IDemr_journal', $data['IDemr_journal']);
		$this->db->update('emr_journal', $updateArray);

		return;
	}

	public function getJournals($IDemr_user)
	{
		$this->db->from('emr_journal as journal');
		$this->db->join('emr_patient AS patient','patient.IDemr_patient = journal.emr_patient_IDemr_patient');
		$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$return = $this->db->get()->result();

		return $return;
	}

	public function displayDashboardJournal($IDsafe_personalInfo, $IDemr_user, $limit = 10, $whatpage = 0)
	{
		// if ((!empty($limit)) || (!empty($start)))
			// $this->db->limit($limit, $start);
		# computation unsa ang sakto ibutang sa limit(start, end)
		# exmaple 10 * 5 = 50
		# example 10 * 2 = 20
		$start = ($limit * $whatpage) - $limit;

		if ($start < 0)
			$start = 0;

		$this->db->limit($limit, $start);

		$this->db->from('emr_journal as journal');
		$this->db->join('emr_patient AS patient','patient.IDemr_patient = journal.emr_patient_IDemr_patient');
		$this->db->where('patient.IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->where('journal.active', TRUE);
		$this->db->where('journal.emr_user_IDemr_user', $IDemr_user);
		$this->db->order_by("dateinputed", "desc"); 
		$return = $this->db->get()->result();

		return $return;
	}

	public function setConnectPatient($data)
	{
		$insertData = array(
			'IDemr_user' => @$data['IDemr_user'],
			'IDsafe_personalInfo' => @$data['IDsafe_personalInfo'],
			'IDemr_personalinfo' => @$data['IDemr_personalinfo']
		);
		$this->db->insert('emr_patient', $insertData);

		return;
	}

	public function checkConnected($IDsafe_user = NULL, $IDemr_personalinfo = NULL, $IDemr_user)
	{
		$return = array();
		if (!empty($IDsafe_user))
		{
			$this->db->from('safe_personalinfo AS patient');
			$this->db->join('emr_patient AS docpatient', 'docpatient.IDsafe_personalInfo = patient.IDsafe_personalInfo');
			$this->db->where('patient.IDsafe_user', $IDsafe_user);
			$this->db->where('docpatient.IDemr_user', $IDemr_user);		
			$return = $this->db->get()->row();
		}

		if (!empty($IDemr_personalinfo))
		{
			$this->db->from('emr_patient AS docpatient');
			$this->db->join('emr_personalinfo AS patient', 'patient.IDemr_personalinfo = docpatient.IDemr_personalinfo');
			$this->db->where('patient.IDemr_personalinfo', $IDemr_personalinfo);
			$this->db->where('docpatient.IDemr_user', $IDemr_user);
			$return = $this->db->get()->row();
		}

		return $return;
	}

	public function getidemrpatient($IDemr_user, $IDsafe_personalInfo)
	{
		$this->db->where('IDemr_user', $IDemr_user);
		$this->db->where('IDsafe_personalInfo', $IDsafe_personalInfo);
		$this->db->from('emr_patient');
		$return = $this->db->get()->row();

		// IDemr_user2
		// IDsafe_personalInfo5
		return $return;
	}

	public function record_count($IDemr_user, $IDemr_patient) 
    {
    	$this->db->where('emr_user_IDemr_user', $IDemr_user);
		$this->db->where('active', TRUE);
    	$this->db->where('emr_patient_IDemr_patient', $IDemr_patient);
    	$this->db->from('emr_journal');
        return $this->db->get()->num_rows();
    }

	/* paging testing only */

   
 
    public function fetch_journals($limit, $start) 
    {
        $this->db->limit($limit, $start);
        $this->db->where('emr_user_IDemr_user', 11);
        $query = $this->db->get("emr_journal");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    /* end of paging testing only */
}
?>