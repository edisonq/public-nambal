<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\createBlobOptions;
use WindowsAzure\Blob\Models\PublicAccessType;
use WindowsAzure\Common\ServiceException;
class Azuretest extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('azure');
	}

	public function index() {
		// Create blob REST proxy.
		$connectionString = $this->azure->getConnectionString();
		$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
		$createContainerOptions = new CreateContainerOptions();

		$createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);

		// Set container metadata.
		$createContainerOptions->addMetaData("key1", "value1");
		$createContainerOptions->addMetaData("key2", "value2");

		try {
		    // Create container.
		    $blobRestProxy->createContainer("mycontainer", $createContainerOptions);
		}
		catch(ServiceException $e){
		    // Handle exception based on error codes and messages.
		    // Error codes and messages are here:
		    // http://msdn.microsoft.com/library/azure/dd179439.aspx
		    $code = $e->getCode();
		    $error_message = $e->getMessage();
		    echo $code.": ".$error_message."<br />";
		}


		// $content = fopen("c:\myfile.txt", "r");
		// $blob_name = "myblob.txt";

		// try {
		//     //Upload blob
		//     $blobRestProxy->createBlockBlob("mycontainer", $blob_name, $content);
		// }
		// catch(ServiceException $e){
		//     // Handle exception based on error codes and messages.
		//     // Error codes and messages are here:
		//     // http://msdn.microsoft.com/library/azure/dd179439.aspx
		//     $code = $e->getCode();
		//     $error_message = $e->getMessage();
		//     echo $code.": ".$error_message."<br />";
		// }

	}

	public function upload() {

		// Create blob REST proxy.
		$connectionString = $this->azure->getConnectionString();
		$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);


		// $content = fopen("c:\myfile.txt", "r");
		$content = fopen("profilepic/1fc4f9b700d6c746823ef2114781e123.jpg", "r");
		$blob_name = "asdf.jpg";
		$options = new CreateBlobOptions();
		$options->setBlobContentType("image/jpeg");
		

		try {
		    //Upload blob
		    $blobRestProxy->createBlockBlob("mycontainer", $blob_name, $content, $options);
		}
		catch(ServiceException $e){
		    // Handle exception based on error codes and messages.
		    // Error codes and messages are here:
		    // http://msdn.microsoft.com/library/azure/dd179439.aspx
		    $code = $e->getCode();
		    $error_message = $e->getMessage();
		    echo $code.": ".$error_message."<br />";
		}
		// echo getcwd ();
	}
}


?>