<?php

class Qr extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
        $this->load->library('form_validation');

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Qr_model', 'qrModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_model', 'emrUser');
	}

	public function index() {
		$this->load->library('ciqrcode');
		header("Content-Type: image/png");
		$params['data'] = 'https://www.nambal.com';
		echo $this->ciqrcode->generate($params);
	}

	public function generateSelf()
	{
		$this->load->library('ciqrcode');
		$params['data'] = base_url();
		$save = false;
		

		$this->form_validation->set_rules('save','saving', 'trim|xss_clean');
        $this->form_validation->set_rules('redirect','redirect', 'trim|xss_clean');
		$this->form_validation->set_rules('sessionname','session name', 'trim|xss_clean');
		$this->form_validation->set_rules('idsu','ID safe user', 'trim|xss_clean');
		$this->form_validation->set_rules('idsp','ID personal info', 'trim|xss_clean');
		$this->form_validation->set_rules('filename','filename', 'trim|xss_clean');

		$_POST = $_GET;
        
        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        } else {
            $error = FALSE;
        }

        $save = filter_var($this->input->get('save'), FILTER_SANITIZE_STRING);
        $getSessionName = filter_var($this->input->get('sessionname'), FILTER_SANITIZE_STRING);
    	$getIDsafe_user = filter_var($this->input->get('idsu'), FILTER_SANITIZE_STRING);
    	$getIDsafe_personalInfo = filter_var($this->input->get('idsp'), FILTER_SANITIZE_STRING);
    	$filename = filter_var($this->input->get('filename'), FILTER_SANITIZE_STRING);
        $redirect = filter_var($this->input->get('redirect'), FILTER_SANITIZE_STRING);


		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
        	# check if naa na ba sa database
        	if ($this->qrModel->retrieveCurrent($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
        	{
        		# if naa sa database show the current qr
        		$filename = $this->qrModel->qrInfo;
        		$filename = $filename->filename;
        		# end if naa na sa database show the current qr
        	}
        	else
        	{
        		# generate filename here for the database and save 
				$filename = $this->nambal_session['IDsafe_user'].substr(sha1($this->nambal_session['IDsafe_user'].date('h:i:s')), 0, 10);
				
				# end of genrating filename here for the database and save	
				# input in database
				$this->qrModel->saveGenerated($filename, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
				# end of input in database

                # help you save the QR if not yet save
                $retrieveimage = file_get_contents(base_url()."qr/generateSelf?save=TRUE&sessionname=".$this->nagkamoritsing->ibalik(@$this->nambal_session['sessionName'])."&idsu=".$this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user'])."&idsp=".$this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_personalInfo'])."&filename=".$filename);
                $ch = curl_init(base_url()."qr/generateSelf?save=TRUE&sessionname=".$this->nagkamoritsing->ibalik(@$this->nambal_session['sessionName'])."&idsu=".$this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user'])."&idsp=".$this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_personalInfo'])."&filename=".$filename);
                // curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                curl_close($ch);
                # end of generating the QR code image'

               
        	}
        	# end if naa ba sa database        	

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $params['data'] = base_url().'qr';
            }
            else
	        {
	        	$params['data'] = base_url().'profileViewer?SN='.$this->nambal_session['sessionName'].'&IDSU='.$this->nambal_session['IDsafe_user'].'&IDSP='.$this->nambal_session['IDsafe_personalInfo'].'&FIL='.$filename;
	        }
            # end of checking if the usersIP is same with session IP    
        }  # end of redirecting users to dashboard if logged in
        else
        {
        	$params['data'] = base_url().'profileViewer?SN='.$this->nagkamoritsing->bungkag($getSessionName).'&IDSU='.$this->nagkamoritsing->bungkag($getIDsafe_user).'&IDSP='.$this->nagkamoritsing->bungkag($getIDsafe_personalInfo).'&FIL='.$filename;

        }

		if ($save == TRUE)
		{
			$params['quality'] = true; //boolean, the default is true
			$params['savename'] = APPPATH.'../public/qr/'.$filename.'.png';
		}

		header("Content-Type: image/png");
		$this->ciqrcode->generate($params);
        // echo 'ambot<br>';
        // print_r($params);

        if ($redirect == TRUE)
            redirect(base_url().'healthprofile/saveit', 'refresh');

	}

    public function otherusergenerator()
    {
        $error = false;
        $_POST = $_GET;
        $redirect = TRUE;

        $this->form_validation->set_rules('save','saving', 'trim|xss_clean');
        $this->form_validation->set_rules('sessionname','session name', 'trim|xss_clean');
        $this->form_validation->set_rules('idsu','ID safe user', 'trim|xss_clean');
        $this->form_validation->set_rules('idsp','ID personal info', 'trim|xss_clean');
        $this->form_validation->set_rules('filename','filename', 'trim|xss_clean');
        $this->form_validation->set_rules('docuser','doc username', 'trim|xss_clean|required');
        $this->form_validation->set_rules('redirect','redirect', 'trim|xss_clean');        
        
        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        } else {
            $error = FALSE;
        }

        # kailangan i check kung doctor ba jud
        # check if naka login ba as doctor, 
        # doctor ray authorize mo generate og nambal card sa lain.
        $docuser = filter_var($this->input->get('docuser'), FILTER_SANITIZE_STRING);
        if (!empty($docuser))
        {
            if (!$this->emrUser->checkIfDoctor($docuser))
            {
                $error = TRUE;
                // $errorMessage .= 'You are not authorize to print this nambal card.  ';
            }
        }
        # not authorize to generate other qr if dili ka doctor

        $this->load->library('ciqrcode');
        $params['data'] = base_url();
        $save = false;

        // echo $error;

        # sugod og generate sa ubos
        if ($error == false)
        {
            

            $save = filter_var($this->input->get('save'), FILTER_SANITIZE_STRING);
            $getSessionName = filter_var($this->input->get('sessionname'), FILTER_SANITIZE_STRING);
            $getIDsafe_user = filter_var($this->input->get('idsu'), FILTER_SANITIZE_STRING);
            $getIDsafe_personalInfo = filter_var($this->input->get('idsp'), FILTER_SANITIZE_STRING);
            $filename = filter_var($this->input->get('filename'), FILTER_SANITIZE_STRING);
            $redirect = filter_var($this->input->get('redirect'), FILTER_SANITIZE_STRING);
            
            # check if naa na ba sa database
            if ($this->qrModel->retrieveCurrent($getIDsafe_personalInfo))
            {
                # if naa sa database show the current qr
                $filename = $this->qrModel->qrInfo;
                $filename = $filename->filename;
                # end if naa na sa database show the current qr

            }
            else
            {
                # generate filename here for the database and save 
                $filename = $this->nagkamoritsing->bungkag($getIDsafe_user).substr(sha1($this->nagkamoritsing->bungkag($getIDsafe_user).date('h:i:s')), 0, 20);
                
                # end of genrating filename here for the database and save  
                # input in database
                $this->qrModel->saveGenerated($filename, $getIDsafe_personalInfo);
                # end of input in database
            }
            # end if naa ba sa database           

            


            # redirect users to login who are not properly logged in
            $params['data'] = base_url().'profileViewer?SN='.$this->nagkamoritsing->bungkag($getSessionName).'&IDSU='.$this->nagkamoritsing->bungkag($getIDsafe_user).'&IDSP='.$this->nagkamoritsing->bungkag($getIDsafe_personalInfo).'&FIL='.$filename;

            if ($save == TRUE)
            {
                $params['quality'] = true; //boolean, the default is true
                $params['savename'] = APPPATH.'../public/qr/'.$filename.'.png';
            }

            header("Content-Type: image/png");
            $this->ciqrcode->generate($params);
        }
        else
        {
             $params['data'] = base_url().'qr';
        }

        if ($redirect == TRUE)
            redirect(base_url().'healthprofile/saveitfromdoctor?forceto=pdf&sessionid='.$this->nagkamoritsing->bungkag($getIDsafe_user), 'refresh');
        
    }

    public function apppath()
    {
        echo APPPATH;
    }

	public function getCodeOnly()
	{
        $ajax = TRUE;
        $ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

        if (!empty($this->nambal_session['IDsafe_personalInfo']))
        {
            $returnValue = array ('result' => '', 'textcode' => '');
            $returnValue = $this->qrModel->getCodeOnly($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

            if (empty($returnValue['textcode']))
            {
                redirect(base_url().'qr/getCodeOnly', 'refresh');
            }
            if ($ajax ==TRUE)
            {
                # ajax ang display diri
                header('Content-Type: application/json');

                echo json_encode($returnValue);
                # end of ajax ang display
            }
            else
            {
                echo @$returnValue['textcode'];
            }        	
    	}
    	
	}
}

?>
