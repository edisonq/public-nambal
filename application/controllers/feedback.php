<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Feedback extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Help and Support', 
			'view' => 'feedback/index'));
		return;
	}
}

?>
