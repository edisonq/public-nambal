<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class HealthProfile extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		 $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		 $this->load->model('ContactInformation_model', 'contactinfo');
		 $this->load->model('Qr_model', 'qrModel');
		 $this->load->model('Safeuser_model', 'safeusermodel');
		 $this->load->model('Personalprofile_model', 'personalprofile');
		 $this->load->model('Addresses_model', 'addressesmodel');
		 // $this->baseurl_wouthttps = 'https://www.nambal.com/';
		 // $this->baseurl_wouthttps = 'http://www.nambal.com/';
		 // $this->baseurl_wouthttps = 'http://localhost:4545/';
		 $this->baseurl_wouthttps = 'http://www.nambal.com/';
		 
		 $this->load->library('form_validation');
		 $this->load->helper('url');

		 $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		# loading the fpdf
		define('FPDF_FONTPATH',APPPATH .'plugins/fpdf17/font/');
      	require(APPPATH .'plugins/fpdf17/fpdf.php');




		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Emruser_model', 'emrUser');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

       
	}

	public function index()
	{
		//$sessionName = $this->nagkamoritsing->ibalik(filter_var($this->input->get('SN'), FILTER_SANITIZE_STRING));
		// IDSU
		// IDSP


		$this->load->view(
			'homepage.phtml', array('title' => 'Frequently Ask Question', 
			'view' => 'health-profile/index'));
		

		$pagenum = $this->input->get('id');
		echo 'ID:'.$pagenum;

		return;
	}

	public function printit()
	{
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$cardData = array();
		$error = false;
		$customCss = array('custom-register');
		$filename = 'temporary';

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}


		$user_profile = array(
			'fullname' => $this->nagkamoritsing->ibalik($user_profile['fullname']),
			'street' => $user_profile['street'],
			'city' => $user_profile['city'],
			'state' => $user_profile['state'],
			'postcode' => $user_profile['postcode'],
			'country' => $user_profile['country'],
			'dateofbirth' => $user_profile['dateofbirth'],
			'sex' => $user_profile['sex'],
			'bloodtype' => $user_profile['bloodtype'],
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_user']),
			'sessionName' => $this->nagkamoritsing->ibalik(@$user_profile['sessionName']),
            'firstname' => $this->nagkamoritsing->ibalik(@$user_profile['firstname']),
            'middlename' => $this->nagkamoritsing->ibalik(@$user_profile['middlename']),
            'lastname' => $this->nagkamoritsing->ibalik(@$user_profile['lastname']),
            'default' => @$user_profile['default'],
            'sessionAddress' => $this->ipAddress
		);

		$personalInfodata = array(
			'IDsafe_personalInfo' =>  $user_profile['IDsafe_personalInfo']
		);
		$safeContact = $this->contactinfo->getContact($personalInfodata);

		$cardData = array(
			'fullname' => $safeContact['fullname'],
			'contactphone' => $safeContact['countrycode'].'('.$safeContact['areacode'].')'.$safeContact['number'],
			'contactemail' => $safeContact['email'],
			'fulladdress' => $safeContact['fulladdress']
		);


		$pdf = new FPDF();

		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->SetTitle('Nambal.com Health Card');
		$pdf->SetAuthor('Nambal.com software consultancy');
		$pdf->SetCompression(false);
		$pdf->SetSubject('Nambal.com Health Card');
		$pdf->Cell(40,10,'Your healthy lifestyle starts here.');
		$pdf->SetFont('Arial','B',13);
		$pdf->setXY(20,10);
		$pdf->Cell(40,30,'Please press ctrl + p (OR cmd + p for MAC) to print.');

		$pdf->Image(APPPATH.'card/nambal-card-fold-cut.jpg',11,50,115);
		$pdf->Image(APPPATH.'card/card.jpg',140,80,0);

		$instructions = "Keep this card in your wallet. This will help you in time of emergency care, an emergency professionals can access your health information by going to listed webpage or by scanning the QR code.\n
Keep the information current and print a new card when you update it. If your card is lost or stolen, you can cancel the QR code and print out a new card with new QR code.\n
A doctor who is subscribed to our Health Record for Medical Professsionals can also access your Health Record for their diagnosis. They will be granted access to update your Health Record.
Please keep posted for the uses of your Health Card by checking your Nambal Account often.";

		$pdf->SetFont('Arial','',11);
		$pdf->setXY(120,130);
		$pdf->MultiCell(80,5,$instructions);

		# generate the QR code image
		// echo 'IDsafe_personalInfo: '.$user_profile['IDsafe_personalInfo'].'<br>';
		if ($this->qrModel->retrieveCurrent(@$user_profile['IDsafe_personalInfo']))
    	{
			# if naa sa database show the current qr
			$filename = $this->qrModel->qrInfo;
			$filename = $filename->filename;
		}
		$retrieveimage = file_get_contents($this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename);
		$ch = curl_init($this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename);

		
		curl_setopt($ch, CURLOPT_HEADER, 0);

		# added just for the .htaccess in the prototype
		// $username = 'jambal';
		// $password = '1234!@#$';
		// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		// curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		# end of added just for the .htaccess in the prototype

		curl_exec($ch);
		curl_close($ch);
		# end of generating the QR code image'


		$pdf->Image(APPPATH.'../public/qr/'.$filename.'.png', 22, 63 ,36);

        // @$user_profile['state'];@$user_profile['country'];@$user_profile['postcode']

		$cardContent = "Name: ".$user_profile['fullname']."    Blood type: ".$user_profile['bloodtype']."\n";
		$cardContent .= "Gender: ".$user_profile['sex']."   Date of birth: ".$user_profile['dateofbirth']."\n";
		$cardContent .= "Address: ".$user_profile['street'].", ".@$user_profile['city'].", ".@$user_profile['state'].", ".@$user_profile['country'].", ".@$user_profile['postcode']."\n";
		$cardContent .= "Emergency contact information:"."\n";
		$cardContent .= "(".@$cardData['fullname'].") ".@$cardData['fulladdress'].'; '.@$cardData['contactphone'].'; '.@$cardData['contactemail'];
		$pdf->setXY(19,125);
		$pdf->SetFont('Arial','B',8);
		$pdf->MultiCell(80,3.2,$cardContent);

		$code = $this->qrModel->qrInfo->textcode;
		$pdf->setXY(63,93);
		$pdf->SetFont('Arial','',9);
		$pdf->SetTextColor(165,13,13);
		$pdf->MultiCell(80,4,$code);


		// $pdf->Image(APPPATH.'card/ruler.jpg',0,0,209);
		$pdf->Output();
	}

	public function saveitfromdoctor()
	{
		$error = FALSE;
		$errorMessage = '';
		$this->user_profile['IDsafe_user'] = filter_var(@$this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$this->user_profile['forceto'] = filter_var(@$this->input->get('forceto'), FILTER_SANITIZE_STRING);
		
		# get the information using IDsafe_user
		$patientinformation = $this->safeusermodel->getSafeUserInfo(@$this->user_profile['IDsafe_user']);
		$patientpersonalinformation = $this->personalprofile->getPersonalInfo($this->nagkamoritsing->ibalik(@$this->user_profile['IDsafe_user']));

		if (empty($patientinformation))
		{
			$error = TRUE;
		}

		if (empty($patientpersonalinformation))
		{
			$error = TRUE;
		}

		# handling emergency contact information
		$data = array(
			'IDsafe_personalInfo' => @$patientpersonalinformation->IDsafe_personalInfo
		);
		$emergencycontact = $this->contactinfo->getContact($data);

		# address of the patient
		$patientaddress = $this->addressesmodel->getPersonalInfoAddress(@$patientpersonalinformation->IDsafe_personalInfo);

		// print_r($patientinformation);
		// echo 'test: '.$this->user_profile['IDsafe_user'];
		// $this->output->enable_profiler(TRUE);

		# check if naka login ba as doctor, 
		# doctor ray authorize mo generate og nambal card sa lain.
		if (empty($this->nambal_session['sessionName']))
        {
        	$error = TRUE;
        	$errorMessage .= 'Need to login again.  ';
        }
    	if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	$error = TRUE;
        	$errorMessage .= 'You are not authorize to print this nambal card.  ';
        }
        # check if naa ba IDSafe_user
		if (empty($patientpersonalinformation))
		{
			$error = TRUE;
			$errorMessage .= 'No patient to print nambal card.  ';
		}

		
		# start printing now.
		if ($error == FALSE)
		{
			$doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			# butang sa database kinsa ang ni print
			$this->qrModel->inputprinter($this->nagkamoritsing->ibalik($this->user_profile['IDsafe_user']), $doctorInformation->IDemr_user);

			$errorMessage = '';
			$cardData = array();
			$error = false;
			$customCss = array('custom-register');

			$fullnamecard = ucwords($this->nagkamoritsing->ibalik(@$patientinformation->firstname).' '.$this->nagkamoritsing->ibalik(@$patientinformation->lastname));


			$user_profile = array(
				'fullname' => @$fullnamecard,
				'street' => @$patientaddress->street,
				'city' => @$patientaddress->city,
				'state' => @$patientaddress->state,
				'postcode' => @$patientaddress->postcode,
				'country' => @$patientaddress->country,
				'dateofbirth' => @$patientpersonalinformation->dateofbirth,
				'sex' => @$patientpersonalinformation->sex,
				'bloodtype' => @$patientpersonalinformation->bloodtype,
				'IDsafe_personalInfo' => @$patientpersonalinformation->IDsafe_personalInfo,
	            'IDsafe_user' => @$patientpersonalinformation->IDsafe_user,
				'sessionName' => $this->nagkamoritsing->ibalik(@$patientinformation->username),
	            'firstname' => $this->nagkamoritsing->ibalik(@$patientinformation->firstname),
	            'middlename' => $this->nagkamoritsing->ibalik(@$patientinformation->middlename),
	            'lastname' => $this->nagkamoritsing->ibalik(@$patientinformation->lastname),
	            'default' => TRUE,
	            'sessionAddress' => $this->ipAddress
			);

			$personalInfodata = array(
				'IDsafe_personalInfo' =>  $user_profile['IDsafe_personalInfo']
			);
			$safeContact = $this->contactinfo->getContact($personalInfodata);
			
			if(!empty($safeContact['areacode']))
			{
				$areaCode = " (".$safeContact['areacode'].") ";
			}
			else
			{
				$areaCode = "";
			}

			$cardData = array(
				'fullname' => $safeContact['fullname'],
				'contactphone' => '+'.$safeContact['countrycode'].' '.$areaCode.' '.$safeContact['number'],
				'contactemail' => $safeContact['email'],
				'fulladdress' => $safeContact['fulladdress']
			);

			$pdf = new FPDF();

			$pdf->AddPage();
			$pdf->SetFont('Arial','B',16);
			$pdf->SetTitle('Nambal.com Health Card');
			$pdf->SetAuthor('Nambal.com software consultancy');
			$pdf->SetCompression(false);
			$pdf->SetSubject('Nambal.com Health Card');
			// $pdf->Cell(40,10,'Your healthy lifestyle starts here.');
			$pdf->SetFont('Arial','B',13);
			$pdf->setXY(20,10);
			// $pdf->Cell(40,30,'Please press ctrl + s (OR cmd + s for MAC) to save.');

			$pdf->Image(APPPATH.'card/nambal-card-fold-cut.jpg',11,50,115);
			$pdf->Image(APPPATH.'card/card.jpg',140,80,0);

			$instructions = "Your healthy lifestyle starts here. Keep this card in your wallet. This will help you in time of emergency care, an emergency professionals can access your health information by going to listed webpage or by scanning the QR code.\n
	Keep the information current and print a new card when you update it. If your card is lost or stolen, you can cancel the QR code and print out a new card with new QR code.\n
	A doctor who is subscribed to our Health Record for Medical Professsionals can also access your Health Record for their diagnosis. They will be granted access to update your Health Record.
	Please keep posted for the uses of your Health Card by checking your Nambal Account often.";

			$pdf->SetFont('Arial','',8);
			$pdf->setXY(130,130);
			$pdf->MultiCell(60,5,$instructions,0,"L");

			# generate the QR code image
			// echo 'IDsafe_personalInfo: '.$user_profile['IDsafe_personalInfo'].'<br>';
			if ($this->qrModel->retrieveCurrent(@$user_profile['IDsafe_personalInfo']))
	    	{
				# if naa sa database show the current qr
				$filename = $this->qrModel->qrInfo;
				$filename = $filename->filename;
				$filenameURL = "&filename=".$filename;
			}
			else
			{
				$filename = 'temporary';
				$filenameURL = '';
			}

			// echo base_url()."qr/otherusergenerator?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo'].$filename.'<br>';
			$retrieveimage = file_get_contents($this->baseurl_wouthttps."qr/otherusergenerator?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo'].'&docuser='.$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']).$filenameURL);
			$ch = curl_init($this->baseurl_wouthttps."qr/otherusergenerator?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo'].'&docuser='.$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']).$filenameURL);

			// curl_setopt($ch, CURLOPT_HEADER, 0);

			// # added just for the .htaccess in the prototype
			// // $username = 'jambal';
			// // $password = '1234!@#$';
			// // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			// // curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
			// # end of added just for the .htaccess in the prototype

			curl_exec($ch);
			curl_close($ch);
			# end of generating the QR code image'

			if ($this->qrModel->retrieveCurrent(@$user_profile['IDsafe_personalInfo']))
	    	{
				# if naa sa database show the current qr
				$filename = $this->qrModel->qrInfo;
				$filename = $filename->filename;
				$filenameURL = "&filename=".$filename;
			}
			

			if (!empty($this->qrModel->qrInfo->textcode))
				$code = $this->qrModel->qrInfo->textcode;
			else {
				// $this->qrModel->retrieveCurrent();
				$codeModel = $this->qrModel->getCodeOnly(@$user_profile['IDsafe_personalInfo']);
				// print_r($codeModel);
				$code = $codeModel['textcode'];
				// $code = base_url()."qr/otherusergenerator?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo'].$filenameURL;
			}

			if (!file_exists(APPPATH.'../public/qr/'.$filename.'.png'))
			{	
				$retrieveimage = file_get_contents($this->baseurl_wouthttps.'qr/otherusergenerator?save=TRUE&redirect=TRUE&sessionname='.@$user_profile['sessionName'].'&idsu='.@$user_profile['IDsafe_user'].'&idsp='.@$user_profile['IDsafe_personalInfo'].'&filename='.$filename, 'refresh');		
				// redirect($this->baseurl_wouthttps.'qr/otherusergenerator?save=TRUE&redirect=TRUE&sessionname='.@$user_profile['sessionName'].'&idsu='.@$user_profile['IDsafe_user'].'&idsp='.@$user_profile['IDsafe_personalInfo'].'&filename='.$filename, 'refresh');
			}
			$pdf->Image(APPPATH.'../public/qr/'.$filename.'.png', 19, 61 ,43);
					
					//date of birth
					if(!empty($user_profile['dateofbirth']))
					{
						$dob = explode('-',@$user_profile['dateofbirth']);
						switch($dob[1])
						{
							case '01':
								$month = 'January';
							break;
							
							case '02':
								$month = 'February';
							break;
							
							case '03':
								$month = 'March';
							break;
							
							case '04':
								$month = 'April';
							break;
							
							case '05':
								$month = 'May';
							break;
							
							case '06':
								$month = 'June';
							break;
							
							case '07':
								$month = 'July';
							break;
							
							case '08':
								$month = 'August';
							break;
							
							case '09':
								$month = 'September';
							break;
							
							case '10':
								$month = 'Octber';
							break;
							
							case '11':
								$month = 'November';
							break;
							
							case '12':
								$month = 'December';
							break;
							
							default:
								$month = '';
							break;
						}
						$dayOfBirth = explode(' ', $dob[2]);
						$dateOfBirth = $month.' '.$dayOfBirth[0].', '.$dob[0]; 
					}
					else
					{
						$dateOfBirth = ''; 
					} 
					
					//blood type
					if(strcmp($user_profile['bloodtype'], 'DKY')==0)
					{
						$blood_type = "don't know yet";
					}
					else
					{
						$blood_type = $user_profile['bloodtype'];
					}

	        // @$user_profile['state'];@$user_profile['country'];@$user_profile['postcode']

			$pdf->setXY(19,125);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(75,2.5,"Name: ",0,"L");
			$pdf->setXY(27,125);
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(80,2.5,ucwords($user_profile['fullname'])."\n",0,"L");
			$pdf->setXY(19,127.5);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(80,2.5,"Date of birth: ",0,"L");
			$pdf->setXY(35,127.5);
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(80,2.5,$dateOfBirth."\n",0,"L");
			$pdf->setXY(19,130);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(80,2.5,"Gender: ",0,"L");
			$pdf->setXY(29,130);
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(80,2.5,$user_profile['sex'],0,"L");
			$pdf->setXY(40,130);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(80,2.5,"Blood type: ",0,"L");
			$pdf->setXY(54,130); 
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(80,2.5,$blood_type."\n",0,"L");
			$pdf->setXY(19,132.5);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(80,2.5,"Address: ",0,"L");
			$pdf->setXY(30,132.5);
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(70,2.5,$user_profile['street'].", ".@$user_profile['city'].", ".@$user_profile['state'].", ".@$user_profile['country'].", ".@$user_profile['postcode']."\n",0,"L");
			$pdf->setXY(19,140);
			$pdf->SetFont('Arial','B',6);
			$pdf->MultiCell(80,2.5,"Emergency contact information:"."\n",0,"L");
			$pdf->setXY(19,143);
			$pdf->SetFont('Arial','',6);
			$pdf->MultiCell(80,2.5,"(".ucwords(@$cardData['fullname']).") ".$cardData['contactphone'].' '.@$cardData['contactemail'].' '.@$cardData['fulladdress'],0,"L");

			
			$pdf->setXY(63,93);
			$pdf->SetFont('Arial','',9);
			$pdf->SetTextColor(165,13,13);
			$pdf->MultiCell(80,4,$code);


			// $pdf->Image(APPPATH.'card/ruler.jpg',0,0,209);
			if ( (!empty($this->user_profile['forceto'])) && ($this->user_profile['forceto'] == TRUE) )
				$pdf->Output();
			else
				$pdf->Output("Nambal-card-".$user_profile['fullname'].".pdf","D");
			// $pdf->Output();

			

		}
		else
		{
			if (!empty($user_profile['fullname']))
			{
				$pdf = new FPDF();
				$pdf->AddPage();
				$pdf->SetFont('Arial','B',16);
				$pdf->SetTitle('Nambal.com Health Card');
				$pdf->SetAuthor('Nambal.com software consultancy');
				$pdf->SetCompression(false);
				$pdf->SetSubject('Nambal.com Health Card');
				// $pdf->Cell(40,10,'Your healthy lifestyle starts here.');
				$pdf->SetFont('Arial','B',13);
				$pdf->MultiCell(225,7,$errorMessage,0,"L");
				if ( (!empty($this->user_profile['forceto'])) && ($this->user_profile['forceto'] == TRUE) )
					$pdf->Output();
				else
					$pdf->Output("Nambal-card-".$user_profile['fullname'].".pdf","D");
			}
		}
		
	}
	
	public function saveit()
	{
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$cardData = array();
		$error = false;
		$customCss = array('custom-register');

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}


		$user_profile = array(
			'fullname' => ucwords($this->nagkamoritsing->ibalik($user_profile['fullname'])),
			'street' => $user_profile['street'],
			'city' => $user_profile['city'],
			'state' => $user_profile['state'],
			'postcode' => $user_profile['postcode'],
			'country' => $user_profile['country'],
			'dateofbirth' => $user_profile['dateofbirth'],
			'sex' => $user_profile['sex'],
			'bloodtype' => $user_profile['bloodtype'],
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_user']),
			'sessionName' => $this->nagkamoritsing->ibalik(@$user_profile['sessionName']),
            'firstname' => ucwords($this->nagkamoritsing->ibalik(@$user_profile['firstname'])),
            'middlename' => ucwords($this->nagkamoritsing->ibalik(@$user_profile['middlename'])),
            'lastname' => ucwords($this->nagkamoritsing->ibalik(@$user_profile['lastname'])),
            'default' => @$user_profile['default'],
            'sessionAddress' => $this->ipAddress
		);

		$personalInfodata = array(
			'IDsafe_personalInfo' =>  $user_profile['IDsafe_personalInfo']
		);
		$safeContact = $this->contactinfo->getContact($personalInfodata);
		
		if(!empty($safeContact['areacode']))
		{
			$areaCode = " (".$safeContact['areacode'].") ";
		}
		else
		{
			$areaCode = "";
		}

		$cardData = array(
			'fullname' => ucwords($safeContact['fullname']),
			'contactphone' => '+'.$safeContact['countrycode'].' '.$areaCode.' '.$safeContact['number'],
			'contactemail' => $safeContact['email'],
			'fulladdress' => $safeContact['fulladdress']
		);

		$pdf = new FPDF();

		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->SetTitle('Nambal.com Health Card');
		$pdf->SetAuthor('Nambal.com software consultancy');
		$pdf->SetCompression(false);
		$pdf->SetSubject('Nambal.com Health Card');
		// $pdf->Cell(40,10,'Your healthy lifestyle starts here.');
		$pdf->SetFont('Arial','B',13);
		$pdf->setXY(20,10);
		// $pdf->Cell(40,30,'Please press ctrl + s (OR cmd + s for MAC) to save.');

		$pdf->Image(APPPATH.'card/nambal-card-fold-cut.jpg',11,50,115);
		$pdf->Image(APPPATH.'card/card.jpg',140,80,0);

		$instructions = "Your healthy lifestyle starts here. Keep this card in your wallet. This will help you in time of emergency care, an emergency professionals can access your health information by going to listed webpage or by scanning the QR code.\n
Keep the information current and print a new card when you update it. If your card is lost or stolen, you can cancel the QR code and print out a new card with new QR code.\n
A doctor who is subscribed to our Health Record for Medical Professsionals can also access your Health Record for their diagnosis. They will be granted access to update your Health Record.
Please keep posted for the uses of your Health Card by checking your Nambal Account often.";

		$pdf->SetFont('Arial','',8);
		$pdf->setXY(130,130);
		$pdf->MultiCell(60,5,$instructions,0,"L");

		$filename = '';		
		// curl_setopt($ch, CURLOPT_HEADER, 0);

		# added just for the .htaccess in the prototype
		// $username = 'jambal';
		// $password = '1234!@#$';
		// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		// curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		# end of added just for the .htaccess in the prototype


		#---
		$ch = curl_init();
		
		// $retrieveimage = file_get_contents($this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename);
		$url = $this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename;

		$browser = array(
        "user_agent" => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 (.NET CLR 3.5.30729)",
        "language" => "en-us,en;q=0.5"
        );
 
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
 
        // set browser specific headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "User-Agent: {$browser['user_agent']}",
                "Accept-Language: {$browser['language']}",
                "Content-Type: image/png"
            ));
 
        // we don't want the page contents
        // curl_setopt($ch, CURLOPT_NOBODY, 1);
        // we need the HTTP Header returned
        curl_setopt($ch, CURLOPT_HEADER, 1);
        // return the results instead of outputting it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
		#---


		# generate the QR code image
		// echo 'IDsafe_personalInfo: '.$user_profile['IDsafe_personalInfo'].'<br>';
		if ($this->qrModel->retrieveCurrent(@$user_profile['IDsafe_personalInfo']))
    	{
			# if naa sa database show the current qr
			$filename = $this->qrModel->qrInfo;
			$filename = $filename->filename;
		}

		// $retrieveimage = file_get_contents($this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename);
		$ch = curl_init($this->baseurl_wouthttps."qr/generateSelf?save=TRUE&sessionname=".@$user_profile['sessionName']."&idsu=".@$user_profile['IDsafe_user']."&idsp=".@$user_profile['IDsafe_personalInfo']."&filename=".$filename);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// curl_setopt($ch, CURLOPT_HEADER, 0);

		# added just for the .htaccess in the prototype
		// $username = 'jambal';
		// $password = '1234!@#$';
		// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		// curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		# end of added just for the .htaccess in the prototype

		curl_exec($ch);
		curl_close($ch);

		# end of generating the QR code image'


		if (!file_exists(APPPATH.'../public/qr/'.$filename.'.png'))
		{			
			// redirect($this->baseurl_wouthttps.'qr/generateSelf?save=TRUE&redirect=TRUE&sessionname='.@$user_profile['sessionName'].'&idsu='.@$user_profile['IDsafe_user'].'&idsp='.@$user_profile['IDsafe_personalInfo'].'&filename='.$filename, 'refresh');
			print_r($output);
		}
		$pdf->Image(APPPATH.'../public/qr/'.$filename.'.png', 19, 61 ,43);
				//date of birth
				if(!empty($user_profile['dateofbirth']))
				{
					$dob = explode('-',@$user_profile['dateofbirth']);
					switch($dob[1])
					{
						case '01':
							$month = 'January';
						break;
						
						case '02':
							$month = 'February';
						break;
						
						case '03':
							$month = 'March';
						break;
						
						case '04':
							$month = 'April';
						break;
						
						case '05':
							$month = 'May';
						break;
						
						case '06':
							$month = 'June';
						break;
						
						case '07':
							$month = 'July';
						break;
						
						case '08':
							$month = 'August';
						break;
						
						case '09':
							$month = 'September';
						break;
						
						case '10':
							$month = 'Octber';
						break;
						
						case '11':
							$month = 'November';
						break;
						
						case '12':
							$month = 'December';
						break;
						
						default:
							$month = '';
						break;
					}
					$dayOfBirth = explode(' ', $dob[2]);
					$dateOfBirth = $month.' '.$dayOfBirth[0].', '.$dob[0];  
				}
				else
				{
					$dateOfBirth = ''; 
				} 
				
				//blood type
				if(strcmp($user_profile['bloodtype'], 'DKY')==0)
				{
					$blood_type = "don't know yet";
				}
				else
				{
					$blood_type = $user_profile['bloodtype'];
				}

        // @$user_profile['state'];@$user_profile['country'];@$user_profile['postcode']

		$pdf->setXY(19,125);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(75,2.5,"Name: ",0,"L");
		$pdf->setXY(27,125);
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(80,2.5,$user_profile['fullname']."\n",0,"L");
		$pdf->setXY(19,127.5);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(80,2.5,"Date of birth: ",0,"L");
		$pdf->setXY(35,127.5);
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(80,2.5,$dateOfBirth."\n",0,"L");
		$pdf->setXY(19,130);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(80,2.5,"Gender: ",0,"L");
		$pdf->setXY(29,130);
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(80,2.5,$user_profile['sex'],0,"L");
		$pdf->setXY(40,130);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(80,2.5,"Blood type: ",0,"L");
		$pdf->setXY(54,130); 
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(80,2.5,$blood_type."\n",0,"L");
		$pdf->setXY(19,132.5);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(80,2.5,"Address: ",0,"L");
		$pdf->setXY(30,132.5);
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(70,2.5,$user_profile['street'].", ".@$user_profile['city'].", ".@$user_profile['state'].", ".@$user_profile['country'].", ".@$user_profile['postcode']."\n",0,"L");
		$pdf->setXY(19,140);
		$pdf->SetFont('Arial','B',6);
		$pdf->MultiCell(80,2.5,"Emergency contact information:"."\n",0,"L");
		$pdf->setXY(19,143);
		$pdf->SetFont('Arial','',6);
		$pdf->MultiCell(80,2.5,"(".@$cardData['fullname'].") ".$cardData['contactphone'].' '.@$cardData['contactemail'].' '.@$cardData['fulladdress'],0,"L");

		$code = $this->qrModel->qrInfo->textcode;
		$pdf->setXY(63,93);
		$pdf->SetFont('Arial','',9);
		$pdf->SetTextColor(165,13,13);
		$pdf->MultiCell(80,4,$code);


		// $pdf->Image(APPPATH.'card/ruler.jpg',0,0,209);
		// $pdf->Output("Nambal-card-".$user_profile['fullname'].".pdf","D");
		$pdf->Output("Nambal-card-".$user_profile['fullname'].".pdf","D");
	}

	public function justestcurl()
	{
		$ch = curl_init();
		$url = 'http://www.nambal.com/qr/generateSelf';
		echo $url.'<br>';

		$browser = array(
        "user_agent" => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 (.NET CLR 3.5.30729)",
        "language" => "en-us,en;q=0.5"
        );
 
        // set url
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
 
        // set browser specific headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "User-Agent: {$browser['user_agent']}",
                "Accept-Language: {$browser['language']}",
                "Content-Type: image/png"
            ));
 
        // we don't want the page contents
        // curl_setopt($ch, CURLOPT_NOBODY, 1);
 
        // we need the HTTP Header returned
        curl_setopt($ch, CURLOPT_HEADER, 1);
 
        // return the results instead of outputting it
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
        $output = curl_exec($ch);

        print_r($output);
 
        curl_close($ch);
        error_reporting(E_ALL);
		ini_set('display_errors', 1);

		echo '<br><br>';


		if ($this->iscurlsupported()) 
		{
	            echo "cURL is supported\n";
	    }
	    else {
	            echo "cURL isn't supported\n";
	    }

	}

	private function iscurlsupported() {
            if (in_array ('curl', get_loaded_extensions())) {
                return true;
            }
            else {
                return false;
            }
    }

    public function phpinfo()
    {
    	echo phpinfo();
    }

    public function notcurltest()
    {
    	$xmlData = file_get_contents('https://www.nambal.com/qr/generateSelf');
    	print_r($xmlData);

    }

	
}

?>
