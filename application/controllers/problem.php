<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');

class Problem extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct();

		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");

		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Problem_model', 'problem');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->model('Safeuser_model', 'safeuser');
		
		
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
         
	}

	public function index()
	{
		$this->checkSession();
		$this->load->view(
			'homepage.phtml', array('title' => 'Problem Information', 
			'view' => 'problem/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$problemlist = array();

		$this->form_validation->set_rules('conditionOrSymptom','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('problem-now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$conditionOrSymptom = ucwords(filter_var($this->input->post('conditionOrSymptom'), FILTER_SANITIZE_STRING));
			$problemnow = filter_var($this->input->post('problem-now'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);		

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (strcasecmp($problemnow,'TRUE')==0)
			{ $problemnow = TRUE; }
			else
			{ $problemnow = FALSE; }

			# here you can add the symptoms
			$problemlist = array(
				'conditionOrSymptom' => $conditionOrSymptom,
				'problemnow' => (boolean)$problemnow,
				'datetimestarted' => @$datetimeStart,
				'datetimeended' => @$datetimeEnd,
				'notes' => @$notes
			);

			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage = "<p>Date ended should be after date started<p>";
				}
			}
			
			if ($error == FALSE)
				$this->problem->insertProblem($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $problemlist);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $problemlist)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
			'view' => 'problem/insert', 
            'safeContact' => $problemlist
            ));
		}

		return;
	}

	public function insertDoctor()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$problemlist = array();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		$this->form_validation->set_rules('conditionOrSymptom','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('problem-now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Private Journal for the', 'trim|xss_clean'); //doctor-notes

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$conditionOrSymptom = ucwords(filter_var($this->input->post('conditionOrSymptom'), FILTER_SANITIZE_STRING));
			$problemnow = filter_var($this->input->post('problem-now'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);

			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo
			# get the IDsafe_personalInfo
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (strcasecmp($problemnow,'TRUE')==0)
			{ $problemnow = TRUE; }
			else
			{ $problemnow = FALSE; }

			# here you can add the symptoms
			$problemlist = array(
				'conditionOrSymptom' => $conditionOrSymptom,
				'problemnow' => (boolean)$problemnow,
				'datetimestarted' => @$datetimeStart,
				'datetimeended' => @$datetimeEnd,
				'notes' => @$notes
			);

			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage .= "<p>Date ended should be after date started<p>";
				}
			}
			
			if ($error == FALSE)
			{
				$IDsafe_problem = $this->problem->insertProblem($IDsafe_personalInfo, $problemlist);

				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				
				$problemlist['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$problemlist['IDemr_user'] = $doctorInformation->IDemr_user;
				$problemlist['doctor-notes'] = $doctorNotes;
				$problemlist['IDsafe_problem'] = $IDsafe_problem;
				$problemlist['IDemr_patient'] = $IDemr_patient;
				
				$this->safejournal->insertjournalproblem($problemlist);
				$patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);
				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		
				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
			}
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $problemlist)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
			'view' => 'problem/insert', 
            'safeContact' => $problemlist
            ));
		}

		return;
	}

	public function update()
	{
		$this->checkSession();
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$error = TRUE;
		$errorMessage = '';
		$customCss = array('custom-dashboard');
		$safeProblem = array();
		$problemlist = array();

		$this->form_validation->set_rules('conditionOrSymptom','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('problem-now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('IDsafe_problem', 'IDsafe_problem', 'trim|xss_clean|required');


		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = FALSE;
		}

		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
		$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);
		
		if ($error == FALSE)
		{
			$conditionOrSymptom = ucwords(filter_var($this->input->post('conditionOrSymptom'), FILTER_SANITIZE_STRING));
			$problemnow = ucwords(filter_var($this->input->post('problem-now'), FILTER_SANITIZE_STRING));
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$notes = (filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING));
			$IDsafe_problem = (filter_var($this->input->post('IDsafe_problem'), FILTER_SANITIZE_STRING));
			
			$IDemr_journal = (filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING));
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING); # notes for doctor

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (strcasecmp($problemnow,'TRUE')==0)
			{ $problemnow = TRUE; }
			else
			{ $problemnow = FALSE; }

			# here you can add the symptoms
			$problemlist = array(
				'conditionOrSymptom' => $conditionOrSymptom,
				'problemnow' => (boolean)$problemnow,
				'datetimestarted' => @$datetimeStart,
				'datetimeended' => @$datetimeEnd,
				'notes' => @$notes,
				'IDsafe_problem' => $IDsafe_problem
			);							

			$dataInDb = $this->problem->getToModifyProblem($IDsafe_problem);
			if (($dataInDb->havingitnow == $problemnow) &&
				(strcasecmp($dataInDb->dateStart, $datetimeStart)==0) &&
				(strcasecmp($dataInDb->dateEnd, $datetimeEnd)==0) &&
				(strcasecmp($dataInDb->problemName, $conditionOrSymptom)==0) &&
				(strcasecmp($dataInDb->notes, $notes)==0))
			{
				# no update needed all the same.
			}
			else
			{
				if (!empty($datetimeEnd))
				{
					if ( $datetimeStart > $datetimeEnd )
					{
						$error = TRUE;
						$errorMessage = "<p>Date ended should be after date started<p>";
					}
				}
				
				if ($error == FALSE)
					$this->problem->updateSafeProblem($problemlist);
			}

			if (strcasecmp($isdoctor, 'TRUE')==0)
            {
	            # added for doctor updates
	            # check if patient ba ni siya sa doctor
	            # if dili i add siya as one of the patient
	            // $IDsafe_user = $this->nagkamoritsing->ibalik($this->sessionid);
	            $IDsafe_personalInfo = $dataInDb->IDsafe_personalInfo;
	            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
	            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
	            $IDemr_patient = @$IDemr_patient->IDemr_patient;
	            if (empty($IDemr_patient))
	                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

	            $problemlist['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$problemlist['IDemr_user'] = $doctorInformation->IDemr_user;
				$problemlist['doctor-notes'] = $doctorNotes;
				$problemlist['IDemr_patient'] = $IDemr_patient;
				$problemlist['IDemr_journal'] = $IDemr_journal;

	            # doctor notes connection and for sending email
	            # if empty ang IDemr_journal
	            # meaning patient input ni
	            if ((empty($IDemr_journal)) || ($IDemr_journal == 0)) {
	            	$this->safejournal->insertjournalproblem($problemlist);
	            } else {
	            	$this->safejournal->addjournal($problemlist,'safe_problem');
	            }
	            $patientinfo = $this->safeuser->getSafeUserInfoUsingPersonalInfo($IDsafe_personalInfo);
	            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($patientinfo->IDsafe_user);
	            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

	            $emailData = array(
	                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
	                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
	            );
	            $message = $this->load->view(
	                    'for-email.phtml', array('title' => 'Notify patient', 
	                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
	    
	            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
            	$this->safeuserlog->notifypatientbyemail($patientinfo->IDsafe_user,$message);
        	}
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $problemlist)
			);
			
			# end of ajax ang display
		}
		else
		{
			// $this->load->view(
			// 	'homepage.phtml', array('title' => 'Update Contact Information', 
			// 	'view' => 'emergency-contact-information/update'));
			// $sampleData = $this->input->get('sampleData');
			// echo 'profileID:'.$sampleData;
		}

		
		
		return;
	}

	public function delete()
	{
		
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('problemid', 'problemid',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$IDsafe_problem = filter_var($this->input->post('problemid'), FILTER_SANITIZE_STRING);
			
			$returnlang = $this->problem->deleteSafeProblem($IDsafe_problem);

			header('Content-Type: application/json');
			$jsonDisplay['returnlang'] = $returnlang;
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Problem Information', 
			'view' => 'problem/delete'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}

	public function deleteDoctor()
	{
		
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		# check if doctor ba jud ang ga delete

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('problemid', 'problemid',  'trim|required|xss_clean');

		# know who is the patient
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);


		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$IDsafe_problem = filter_var($this->input->post('problemid'), FILTER_SANITIZE_STRING);
			
			$returnlang = $this->problem->deleteSafeProblem($IDsafe_problem, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));  #butangan og sakto nga doctor ID

			header('Content-Type: application/json');
			$jsonDisplay['returnlang'] = $returnlang;
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Problem Information', 
			'view' => 'problem/delete'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}

	public function dashboardDisplay()
	{
		$checkReturn = $this->checksession('DOM');
		$this->safeproblem = $this->problem->displayDashboardProblem($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);


		# for paging result
        # will work with custom-dashboard-problem.js pagination code
        # and display_problem-information.phtml div problem-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'problem/dashboardDisplay?q=1';
        $config["total_rows"] = $this->problem->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->problem->displayDashboardProblem($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('problem/display_problem-information.phtml', $data, array('somevariable'));
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;
		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        $this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->safeproblem = $this->problem->displayDashboardProblem($IDsafe_personalInfo);

		# for paging result
        # will work with custom-dashboard-problem-profileviewer.js pagination code
        # and display_jounal-information.phtml div journal-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'problem/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->problem->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->problem->displayDashboardProblem($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('problem/display_problem-information-profileviewer.phtml', $data, array('somevariable'));
	}

	public function insertProblemDisplay()
	{
		$checkReturn = $this->checksession('DOM');
		$arrayGetContact = array();

		$IDsafe_problem = filter_var($this->input->get('IDsafe_problem'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_problem' => $IDsafe_problem
		);

		# get the problem information
		$problemInformation = $this->problem->getToModifyProblem($IDsafe_problem);
	
		$dateEnd = $this->problem->checkDateStartEnd(@$problemInformation->dateStart, @$problemInformation->dateEnd, TRUE);

		$this->problemInformation = array(
			'problemName' => @$problemInformation->problemName,
			'havingitnow' => (string)@$problemInformation->havingitnow,
			'dateStart' => @$problemInformation->dateStart,
			'dateEnd' => $dateEnd,
			'notes' => @$problemInformation->notes,
			'IDsafe_problem' => @$problemInformation->IDsafe_problem
		);
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('problem/display_problem-insert-information.phtml', array());

	}

	public function insertProblemDisplayDoctor()
	{
		$checkReturn = $this->checksession('DOM');
		$arrayGetContact = array();

		$IDsafe_problem = filter_var($this->input->get('IDsafe_problem'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_problem' => $IDsafe_problem
		);

		# get the problem information
		$problemInformation = $this->problem->getToModifyProblem($IDsafe_problem);

		if (!empty($problemInformation->IDemr_journal))
			$this->journalinformation = $this->safejournal->getjournal($problemInformation->IDemr_journal);

		$dateEnd = $this->problem->checkDateStartEnd(@$problemInformation->dateStart, @$problemInformation->dateEnd, TRUE);
	
		$this->problemInformation = array(
			'problemName' => @$problemInformation->problemName,
			'havingitnow' => (string)@$problemInformation->havingitnow,
			'dateStart' => @$problemInformation->dateStart,
			'dateEnd' => $dateEnd,
			'notes' => @$problemInformation->notes,
			'IDsafe_problem' => @$problemInformation->IDsafe_problem,
			'doctor-notes' => @$this->journalinformation->notes,
			'IDemr_journal' => @$this->journalinformation->IDemr_journal
		);	
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('problem/display_problem-insert-information-profileviewer.phtml', array());

	}

	public function displayProblems()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->problem->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	public function resolved()
	{
		$error = FALSE;
		$errorMessage = '';
		$problemlist = array();

		$checkReturn = $this->checksession('DOM');
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
		{
			echo '';
			return;
		}
			
		$arrayGetContact = array();

		$sessionid = filter_var($this->input->get('IDsafe_problem'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

		$this->form_validation->set_rules('problemid', 'Problem ID', 'trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$problemid = ucwords(filter_var($this->input->post('problemid'), FILTER_SANITIZE_STRING));

			# maka resolve kung ikaw tag-iya sa problem or ikaw ang doctor
			$this->problem->setResolveProblem($problemid, $value = FALSE);

			$problemlist = array(
				'IDsafe_problem' => $problemid
			);

		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $problemlist)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
			'view' => 'problem/insert', 
            'safeContact' => $problemlist
            ));
		}
	}

	public function fortesting()
	{
		# delete this after testing

		$this->load->view('problem/fortesting.phtml');
	}

	public function justfortesting()
	{
		$forArray = $this->problem->displayDashboardProblem($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

		foreach ($forArray as $value) 
		{
			print_r($value);
			echo '<br>';
		}
	}

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }

    public function justest()
    {
    	// print_r($this->problem->justanothertest(3, 2));
    	print_r($this->problem->deleteSafeProblem($IDsafe_problem));
    	$this->output->enable_profiler(TRUE);
    }
	
}
?>