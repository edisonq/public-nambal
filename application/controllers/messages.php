<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Messages extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
		$this->load->library('form_validation');
		$this->load->model('Personalprofile_model', 'personalProfile');
		$this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->model('Messages_model', 'messagesModel');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->helper('text');
        $this->activeMainMenu = "messages";
        $this->navOpen = "";

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
    
        # check user's if properly logged-in
        if (empty($this->nambal_session['sessionName']))
        {
            redirect(base_url().'login', 'refresh');
        }


        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        //     $fbUser = $this->facebook->getUser();
        //     if (!empty($fbUser))
        //     {
        //         if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        //         {
        //             redirect(base_url().'logout', 'refresh');
        //         }
        //     } else {
        //         redirect(base_url().'login/facebook', 'refresh');
        //     }
        // }
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }
        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in

        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 

	}

	public function index()
    {
		$errorMessage = '';
        $personalInfoOutput = '';
        $urlAdd = 'doctorstaff';
        
        $customCss = array('custom-dashboard','zocial', 'custom-messaging','custom-patient-dashboard');
        // $customJs = array('tables-dynamic');
        $customJs = array('message-doctor','../lib/jquery.dataTables.min-messaging','chat');
        $customJsTop = array('countries');

        $this->load->helper('date');
        $navOpen = $this->navOpen;  

        $arrayGetDocInfo = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 

        $this->to = filter_var($this->input->get('to'), FILTER_SANITIZE_STRING);
        $this->from = filter_var($this->input->get('from'), FILTER_SANITIZE_STRING);

        # get the information of the  user
        $this->returnedSearch = $this->safeuser->getSafeUserInfo($this->to);
        
        # end of getting the information of the user.

        # get inbox
        $this->inboxes = $this->messagesModel->getListSender($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        # end of getting inbox

        // check if receiver is doctor
        // if ($this->safeuser->checkIfDoctor($this->nagkamoritsing->ibalik($this->to)))
        // {

        // }
        // end checking if doctor


        # get the message to display
        if ((!empty($this->from)))
        {
            # display the "from" users
            $this->messageToDisplay = $this->messagesModel->getmessages($this->nagkamoritsing->ibalik($this->from),$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $this->IDsafe_userTO = $this->nagkamoritsing->ibalik($this->from);
            $this->fullname = $this->safeuser->getSafeUserInfo($this->from);
            $this->fullname = ucfirst($this->nagkamoritsing->ibalik(@$this->fullname->firstname)).' '.ucfirst($this->nagkamoritsing->ibalik(@$this->fullname->lastname));
            # end of display the "from" users  
        }
        elseif (!empty($this->to)) 
        {
            $this->messageToDisplay = $this->messagesModel->getmessages($this->nagkamoritsing->ibalik($this->to),$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $this->IDsafe_userTO = $this->nagkamoritsing->ibalik($this->to);
            $this->fullname = $this->safeuser->getSafeUserInfo($this->to);
            $this->fullname = ucfirst($this->nagkamoritsing->ibalik(@$this->fullname->firstname)).' '.ucfirst($this->nagkamoritsing->ibalik(@$this->fullname->lastname));
        }
        else
        {
            $looper = 0;
            foreach ($this->inboxes as $gettheFirst) 
            {
                if ($looper == 0)
                {
                    $this->theFirstMessage = $gettheFirst;
                }
                $looper++;
            }
            # getting all the unread conversation "from" this user
            if (!empty($this->theFirstMessage))
            {
                $from = $this->theFirstMessage->from_IDsafe_user;
                if ($from == $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']))
                {
                    $from = @$this->theFirstMessage->to_IDsafe_user;
                }

                $this->messageToDisplay = $this->messagesModel->getmessages($from,$this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
                $this->IDsafe_userTO = $from;

                if ($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']) == $this->theFirstMessage->to_IDsafe_user)
                {
                    $this->fullname = ucfirst($this->nagkamoritsing->ibalik($this->theFirstMessage->fromfirstname)).' '.ucfirst($this->nagkamoritsing->ibalik($this->theFirstMessage->fromlastname));
                }
                else
                {
                    $this->fullname = ucfirst($this->nagkamoritsing->ibalik($this->theFirstMessage->firstname)).' '.ucfirst($this->nagkamoritsing->ibalik($this->theFirstMessage->lastname));   
                }
            }
            //print_r($messageToDisplay);
            # end of getting all the unread conversation from this user
        }
        # end of getting the message to display

        $this->load->view(
        'patient-dashboard-light-blue.phtml', array(
            'title' => 'Connect to nambal community', 
            'customCss' => $customCss,
            'arrayGetDocInfo' => $arrayGetDocInfo,
            'personalInfoOutput' => $personalInfoOutput,
            'errorMessage' => $errorMessage,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'to' => $this->to,
            'navOpen' => $navOpen,
            'view' => 'messages/index',
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'
        )); 
        // $this->output->enable_profiler(TRUE);
        return;

	}

    public function create()
    {
        $this->load->model('Pusher_model', 'pushery');
        $returnedSearch = array();
        
        $json = '';
        $errorMessage = '';
        $urlAdd = 'messages';
        
        $customCss = array('custom-dashboard','zocial', 'custom-messaging','custom-patient-dashboard');
        // $customJs = array('tables-dynamic');
        $customJs = array('message-doctor','../lib/jquery.dataTables.min-messaging','chat');
        $customJsTop = array('countries');

        $navOpen = 'doctor options';

        $this->form_validation->set_rules('fromIDsafeUser', 'trim|required|xss_clean');
        $this->form_validation->set_rules('toIDsafeUser', 'trim|required|xss_clean');
        $this->form_validation->set_rules('message','trim|required|xss_clean');
        $this->form_validation->set_rules('json','trim|required|xss_clean');
        $this->form_validation->set_rules('messageid','trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
        }
        else
        {
            $error = false;
        }

        # sending message main area here
        if ($error != TRUE)
        {
            $fromIDsafeUser = filter_var($this->input->post('fromIDsafeUser'), FILTER_SANITIZE_STRING);
            $toIDsafeUser = filter_var($this->input->post('toIDsafeUser'), FILTER_SANITIZE_STRING);
            $message = filter_var($this->input->post('message'), FILTER_SANITIZE_STRING);
            $json = filter_var($this->input->post('json'), FILTER_SANITIZE_STRING);
            $messageid = filter_var($this->input->post('messageid'), FILTER_SANITIZE_STRING);
            
            $fromIDsafeUser = $this->nagkamoritsing->ibalik($fromIDsafeUser);
            $toIDsafeUser = $this->nagkamoritsing->ibalik($toIDsafeUser);

            $this->messagesModel->createMessage($toIDsafeUser, $fromIDsafeUser, $message);
            $this->pushery->sendChat($message, $fromIDsafeUser, $toIDsafeUser,  $messageid);
        }
        # end of sending message main area

        # if it's doing the the search
        $searchUsernameOrEmail = filter_var($this->input->get('search-username-or-email'), FILTER_SANITIZE_STRING);
        if (!empty($searchUsernameOrEmail))
        {
            $returnedSearch = $this->safeuser->searchUsingEmailUsername($searchUsernameOrEmail);
        }
        # end if it's not doing a search

        
        if ((empty($json)) || ($json != true))
        {
            $this->load->view(
            // 'doctor-dashboard-light-blue-wout-grid.phtml', array(
                'patient-dashboard-light-blue.phtml', array(
                'title' => 'Doctor Dashboard', 
                'customCss' => $customCss,
                'returnedSearch' => $returnedSearch,
                'view' => 'messages/create',
                'errorMessage' => $errorMessage,
                'customJs' => $customJs,
                'urlAdd' => $urlAdd,
                'navOpen' => $navOpen,
                'customJsTop' => $customJsTop,
                'css' => 'dashboard-custom'
            ));    
        }
        else
        {
            # you also need to check if this user is logged in
            
            # if logged in still show the json but logged in error
            # jason ouput here
            $ajaxResult = array (
                'error' => $error,
                'errorMessage' => $errorMessage
            );
            echo json_encode($ajaxResult);
            # end of jason output here
        }
        return;
    }

    public function inboxListOnly()
    {
        $this->sendto = filter_var($this->input->get('sendto'), FILTER_SANITIZE_STRING);
        $this->inboxes = $this->messagesModel->getListSender($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->load->view('messages/inbox-list.phtml',array());
        // $this->output->enable_profiler(TRUE);
    }


    public function individualMsg()
    {
        $errorMessage = '';
        $personalInfoOutput = '';
        $customCss = array('custom-dashboard');

        $this->load->view(
            'patient-dashboard-light-blue.phtml', array('title' => 'Your Health Dashboard', 
            'view' => 'messages/chat', 
            'errorMessage' => $errorMessage,
            'personalInfoOutput' => $personalInfoOutput,
            'customCss' => $customCss,
            'css' => 'dashboard-custom'));

        return;
    }

    public function dashboardNotification()
    {
        $this->inboxes = $this->messagesModel->getListSender($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->load->view('structures/patient/header_messages.phtml', array());
    }

    public function countUnreadMessage()
    {
        $ajax = TRUE;
        $unreadMessage = 0;

        $ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);


        $unreadMessage = $this->messagesModel->countUnreadMessage($this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user']));
        $jsonDisplay = array(
            'unreadMessage' => $unreadMessage
        );
    
        if ($ajax == TRUE)
        {  
            header('Content-Type: application/json');
            echo json_encode($jsonDisplay);
        }
        else
        {
            if ($unreadMessage > 999)
            {
                $unreadMessage = '999'.'&#43;';
            }
            echo $unreadMessage;
        }
        return;
    }
}
?>