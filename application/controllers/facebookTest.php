<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class FacebookTest extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$logout_url = null;
		$login_url = null;
		$user_profile = null;
		$fb_config = array(
            'appId'  => '458830587537119',
            'secret' => '3cc1581e3c421c2f4c6b884d5e6201a6'
        );
        // access token
        // with SMS
        // birthday
        // permission to see email
        // CAACEdEose0cBAMYaIDLTS8uD1fp3bgDdZC2vcVadVNPIvX0eLlhlPSWcwO7ZAhwJWRwKKj3VFKgEQLZBu7NjjXGZB4iLVIoK4gEAwZC0jwB5SAKkByZAFbS1YZAZA3SBQjhdHkbNaaLqBZAl7XStpxfWDLfxDDpfGZBKsZD

        $this->load->library('facebook', $fb_config);

        
        $user = $this->facebook->getUser();
        // Set a new access token, by first getting it via means other than the SDK
        $access_token = $this->facebook->getAccessToken();
		$this->facebook->setAccessToken($access_token);

        //check permissions list
        try{
            $permissions_list = $this->facebook->api('/me/permissions', 'GET', array('access_token' => $access_token));
        } catch (FacebookApiException $e){

        }
        $permissions_needed = array('email');
        foreach ($permissions_needed as $perm) {
            if (!isset($permissions_list['data'][0][$perm]) || $permissions_list['data'][0][$perm] != 1) {
                $login = $this->facebook->getLoginUrl(array(
                    'scope' => 'email,user_birthday',
                    'redirect_uri' => 'http://localhost:4848/facebookTest',
                    'display' => 'popup'
            ));
                header("location:$login");
            }
        }
        
        if ($user) {
            try {
                $user_profile = $this->facebook->api('/me/?fields=id,address,first_name,middle_name,last_name,email,birthday,gender');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }

        if ($user) {
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
            $logout_url = $this->facebook->getLogoutUrl();
        } else {
            $login_url = $this->facebook->getLoginUrl();
        }

      //  $this->load->view('view',$data);
        $this->load->view(
			'homepage.phtml', array('title' => 'Insert Medication Information', 
			'view' => 'facebook-test/index', 'logout_url' =>  $logout_url , 'login_url' => $login_url, 'user_profile' =>  $user_profile ));
	}

    public function anothertest(){
       /* //You have to provide an extended permission for email at the time of login..
        $access_token = $this->facebook->getAccessToken();
        //check permissions list
        $permissions_list = $this->facebook->api('/me/permissions', 'GET', array('access_token' => $access_token));
        $permissions_needed = array('email');
        foreach ($permissions_needed as $perm) {
            if (!isset($permissions_list['data'][0][$perm]) || $permissions_list['data'][0][$perm] != 1) {
                $login = $facebook->getLoginUrl(array('scope' => 'email,user_birthday',
                'redirect_uri' => your site redirectUri,
                'display' => 'popup'
            ));
                header("location:$login");
            }
        }
        $user = $facebook->getUser();
        if ($user) {
            try {
                $userInfo = $facebook->api("/$user");
            } catch (FacebookApiException $e) {
                echo $e->getMessage();
            }
        }
        print_r($userInfo);

*/       

        $logout_url = null;
        $login_url = null;
        $user_profile = null;
        $fb_config = array(
            'appId'  => '458830587537119',
            'secret' => '3cc1581e3c421c2f4c6b884d5e6201a6'
        );
        // access token
        // with SMS
        // birthday
        // permission to see email
        // CAACEdEose0cBAMYaIDLTS8uD1fp3bgDdZC2vcVadVNPIvX0eLlhlPSWcwO7ZAhwJWRwKKj3VFKgEQLZBu7NjjXGZB4iLVIoK4gEAwZC0jwB5SAKkByZAFbS1YZAZA3SBQjhdHkbNaaLqBZAl7XStpxfWDLfxDDpfGZBKsZD

        $this->load->library('facebook', $fb_config);

        $user = $this->facebook->getUser();
        print_r($user);
        echo '<a href="'.$this->facebook->getLogoutUrl().'" >logout</a>';
        $params = array(
          'ok_session' => 'http://localhost:4848/',
          'no_user' => 'http://localhost:4848/register',
          'no_session' => 'http://www.google.com',
        );
        $next_url = $this->facebook->getLoginStatusUrl( );
        print_r($next_url);

    }
}
?>