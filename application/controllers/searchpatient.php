<?php if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Searchpatient extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->redirector = $this->session->userdata('urldirect');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Patientsearch_model', 'patientSearch');
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->helper('s3');
        $this->load->helper('text');
		
        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

         # get the profile picture
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

	public function index() 
	{
		$customJs = array();
        $customJsTop = array('countriesV2');
        $customCss = array('custom-search-patient-doctor');
        
        $urlAdd = 
        $this->errorMessage = 
        $urlAdd = 
        $onlyKeyword = 
        $moreoptions = 
        $firstname = 
        $middlename = 
        $lastname = 
        $bloodtype = 
        $gender = 
        $dateofbirth = 
        $emailaddress = 
        $country = 
        $state = 
        $city = 
        $postal = $streetaddress =
        $validateBloodtype =
        $defaultDatas = 
        $searchmMyPatientOnly = '';
        $perpage = 3;

        $this->activeMainMenu = "Patients";
        $this->breadcrumb[] = array(
            'link' => 'profileViewer/doctorViewing',
            'linkname' => 'Nambal&prime;s Safe'
        );

        $baseLink = base_url().lcfirst(get_class($this)).'/';
        $navOpen = 'Patient records';

        $this->checksession();
        $this->checkdoctor();

        $this->pagenumber = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->pagenumber = preg_replace('#[^a-z0-9_]#', '', $this->pagenumber);

        $this->searchpatientkeyword = filter_var($this->input->get('search-patient-keyword'), FILTER_SANITIZE_STRING);
        $this->searchpatientkeyword = preg_replace('#[^a-z0-9_]#', '', $this->searchpatientkeyword);


		if (empty($this->searchpatientkeyword))
            redirect(base_url().'profileViewer/search', 'refresh');
        // $patientSearch = $this->patientSearch->getSearchPatientInformation('DOST-UP Cebu');
		$searchedPatients =  $this->patientSearch->getSearchPatientInformation($this->searchpatientkeyword, FALSE, $perpage, $this->pagenumber);
        $totalSearchCount = $this->patientSearch->getPatientCount();

        # for paging result 
        $config = array();
        $config["base_url"] = base_url() .'searchpatient/index?q=0&search-patient-keyword='.$this->searchpatientkeyword;
        $config["total_rows"] = $this->patientSearch->getPatientCount();;
        $config["per_page"] = $perpage;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 10;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->pagenumber;
        $this->pagination->initialize($config);
        # end for paging result

        $resultsBeforeAndCurrent = $config["per_page"] *($page <= 0 ? 1 : $page);
        $resultsBeforeAndCurrent = ($resultsBeforeAndCurrent > $config["total_rows"] ? $config["total_rows"] : $resultsBeforeAndCurrent);

         $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Nambal safe - Search Patient Dashboard', 
            'customCss' => $customCss,
            'customJsTop' => $customJsTop,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'data' => array(
                'moreoptions'           => $moreoptions,
                'onlyKeyword'           => $onlyKeyword,
                'firstname'             => $firstname,
                'middlename'            => $middlename,
                'lastname'              => $lastname,
                'dateofbirth'           => $dateofbirth,
                'emailaddress'          => $emailaddress,
                'bloodtype'             => $bloodtype,
                'totalSearchCount'      => $totalSearchCount,
                'resultsBeforeAndCurrent' => $resultsBeforeAndCurrent,
                'validateBloodtype'     => $validateBloodtype,
                'gender'                => $gender,
                'country'               => $country,
                'state'                 => $state,
                'city'                  => $city,
                'postal'                => $postal,
                'streetaddress'         => $streetaddress,
                'searchmMyPatientOnly'  => $searchmMyPatientOnly,
                'baseLink'              => $baseLink,
                'links'                 => $this->pagination->create_links(),
                'results'               => $searchedPatients
            ),
            'view' => 'search/search-patient'
        ));

        // $this->output->enable_profiler(TRUE);

	}

	private function checksession()
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
    }

    private function checkdoctor()
    {
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
}

?>