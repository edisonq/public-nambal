<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
		$this->load->library('form_validation');
		$this->load->model('Personalprofile_model', 'personalProfile');
		$this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

		# redirect users to dashboard who are logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP     
        }
        # end of redirecting users to dashboard if logged in


	}

	public function index()
	{
		$errorMessage = '';
		$personalInfoOutput = '';
		//$customCss = array('custom-dashboard');
		$customCss = array('doctor-dashboard');
		$this->load->model('Emruser_model', 'emrusermodel');

		$arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

		$this->load->view(
			//'dashboard-template.phtml', array('title' => 'Your Health Dashboard',
			'personal-profile-doctor-dashboard.phtml', array('title' => 'Your Health Dashboard', 
			'view' => 'profile/index', 
			'errorMessage' => $errorMessage,
			'personalInfoOutput' => $personalInfoOutput,
			'customCss' => $customCss,
			'arrayGetDocInfo' => $arrayGetDocInfo,
			//'css' => 'dashboard-custom'));
			'css' => 'doctor-dashboard'));

		return;
	}

	public function add()
	{
		$this->load->model('Emruser_model', 'emrusermodel');
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$safeContact = array();
		$error = false;
		$race =  array();
		$fromAjax = false;
		$customCss = array('custom-dashboard');

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}

		$arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

		$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('sex','Gender', 'trim|xss_clean|required');
		$this->form_validation->set_rules('race','Race', 'xss_clean|required');
		$this->form_validation->set_rules('street','Street', 'trim|xss_clean|required');
		$this->form_validation->set_rules('state','State', 'trim|xss_clean|required');
		$this->form_validation->set_rules('city','City', 'trim|xss_clean|required');
		$this->form_validation->set_rules('country','Country', 'trim|xss_clean|required');
		$this->form_validation->set_rules('postcode','Postcode', 'trim|xss_clean|required');
		$this->form_validation->set_rules('bloodtype','Bloodtype', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateofbirth','dateofbirth', 'trim|xss_clean|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}

		$fullname = ucwords(filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING));
		$sex = ucwords(filter_var($this->input->post('sex'), FILTER_SANITIZE_STRING));
		
		// $race = ucwords(filter_var($this->input->post('race'), FILTER_SANITIZE_STRING));
		$race = $this->input->post('race');

		$street = ucwords(filter_var($this->input->post('street'), FILTER_SANITIZE_STRING));
		$state = ucwords(filter_var($this->input->post('state'), FILTER_SANITIZE_STRING));
		$city = ucwords(filter_var($this->input->post('city'), FILTER_SANITIZE_STRING));
		$country = ucwords(filter_var($this->input->post('country'), FILTER_SANITIZE_STRING));
		$postcode = ucwords(filter_var($this->input->post('postcode'), FILTER_SANITIZE_STRING));
		$bloodtype = ucwords(filter_var($this->input->post('bloodtype'), FILTER_SANITIZE_STRING));
		$dateofbirth = ucwords(filter_var($this->input->post('dateofbirth'), FILTER_SANITIZE_STRING));
		$fromAjax = ucwords(filter_var($this->input->get('fromAjax'), FILTER_SANITIZE_STRING));

		$personalInfoOutput = array(
			'fullname' => $fullname,
			'sex' => $sex,
			'race' => $race,
			'street' => $street,
			'state' => $state,
			'city' => $city,
			'country' => $country,
			'postcode' => $postcode,
			'bloodtype' => $bloodtype,
			'dateofbirth' => $dateofbirth,
			'IDsafe_user' => $this->nagkamoritsing->ibalik($user_profile['IDsafe_user'])
		);

		if ($_POST)
		{
			if ($error == false)
			{
				$this->personalProfile->addAnotherProfile($personalInfoOutput);
			}
		}
		
		if ($fromAjax == true)
		{
			echo json_encode($personalInfoOutput);
		}
		else
		{
			// $this->load->view(
			// 'homepage.phtml', array('title' => 'Add another profile', 
			// 'view' => 'profile/add',  'personalInfoOutput' => $personalInfoOutput, 'errorMessage' => $errorMessage));


			$this->load->view(
			'dashboard-template.phtml', array('title' => 'Your Health Dashboard',			  
			'view' => 'profile/add', 			
			'errorMessage' => $errorMessage,
			'personalInfoOutput' => $personalInfoOutput,
			'customCss' => $customCss,
			'arrayGetDocInfo' => $arrayGetDocInfo,
			'css' => 'dashboard-custom'));
		}

		
	}

	public function edit(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Modify profile',
				'view' => 'profile/edit'));
		$profileID = $this->input->get('profileID');
		echo 'profileID:'.$profileID;

		return;
	}

	public function viewerList(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Viewer List',
				'view' => 'profile/viewerList'));
		return;
	}

	public function addedInformation(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Added information',
				'view' => 'profile/added-information'));
		return;
	}
}

?>