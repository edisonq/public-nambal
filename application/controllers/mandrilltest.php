<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Mandrilltest extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Signup_model', 'signup');
	}

	public function index()
	{
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;
		try 
		{
		    $this->mandrill->init(config_item('mandrill_api_key'));
		    $mandrill_ready = TRUE;
		}
		catch(Mandrill_Exception $e) 
		{
		    $mandrill_ready = FALSE;
		}

		if( $mandrill_ready ) 
		{
		    //Send us some email!
		    $email = array(
		        'html' => '<p>This is my message<p>', //Consider using a view file
		        'text' => 'This is my plaintext message',
		        'subject' => 'This is my subject',
		        'from_email' => 'information@nambal.com',
		        'from_name' => 'information-no-reply',
		        'to' => array(array('email' => 'edisonq@gmail.com' )) //Check documentation for more details on this one
		        //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
		        );

		    $result = $this->mandrill->messages_send($email);
		}
		else
		{
			echo 'wala ma dayon';
		}
	}

	public function anothertest()
	{
		$this->load->library('email');
 
		$this->email->from('information@nambal.com', 'Nambal Information');
		$this->email->to('edisonq@gmail.com');
		$this->email->cc('edisonq@yahoo.com');
		$this->email->bcc('edisonq@hotmail.com');
		 
		$this->email->subject('Just testing');
		$this->email->message('Testing the email class.');
		 
		$this->email->send();
		 
		echo $this->email->print_debugger();
	}
}

?>