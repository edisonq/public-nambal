<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Test extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Test_model', 'test');

		$this->load->model('Problem_model', 'problem');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->model('Safeuser_model', 'safeuser');

		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Test Information', 
			'view' => 'test/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$testList = array();

		// IDsafe_test		@$this->testInformation['IDsafe_test']
		// testname		@$this->testInformation['testname']
		// dateoftest		 if (!empty($this->testInformation['dateoftest'])) echo date('m/d/o', strtotime($this->testInformation['dateoftest'])); else echo date('m/d/o'); 
		// flag		echo ((strcasecmp(@$this->testInformation['flag'], 'Abnormal')==0) ? 'selected' : ''); 
		// result		@$this->testInformation['result']
		// units		@$this->testInformation['units']
		// minimumrange1		@$this->testInformation['minimumrange1']
		// maximumrange1		@$this->testInformation['maximumrange1']
		// rangetext1		@$this->testInformation['rangetext1']
		// rangecount

		$this->form_validation->set_rules('testname','Test name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateoftest', 'Date of test', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('flag','Flag', 'trim|xss_clean|required|max_length[45]|should_select');
		$this->form_validation->set_rules('notes','notes', 'trim|xss_clean|required');
		$this->form_validation->set_rules('result','result', 'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('units','units', 'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('rangecount','rangecount', 'trim|xss_clean|integer');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$testname = ucwords(filter_var($this->input->post('testname'), FILTER_SANITIZE_STRING));
			$dateoftest = $this->input->post('dateoftest');
			$flag = filter_var($this->input->post('flag'), FILTER_SANITIZE_STRING);
			$result = filter_var($this->input->post('result'), FILTER_SANITIZE_STRING);
			$units = filter_var($this->input->post('units'), FILTER_SANITIZE_STRING);
			$rangecount = filter_var($this->input->post('rangecount'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			// # check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateoftest);
			$dateoftest = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			for ($range = 1; $range <= $rangecount ; $range++) 
			{ 
				$minrange = @filter_var($this->input->post('minimumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['minimumrange'] = @$minrange;

				$maxrange = @filter_var($this->input->post('maximumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['maximumrange'] = @$maxrange;

				$rangetext = @filter_var($this->input->post('rangetext'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['rangetext'] = @$rangetext;

				$safe_test_range[$range] = @$safetestrange;
			}

			$testList = array(
				'testname' => $testname,
				'dateoftest' => $dateoftest,
				'flag' => @$flag,
				'notes' => $notes,
				'result' => @$result,
				'units' => @$units,
				'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']),
				'safe_test_range' => @$safe_test_range,
			);

			if ($error == FALSE)
				$this->test->insertSafe($testList);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $testList)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Test Information', 
			'view' => 'test/insert'));
		}
		return;
	}

	public function insertDoctor()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$testList = array();

		// IDsafe_test		@$this->testInformation['IDsafe_test']
		// testname		@$this->testInformation['testname']
		// dateoftest		 if (!empty($this->testInformation['dateoftest'])) echo date('m/d/o', strtotime($this->testInformation['dateoftest'])); else echo date('m/d/o'); 
		// flag		echo ((strcasecmp(@$this->testInformation['flag'], 'Abnormal')==0) ? 'selected' : ''); 
		// result		@$this->testInformation['result']
		// units		@$this->testInformation['units']
		// minimumrange1		@$this->testInformation['minimumrange1']
		// maximumrange1		@$this->testInformation['maximumrange1']
		// rangetext1		@$this->testInformation['rangetext1']
		// rangecount
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		$this->form_validation->set_rules('testname','Test name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateoftest', 'Date of test', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('flag','Flag', 'trim|xss_clean|required|max_length[45]|should_select');
		$this->form_validation->set_rules('notes','notes', 'trim|xss_clean|required');
		$this->form_validation->set_rules('result','result', 'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('units','units', 'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('rangecount','rangecount', 'trim|xss_clean|integer');
		$this->form_validation->set_rules('doctor-notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$testname = ucwords(filter_var($this->input->post('testname'), FILTER_SANITIZE_STRING));
			$dateoftest = $this->input->post('dateoftest');
			$flag = filter_var($this->input->post('flag'), FILTER_SANITIZE_STRING);
			$result = filter_var($this->input->post('result'), FILTER_SANITIZE_STRING);
			$units = filter_var($this->input->post('units'), FILTER_SANITIZE_STRING);
			$rangecount = filter_var($this->input->post('rangecount'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);

			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			// # check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateoftest);
			$dateoftest = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			for ($range = 1; $range <= $rangecount ; $range++) 
			{ 
				$minrange = @filter_var($this->input->post('minimumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['minimumrange'] = @$minrange;

				$maxrange = @filter_var($this->input->post('maximumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['maximumrange'] = @$maxrange;

				$rangetext = @filter_var($this->input->post('rangetext'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['rangetext'] = @$rangetext;

				$safe_test_range[$range] = @$safetestrange;
			}

			$testList = array(
				'testname' => $testname,
				'dateoftest' => $dateoftest,
				'flag' => @$flag,
				'notes' => $notes,
				'result' => @$result,
				'units' => @$units,
				'IDsafe_personalInfo' => $IDsafe_personalInfo,
				'safe_test_range' => @$safe_test_range,
			);

			if ($error == FALSE)
			{
				$IDsafe_test = $this->test->insertSafe($testList);

				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				
				$testList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$testList['IDemr_user'] = $doctorInformation->IDemr_user;
				$testList['doctor-notes'] = $doctorNotes;
				$testList['IDsafe_test'] = $IDsafe_test;
				$testList['IDemr_patient'] = $IDemr_patient;
				
				$this->safejournal->insertjournaltest($testList);
				$patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);
				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		
				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
			}
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $testList)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Test Information', 
			'view' => 'test/insert'));
		}
		return;
	}

	public function update()
	{
		$this->checkSession();
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$error = TRUE;
		$errorMessage = '';
		$customCss = array('custom-dashboard');
		$safeProblem = array();
		$testLister = array();

		$this->form_validation->set_rules('IDsafe_test','IDsafe_test', 'trim|xss_clean|required');
		$this->form_validation->set_rules('testname','Test name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateoftest', 'Date of test', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('flag','Flag', 'trim|xss_clean|required|max_length[45]|should_select');
		$this->form_validation->set_rules('notes','notes', 'trim|xss_clean|required');
		$this->form_validation->set_rules('result','result', 'trim|xss_clean|required|max_length[20]');
		$this->form_validation->set_rules('units','units', 'trim|xss_clean|required|max_length[20]');
		$this->form_validation->set_rules('rangecount','rangecount', 'trim|xss_clean|integer|required');


		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{

			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
			$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
			$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);

			$IDsafe_test = filter_var($this->input->post('IDsafe_test'), FILTER_SANITIZE_STRING);
			$testname = ucwords(filter_var($this->input->post('testname'), FILTER_SANITIZE_STRING));
			$dateoftest = $this->input->post('dateoftest');
			$flag = filter_var($this->input->post('flag'), FILTER_SANITIZE_STRING);
			$result = filter_var($this->input->post('result'), FILTER_SANITIZE_STRING);
			$units = filter_var($this->input->post('units'), FILTER_SANITIZE_STRING);
			$rangecount = filter_var($this->input->post('rangecount'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			$IDemr_journal = (filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING));
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING); # notes for doctor

			// # check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $dateoftest);
			$dateoftest = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			for ($range = 1; $range <= $rangecount ; $range++) 
			{ 
				$minrange = @filter_var($this->input->post('minimumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['minimumrange'] = @$minrange;

				$maxrange = @filter_var($this->input->post('maximumrange'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['maximumrange'] = @$maxrange;

				$rangetext = @filter_var($this->input->post('rangetext'.$range), FILTER_SANITIZE_STRING);
				if (!empty($minrange))
					$safetestrange['rangetext'] = @$rangetext;

				$safe_test_range[$range] = @$safetestrange;
			}

			$testLister = array(
				'IDsafe_test' => $IDsafe_test,
				'testname' => $testname,
				'dateoftest' => $dateoftest,
				'notes' => $notes,
				'flag' => @$flag,
				'result' => @$result,
				'units' => @$units,
				'safe_test_range' => @$safe_test_range
			);

			$dataInDb = $this->test->getToModify($IDsafe_test);
			
			if ($error == FALSE)
			{
				
				
				if (strcasecmp($isdoctor, 'TRUE')==0)
	            {
		            # added for doctor updates
		            # check if patient ba ni siya sa doctor
		            # if dili i add siya as one of the patient
		            // $IDsafe_user = $this->nagkamoritsing->ibalik($this->sessionid);
		            $IDsafe_personalInfo = $dataInDb['IDsafe_personalInfo'];
		            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
		            $IDemr_patient = @$IDemr_patient->IDemr_patient;
		            if (empty($IDemr_patient))
		                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);


		            $testLister['IDsafe_personalInfo'] = $IDsafe_personalInfo;
					$testLister['IDemr_user'] = $doctorInformation->IDemr_user;
					$testLister['doctor-notes'] = $doctorNotes;
					$testLister['IDemr_patient'] = $IDemr_patient;
					$testLister['IDemr_journal'] = $IDemr_journal;

		            if (!empty($IDemr_journal))
					{
						$testLister['IDemr_journal'] = $IDemr_journal;
						$this->safejournal->updateJounal($testLister);
					} elseif ((empty($IDemr_journal)) || ($IDemr_journal == 0)) {
		            	$this->safejournal->insertjournaltest($testLister);
		            } else {
		            	$this->safejournal->addjournal($testLister,'safe_test');
		            }

		            $patientinfo = $this->safeuser->getSafeUserInfoUsingPersonalInfo($IDsafe_personalInfo);
		            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($patientinfo->IDsafe_user);
		            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

		            $emailData = array(
		                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
		                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
		            );
		            $message = $this->load->view(
		                    'for-email.phtml', array('title' => 'Notify patient', 
		                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		    
		            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
	            	$this->safeuserlog->notifypatientbyemail($patientinfo->IDsafe_user,$message);
	        	}
	        	else
	        	{
	        		$testLister['IDsafe_personalInfo'] = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);
	        	}
	        	$this->test->updateSafe($testLister);
	        }
	        
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			// header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, @$testLister)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Update Test Information', 
			'view' => 'test/update'));
		}
		return;
	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('testid', 'testid',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		
		if ($error == FALSE)
		{
			$IDsafe_test = filter_var($this->input->post('testid'), FILTER_SANITIZE_STRING);		
			$this->test->deleteSafe($IDsafe_test);
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);

	
		if ($ajax == TRUE)
		{			
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Test Information', 
			'view' => 'test/delete'));
		}

		return;
	}

	public function deleteDoctor()
	{
		# balikan ni nako
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		# check if doctor ba jud ang ga delete

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('testid', 'testid',  'trim|required|xss_clean');

		# know who is the patient
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);


		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$testid = filter_var($this->input->post('testid'), FILTER_SANITIZE_STRING);
			// $returnlang = $this->procedures->deleteSafeProcedure($procedureid, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));  #butangan og sakto nga doctor ID
			$returnlang = $this->test->deleteSafe($testid);

			# sending email if wala pa siya ma notify within this day.
			$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
			$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);

			$IDsafe_test = filter_var($this->input->post('testid'), FILTER_SANITIZE_STRING);		
			$this->test->deleteSafe($IDsafe_test);

			$patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
			$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
			$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

			$emailData = array(
				'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
				'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
			);
			$message = $this->load->view(
		            'for-email.phtml', array('title' => 'Notify patient', 
		            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
	
			# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
			$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
			# sending email sa patient

			header('Content-Type: application/json');
			$jsonDisplay['returnlang'] = $returnlang;
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Problem Information', 
			'view' => 'problem/delete'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}


	public function dashboardDisplay()
	{
		$this->checkSession();
		// $this->testLists = $this->test->displayDashboard($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        
        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);

		// $this->testLists = $this->test->displayDashboard($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-test.js pagination code
        # and display_test-information.phtml div tests-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'test/dashboardDisplay?q=0';
        $config["total_rows"] = $this->test->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->test->displayDashboard($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		

		$this->load->view('test/display_test-information.phtml', $data, array());
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;
		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        $this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->testLists = $this->test->displayDashboard($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-test-profileviewer.js pagination code
        # and display_test-information-profileviewer.phtml div tests-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'test/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->test->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->test->displayDashboard($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		

		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('test/display_test-information-profileviewer.phtml', $data, array());
	}

	public function displayTest()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->test->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	public function insertDisplay()
	{
		$this->checkSession();
		$arrayGetTest = array();

		$IDsafe_test = filter_var($this->input->get('IDsafe_test'), FILTER_SANITIZE_STRING);

		// $retData = array(
		// 	'IDsafe_test' => $IDsafe_test
		// );

		# get the problem information
		$this->testInformation = $this->test->getToModify($IDsafe_test);

		$this->load->view('test/display_test-insert-information.phtml', array());
	}

	public function insertDisplayDoctor()
	{
		$this->checkSession();
		$arrayGetTest = array();

		$IDsafe_test = filter_var($this->input->get('IDsafe_test'), FILTER_SANITIZE_STRING);


		// $retData = array(
		// 	'IDsafe_test' => $IDsafe_test
		// );

		# get the problem information
		$this->testInformation = $this->test->getToModify($IDsafe_test);

		if (!empty($this->testInformation))
		{
			if (!empty($this->testInformation['IDemr_journal']))
				$this->journalinformation = $this->safejournal->getjournal($this->testInformation['IDemr_journal']);
			$this->testInformation['doctor-notes'] = @$this->journalinformation->notes;
			$this->testInformation['IDemr_journal'] = @$this->journalinformation->IDemr_journal;
		}

		$this->load->view('test/display_test-insert-information-profileviewer.phtml', array());
	}

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
}
?>