<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Azureblobtest extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('azure');
	}

	public function index()
	{
		// require('entities.php'); // Only needed if you are going to define entity classes and have created this file with the classes in it
		print_r($this->azure->get_blob_storage());

		// foreach ($this->azure->get_blob_storage() as $key) {
		// 	print_r($key);
		// 	echo '<br>';
		// }


		// require_once 'vendor\autoload.php';

		// use WindowsAzure\Common\ServicesBuilder;
		// use WindowsAzure\Common\ServiceException;

		// // Create blob REST proxy.
		// $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);


		// $content = fopen("c:\myfile.txt", "r");
		// $blob_name = "myblob";

		// try {
		//     //Upload blob
		//     $blobRestProxy->createBlockBlob("mycontainer", $blob_name, $content);
		// }
		// catch(ServiceException $e){
		//     // Handle exception based on error codes and messages.
		//     // Error codes and messages are here:
		//     // http://msdn.microsoft.com/library/azure/dd179439.aspx
		//     $code = $e->getCode();
		//     $error_message = $e->getMessage();
		//     echo $code.": ".$error_message."<br />";
		// }
		echo 'test';
		echo getcwd();
	}
}