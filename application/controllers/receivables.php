<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class Receivables extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('ContactInformation_model', 'contactinformation');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Appointment_model', 'appointmentsmodel');
        $this->load->model('Location_model', 'locationmodel');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'), 
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

            

        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in

        

	}

	public function index()
	{

        
        $urlAdd = 'receivables';
        $navOpen = 'dashboard';        
        
		//doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/index'
        ));

	}

    public function all()
    {
        $urlAdd = 'receivables';
        $navOpen = '';        
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/index'
        ));
    }

    public function day()
    {
        $urlAdd = 'receivables';
        $navOpen = 'receivables';        
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/day'
        ));
    }

    public function week()
    {
        $urlAdd = 'receivables';
        $navOpen = 'receivables';     
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/week'
        ));
    }

    public function month()
    {
        $urlAdd = 'receivables';
        $navOpen = 'receivables';       
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/month'
        ));
    }

    public function addpatient()
    {
        $urlAdd = 'receivables/patients';
        $navOpen = 'patients';      
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/addpatient'
        ));
    }

    public function patients()
    {
        $urlAdd = 'receivables/patients';
        $navOpen = 'patients';       
        
        //doctor-dashboard-light-payables.phtml # new UI
        $this->load->view(
        'doctor-dashboard-light-receivable.phtml', array(
            'title' => 'Doctor Dashboard', 
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'receivable/patients'
        ));
    }
}

?>