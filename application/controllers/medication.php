<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Medication extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Medication_model', 'medication');
		$this->load->model('Prescription_model','prescription');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('Safeuser_model', 'safeuser');
		$this->load->model('safeuserlog_model', 'safeuserlog');

		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Medication Information', 
			'view' => 'medication/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{
		// medication	@$this->medicationInfo['medicationName']
		// now		@$this->medicationInfo['takingitnow']
		// datestarted		$this->medicationInfo['dateStart']
		// timestarted		$this->medicationInfo['timestarted']
		// dateended		$this->medicationInfo['dateEnd']
		// timeend		$this->medicationInfo['timeend']
		// route		$this->medicationInfo['route']
		// strength	@$this->medicationInfo['strength']
		// strengthunit		@$this->medicationInfo['strengthunit']
		// dosage		@$this->medicationInfo['dosage']
		// dosagesunit		$this->medicationInfo['dosagesunit']
		// writtenon		@$this->medicationInfo['writtenon']
		// expirydate		@$this->medicationInfo['expirydate']
		// doctorsname		@$this->medicationInfo['doctorsname']
 		// rxid		@$this->medicationInfo['rxid']
		// dremail		@$this->medicationInfo['dremail']
		// dispensedon1	this->medicationInfo['dispensedon1'
		// dispensedqty1	@$this->medicationInfo['dispensedqty1']
		// notes		@$this->medicationInfo['notes']

		
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$medicationList = array();

		$this->form_validation->set_rules('medication','Medication', 'trim|xss_clean|required');
		$this->form_validation->set_rules('now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('frequency','Frequency', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('route','Method/route in taking', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strength','Strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strengthunit','Unit of strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosage','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosagesunit','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('writtenon', 'Written on', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('expirydate', 'Expiry date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('doctorsname','Doctors name', 'trim|xss_clean');
		$this->form_validation->set_rules('rxid','RX or license number', 'trim|xss_clean');
		$this->form_validation->set_rules('dremail','Doctors email', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('dispensedon1', 'Dispense date', 'xss_clean|valid_date');
		$this->form_validation->set_rules('dispensedqty1', 'Dispense quantity', 'xss_clean|is_natural_no_zero');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$medication = ucwords(filter_var($this->input->post('medication'), FILTER_SANITIZE_STRING));
			$now = filter_var($this->input->post('now'), FILTER_SANITIZE_STRING);
			$frequency = filter_var($this->input->post('frequency'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$route = filter_var($this->input->post('route'), FILTER_SANITIZE_STRING);
			$strength = filter_var($this->input->post('strength'), FILTER_SANITIZE_STRING);
			$strengthunit = filter_var($this->input->post('strengthunit'), FILTER_SANITIZE_STRING);
			$dosage = filter_var($this->input->post('dosage'), FILTER_SANITIZE_STRING);
			$dosagesunit = filter_var($this->input->post('dosagesunit'), FILTER_SANITIZE_STRING);
			$writtenon = filter_var($this->input->post('writtenon'), FILTER_SANITIZE_STRING);
			$expirydate = $this->input->post('expirydate');
			$doctorsname = filter_var($this->input->post('doctorsname'), FILTER_SANITIZE_STRING);
			$rxid = filter_var($this->input->post('rxid'), FILTER_SANITIZE_STRING);
			$dremail = filter_var($this->input->post('dremail'), FILTER_SANITIZE_STRING);
			$dispensedon1 = $this->input->post('dispensedon1');
			$dispensedqty1 = filter_var($this->input->post('dispensedqty1'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);		

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			if (!empty($timestarted))
				list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (!empty($writtenon)) 
			{
				list($monthwriteon, $daywriteon, $yearwriteon) = explode('/', $writtenon);
				$writtenon =date('o-m-d h:i:s' ,strtotime(@$yearwriteon.'-'.@$monthwriteon.'-'.@$daywriteon));
			}

			if (!empty($expirydate))
			{
				list($monthexpirydate, $dayexpirydate, $yearexpirydate) = explode('/', $expirydate);
				$expirydate =date('o-m-d h:i:s' ,strtotime(@$yearexpirydate.'-'.@$monthexpirydate.'-'.@$dayexpirydate));
			}

			if (!empty($dispensedon1))
			{
				list($monthdispensedon1, $daydispensedon1, $yeardispensedon1) = explode('/', $dispensedon1);
				$dispensedon1 =date('o-m-d h:i:s' ,strtotime(@$yeardispensedon1.'-'.@$monthdispensedon1.'-'.@$daydispensedon1));
			}

			if (strcasecmp($now,'YES')==0)
			{ $now = TRUE; }
			else
			{ $now = FALSE; }

			# here you can add the symptoms
			$medicationList = array(
				'medication' =>$medication,
				'now' => $now,
				'frequency' => $frequency,
				'datetimeStart' => $datetimeStart,
				'datetimeEnd' => @$datetimeEnd,
				'route' => $route,
				'strength' => $strength,
				'strengthunit' => $strengthunit,
				'dosage' => $dosage,
				'dosagesunit' => $dosagesunit,
				'writtenon' => @$writtenon,
				'expirydate' => @$expirydate,
				'doctorsname' => @$doctorsname,
		 		'rxid' => @$rxid,
				'dremail' => @$dremail,
				'dispensedon1' => @$dispensedon1,
				'dispensedqty1' => @$dispensedqty1,
				'notes' => @$notes
			);

			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage = "<p>Date ended should be after date started<p>";
				}
			}
			
			if ($error == FALSE)
				$this->medication->insertMedication($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $medicationList);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $medicationList)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
			'view' => 'problem/insert', 
            'safeContact' => $problemlist
            ));
		}

		return;
	}

	public function insertDoctor()
	{
		$this->checkSession();

		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		$sessionid = $this->nagkamoritsing->ibalik($sessionid);
		$createnewprescription = FALSE;

		if (empty($ajax))
			$ajax = TRUE;
		
		$error = TRUE;
		$errorMessage = '';
		
		$medicationList = array();

		$this->form_validation->set_rules('medication','Medication', 'trim|xss_clean|required');
		$this->form_validation->set_rules('now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('frequency','Frequency', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('route','Method/route in taking', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strength','Strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strengthunit','Unit of strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosage','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosagesunit','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('writtenon', 'Written on', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('expirydate', 'Expiry date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('doctorsname','Doctors name', 'trim|xss_clean');
		$this->form_validation->set_rules('rxid','RX or license number', 'trim|xss_clean');
		$this->form_validation->set_rules('dremail','Doctors email', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('dispensedon1', 'Dispense date', 'xss_clean|valid_date');
		$this->form_validation->set_rules('dispensedqty1', 'Dispense quantity', 'xss_clean|is_natural_no_zero');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('activeprescription', 'Active Prescription', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Private Journal for the', 'trim|xss_clean'); //doctor-notes

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage .= validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$medication = ucwords(filter_var($this->input->post('medication'), FILTER_SANITIZE_STRING));
			$now = filter_var($this->input->post('now'), FILTER_SANITIZE_STRING);
			$frequency = filter_var($this->input->post('frequency'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$route = filter_var($this->input->post('route'), FILTER_SANITIZE_STRING);
			$strength = filter_var($this->input->post('strength'), FILTER_SANITIZE_STRING);
			$strengthunit = filter_var($this->input->post('strengthunit'), FILTER_SANITIZE_STRING);
			$dosage = filter_var($this->input->post('dosage'), FILTER_SANITIZE_STRING);
			$dosagesunit = filter_var($this->input->post('dosagesunit'), FILTER_SANITIZE_STRING);
			$writtenon = filter_var($this->input->post('writtenon'), FILTER_SANITIZE_STRING);
			$expirydate = $this->input->post('expirydate');
			$doctorsname = filter_var($this->input->post('doctorsname'), FILTER_SANITIZE_STRING);
			$rxid = filter_var($this->input->post('rxid'), FILTER_SANITIZE_STRING);
			$dremail = filter_var($this->input->post('dremail'), FILTER_SANITIZE_STRING);
			$dispensedon1 = $this->input->post('dispensedon1');
			$dispensedqty1 = filter_var($this->input->post('dispensedqty1'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$activeprescription = filter_var($this->input->post('activeprescription'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);

			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($sessionid);
			// activeprescription

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			if (!empty($timestarted))
				list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (!empty($writtenon)) 
			{
				list($monthwriteon, $daywriteon, $yearwriteon) = explode('/', $writtenon);
				$writtenon =date('o-m-d h:i:s' ,strtotime(@$yearwriteon.'-'.@$monthwriteon.'-'.@$daywriteon));
			}

			if (!empty($expirydate))
			{
				list($monthexpirydate, $dayexpirydate, $yearexpirydate) = explode('/', $expirydate);
				$expirydate =date('o-m-d h:i:s' ,strtotime(@$yearexpirydate.'-'.@$monthexpirydate.'-'.@$dayexpirydate));
			}

			if (!empty($dispensedon1))
			{
				list($monthdispensedon1, $daydispensedon1, $yeardispensedon1) = explode('/', $dispensedon1);
				$dispensedon1 =date('o-m-d h:i:s' ,strtotime(@$yeardispensedon1.'-'.@$monthdispensedon1.'-'.@$daydispensedon1));
			}

			if (strcasecmp($now,'YES')==0)
			{ $now = TRUE; }
			else
			{ $now = FALSE; }

			# here you can add the symptoms
			$medicationList = array(
				'medication' =>$medication,
				'now' => $now,
				'frequency' => $frequency,
				'datetimeStart' => $datetimeStart,
				'datetimeEnd' => @$datetimeEnd,
				'route' => $route,
				'strength' => $strength,
				'strengthunit' => $strengthunit,
				'dosage' => $dosage,
				'dosagesunit' => $dosagesunit,
				'writtenon' => @$writtenon,
				'expirydate' => @$expirydate,
				'doctorsname' => @$doctorsname,
		 		'rxid' => @$rxid,
				'dremail' => @$dremail,
				'dispensedon1' => @$dispensedon1,
				'dispensedqty1' => @$dispensedqty1,
				'notes' => @$notes
			);

			# active prescription
			if (!empty($activeprescription))
			{
				if (strcasecmp('create-new-prescription', $activeprescription)!=0)
				{
					$medicationList['IDsafe_prescription'] = $activeprescription;
				}
				else
				{
					# balik-ko-diri-for-create-new-prescription-task
					# this is if selected ang create new prescription
					# mo create na lang sa prescrition para ready mo hatod sa final UI
					// $IDsafe_prescription = $this->prescription->createPrescription($prescriptionList);
					// $drfullname = ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['firstname'])).' '.ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['lastname'])).' ,MD';
					// $dremailaddress = $this->safeuseremail->getuseremaildefault($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
					// $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
					//        		$rxid = $doctorInformation->rxId;
					//        		$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($sessionid));
					// $writtenon = date('Y-m-d', strtotime($writtenon));
					//        		$expirydate;

					// $prescriptionList = array(
					//              'prescriptiondate' => @$writtenon,
					//              'prescriptionend' => @$expirydate,
					//              'dr_name' => @$drfullname,
					//              'dr_rx' => @$rxid,
					//              'dr_email' => $this->nagkamoritsing->ibalik(@$dremailaddress->emailAddress),
					//              'approve' => TRUE,
					//              'active' => TRUE,
					//              'safe_personalinfo_IDsafe_personalInfo' => $IDsafe_personalInfo,
					//              'includeaddress' => TRUE,
					//              'includeage' => TRUE,
					//              'includegender' => TRUE,
					//              'includedob' => TRUE,
					//              'includeexpiry' => TRUE
					//          );
					# dont forget to send to ajax json "data.createnewprescription"
					# set to TRUE if create-new-prescription ang $activeprescription


					# end of this create new prescription
				}
			}
			# end of listing the active prescription

			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage = "<p>Date ended should be after date started<p>";
				}
			}
			
			if ($error == FALSE)
			{
				$IDsafe_drug = $this->medication->insertMedication($sessionid, $medicationList);

				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				
				$medicationList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$medicationList['IDemr_user'] = $doctorInformation->IDemr_user;
				$medicationList['doctor-notes'] = $doctorNotes;
				$medicationList['IDsafe_drug'] = $IDsafe_drug;
				$medicationList['IDemr_patient'] = $IDemr_patient;
				
				$this->safejournal->insertjournalmedication($medicationList);
				$patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($sessionid));
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($sessionid);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);
				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		
				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				$this->safeuserlog->notifypatientbyemail($sessionid,$message);
			}
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error,
				'IDsafeprescription' => $this->nagkamoritsing->bungkag(@$activeprescription),
				'createnewprescription' => $createnewprescription, //added para guide nga mo balhin sa final UI sa prescription 
				'errorMessage' => $errorMessage, $medicationList)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
			'view' => 'problem/insert', 
            'safeContact' => $problemlist
            ));
		}

		return;
	}

	public function updateDoctor()
	{
		$this->checkSession();
		$error = false;
		$errorMessage = '';
		$medicationList = array();

		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
		$sessionid = $this->nagkamoritsing->ibalik($sessionid);
		$createnewprescription = FALSE;

		if (empty($ajax))
			$ajax = TRUE;


		$this->form_validation->set_rules('medication','Medication', 'trim|xss_clean|required');
		$this->form_validation->set_rules('now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('frequency','Frequency', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('route','Method/route in taking', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strength','Strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strengthunit','Unit of strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosage','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosagesunit','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('writtenon', 'Written on', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('expirydate', 'Expiry date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('doctorsname','Doctors name', 'trim|xss_clean');
		$this->form_validation->set_rules('rxid','RX or license number', 'trim|xss_clean');
		$this->form_validation->set_rules('dremail','Doctors email', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('dispensedon1', 'Dispense date', 'xss_clean|valid_date');
		$this->form_validation->set_rules('dispensedqty1', 'Dispense quantity', 'xss_clean|is_natural_no_zero');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('activeprescription', 'Active Prescription', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Private Journal for the', 'trim|xss_clean'); //doctor-notes


		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage .= validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$medication = ucwords(filter_var($this->input->post('medication'), FILTER_SANITIZE_STRING));
			$now = filter_var($this->input->post('now'), FILTER_SANITIZE_STRING);
			$frequency = filter_var($this->input->post('frequency'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$route = filter_var($this->input->post('route'), FILTER_SANITIZE_STRING);
			$strength = filter_var($this->input->post('strength'), FILTER_SANITIZE_STRING);
			$strengthunit = filter_var($this->input->post('strengthunit'), FILTER_SANITIZE_STRING);
			$dosage = filter_var($this->input->post('dosage'), FILTER_SANITIZE_STRING);
			$dosagesunit = filter_var($this->input->post('dosagesunit'), FILTER_SANITIZE_STRING);
			$writtenon = filter_var($this->input->post('writtenon'), FILTER_SANITIZE_STRING);
			$expirydate = $this->input->post('expirydate');
			$doctorsname = filter_var($this->input->post('doctorsname'), FILTER_SANITIZE_STRING);
			$rxid = filter_var($this->input->post('rxid'), FILTER_SANITIZE_STRING);
			$dremail = filter_var($this->input->post('dremail'), FILTER_SANITIZE_STRING);
			$dispensedon1 = $this->input->post('dispensedon1');
			$dispensedqty1 = filter_var($this->input->post('dispensedqty1'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$activeprescription = filter_var($this->input->post('activeprescription'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);
			$IDsafe_drug = filter_var($this->input->post('IDsafe_drug'), FILTER_SANITIZE_STRING);
			$IDemr_journal = filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING);

			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($sessionid);
			// activeprescription

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			if (!empty($timestarted))
				list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (!empty($writtenon)) 
			{
				list($monthwriteon, $daywriteon, $yearwriteon) = explode('/', $writtenon);
				$writtenon =date('o-m-d h:i:s' ,strtotime(@$yearwriteon.'-'.@$monthwriteon.'-'.@$daywriteon));
			}

			if (!empty($expirydate))
			{
				list($monthexpirydate, $dayexpirydate, $yearexpirydate) = explode('/', $expirydate);
				$expirydate =date('o-m-d h:i:s' ,strtotime(@$yearexpirydate.'-'.@$monthexpirydate.'-'.@$dayexpirydate));
			}

			if (!empty($dispensedon1))
			{
				list($monthdispensedon1, $daydispensedon1, $yeardispensedon1) = explode('/', $dispensedon1);
				$dispensedon1 =date('o-m-d h:i:s' ,strtotime(@$yeardispensedon1.'-'.@$monthdispensedon1.'-'.@$daydispensedon1));
			}

			if (strcasecmp($now,'YES')==0)
			{ $now = TRUE; }
			else
			{ $now = FALSE; }

			$medicationList = array(
				'medication' =>$medication,
				'now' => $now,
				'frequency' => $frequency,
				'datetimeStart' => $datetimeStart,
				'datetimeEnd' => @$datetimeEnd,
				'route' => $route,
				'strength' => $strength,
				'strengthunit' => $strengthunit,
				'dosage' => $dosage,
				'dosagesunit' => $dosagesunit,
				'writtenon' => @$writtenon,
				'expirydate' => @$expirydate,
				'doctorsname' => @$doctorsname,
		 		'rxid' => @$rxid,
				'dremail' => @$dremail,
				'dispensedon1' => @$dispensedon1,
				'dispensedqty1' => @$dispensedqty1,
				'notes' => @$notes,
				'IDsafe_drug' => @$IDsafe_drug,
				'IDsafe_personalInfo' => @$IDsafe_personalInfo,
				'IDsafe_prescription' => @$activeprescription
			);

			# no need to modify
			$medicationList['IDsafe_prescription'] = $activeprescription;

			# date start and end checking
			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage = "<p>Date ended should be after date started<p>";
				}
			}



			# start saving
			if ($error == FALSE)
			{
				# update here
				$this->medication->updateMedication($medicationList);

				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				
				$medicationList['IDemr_user'] = $doctorInformation->IDemr_user;
				$medicationList['doctor-notes'] = $doctorNotes;
				$medicationList['IDemr_patient'] = $IDemr_patient;

				// if ((!empty($medicationList['doctor-notes'])))
				// {
				if (!empty($IDemr_journal))
				{
					$medicationList['IDemr_journal'] = $IDemr_journal;
					$this->safejournal->updateJounal($medicationList);
				} elseif ((empty($IDemr_journal)) || ($IDemr_journal == 0)) {
	            	$this->safejournal->insertjournalmedication($medicationList);
	            } else {
	            	$this->safejournal->addjournal($medicationList,'safe_problem');
	            }
				// }
				
				$patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($sessionid));
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($sessionid);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);
				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
		
				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				$this->safeuserlog->notifypatientbyemail($sessionid,$message);
			}

			

		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			if (!empty($medicationList['IDsafe_prescription']))
				$medicationList['IDsafe_prescription'] = $this->nagkamoritsing->bungkag($medicationList['IDsafe_prescription']);
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, 'medicationList' => $medicationList)
			);
		}	
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Update Medication Information', 
			'view' => 'medication/update'));
		}
		
		return;

	}

	public function update()
	{
		$this->checkSession();
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$error = TRUE;
		$errorMessage = '';
		$customCss = array('custom-dashboard');
		$medicationList = array();

		$this->form_validation->set_rules('medication','Medication', 'trim|xss_clean|required');
		$this->form_validation->set_rules('IDsafe_drug','IDsafe_drug', 'trim|xss_clean|required');
		$this->form_validation->set_rules('now','Is it occuring now?', 'trim|xss_clean|required');
		$this->form_validation->set_rules('frequency','Frequency', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datestarted', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('dateended', 'Date ended', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('timeend', 'Time problem ended', 'trim|min_length[3]|max_length[5]|validate_time');
		$this->form_validation->set_rules('route','Method/route in taking', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strength','Strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('strengthunit','Unit of strength', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosage','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dosagesunit','Dosage', 'trim|xss_clean|required');
		$this->form_validation->set_rules('writtenon', 'Written on', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('expirydate', 'Expiry date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('doctorsname','Doctors name', 'trim|xss_clean');
		$this->form_validation->set_rules('rxid','RX or license number', 'trim|xss_clean');
		$this->form_validation->set_rules('dremail','Doctors email', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('dispensedon1', 'Dispense date', 'xss_clean|valid_date');
		$this->form_validation->set_rules('dispensedqty1', 'Dispense quantity', 'xss_clean|is_natural_no_zero');
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$medication = ucwords(filter_var($this->input->post('medication'), FILTER_SANITIZE_STRING));
			$IDsafe_drug = ucwords(filter_var($this->input->post('IDsafe_drug'), FILTER_SANITIZE_STRING));
			$now = filter_var($this->input->post('now'), FILTER_SANITIZE_STRING);
			$frequency = filter_var($this->input->post('frequency'), FILTER_SANITIZE_STRING);
			$datestarted = $this->input->post('datestarted');
			$timestarted = $this->input->post('timestarted');
			$dateended = $this->input->post('dateended');
			$timeend = $this->input->post('timeend');
			$route = filter_var($this->input->post('route'), FILTER_SANITIZE_STRING);
			$strength = filter_var($this->input->post('strength'), FILTER_SANITIZE_STRING);
			$strengthunit = filter_var($this->input->post('strengthunit'), FILTER_SANITIZE_STRING);
			$dosage = filter_var($this->input->post('dosage'), FILTER_SANITIZE_STRING);
			$dosagesunit = filter_var($this->input->post('dosagesunit'), FILTER_SANITIZE_STRING);
			$writtenon = filter_var($this->input->post('writtenon'), FILTER_SANITIZE_STRING);
			$expirydate = $this->input->post('expirydate');
			$doctorsname = filter_var($this->input->post('doctorsname'), FILTER_SANITIZE_STRING);
			$rxid = filter_var($this->input->post('rxid'), FILTER_SANITIZE_STRING);
			$dremail = filter_var($this->input->post('dremail'), FILTER_SANITIZE_STRING);
			$dispensedon1 = $this->input->post('dispensedon1');
			$dispensedqty1 = filter_var($this->input->post('dispensedqty1'), FILTER_SANITIZE_STRING);
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
			if (!empty($dateended)) 
				list($monthEnd, $dayEnd, $yearEnd) = explode('/', $dateended);
			if (!empty($timestarted))
				list($hhStart, $mmStart) = explode(':', $timestarted);
			if (!empty($timeend))
				list($hhEnd, $mmEnd) = explode(':', $timeend);
			$datetimeStart = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.@$hhStart.':'.@$mmStart));
			if (!empty($yearEnd))
				$datetimeEnd = date('o-m-d h:i:s' ,strtotime(@$yearEnd.'-'.@$monthEnd.'-'.@$dayEnd.' '.@$hhEnd.':'.@$mmEnd));
			else
				$datetimeEnd = '';

			if (!empty($writtenon)) 
			{
				list($monthwriteon, $daywriteon, $yearwriteon) = explode('/', $writtenon);
				$writtenon =date('o-m-d h:i:s' ,strtotime(@$yearwriteon.'-'.@$monthwriteon.'-'.@$daywriteon));
			}

			if (!empty($expirydate))
			{
				list($monthexpirydate, $dayexpirydate, $yearexpirydate) = explode('/', $expirydate);
				$expirydate =date('o-m-d h:i:s' ,strtotime(@$yearexpirydate.'-'.@$monthexpirydate.'-'.@$dayexpirydate));
			}

			if (!empty($dispensedon1))
			{
				list($monthdispensedon1, $daydispensedon1, $yeardispensedon1) = explode('/', $dispensedon1);
				$dispensedon1 =date('o-m-d h:i:s' ,strtotime(@$yeardispensedon1.'-'.@$monthdispensedon1.'-'.@$daydispensedon1));
			}

			if (strcasecmp($now,'YES')==0)
			{ $now = TRUE; }
			else
			{ $now = FALSE; }

			# here you can add the symptoms
			$medicationList = array(
				'medication' =>$medication,
				'now' => $now,
				'frequency' => $frequency,
				'datetimeStart' => $datetimeStart,
				'datetimeEnd' => @$datetimeEnd,
				'route' => $route,
				'strength' => $strength,
				'strengthunit' => $strengthunit,
				'dosage' => $dosage,
				'dosagesunit' => $dosagesunit,
				'writtenon' => @$writtenon,
				'expirydate' => @$expirydate,
				'doctorsname' => @$doctorsname,
		 		'rxid' => @$rxid,
				'dremail' => @$dremail,
				'dispensedon1' => @$dispensedon1,
				'dispensedqty1' => @$dispensedqty1,
				'notes' => @$notes,
				'IDsafe_drug' => $IDsafe_drug,
				'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
			);

			if (!empty($datetimeEnd))
			{
				if ( $datetimeStart > $datetimeEnd )
				{
					$error = TRUE;
					$errorMessage = "<p>Date ended should be after date started<p>";
				}
			}
			
			if ($error == FALSE)
				$this->medication->updateMedication($medicationList);
		}		
		
		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $medicationList)
			);
		}	
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Update Medication Information', 
			'view' => 'medication/update'));
		}
		
		return;
	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('medicationid', 'medicationid',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);

		if ($error == FALSE)
		{
			$IDsafe_drug = filter_var($this->input->post('medicationid'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$this->medication->deleteSafeDrug($IDsafe_drug);
		}
	
		if ($ajax == TRUE)
		{			
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Medication Information', 
			'view' => 'medication/delete'));
		}
		return;
	}

	public function deleteDoctor()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

		$IDsafe_drug = filter_var($this->input->get('IDsafedrug'), FILTER_SANITIZE_STRING);
		$IDsafe_drug = preg_replace('#[^a-z0-9_]#', '', $IDsafe_drug);
	}

	public function modificationMedicationDisplay()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

		$IDsafe_drug = filter_var($this->input->get('IDsafedrug'), FILTER_SANITIZE_STRING);
		$IDsafe_drug = preg_replace('#[^a-z0-9_]#', '', $IDsafe_drug);

		$this->medicationInformation = $this->medication->getToModifyMedication($IDsafe_drug);

		if (!empty($this->medicationInformation->IDemr_journal))
			$this->journalinformation = $this->safejournal->getjournal($this->medicationInformation->IDemr_journal);		

		$this->load->view('medication/display_medication-update-information-prescription.phtml', array());
	}

	public function insertMedicationDisplay()
	{
		$this->checkSession();
		$arrayGetMedication = array();


		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$problemlist = array();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);


		$IDsafe_drug = filter_var($this->input->get('IDsafe_drug'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_drug' => $IDsafe_drug
		);

		# get the problem information
		$medicationInformation = $this->medication->getToModifyMedication($IDsafe_drug);

		$this->medicationInfo = array(
			'IDsafe_drug' => @$IDsafe_drug,
			'medicationName' => @$medicationInformation->brandName,
			'takingitnow' => (boolean)@$medicationInformation->now,
			'frequency' => @$medicationInformation->frequency,
			'dateStart' => @$medicationInformation->dateStart,
			'dateEnd' => @$medicationInformation->dateEnd,
			'route' => @$medicationInformation->route,
			'strength' => @$medicationInformation->strength,
			'strengthunit' => @$medicationInformation->strengthtype,
			'dosage' => @$medicationInformation->dosage,
			'dosagesunit' => @$medicationInformation->dosagetype,
			'notes' => @$medicationInformation->notes
		);

		$prescriptionInformation = $this->medication->getPrescription(@$medicationInformation->IDsafe_prescription);

		# get all prescriptions
		$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($sessionid));
		$allprescriptions = $this->prescription->getAllPrescription($IDsafe_personalInfo);
		$this->medicationInfo['prescriptionlist'] = @$allprescriptions;

		$this->prescriptionInfo = array(
			'writtenon' => @$prescriptionInformation->prescriptiondate,
			'expirydate' => @$prescriptionInformation->prescriptionend,
			'doctorsname' => @$prescriptionInformation->dr_name,
			'rxid' => @$prescriptionInformation->dr_rx,
			'dremail' => @$prescriptionInformation->dr_email
		);

		$this->dispenseInformation = $this->medication->getDispense(@$medicationInformation->IDsafe_prescription);

		$this->load->view('medication/display_medication-insert-information.phtml', array());
	}
	public function insertMedicationDisplayDoctor()
	{
		$this->checkSession();
        $ajax = TRUE;
        $error = TRUE;
        $errorMessage = '';
        $prescriptionList = array();
        $arrayGetMedication = array();

        $sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);


		$IDsafe_drug = filter_var($this->input->get('IDsafe_drug'), FILTER_SANITIZE_STRING);
		$IDsafe_drug = preg_replace('#[^a-z0-9_]#', '', $IDsafe_drug);

		$retData = array(
			'IDsafe_drug' => $IDsafe_drug
		);

		# get the problem information
		$medicationInformation = $this->medication->getToModifyMedication($IDsafe_drug);

		# get all prescriptions
		$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($sessionid));
		$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$allprescriptions = $this->prescription->getAllActivePrescription($IDsafe_personalInfo, $doctorInformation->IDemr_user);
		$this->prescriptionlist = @$allprescriptions;


		$this->medicationInfo = array(
			'IDsafe_drug' => @$IDsafe_drug,
			'medicationName' => @$medicationInformation->brandName,
			'takingitnow' => (boolean)@$medicationInformation->now,
			'frequency' => @$medicationInformation->frequency,
			'dateStart' => @$medicationInformation->dateStart,
			'dateEnd' => @$medicationInformation->dateEnd,
			'route' => @$medicationInformation->route,
			'strength' => @$medicationInformation->strength,
			'strengthunit' => @$medicationInformation->strengthtype,
			'dosage' => @$medicationInformation->dosage,
			'dosagesunit' => @$medicationInformation->dosagetype,
			'notes' => @$medicationInformation->notes
		);

		$prescriptionInformation = $this->medication->getPrescription(@$medicationInformation->IDsafe_prescription);

		$this->prescriptionInfo = array(
			'writtenon' => @$prescriptionInformation->prescriptiondate,
			'expirydate' => @$prescriptionInformation->prescriptionend,
			'doctorsname' => @$prescriptionInformation->dr_name,
			'rxid' => @$prescriptionInformation->dr_rx,
			'dremail' => @$prescriptionInformation->dr_email
		);

		$this->dispenseInformation = $this->medication->getDispense(@$medicationInformation->IDsafe_prescription);

		$this->load->view('medication/display_medication-insert-information-profileviewer.phtml', array());
	}

	public function insertMedicationDisplayDoctorTable()
	{
		$this->checkSession();
        $ajax = TRUE;
        $error = TRUE;
        $errorMessage = '';
        $prescriptionList = array();
        $arrayGetMedication = array();

        $this->load->view('medication/display_medication-insert-information-tableformat.phtml', array());
	}

	public function dashboardDisplay()
	{
		$this->checkSession();
		$this->safedrug = $this->medication->displayDashboardMedication($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);

		# for paging result 
        # will work with custom-dashboard-medication.js pagination code
        # and display_medication-information.phtml div medication-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'medication/dashboardDisplay?q=1';
        $config["total_rows"] = $this->medication->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->medication->displayDashboardMedication($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result


		$this->load->view('medication/display_medication-information.phtml', $data, array());
	}
	public function displayMedication()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->medication->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;

		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->safedrug = $this->medication->displayDashboardMedication($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-medication-profileviewer.js pagination code
        # and display_medication-information-profileviewer.phtml div medication-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'medication/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->medication->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->medication->displayDashboardMedication($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('medication/display_medication-information-profileviewer.phtml', $data, array());
	}

	
	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }

            #
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
	
}
?>