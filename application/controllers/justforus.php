<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Justforus extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->model('Personalprofile_model', 'personalProfile');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Facebook_model', 'fblogin');
		$this->load->helper(array('form', 'url'));

		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # check if logged in to nambal using any social network account
        if (!empty($this->nambal_session['sessionName']))
        {
            # if true, redirect to dashboard
        }
        elseif (!empty($this->facebook_session['id'])) {
            # code...
        }
	}
	public function index()
	{

	}

	public function login()
	{
		
	}
}

?>