<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ConnectPharmacy extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Application List',
				'view' => 'connect-to-pharmacy/index'));
		$pharmaID = $this->input->get('pharmaID');
		echo 'profileID:'.$pharmaID;

		return;
	}
}
?>