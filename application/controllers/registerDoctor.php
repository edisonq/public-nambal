<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class RegisterDoctor extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->redirector = $this->session->userdata('urldirect');

        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_model', 'emruser');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index()
	{
        $this->load->model('Promo_model', 'promomodel');
        $this->load->model('Emrspecialty_model', 'emrspecialty');
        $this->load->model('Error_model','errormodel');
        $this->load->library('email');

        

		$licenseNumber = '';
        $arrayEmail = array();
		$specialty = array();
		$customCss = array('custom-register');
        $error = FALSE;
        $errorMessage = '';
        $approve = false;
        $allEmrSpecialty = $this->emrspecialty->getAll();

        # getting some pre-errors
        $errorNum = filter_var($this->input->get('errorNum'), FILTER_SANITIZE_STRING);
        if (!empty($errorNum))
        {
            $errorNum = $this->errormodel->identifyError($errorNum);
            $errorMessage = $errorNum['userdisplaytext'].'. '.$errorNum['resolutiontext'];
        }
        # end of getting some pre-errors


		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

            // # IF DOCTOR redirect to dashboard
            // if ($this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            // {
            //     redirect(base_url().'doctorDashboard', 'refresh');
            // }

            # if doctor already requested access redirect to please wait page or request again
            if ($this->emruser->isAlreadyRequesting($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {
                redirect(base_url().'registerDoctor/done','refresh');
            }
            # end  doctor already requested access redirect to please wait page or request again

        }
        # end of redirecting users to dashboard if logged in
		
		if ($_POST)
		{
			$this->form_validation->set_rules('licensenumber', 'Professional license number', 'trim|xss_clean|required');
			$this->form_validation->set_rules('specialty', 'Specialty', 'xss_clean|required');
            $this->form_validation->set_rules('promocode', 'Promo Code', 'trim|xss_clean');
            $this->form_validation->set_rules('countemail', 'Email count', 'trim|xss_clean|required');

			
			if ($this->form_validation->run() == FALSE)
			{
				$error = TRUE;
			}
			else
			{
				$error = FALSE;
			}

			$licenseNumber = filter_var($this->input->post('licensenumber'), FILTER_SANITIZE_STRING);
            $specialty = $this->input->post('specialty');   
            $promocode = filter_var($this->input->post('promocode'), FILTER_SANITIZE_STRING);
            $countEmail = filter_var($this->input->post('countemail'), FILTER_SANITIZE_STRING);
        
            # check promo code
            if (!empty($promocode))
            {
                if (!$this->promomodel->checkPromoCode($promocode))
                {
                    $error = TRUE;
                    $errorMessage .= $this->promomodel->errorMessage;
                }
                else{
                    $approve = TRUE;
                }
            }
            # end check promo code

            # add the doctor
            if (!$error)
            {
                $dataEmr = array(
                    'rxId' => '',
                    'licenseNumber' => $licenseNumber,
                    'IDsafe_user' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']),
                    'specialty' => $specialty
                );
                $IDemr_user = $this->emruser->insertDocInfo($dataEmr);
                if ($IDemr_user == 0)
                {
                    redirect(base_url().'registerDoctor?errorNum=2002');
                }

                $applicantEmail = $this->nagkamoritsing->ibalik($this->emruser->getMainEmail($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])));

                # add email address of approver
                while($countEmail >= 1)
                {
                    $this->form_validation->set_rules('email'.$countEmail, 'Email count', 'trim|xss_clean|valid_email');

                    if ($this->form_validation->run() == FALSE)
                    {
                        $error = TRUE;
                    }
                    else
                    {
                        $error = FALSE;
                    }

                    $email = filter_var($this->input->post('email'.$countEmail), FILTER_SANITIZE_EMAIL);

                    array_push($arrayEmail, $email);
                    $countEmail--;
                }
                $arrayEmail = array_unique($arrayEmail);
                foreach ($arrayEmail as $emails) 
                {
                    if (!empty($emails))
                    {
                        $this->emruser->addDoctorApprover($IDemr_user, $emails);

                        # send email to all approver
                        $this->email->from('no-reply@nambal.com', 'Nambal Information');
                        $this->email->to($emails);
                        // $this->email->cc($applicantEmail);
                        // $this->email->bcc('edisonq@hotmail.com');
                         
                        $this->email->subject('Your friend doctor need\'s approval');
                        $emailData = array(
                            'firstname' => $this->nagkamoritsing->ibalik($this->nambal_session['firstname']),
                            'lastname' => $this->nagkamoritsing->ibalik($this->nambal_session['lastname']),
                            'gender' => $this->nambal_session['sex'],
                            'applicantEmail' => $applicantEmail,
                            'state' => $this->nambal_session['state'],
                            'country' => $this->nambal_session['country'],
                            'specialty' => $specialty,
                            'IDemr_user' => $this->nagkamoritsing->bungkag($IDemr_user),
                            'toEmail' => $emails
                        );

                        $this->email->message(
                            $this->load->view(
                                    'for-email.phtml', array('title' => 'Forgot Password', 
                                    'view' => 'emails/register-doctor-approval', 
                                    'emailData' => $emailData,
                                    'title' => 'Doctor, your friend needs help', 
                                    'footerEmail' => $emails
                            ), true));

                        $this->email->send();
                        # end of sending email all approver
                    }
                }
                # end of adding email address of approver

                # immediately approve dashboard if promo is good
                if ($approve)
                {
                    $this->emruser->approveDoctor($IDemr_user);
                    // if (!empty($_SESSION['urldirect']))
                    // {
                    //     redirect($_SESSION['urldirect'], 'refresh');
                    // }
                }

                # reduce limit to 1
                $this->promomodel->reducePromoLimit($promocode, 1);
                $this->promomodel->linktouser($promocode, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
                redirect(base_url().'registerDoctor/done', 'refresh');
            }            		
		} 

		$this->load->view(
				'homepage.phtml', array('title' => 'Register Doctor', 
				'customCss' => $customCss,
                'errorMessage' => $errorMessage,
                'allEmrSpecialty' => $allEmrSpecialty,
                'licenseNumber' => $licenseNumber,
                'specialty' => $specialty,                
				'view' => 'register-doctor/index'));

	}

    public function done()
    {
        $customCss = array('custom-register');
        $errorMessage = '';
        $error = FALSE;

        
        if ($this->emruser->isAlreadyRequesting($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {   
            if ($this->emruser->isAlreadyApprove($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {

                $this->load->view(
                'homepage.phtml', array('title' => 'Welcome to Doctor Dashboard.', 
                'customCss' => $customCss,
                'errorMessage' => $errorMessage,
                'view' => 'register-doctor/approved'));
            }
            else
            {
                $this->load->view(
                'homepage.phtml', array('title' => 'Done with the registration, Please wait for the approval.', 
                'customCss' => $customCss,
                'errorMessage' => $errorMessage,
                'view' => 'register-doctor/done'));
            }
        }
        else
        {
            redirect(base_url().'registerDoctor');
        }
    }

    public function approve()
    {
        $customCss = array('custom-register');
        $errorMessage = '';
        $error = FALSE;

         

        $this->load->view(
                'homepage.phtml', array('title' => 'Thank you!', 
                'customCss' => $customCss,
                'errorMessage' => $errorMessage,
                'view' => 'register-doctor/approve'));
    }
}
?>