<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paginpractice extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        $this->load->helper("url");
        $this->load->library("pagination");

        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Emrjournal_model', 'journal');
        $this->load->model('Error_model', 'errordisplay');
        $this->load->model('personalprofile_model', 'personalprofile');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'), 
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 

	}

    public function index()
    {
        // record_count();
        // fetch_journals();
        $config = array();
        $config["base_url"] = base_url() . "paginpractice/index";

        $config["total_rows"] = $this->journal->record_count(11,9); #record_count($IDemr_user, $IDemr_patient)
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->journal->fetch_journals($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
        $this->load->view("pagepractice.phtml", $data);
        $this->output->enable_profiler(TRUE);
    }

    public function querystyle()
    {
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

        $config = array();
        $config["base_url"] = base_url() . "paginpractice/querystyle?a=";

        $config["total_rows"] = $this->journal->record_count(11,9); #record_count($IDemr_user, $IDemr_patient)
        $config["per_page"] = 10;
        $config["uri_segment"] = 'per_page';
        $config["use_page_numbers"] = TRUE;       
        $config['num_links'] = 20;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
 
        $this->pagination->initialize($config);
 
        $page = @$this->per_page;
        echo $page;
        $data["results"] = $this->journal->fetch_journals($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
        $this->load->view("pagepractice.phtml", $data);
        $this->output->enable_profiler(TRUE);
    }
}


?>