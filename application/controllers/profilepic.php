<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\CreateBlobOptions;
use WindowsAzure\Blob\Models\PublicAccessType;
use WindowsAzure\Common\ServiceException;
class Profilepic extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->helper('s3');

        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('ContactInformation_model', 'contactinformation');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('profilepic_model', 'profilepic');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index()
	{

	}

	public function take()
	{
		$customCss = array('custom-register');

		$this->load->view(
			'homepage.phtml', array('title' => 'Take picture from your webcam', 
			'view' => 'profilepic/take', 'customCss' => $customCss));
	}

	public function deactivateprof()
	{
		$this->profilepic->deactivateprofilepic($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->output->enable_profiler(TRUE);
	}

	public function upload()
	{
		# check session
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->checkSession();
		$image1=$this->input->post('image');

		$this->load->library('upload', config_item('uploader'));

		if ( ! $this->upload->do_upload('file_upload')) // userfile default
		{
			$this->error = array('error' => $this->upload->display_errors());
			$data = array('uploaded' => FALSE, 'formData' => $this->error);
		}
		else
		{
			$this->data = array('upload_data' => $this->upload->data());
			
			# database
			$dataUpload = array(
				'IDsafe_user' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']),
				'filename' => $this->data['upload_data']['file_name']
			);
			$this->profilepic->uploadprofile($dataUpload);
			# end of database input

			$data = array('uploaded' => TRUE, 'formData' => $this->data);
		}
		
		if (!empty($ajax) && (strcasecmp($ajax, 'TRUE')==0))
		{
			// header('Content-Type: application/json');
			echo json_encode($data);
		}
		else
		{
			redirect(base_url().'profilepic/change', 'refresh');
		}
	}

	public function uploadarea()
	{
		$this->load->view(
            'profilepic/upload-area.phtml', array(
            'upload-area' => ''));
	}

	public function change()
	{
		$this->checkSession();
		$customCss = array('jquery.Jcrop');
		$customJs = array('jquery.Jcrop.min');
		$customJsTop = array();
		
		$this->uploadInfo = config_item('uploader');

		# retrieve profile image if naa na
		$this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		# end of kuha profile image if naa

		$this->load->view(
			//'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Change profile pic',
			'view' => 'profilepic/dashboard-upload', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'customJsTop' => $customJsTop,
			'css' => 'dashboard-custom'));
	}


	public function s3test()
	{
		$this->load->library('s3');
		  
		// List Buckets
		var_dump($this->s3->listBuckets());

		$this->contents= $this->s3->getBucket("resources.nambal.com");    
		
		//check whether a form was submitted
		if(isset($_POST['Submit']))
		{
		
			//retreive post variables
			$fileName = 'profilepic/'.$_FILES['theFile']['name'];
			$fileTempName = $_FILES['theFile']['tmp_name'];

						
			//move the file
			if ($this->s3->putObjectFile($fileTempName, "resources.nambal.com", $fileName, S3::ACL_PUBLIC_READ)) {
				echo "<strong>We successfully uploaded your file.</strong>";
			}else{
				echo "<strong>Something went wrong while uploading your file... sorry.</strong>";
			}
		}

		$this->load->view('profilepic/s3.phtml');
	}

	public function cropimgaws()
	{
		$this->load->library('s3');
		$s3 = "resources.nambal.com";

		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$targ_w = $targ_h = 145;
			$jpeg_quality = 90;

			# get the image
			$profileInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			if (strcasecmp($profileInfo->type, 'profile-pic-ec2')==0)
			{
				$src = APPPATH.'../public/uploads/'.$profileInfo->filename;
				$img_r = imagecreatefromjpeg($src);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
				$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				// header('Content-type: image/jpeg');
				imagejpeg($dst_r,$src,$jpeg_quality);	
				imagedestroy($dst_r);

				$fileName = 'profilepic/'.$profileInfo->filename;

				//move the file
				if ($this->s3->putObjectFile($src, $s3, $fileName, S3::ACL_PUBLIC_READ)) 
				{
					# success
					# database update
					$this->profilepic->transfertos3($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']), $profileInfo->filename);
					# end of database update

					unlink($src);
				}
			}
			# end of getting the image		
		}
		redirect(base_url().'profilepic/change', 'refresh');
	}

	public function cropimg()
	{
		$this->load->library('azure');
		// Create blob REST proxy.
		$connectionString = $this->azure->getConnectionString();
		$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);


		
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$targ_w = $targ_h = 145;
			$jpeg_quality = 90;

			# get the image
			$profileInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			if (strcasecmp($profileInfo->type, 'profile-pic-ec2')==0)
			{
				$src = APPPATH.'../public/uploads/'.$profileInfo->filename;
				$img_r = imagecreatefromjpeg($src);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
				$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				// header('Content-type: image/jpeg');
				imagejpeg($dst_r,$src,$jpeg_quality);	
				imagedestroy($dst_r);

				//--- putting it in azure.
				$content = fopen(APPPATH.'../public/uploads/'.$profileInfo->filename, "r");
				$blob_name = $profileInfo->filename;
				$options = new CreateBlobOptions();
				$options->setBlobContentType("image/jpeg");
				

				try {
				    //Upload blob
				    $blobRestProxy->createBlockBlob("profilepic", $blob_name, $content, $options);
				}
				catch(ServiceException $e){
				    // Handle exception based on error codes and messages.
				    // Error codes and messages are here:
				    // http://msdn.microsoft.com/library/azure/dd179439.aspx
				    $code = $e->getCode();
				    $error_message = $e->getMessage();
				    // echo $code.": ".$error_message."<br />";
				}
				// -- end of putting it in azure.

		// 		# success
		// 		# database update
				$this->profilepic->transfertos3($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']), $profileInfo->filename);
		// 		# end of database update

				unlink($src);
			}
			# end of getting the image		
		}
		redirect(base_url().'profilepic/change', 'refresh');
	}


	public function checkSession(){
		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
	}

}


?>