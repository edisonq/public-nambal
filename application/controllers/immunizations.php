<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Immunizations extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Immunization_model', 'immunizations');
		$this->load->model('Procedures_model', 'procedures');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('Safeuser_model', 'safeuser');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Immunizations Information', 
			'view' => 'immunizations/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{

		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$problemlist = array();

		$this->form_validation->set_rules('immunization','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datereceived', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$immunization = ucwords(filter_var($this->input->post('immunization'), FILTER_SANITIZE_STRING));
			$datereceived = $this->input->post('datereceived');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);		

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datereceived);
			$datereceived = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			# here you can add the symptoms
			$immunizationList = array(
				'immunization' => $immunization,
				'datereceived' => @$datereceived,
				'notes' => @$notes
			);

			if ($error == FALSE)
				$this->immunizations->insert($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $immunizationList);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Immunizations Information', 
			'view' => 'immunizations/insert'));
		}

		return;

	}

	public function update()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$problemlist = array();

		$this->form_validation->set_rules('IDsafe_immunization', 'IDsafe_immunization', 'trim|xss_clean|required');
		$this->form_validation->set_rules('immunization','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datereceived', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$IDsafe_immunization = filter_var($this->input->post('IDsafe_immunization'), FILTER_SANITIZE_STRING);
			$immunization = ucwords(filter_var($this->input->post('immunization'), FILTER_SANITIZE_STRING));
			$datereceived = $this->input->post('datereceived');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);		

			# check dates and time
			list($monthStart, $dayStart, $yearStart) = explode('/', $datereceived);
			$datereceived = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

			# here you can add the symptoms
			$immunizationList = array(
				'IDsafe_immunization' => $IDsafe_immunization,
				'immunization' => $immunization,
				'datereceived' => @$datereceived,
				'notes' => @$notes
			);

			if ($error == FALSE)
				$this->immunizations->update($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $immunizationList);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $problemlist)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Update Immunizations Information', 
			'view' => 'immunizations/update'));
		}

		return;
	}

	public function insertDoctor()
	{

		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$immunization_userlog = array();

		$immunizationList = array();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

		$this->form_validation->set_rules('immunization','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datereceived', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('boosterdate', 'Booster Date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Doctor Notes or journal entry', 'trim|xss_clean|required');


		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}


		if ($error == FALSE)
		{

			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$immunization = ucwords(filter_var($this->input->post('immunization'), FILTER_SANITIZE_STRING));
			$datereceived = $this->input->post('datereceived');
			$boosterdate = $this->input->post('boosterdate');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);		


			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo
			
			# get IDsafe_personalInfo (page or account of the patient you are in)
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);

			# get doctor information (my information as the doctor logged in)
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));


			# check dates and time
			if(!empty($datereceived))
			{
				list($monthStart, $dayStart, $yearStart) = explode('/', $datereceived);
				$datereceived = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

				# ako ni gi butang sa sulod kay dapat naa ju'y date received before naay boosterdate
				if(!empty($boosterdate))
				{
					list($monthBooster, $dayBooster, $yearBooster) = explode('/', $boosterdate);

					$boosterdate = date('o-m-d h:i:s' ,strtotime(@$yearBooster.'-'.@$monthBooster.'-'.@$dayBooster));

					if($boosterdate < $datereceived)
					{
						$error = TRUE;
						$errorMessage = 'Booster Date must not be before Date Received';
					}	
					else if ($boosterdate == $datereceived)
					{
						$boosterdate = NULL;
					}
				}
				else
				{
					# set boosterdate to NULL since there would be a default date data in the database even if boosterdate is empty
					$boosterdate = NULL;

				}
			}
			
			if($error == FALSE)
			{
				# here you can add the immunization
				$immunizationList = array(
					'immunization' => $immunization,
					'datereceived' => @$datereceived,
					'boosterdate' => @$boosterdate,
					'notes' => @$notes
				);

				# add immunization
				$immunization_userlog = $this->immunizations->insert($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $immunizationList);

				# check if patient ba ni siya sa doctor
				# if dili i add siya as one of the patient
				$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
				$IDemr_patient = @$IDemr_patient->IDemr_patient;
				if (empty($IDemr_patient))
					$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				$immunizationList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$immunizationList['IDemr_user'] = $doctorInformation->IDemr_user;
				$immunizationList['doctor-notes'] = $doctorNotes;
				$immunizationList['IDsafe_immunization'] = $immunization_userlog['IDsafe_immunization'];
				$immunizationList['IDemr_patient'] = $IDemr_patient;
				
				$immunization_userlog['IDemr_journal'] = $IDemr_journal = $this->safejournal->insertjournalimmunization($immunizationList);

				# send email notification
				$patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
				$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
				$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
					'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				);

				$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);

				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				// $this->safeuserlog->notifypatientbyemail($sessionid,$message);

				# userlogs
				$safe_userlog = array(
					'IDsafe_user' => $IDsafe_personalInfo,
					'activity' => 'ADD immunization',
					'activityDescription' => 'Added patient immunization record',
					'access_type' => 2,
					'log_type' => 'immunization'
				); 

				$immunization_userlog['IDsafe_userLog'] = $this->safeuserlog->logUser($safe_userlog);
				$immunization_userlog['emr_notes'] = $immunizationList['notes'];

				# add immunization userlog
				$this->safeuserlog->logUser($immunization_userlog, 'immunization'); 
			}
				
		}


		if ($ajax == TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Immunizations Information', 
			'view' => 'immunizations/insert'));
		}

		return;

	}

	public function updateDoctor()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$originalImmunizationList = array();

		$immunizationList = array();

		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

		$this->form_validation->set_rules('immunization','Condition or symptoms', 'trim|xss_clean|required');
		$this->form_validation->set_rules('datereceived', 'Date started', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('boosterdate', 'Booster Date', 'xss_clean|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Doctor Notes or journal entry', 'trim|xss_clean|required');


		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}
 

		if ($error == FALSE)
		{

			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$IDsafe_immunization = filter_var($this->input->post('IDsafe_immunization'), FILTER_SANITIZE_STRING);
			$immunization = ucwords(filter_var($this->input->post('immunization'), FILTER_SANITIZE_STRING));
			$datereceived = $this->input->post('datereceived');
			$boosterdate = $this->input->post('boosterdate');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);		


			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo

			# get IDsafe_personalInfo (page or account of the patient you are in)
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);

			# get doctor information (my information as the doctor logged in)
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));


			# check dates and time
			if(!empty($datereceived))
			{
				list($monthStart, $dayStart, $yearStart) = explode('/', $datereceived);
				$datereceived = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));

				# ako ni gi butang sa sulod kay dapat naa ju'y date received before naay boosterdate
				if(!empty($boosterdate))
				{
					list($monthBooster, $dayBooster, $yearBooster) = explode('/', $boosterdate);

					$boosterdate = date('o-m-d h:i:s' ,strtotime(@$yearBooster.'-'.@$monthBooster.'-'.@$dayBooster));

					if($boosterdate < $datereceived)
					{
						$error = TRUE;
						$errorMessage = 'Booster Date must not be before Date Received';
					}	
					else if ($boosterdate == $datereceived)
					{
						$error = TRUE;
						$errorMessage = 'Date Received and Booster Date are equal';
					}
				}
				else
				{
					# set boosterdate to NULL since there would be a default date data in the database even if boosterdate is empty
					$boosterdate = NULL;

				}
			}
			
			if($error == FALSE)
			{
				$immunizationList = array(
					'IDsafe_immunization' => $IDsafe_immunization,
					'immunization' => $immunization,
					'datereceived' => @$datereceived,
					'notes' => @$notes
				);
				
				if(!empty($boosterdate) || $boosterdate != NULL)
				{
					$immunizationList['boosterdate'] = @$boosterdate;
				}

				$this->immunizations->update($immunizationList);

				// $patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
				// $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
				// $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

				// $emailData = array(
				// 	'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
				// 	'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
				// );

				// $message = $this->load->view(
			 //            'for-email.phtml', array('title' => 'Notify patient', 
			 //            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);

				# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
				// $this->safeuserlog->notifypatientbyemail($sessionid,$message);

			}
				
		}


		if ($ajax == TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Immunizations Information', 
			'view' => 'immunizations/insert'));
		}

		return;

	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();
		$errorMessage = '';

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('IDsafeimmunization', 'IDsafe_immunization',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		if ($error == FALSE)
		{
			$IDsafe_immunization = filter_var($this->input->post('IDsafeimmunization'), FILTER_SANITIZE_STRING);		
			$this->immunizations->deleteSafe($IDsafe_immunization);
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);

	
		
		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Immunizations Information', 
			'view' => 'immunizations/delete'));
		}

		return;
	}

	public function displayImmunization()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->immunizations->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	public function insertDisplay()
	{
		$this->checkSession();
		$arrayGetContact = array();

		$IDsafe_immunization = filter_var($this->input->get('IDsafe_immunization'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_immunization' => $IDsafe_immunization
		);

		# get the immunization information
		if (!empty($IDsafe_immunization))
		{
			$immunizationInformation = $this->immunizations->getToModify($IDsafe_immunization);

			$this->immuneInfo = array(
				'IDsafe_immunization' => @$immunizationInformation->IDsafe_immunization,
				'immunization' => ucwords(@$immunizationInformation->immunizationName),
				'datereceived' => @$immunizationInformation->dateImmuned,
				'boosterdate' => @$immunizationInformation->boosterdate,
				'notes' => @$immunizationInformation->notes
			);
		}

		$this->load->view('immunizations/display_immunization-insert-information.phtml', array());
	}

	public function insertDisplayDoctor()
	{
		$this->checkSession();
		$arrayGetContact = array();

		$IDsafe_immunization = filter_var($this->input->get('IDsafe_immunization'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_immunization' => $IDsafe_immunization
		);

		# get the problem information
		if (!empty($IDsafe_immunization))
		{
			$immunizationInformation = $this->immunizations->getToModify($IDsafe_immunization);

			$this->immuneInfo = array(
				'IDsafe_immunization' => @$immunizationInformation->IDsafe_immunization,
				'immunization' => ucwords(@$immunizationInformation->immunizationName),
				'datereceived' => @$immunizationInformation->dateImmuned,
				'boosterdate' => @$immunizationInformation->boosterdate,
				'notes' => @$immunizationInformation->notes
			);
		}

		$this->load->view('immunizations/display_immunization-insert-information-profileviewer.phtml', array());
	}

	public function dashboardDisplay()
	{
		$this->checkSession();
		// $this->safeImmunization = $this->immunizations->dashboardDisplay($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
		// print_r($this->safeImmunization);
		// $this->output->enable_profiler(TRUE);

		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);

		// $this->safeImmunization = $this->immunizations->dashboardDisplay($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-immunization.js pagination code
        # and display_immunization-information.phtml div immunizations-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'immunizations/dashboardDisplay?q=0';
        $config["total_rows"] = $this->immunizations->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->immunizations->dashboardDisplay($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

		$this->load->view('immunizations/display_immunization-information.phtml', $data, array(
			'somevariable'
		));
	}

	public function dashboardDisplayProfileViewer()
	{
		$this->overdueBoosterdate = false;
		$this->count_overdueBoosterdate = 0;
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;

		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));
        
		// $this->safeImmunization = $this->immunizations->dashboardDisplay($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-immunization-profileviewer.js pagination code
        # and display_immunization-information-profileviewer.phtml div immunizations-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'immunizations/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->immunizations->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->immunizations->dashboardDisplay($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

        # get immunizations information
		// $this->safeImmunization = $this->immunizations->dashboardDisplay($IDsafe_personalInfo);

		# check over due booster date
		$check_overdueBoosterdate = $this->immunizations->check_overdueBoosterdate($this->nagkamoritsing->ibalik($this->sessionid));

		foreach($check_overdueBoosterdate as $cob)
		{
			if($cob->boosterdate != null)
			{
				if(date('M j, Y', strtotime($cob->boosterdate)) < date('M j, Y'))
					$this->overdueBoosterdate = true;
					$this->count_overdueBoosterdate++;
			}
			
		}

		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('immunizations/display_immunization-information-profileviewer.phtml', $data, array('somevariable'));
	}

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
	
}
?>