<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Texttoimage extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('image_lib');   
    }

    public function index()
    {
        
    }

    public function writeToImage()
    {
        $text = filter_var($this->input->get('text'), FILTER_SANITIZE_STRING);
        /*** make sure the file exists ***/

        /*** if the file does not exist we will create our own image ***/
        /*** Create a black image ***/
        $im  = imagecreatetruecolor(250, 30); /* Create a black image */

        /*** the background color ***/
        $bgc = imagecolorallocate($im, 255, 255, 255);

        /*** the text color ***/
        $tc  = imagecolorallocate($im, 0, 0, 0);

        /*** a little rectangle ***/
        imagefilledrectangle($im, 0, 0, 250, 30, $bgc);

        /*** output and error message ***/
        imagestring($im, 3, 5, 5, "$text", $tc);

        header('Content-Type: image/jpeg');
        imagejpeg($im);
        imagedestroy($im);
    }
}