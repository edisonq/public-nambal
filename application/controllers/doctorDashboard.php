<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class DoctorDashboard extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('ContactInformation_model', 'contactinformation');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Appointment_model', 'appointmentsmodel');
        $this->load->model('Location_model', 'locationmodel');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'), 
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 

        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

            # check if the user is a registered doctor in nambal
            if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
            }
            # end of checking if the user is a registered doctor in nambal  
        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in

        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 

	}
	public function index()
	{
        # need to finish this
        # progress report sa users
        $this->load->model('Progress_model', 'sessionModel');

        if (!empty($_SESSION['urldirect']))
        {
            $urldirecter = $_SESSION['urldirect'];
            # check if the user is a registered doctor in nambal
            if ($this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
                $_SESSION['urldirect'] = '';
            # end of checking if the user is a registered doctor in nambal 
            redirect(urldecode($urldirecter), 'refresh');
            
            // $this->session->unset_userdata('urlredirect');
        }


        $customJs = array('fullcalendar.min',
            'calendar-appointment-doctor',
            // 'calendar',
            // 'tables-dynamic',
            '../lib/jquery.autogrow-textarea','../lib/bootstrap-switch','../lib/bootstrap-colorpicker',
            // 'forms-elemets', 
            'tour-doctor-dashboard');
		$firstTime = false;
        
        $urlAdd = 'doctorDashboard';
		$customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = '';        

		#check login
		$this->checkLogin();

        $this->userDoctorInfo = $this->emrUser->getDocInfoComplete($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->userDoctorAppointment = $this->appointmentsmodel->getDoctorAppointments($this->userDoctorInfo[0]->IDemr_user, NULL, FALSE, FALSE, TRUE, TRUE);
        $this->locationsArray = $this->locationmodel->getDoctorLocations($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

		
		#check if user patient is firsttime
		if($this->emrUser->checkFirstTime($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
		{
			$firstTime = true;
		}

        if ($firstTime != true)
        {
            unset($customJs[array_search("tour-doctor-dashboard", $customJs)]);
        }

 

		# old UI
   //      $this->load->view(
			// 'doctor-viewer.phtml', array(
			// 	'title' => 'Doctor Dashboard', 
			// 	'customCss' => $customCss,
			// 	'customJs' => $customJs,
			// 	'urlAdd' => $urlAdd,
			// 	'view' => 'doctor-dashboard/index'
			// ));

        # new UI
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
			'firsttime' => $firstTime,
            'view' => 'doctor-dashboard/index-light-blue'
        ));


	}


    public function indexShowoff()
    {
        # need to finish this
        # progress report sa users
        $this->load->model('Progress_model', 'sessionModel');


        $customJs = array('fullcalendar.min',
            'calendar-appointment-doctor',
            // 'calendar',
            // 'tables-dynamic',
            '../lib/jquery.autogrow-textarea','../lib/bootstrap-switch','../lib/bootstrap-colorpicker',
            // 'forms-elemets', 
            'tour-doctor-dashboard');
        $firstTime = false;
        
        $urlAdd = 'doctorDashboard';
        $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = '';        

        #check login
        $this->checkLogin();

        $this->userDoctorInfo = $this->emrUser->getDocInfoComplete($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->userDoctorAppointment = $this->appointmentsmodel->getDoctorAppointments($this->userDoctorInfo[0]->IDemr_user, NULL, FALSE, FALSE, TRUE, TRUE);
        $this->locationsArray = $this->locationmodel->getDoctorLocations($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        
        #check if user patient is firsttime
        if($this->emrUser->checkFirstTime($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            $firstTime = true;
        }

        if ($firstTime != true)
        {
            unset($customJs[array_search("tour-doctor-dashboard", $customJs)]);
        }

 

        # old UI
   //      $this->load->view(
            // 'doctor-viewer.phtml', array(
            //  'title' => 'Doctor Dashboard', 
            //  'customCss' => $customCss,
            //  'customJs' => $customJs,
            //  'urlAdd' => $urlAdd,
            //  'view' => 'doctor-dashboard/index'
            // ));

        # new UI
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'firsttime' => $firstTime,
            'view' => 'doctor-dashboard/index-showoff'
        ));


    }
	
	public function tour()
	{
		$IDsafe_user = 0;
		$firsttime = 0;
		$status = false;
	
		#check session
		$this->checkLogin();
		
		#update after tour
		if($this->emrUser->updateAfterTour($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
		{
			$status = true;
		}
		
		$data = array(
			'status' => $status
		);
		
        redirect(base_url().'doctorDashboard', 'refresh');
		// echo json_encode($data);
		exit;
	}
	
	public function checkLogin()
	{
		 # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
	}
}

?>