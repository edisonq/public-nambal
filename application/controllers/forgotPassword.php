<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->model('Safeuser_model', 'safe_user');
		$this->load->model('Safeverification_model', 'safe_verification');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->model('ChangePassRequest_model', 'changepass');
		$this->load->model('Signup_model', 'signup');
		$this->load->model('Safeuseremail_model', 'safeuseremail');
		$this->load->model('Passwordchecker_model', 'passwordchecker');
		$this->load->helper(array('form', 'url'));

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);
        $this->currentDate = date('Y-m-d H:i:s');
	}
	public function index()
	{
		$errorMessage = '';
		$error = FALSE;
		$outputData = '';
		$template = 'forgot-password/index';
		$requestDate = date('l jS \of F Y h:i:s A');

		if($_POST)
        {
            $this->form_validation->set_rules('email','Email address', 'trim|xss_clean|required|valid_email');

			if ($this->form_validation->run() == FALSE)
			{
			    $error = TRUE;
			} else 
			{
			    $error = FALSE;
			}

			$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);

			if ($error != TRUE)
			{
				$forgotPasswordInfo = $this->safe_user->forgotPassword($email);

				if (!empty($forgotPasswordInfo['username']) && $forgotPasswordInfo != 0)
				{
					if ($forgotPasswordInfo['verified'] == TRUE)
					{
						# send the email
						$this->email->from('no-reply@nambal.com', 'Nambal Information');
						$this->email->to($email);
						// $this->email->cc('edisonq@yahoo.com');
						// $this->email->bcc('edisonq@hotmail.com');
						 
						$this->email->subject('Username and Password in nambal.com');
						$emailData = array(
							'firstname' => $this->nagkamoritsing->ibalik($forgotPasswordInfo['firstname']),
							'lastname' => $this->nagkamoritsing->ibalik($forgotPasswordInfo['lastname']),
							'email' => $this->nagkamoritsing->bungkag($email),
							'requestDate' => $requestDate,
							'code' => $forgotPasswordInfo['code'],
							'username' => $this->nagkamoritsing->ibalik($forgotPasswordInfo['username']),
							'sessionName' => $forgotPasswordInfo['username']
						);
						$this->email->message(
							$this->load->view(
					            'for-email.phtml', array('title' => 'Forgot Password', 
					            'view' => 'emails/forgot-password', 'emailData' => $emailData, 'footerEmail' => $email), true));
						$this->email->send();
						# end of sending the email

						$outputData = array('email' => $email);
							$userlogdata = array(
		                    'IDsafe_user' => @$forgotPasswordInfo['IDsafe_user'],
		                    'activity' => 'requested change password',
		                    'activityDescription' => 'requested change password',
		                    'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
		                );

		                $this->safeuserlog->logUser($userlogdata);
						$template = 'forgot-password/input-new-password';
					}
					else
					{
						$errorMessage = 'You already registered using this email.  Please check your email for the verification code or you may also <a href="'.base_url().'forgotPassword/requestNewVerificationCode">request another verification code</a>.';
						$outputData = array('email' => $email);
						$error = TRUE;
					}
				}
				else
				{
					$errorMessage = 'Email is not existing. Please <a href="'.base_url().'register">register</a> using that email.';
					$outputData = array('email' => $email);
					$error = TRUE;
				}
			}
		}


		$this->load->view(
            'homepage.phtml', array('title' => 'Forgot Password', 
            'view' => $template, 'errorMessage' => $errorMessage, 'outputData' => $outputData));
	}

	public function changepassword()
	{
		$template ='forgot-password/change-password';
		$outputData = '';
		$errorMessage = '';
		$error = false;
		
		$sessionName = $this->nagkamoritsing->ibalik(filter_var($this->input->get('sessionName'), FILTER_SANITIZE_STRING));
		$code =  filter_var($this->input->get('code'), FILTER_SANITIZE_STRING);
		$code = preg_replace('#[^a-z0-9_]#', '', $code);

		// if (!empty($this->nambal_session['sessionName']))
		// {

		// }

		$IDsafe_user = $this->changepass->changePasswordApproval($code, $sessionName);

		if (!empty($IDsafe_user))
		{
			$outputData = array(
				'code' => $code,
				'sessionName' => $this->nagkamoritsing->bungkag($sessionName),
				'IDsafe_user' => $this->nagkamoritsing->bungkag($IDsafe_user)
			);
		}
		else
		{
			redirect(base_url().'forgotPassword', 'refresh');
		}
		if ($_POST)
		{
			$this->form_validation->set_rules('password','New password', 'trim|xss_clean|required');
			$this->form_validation->set_rules('confirmPassword','Confirm new password', 'trim|xss_clean|required');
			$this->form_validation->set_rules('sessionName','Session name', 'trim|xss_clean|required');
			$this->form_validation->set_rules('IDsafe_user','IDsafe_user', 'trim|xss_clean|required');

			if ($this->form_validation->run() == FALSE)
			{
			    $error = TRUE;
			} else 
			{
			    $error = FALSE;
			}

			if ($error == FALSE)
			{
				$password = filter_var($this->input->post('password'), FILTER_SANITIZE_STRING);
				$confirmPassword = filter_var($this->input->post('confirmPassword'), FILTER_SANITIZE_STRING);
				$sessionName = filter_var($this->input->post('sessionName'), FILTER_SANITIZE_STRING);
				$IDsafe_user = filter_var($this->input->post('IDsafe_user'), FILTER_SANITIZE_STRING);
				$code = filter_var($this->input->post('code'), FILTER_SANITIZE_STRING);

				$approval = $this->changepass->changePasswordApproval($code, $this->nagkamoritsing->ibalik($sessionName));
				$safeUserInfo = $this->changepass->getSafe_userInfo();
				$safeUseremailInfo = $this->safeuseremail->getUseremailInfo_using_IDsafe_user($this->nagkamoritsing->ibalik($IDsafe_user));
				$checkValReturn = $this->passwordchecker->checkValidPassword($password, $confirmPassword);


				if (empty($approval))
				{
					redirect(base_url().'forgotPassword', 'refresh');
				}

				if (strcmp($checkValReturn, 'not matched')==0)
				{
					$errorMessage .= 'Password and confirm password '.$checkValReturn;
					$error = true;
				}
				elseif((strcmp($checkValReturn, 'Very Weak')==0) || (strcmp($checkValReturn, 'Weak')==0) || (strcmp($checkValReturn, 'Medium')==0) )
				{
					$errorMessage .= 'Password strength is '.$checkValReturn.'.  Try combining your password with numbers, symbols or Capital letters. It should have more than 7 characters.';
					$error = true;
				}

				if ($error == false)
				{
					$secret_key = config_item('secret_key_nambal');
					$this->signup->setNewPassword($password, $this->nagkamoritsing->ibalik($safeUserInfo->username), $secret_key);
					$this->changepass->afterChangePass($code, $this->nagkamoritsing->ibalik($IDsafe_user));

					# send the email
					$this->email->from('no-reply@nambal.com', 'Nambal Information');
					$this->email->to($this->nagkamoritsing->ibalik($safeUseremailInfo->emailAddress));
					// $this->email->cc('edisonq@yahoo.com');
					// $this->email->bcc('edisonq@hotmail.com');
					 
					$this->email->subject('Your password in nambal.com account has been changed');
					$emailData = array(
						'firstname' => $this->nagkamoritsing->ibalik($safeUserInfo->firstname),
						'lastname' => $this->nagkamoritsing->ibalik($safeUserInfo->lastname),
						'changeRequest' => date('l jS \of F Y h:i:s A'),
						'username' =>  $this->nagkamoritsing->ibalik($safeUserInfo->username)
					);
					$this->email->message(
						$this->load->view(
				            'for-email.phtml', array('title' => 'Password Changed', 
				            'view' => 'emails/password-changed', 'emailData' => $emailData, 'footerEmail' => $this->nagkamoritsing->ibalik($safeUseremailInfo->emailAddress)), true));
					$this->email->send();
					# end of sending the email

					redirect(base_url().'login', 'refresh');
				}
			}
		}
		$this->load->view(
            'homepage.phtml', array('title' => 'Create new password', 
            'view' => $template, 'errorMessage' => $errorMessage, 'outputData' => $outputData));
	}

	public function requestNewVerificationCode()
	{
		$errorMessage = '';
		$error = false;
		$template ='forgot-password/request';
		$validationUrl = '';

		if ($_POST)
		{
			$this->form_validation->set_rules('email','Email address', 'trim|xss_clean|required|valid_email');

			if ($this->form_validation->run() == FALSE)
			{
			    $error = TRUE;
			} else 
			{
			    $error = FALSE;
			}

			$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);

			$returnVcode = $this->safe_verification->requestNewCode_byemail($email);

			if (!empty($returnVcode['vcode']))
			{
				$validationUrl = base_url().'register/verifyEmail?email='.$email.'&validationCode='.$returnVcode['vcode'];

				# email the verification code
				$this->email->from('no-reply@nambal.com', 'Nambal Information');
				$this->email->to($email);
				// $this->email->cc('edisonq@yahoo.com');
				// $this->email->bcc('edisonq@hotmail.com');
				 
				$this->email->subject('Email Verification');
				$emailData = array(
					'firstname' => $this->nagkamoritsing->ibalik($returnVcode['firstname']),
					'lastname' => $this->nagkamoritsing->ibalik($returnVcode['lastname']),
					'validationUrl' => $validationUrl,
					'vcode' => $returnVcode['vcode']
				);
				$this->email->message(
					$this->load->view(
				            'for-email.phtml', array('title' => 'Email Verification', 
				            'view' => 'emails/request-new-verification-code', 'emailData' => $emailData, 'footerEmail' => $email), true));

				# --------------[ NEED ATTENTION ]-------------
				# un-comment this when launching
				$this->email->send();

				# end of sending the verification code in email

				redirect(base_url().'register/verifyEmail?email='.$email, 'refresh');

			} else 
			{
				if (strcmp('Email is already verified', $returnVcode['errorMessage'])==0)
				{
					$errorMessage = 'Email is already verified.  <a href="'.base_url().'forgotPassword">Try retrieving your username or password.</a>';
				}
				elseif (strcmp('Email is not existing',  $returnVcode['errorMessage'])==0) 
				{
					$errorMessage = 'Email is not existing.  You may <a href="'.base_url().'register">register</a> using this email';
				}
			}


		}
		$this->load->view(
			'homepage.phtml', array('title' => 'Request new verification code', 
			'view' => $template,
			'errorMessage' => $errorMessage
		));	
	}
}
?>