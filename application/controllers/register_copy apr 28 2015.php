<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Register extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('image_lib');
		$this->load->model('Signup_model', 'signup');
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Personalprofile_model', 'personalProfile');
		$this->load->model('ContactInformation_model', 'contactinfo');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->model('Passwordchecker_model', 'passwordchecker');
		$this->load->model('Login_model', 'login');
		$this->load->model('Facebook_model', 'fblogin');
		$this->load->helper(array('form', 'url'));

		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # check if logged in to nambal using any social network account
        if (!empty($this->nambal_session['sessionName']))
        {
            # if true, redirect to dashboard
        }
        elseif (!empty($this->facebook_session['id'])) {
            # code...
        }
        # else if logged in to nambal account w/o using social network account, redirect to dashboard
        # else continue
        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);


	}

	public function index()
	{
		if (!empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

		
		$errorMessage = '';
		$validationCode = '';
		$validationUrl = '';
		$error = false;

		# check input here including the email
		$this->form_validation->set_rules('firstname','First name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('lastname', 'Last name','trim|required|xss_clean');
		$this->form_validation->set_rules('middlename','Middle name', 'trim|xss_clean');
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email|is_unique[safe_useremails.emailAddress]');
		$this->form_validation->set_rules('agreeTerms', 'trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}
		

		# if you are done here please change the line below
		# end of checking input
		$firstname = filter_var($this->input->post('firstname'), FILTER_SANITIZE_STRING);
		// $firstname = preg_replace('#[^a-z0-9_]#', '', $firstname);

		$lastname = filter_var($this->input->post('lastname', FILTER_SANITIZE_STRING));
		// $lastname = preg_replace('#[^a-z0-9_]#', '', $lastname);

		$middlename = filter_var($this->input->post('middlename', FILTER_SANITIZE_STRING));
		// $middlename = preg_replace('#[^a-z0-9_]#', '', $middlename);

		$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);

		$agreeTerms = $this->input->post('agreeTerms');

		if ($_POST)
		{
			if (empty($agreeTerms))
			{
				$error = true;
				$errorMessage .= 'You need to agree our terms and conditions. ';
			}
		}


		if (!$error) # no need to change this
		{
			

			$data = array(
				'username' => $email,
				'firstname' => $firstname,
				'lastname' => $lastname,
				'middlename' => $middlename,
				'email' => $email
			);



			# check if the username and email is existing
			# redudant email checker.. but anyway, this is my first checker.
			# not sure why the form_validation unique check won't work.
			if ((!$this->signup->check_email($data['email'])) && (!$this->signup->check_username($data['username'])))
			{
				$IDsafe_user = $this->signup->firstRegistration($data);

				# get the validation code and url
				$validationCode = $this->signup->getValidationCodeByID($IDsafe_user);
				$validationUrl = base_url().'register/verifyEmail?email='.$data['email'].'&validationCode='.$validationCode->vcode;
				# end of getting the validation code and URL

				$this->email->from('no-reply@nambal.com', 'Nambal Information');
				$this->email->to($data['email']);
				// $this->email->cc('edisonq@yahoo.com');
				// $this->email->bcc('edisonq@hotmail.com');
				 
				$this->email->subject('Email Verification');
				$emailData = array(
					'firstname' => $firstname,
					'lastname' => $lastname,
					'validationUrl' => $validationUrl,
					'vcode' => $validationCode->vcode
				);

				$this->email->message(
					$this->load->view(
				            'for-email.phtml', array('title' => 'Forgot Password', 
				            'view' => 'emails/request-new-verification-code', 'emailData' => $emailData, 'footerEmail' => $email), true));

				# --------------[ NEED ATTENTION ]-------------
				# un-comment this when launching
				$this->email->send();
				

				 $userlogdata = array(
                    'IDsafe_user' => @$IDsafe_user,
                    'activity' => 'created the account using email',
                    'activityDescription' => 'created the account using email',
                    'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
                );

                $this->safeuserlog->logUser($userlogdata);

                # set the session for step 2 of registration
                $secret_key = config_item('secret_key_nambal');
                $dataLogin = array(
                    'username' => $email,
                    'password' => ''
                );

                $login = $this->login->notVerifiedLogin($dataLogin, $secret_key);

               if ($login)
               { 
                    $loginInformation = $this->login->getData();
                    $safePersonalInfo = $this->login->getPersonalInfoData();
                    $safeAddressInfo = $this->login->getSafeAddressData();
                    $data = array(
	                    'sessionName' => $this->nagkamoritsing->bungkag($email),
                        'firstname' => @$loginInformation->firstname,
                        'middlename' =>  @$loginInformation->middlename,
                        'lastname' =>  @$loginInformation->lastname,
                        'fullname' =>  @$safePersonalInfo->fullname,
                        'default' => @$safePersonalInfo->default,
                        'sex' => @$safePersonalInfo->sex,
                        'dateofbirth' => @$safePersonalInfo->dateofbirth,
                        'bloodtype' => @$safePersonalInfo->bloodtype,
                        'IDsafe_personalInfo' =>  $this->nagkamoritsing->bungkag(@$safePersonalInfo->IDsafe_personalInfo),
                        'IDsafe_user' =>  $this->nagkamoritsing->bungkag(@$loginInformation->IDsafe_user),
                        'street' => @$safeAddressInfo->street,
                        'city' => @$safeAddressInfo->city,
                        'state' => @$safeAddressInfo->state,
                        'postcode' => @$safeAddressInfo->postcode,
                        'country' => @$safeAddressInfo->country,
                        'sessionAddress' => $this->ipAddress
	                );
	                $this->session->unset_userdata('logged_in');
	                $this->session->unset_userdata('fb_profile');

	                $this->sessionModel->setSession($data);
	                $this->session->set_userdata('logged_in', $data);
	            }


                # set the session for setp 2 of registration above
                redirect(base_url().'register/createAccountSecond', 'refresh');
                # the old redirect below
				# redirect(base_url().'register/verifyEmail?email='.$data['email'], 'refresh');
			}
			else
			{
				# existing username and email
				# execute output here
				$errorMessage .= 'The email is already used.';
			}
		}

		$customCss = array('custom-register');

		if (!$error)
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Step 1 of Registration', 
				'view' => 'register/index', 
				'firstname' =>  $firstname, 
				'lastname' => $lastname, 
				'middlename' => $middlename,
				'email' => $email,
				'errorMessage' => $errorMessage,
				'customCss' => $customCss
			));
		}
		else{
			$this->load->view(
				'homepage.phtml', array('title' => 'Step 1 of Registration', 
				'view' => 'register/index',
				'errorMessage' => $errorMessage,
				'customCss' => $customCss
			));
		}
	}

	public function verifyEmail()
	{
		$errorMessage = '';
		$url = '';
		$email = '';
		$validationCode = '';
		$error = false;
		$customCss = array('custom-register');

		# validate and clean, get and post input here
		# need to modify pa
		$_POST = $_GET;
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('validationCode', 'Validation Code', 'trim|xss_clean|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}
		# end of validation of get and post.

		$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);
		$validationCode = filter_var($this->input->post('validationCode'), FILTER_SANITIZE_STRING);
		$validationCode = preg_replace('#[^a-z0-9_]#', '', $validationCode);



		if (!$error)
		{
			if ($this->signup->check_verification($validationCode, $this->nagkamoritsing->bungkag($email)))
			{
				 $userlogdata = array(
                    'IDsafe_user' => $this->signup->getIDsafe_user(),
                    'activity' => 'verified the email',
                    'activityDescription' => 'verified the email',
                    'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
                );

                $this->safeuserlog->logUser($userlogdata);

				$this->load->view(
					'homepage.phtml', array('title' => 'Step 2 of Verification, Thanks for verifying your Email', 
					'view' => 'register/verified',
					'email' => $email,
					'url' => $url,
					'customCss' => $customCss,
					'errorMessage' => $errorMessage ));
			}
			else
			{
				$errorMessage .= 'Please make sure your Validation code is correct.';
				$this->load->view(
					'homepage.phtml', array('title' => 'Step 2 of Verification, Verify Your Email Address', 
					'view' => 'register/verify', 
					'validationCode' =>  $validationCode,
					'email' => $email,
					'customCss' => $customCss,
					'errorMessage' => $errorMessage ));
			}
		}
		else
		{
			$this->load->view(
					'homepage.phtml', array('title' => 'Step 2 of Verification, Verify Your Email Address', 
					'view' => 'register/verify', 
					'validationCode' =>  $validationCode,
					'email' => $email,
					'customCss' => $customCss,
					'errorMessage' => $errorMessage ));
		}	
	}

	public function addPassword()
	{
		$errorMessage = '';
		
		$email = '';
		$password = '';
		$confirmPassword = '';
		$retUrl = 'dashboard';
		$customCss = array('custom-register');
		
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required');
		$this->form_validation->set_rules('confirmPassword','Confirm Password','trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			# There's a problem with the user input
			$error = true;
			$errorMessage .= '<br/>Please check your input';
		}
		else
		{
			$error = false;
		}

		# already sanitize input.. sanitize it again... just to be sure.
		$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);
		$password = filter_var($this->input->post('password'), FILTER_SANITIZE_STRING);
		$confirmPassword = filter_var($this->input->post('confirmPassword'), FILTER_SANITIZE_STRING);

		$checkValReturn = $this->passwordchecker->checkValidPassword($password, $confirmPassword);

		if (strcmp($checkValReturn, 'not matched')==0)
		{
			$errorMessage .= 'Password and confirm password do '.$checkValReturn;
			$error = true;
		}
		elseif((strcmp($checkValReturn, 'Very Weak')==0) || (strcmp($checkValReturn, 'Weak')==0) || (strcmp($checkValReturn, 'Medium')==0) )
		{
			$errorMessage .= 'Password strength is '.$checkValReturn.'.  Try combining your password with numbers, symbols or Capital letters. It should have more than 7 characters.';
			$error = true;
		}
		
		if ($error != true)
		{
			$this->signup->setNewPassword($password, $email,  config_item('secret_key_nambal'));

			# login now 
			if (!$error)
            {
                # login
                $secret_key = config_item('secret_key_nambal');
                $dataLogin = array(
                    'username' => $email,
                    'password' => $password
                );

                $login = $this->login->login($dataLogin, $secret_key);
                
               if ($login)
                {
                    $loginInformation = $this->login->getData();
                    $safePersonalInfo = $this->login->getPersonalInfoData();
                    $safeAddressInfo = $this->login->getSafeAddressData();
                    $data = array(
                        'sessionName' => $this->nagkamoritsing->bungkag($email),
                        'firstname' => @$loginInformation->firstname,
                        'middlename' =>  @$loginInformation->middlename,
                        'lastname' =>  @$loginInformation->lastname,
                        'fullname' =>  @$safePersonalInfo->fullname,
                        'default' => @$safePersonalInfo->default,
                        'sex' => @$safePersonalInfo->sex,
                        'dateofbirth' => @$safePersonalInfo->dateofbirth,
                        'bloodtype' => @$safePersonalInfo->bloodtype,
                        'IDsafe_personalInfo' =>  $this->nagkamoritsing->bungkag(@$safePersonalInfo->IDsafe_personalInfo),
                        'IDsafe_user' =>  $this->nagkamoritsing->bungkag(@$loginInformation->IDsafe_user),
                        'street' => @$safeAddressInfo->street,
                        'city' => @$safeAddressInfo->city,
                        'state' => @$safeAddressInfo->state,
                        'postcode' => @$safeAddressInfo->postcode,
                        'country' => @$safeAddressInfo->country,
                        'sessionAddress' => $this->ipAddress
                    );
                    $this->session->unset_userdata('logged_in');
                    $this->session->unset_userdata('fb_profile');

                    $this->sessionModel->setSession($data);

                    $this->session->set_userdata('logged_in', $data);

                    # check if the user is already verified using the session
                    if(!$this->login->checkVerified())
                    {
                        redirect(base_url().'register/verifyEmail', 'refresh');
                    }

                    # check if the user has address
                    if ((empty($safeAddressInfo->street)) || (empty($safeAddressInfo->country)) || (empty($safeAddressInfo->city)) )
                    {
                        redirect(base_url().'register/createAccountSecond', 'refresh');
                    }

                    # check if the user has primary emergency contact person
                    if (!$this->login->checkPrimaryContact($safePersonalInfo->IDsafe_personalInfo))
                    {
                        redirect(base_url().'register/createAccountThird', 'refresh');
                    }   

                    
                    # it should go to the last redirection
                    if (empty($retUrl))
                    {
                        redirect(base_url().'dashboard', 'refresh');
                    }
                    else
                    {
                        redirect(base_url().$retUrl, 'refresh');
                    }
                }
                else
                {
                    $errorMessage = '<ul>';
                    foreach ($this->login->getErrors() as $errorMsg) {
                        $errorMessage .= '<li>'.$errorMsg.'</li>';
                    }
                    $errorMessage .= '</ul>';
                    
                }
                # end of logging in                
            }
			# end of loggin in now.


			redirect(base_url().'login/auto?email='.$email.'&password='.$password.'&retUrl='.$retUrl, 'refresh');
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Step 1 of Registration, Add password', 
			'view' => 'register/password', 
			'email' => $email,
			'customCss' => $customCss,
			'error' => $errorMessage ));	
		}
	}

	public function createAccountSecond()
	{
		// country=string, state=string,dateofbirth=mm/dd/yyyy,gender= :0=none,1=male, 2=female
		$customCss = array('custom-register');
		$customJs = array('countries');
		$user_profile = $this->session->userdata('logged_in');
		$user_safeInformation = array();
		$errorMessage = '';
		$error = false;
		$dateofbirth = '';


		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user'])) ) 
		{
			redirect(base_url().'login', 'refresh');			
		}

		# for birthday format arrangement
		if (!empty($user_profile['dateofbirth']))
		{
			$birthday = explode('-', $user_profile['dateofbirth']);
			// 1984-02-24
			if (!empty($birthday[2]))
			$dateofbirth = $birthday[1].'/'.$birthday[2].'/'.$birthday[0];
		} 

		# end birthday format arrangement

		$user_profile = array(
			'fullname' => $this->nagkamoritsing->ibalik(@$user_profile['fullname']),
			'street' => $user_profile['street'],
			'city' => $user_profile['city'],
			'state' => $user_profile['state'],
			'postcode' => $user_profile['postcode'],
			'country' => $user_profile['country'],
			'dateofbirth' => $dateofbirth,
			'sex' => $user_profile['sex'],
			'bloodtype' => $user_profile['bloodtype'],
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
	        'IDsafe_user' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_user']),
			'sessionName' => $this->nagkamoritsing->ibalik(@$user_profile['sessionName']),
	        'firstname' => $this->nagkamoritsing->ibalik(@$user_profile['firstname']),
	        'middlename' => $this->nagkamoritsing->ibalik(@$user_profile['middlename']),
	        'lastname' => $this->nagkamoritsing->ibalik(@$user_profile['lastname']),
	        'default' => @$user_profile['default'],
	        'sessionAddress' => $this->ipAddress
		);

		if ($_POST)
		{
			$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
			$this->form_validation->set_rules('country','Country', 'trim|xss_clean|required');
			$this->form_validation->set_rules('state', 'State', 'trim|xss_clean|required');
			$this->form_validation->set_rules('street','Street', 'trim|xss_clean|required');
			$this->form_validation->set_rules('city','City', 'trim|xss_clean|required');
			$this->form_validation->set_rules('postcode','Post Code', 'trim|xss_clean|required');		
			$this->form_validation->set_rules('dateofbirth','Date of Birth', 'trim|xss_clean|required');
			$this->form_validation->set_rules('sex','Gender', 'trim|xss_clean|required');
			$this->form_validation->set_rules('bloodtype','Blood type', 'trim|xss_clean|required');

			if ($this->form_validation->run() == FALSE)
			{
				$error = true;
			}
			else
			{
				$error = false;
			}

			$fullname = filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING); 
			$street = filter_var($this->input->post('street'), FILTER_SANITIZE_STRING);
			$city = ucwords(filter_var($this->input->post('city'), FILTER_SANITIZE_STRING));
			$state = filter_var($this->input->post('state'), FILTER_SANITIZE_STRING);
			$postcode = ucwords(filter_var($this->input->post('postcode'), FILTER_SANITIZE_STRING));
			$country = filter_var($this->input->post('country'), FILTER_SANITIZE_STRING);
			$dateofbirth = ucwords(filter_var($this->input->post('dateofbirth'), FILTER_SANITIZE_STRING));
			$sex = ucwords(filter_var($this->input->post('sex'), FILTER_SANITIZE_STRING));
			$bloodtype = ucwords(filter_var($this->input->post('bloodtype'), FILTER_SANITIZE_STRING));
		
			if ($error != true)
			{
				#birthday segration
				$bdayArray = explode('/', $dateofbirth);
				$dateofbirth = $bdayArray[2].'-'.$bdayArray[0].'-'.$bdayArray[1];
				#end of birthday segration

				$user_profile = array(
					'fullname' => $this->nagkamoritsing->bungkag($fullname),
					'country' => $country,
					'state' => $state,
					'street' => $street,
					'city' => $city,
					'postcode' => $postcode,
					'dateofbirth' => $dateofbirth,
					'sex' => $sex,
					'bloodtype' => $bloodtype,
					'IDsafe_personalInfo' => $this->nagkamoritsing->bungkag(@$user_profile['IDsafe_personalInfo']),
		            'IDsafe_user' => $this->nagkamoritsing->bungkag(@$user_profile['IDsafe_user']),
					'sessionName' => $this->nagkamoritsing->bungkag(@$user_profile['sessionName']),
		            'firstname' => $this->nagkamoritsing->bungkag(@$user_profile['firstname']),
		            'middlename' => $this->nagkamoritsing->bungkag(@$user_profile['middlename']),
		            'lastname' => $this->nagkamoritsing->bungkag(@$user_profile['lastname']),
		            'default' => @$user_profile['default'],
		            'sessionAddress' => $this->ipAddress
				);
				$this->session->set_userdata('logged_in', $user_profile);


				$theData = array(
					'fullname' => @$fullname,
					'sex' => @$sex,
					'dateofbirth' => @$dateofbirth,
					'bloodtype' => @$bloodtype,
					'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
					'street' => @$street,
					'state' => @$state,
					'city' => @$city,
					'country' => @$country,
					'postcode' => @$postcode,
					'default' => true,
					'active' => true
				);
				$this->signup->secondRegistration($theData);
				redirect(base_url().'register/createAccountThird', 'refresh');
			} else {
				$user_profile = array(
				'firstname' => $user_profile['firstname'],
				'fullname' => $fullname,
				'street' => $street,
				'city' => $city,
				'state' => $state,
				'postcode' => $postcode,
				'country' => $country,
				'dateofbirth' => $dateofbirth,
				'sex' => $sex,
				'bloodtype' => $bloodtype
				);
			}
		}

		$this->load->view(
			'homepage.phtml', array('title' => 'Step 2 of registration, Personal information', 
			'view' => 'register/second', 'user_profile' =>  $user_profile, 'fullname' =>  $user_profile['fullname'], 'errorMessage' => $errorMessage, 'customCss' => $customCss, 'customJs' => $customJs));
	}

	public function createAccountThird()
	{
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$safeContact = array();
		$error = false;
		$customCss = array('custom-register');
		$customJs = array('numbersOnly', 'phone-numbers');

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}

		# getting the primary contact if there's any
		$personalInfodata = array(
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($user_profile['IDsafe_personalInfo'])
		);
		$safeContact = $this->contactinfo->getContact($personalInfodata);
		# end of getting the primary contact 

		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countryCode','Country code', 'trim|xss_clean|required');
		$this->form_validation->set_rules('areaCode','Area code', 'trim|xss_clean|numeric');
		$this->form_validation->set_rules('number','Phone number', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('fulladdress', 'Full Address', 'trim|xss_clean"required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}

		$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);
		$fullname = filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING);
		$countryCode = filter_var($this->input->post('countryCode'), FILTER_SANITIZE_STRING);
		$areaCode = filter_var($this->input->post('areaCode'), FILTER_SANITIZE_NUMBER_INT);
		$number = filter_var($this->input->post('number'), FILTER_SANITIZE_NUMBER_INT);
		$fulladdress = filter_var($this->input->post('fulladdress'), FILTER_SANITIZE_STRING);
		
		if ($_POST)
		{
			$safeContact = array(
				'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($user_profile['IDsafe_personalInfo']),
				'email' => $email,
				'fullname' => $fullname,
				'countrycode' => $countryCode,
				'areacode' => $areaCode,
				'number' => $number,
				'fulladdress' => $fulladdress
			);
			if ($error != true)
			{
				$this->signup->thirdRegistration($safeContact);
				redirect(base_url().'register/createAccountFourth', 'refresh');
			}
		}		

		$this->load->view(
			'homepage.phtml', array('title' => 'Step 3 of registration, Emergency Information', 
			'view' => 'register/third', 'safeContact' =>  $safeContact, 'user_profile' => $user_profile, 'errorMessage' => $errorMessage, 'customCss' => $customCss, 'customJs' => $customJs));
	}

	public function createAccountFourth()
	{
		$code = '';
		$this->load->model('qr_model', 'qrModel');
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$cardData = array();
		$error = false;
		$customCss = array('custom-register');

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}

		if ($this->qrModel->retrieveCurrent($this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo'])))
		{
			$code = $this->qrModel->qrInfo->textcode;
		}


		$user_profile = array(
			'email' =>  $this->nagkamoritsing->ibalik($user_profile['sessionName']),
			'fullname' => ucwords($this->nagkamoritsing->ibalik($user_profile['fullname'])),
			'street' => $user_profile['street'],
			'city' => $user_profile['city'],
			'state' => $user_profile['state'],
			'postcode' => $user_profile['postcode'],
			'country' => $user_profile['country'],
			'dateofbirth' => $user_profile['dateofbirth'],
			'sex' => $user_profile['sex'],
			'bloodtype' => $user_profile['bloodtype'],
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_user']),
			'sessionName' => $this->nagkamoritsing->ibalik(@$user_profile['sessionName']),
            'firstname' => $this->nagkamoritsing->ibalik(@$user_profile['firstname']),
            'middlename' => $this->nagkamoritsing->ibalik(@$user_profile['middlename']),
            'lastname' => $this->nagkamoritsing->ibalik(@$user_profile['lastname']),
            'default' => @$user_profile['default'],
            'sessionAddress' => $this->ipAddress
		);

		$personalInfodata = array(
			'IDsafe_personalInfo' =>  $user_profile['IDsafe_personalInfo']
		);
		$safeContact = $this->contactinfo->getContact($personalInfodata);

		$cardData = array(
			'fullname' => $safeContact['fullname'],
			'countrycode' => $safeContact['countrycode'],
			'areacode' => $safeContact['areacode'],
			'number' => $safeContact['number'],
			'contactemail' => $safeContact['email'],
			'fulladdress' => $safeContact['fulladdress']
		);

		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Medication Information', 
			'view' => 'register/fourth', 'cardData' =>  $cardData, 'user_profile' => $user_profile,'customCss' => $customCss, 'textcode' => $code));
	}
	
	public function editInformation()
	{
		$user_profile = $this->session->userdata('logged_in');
		$errorMessage = '';
		$cardData = array();
		$error = false;
		$customCss = array('custom-register');
		$customJs = array('numbersOnly', 'countries','phone-numbers');

		if ((!isset($user_profile)) || (empty($user_profile['IDsafe_user']))) 
		{
			redirect(base_url().'login', 'refresh');
		}

		$dateOfBirth = explode(' ', $user_profile['dateofbirth']);
		$dateOfBirth = explode('-', $dateOfBirth[0]);
		$dateOfBirth = $dateOfBirth[0].'-'.$dateOfBirth[1].'-'.$dateOfBirth[2];
		$user_profile['dateofbirth'] = $dateOfBirth;

		$user_profile = array(
			'email' =>  $this->nagkamoritsing->ibalik($user_profile['sessionName']),
			'fullname' => $this->nagkamoritsing->ibalik($user_profile['fullname']),
			'street' => $user_profile['street'],
			'city' => $user_profile['city'],
			'state' => $user_profile['state'],
			'postcode' => $user_profile['postcode'],
			'country' => $user_profile['country'],
			'dateofbirth' => $user_profile['dateofbirth'],
			'sex' => $user_profile['sex'],
			'bloodtype' => $user_profile['bloodtype'],
			'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_personalInfo']),
            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$user_profile['IDsafe_user']),
			'sessionName' => $this->nagkamoritsing->ibalik(@$user_profile['sessionName']),
            'firstname' => $this->nagkamoritsing->ibalik(@$user_profile['firstname']),
            'middlename' => $this->nagkamoritsing->ibalik(@$user_profile['middlename']),
            'lastname' => $this->nagkamoritsing->ibalik(@$user_profile['lastname']),
            'default' => @$user_profile['default'],
            'sessionAddress' => $this->ipAddress
		);



		$personalInfodata = array(
			'IDsafe_personalInfo' =>  $user_profile['IDsafe_personalInfo']
		);
		$safeContact = $this->contactinfo->getContact($personalInfodata);

		$cardData = array(
			'fullname' => $safeContact['fullname'],
			'countrycode' => $safeContact['countrycode'],
			'areacode' => $safeContact['areacode'],
			'number' => $safeContact['number'],
			'contactemail' => $safeContact['email'],
			'fulladdress' => $safeContact['fulladdress']
		);

		$this->load->view(
			'homepage.phtml', array('title' => 'Edit Medication Information', 
			'view' => 'register/edit-information', 'cardData' =>  $cardData, 'user_profile' => $user_profile,'customCss' => $customCss, 'customJs' => $customJs));
		
	}
	
	public function editInfo(){
		$errorMessage = '';
		$validationCode = '';
		$validationUrl = '';
		$customCss = array('custom-register');
		$customJs = array('numbersOnly', 'countries','phone-numbers');
		$error = false;

		# check input here including the email
		$this->form_validation->set_rules('firstname','First name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('middlename','Middle name', 'trim|xss_clean');
		$this->form_validation->set_rules('lastname', 'Last name','trim|required|xss_clean');
		$this->form_validation->set_rules('country','Country', 'trim|xss_clean|required');
		$this->form_validation->set_rules('state', 'State', 'trim|xss_clean|required');
		$this->form_validation->set_rules('street','Street', 'trim|xss_clean|required');
		$this->form_validation->set_rules('city','City', 'trim|xss_clean|required');
		$this->form_validation->set_rules('postcode','Post Code', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateofbirth','Date of Birth', 'trim|xss_clean|required');
		$this->form_validation->set_rules('sex','Gender', 'trim|xss_clean|required');
		$this->form_validation->set_rules('bloodtype','Blood type', 'trim|xss_clean|required');
		$this->form_validation->set_rules('emergencyFullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('emergencyEmail','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('countryCode','Country code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('areaCode','Area code', 'trim|xss_clean|numeric');
		$this->form_validation->set_rules('number','Phone number', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('fulladdress', 'Full Address', 'trim|xss_clean
		|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}
		
		# personal information
		$firstname = filter_var($this->input->post('firstname'), FILTER_SANITIZE_STRING);
		$middlename = filter_var($this->input->post('middlename', FILTER_SANITIZE_STRING));
		$lastname = filter_var($this->input->post('lastname', FILTER_SANITIZE_STRING)); 
		$country = filter_var($this->input->post('country'), FILTER_SANITIZE_STRING);
		$state = filter_var($this->input->post('state'), FILTER_SANITIZE_STRING);
		$street = filter_var($this->input->post('street'), FILTER_SANITIZE_STRING);
		$city = ucwords(filter_var($this->input->post('city'), FILTER_SANITIZE_STRING));
		$postcode = filter_var($this->input->post('postcode'), FILTER_SANITIZE_STRING);
		$dateofbirth = ucwords(filter_var($this->input->post('dateofbirth'), FILTER_SANITIZE_STRING));
		$sex = ucwords(filter_var($this->input->post('sex'), FILTER_SANITIZE_STRING));
		$bloodtype = ucwords(filter_var($this->input->post('bloodtype'), FILTER_SANITIZE_STRING));
		
		# emergency information
		$emergencyFullname = filter_var($this->input->post('emergencyFullname'), FILTER_SANITIZE_STRING);
		$emergencyEmail = filter_var($this->input->post('emergencyEmail'), FILTER_SANITIZE_EMAIL);
		$countryCode = filter_var($this->input->post('countryCode'), FILTER_SANITIZE_STRING);
		$areaCode = filter_var($this->input->post('areaCode'), FILTER_SANITIZE_NUMBER_INT);
		$number = filter_var($this->input->post('number'), FILTER_SANITIZE_NUMBER_INT);
		$fulladdress = ucwords(filter_var($this->input->post('fulladdress'), FILTER_SANITIZE_STRING));
		
		
		if ($_POST)
		{	
				
				$session = $this->session->userdata('logged_in');
		
				#birthday segration
				$bdayArray = explode('/', $dateofbirth);
				$dateofbirth = $bdayArray[2].'-'.$bdayArray[0].'-'.$bdayArray[1];
				#end of birthday segration
				
				# personal information
				$user_profile = array(
					'sessionName' => @$session['sessionName'],
					'IDsafe_personalInfo' => @$session['IDsafe_personalInfo'],
		            'IDsafe_user' => @$session['IDsafe_user'],
		            'firstname' => $this->nagkamoritsing->bungkag($firstname),
		            'middlename' => $this->nagkamoritsing->bungkag($middlename),
		            'lastname' => $this->nagkamoritsing->bungkag($lastname),
					'fullname' => $this->nagkamoritsing->bungkag($firstname.' '.$middlename.' '.$lastname),
					'email' => @$session['sessionName'],
					'country' => $country,
					'state' => $state,
					'street' => $street,
					'city' => $city,
					'postcode' => $postcode,
					'dateofbirth' => $dateofbirth,
					'sex' => $sex,
					'bloodtype' => $bloodtype,
					'default' => @$session['default'],
		            'sessionAddress' => $this->ipAddress
				);
				$this->session->set_userdata('logged_in', $user_profile);
				
			if ($error != true)
			{
				
				$edit_user_profile = array(
					# personal information
					'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik(@$session['IDsafe_personalInfo']),
		            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$session['IDsafe_user']),
		            'firstname' => $this->nagkamoritsing->bungkag($firstname),
		            'middlename' => $this->nagkamoritsing->bungkag($middlename),
		            'lastname' => $this->nagkamoritsing->bungkag($lastname),
					'fullname' => $this->nagkamoritsing->bungkag($firstname.' '.$middlename.' '.$lastname),
					'country' => $country,
					'state' => $state,
					'street' => $street,
					'city' => $city,
					'postcode' => $postcode,
					'dateofbirth' => $dateofbirth,
					'sex' => $sex,
					'bloodtype' => $bloodtype,
					
					# emergency information
					'emergencyfullname' => $emergencyFullname,
					'email' => $emergencyEmail,
					'countrycode' => $countryCode,
					'areacode' => $areaCode,
					'number' => $number,
					'fulladdress' => $fulladdress
				);
			
				$this->signup->editRegistration($edit_user_profile);
				redirect(base_url().'register/createAccountFourth', 'refresh');
					
			} else {
				$user_profile = array(
				'firstname' => $firstname,
				'middlename' => $firstname,
				'lastname' => $lastname,
				'email' => $this->nagkamoritsing->ibalik($session['sessionName']),
				'street' => $street,
				'city' => $city,
				'state' => $state,
				'postcode' => $postcode,
				'country' => $country,
				'dateofbirth' => $dateofbirth,
				'sex' => $sex,
				'bloodtype' => $bloodtype
				);
				
				$cardData = array(
					'fullname' => $emergencyFullname,
					'email' => $emergencyEmail,
					'countrycode' => $countryCode,
					'areacode' => $areaCode,
					'number' => $number,
					'fulladdress' => $fulladdress
				);
			}
		}
		
		$this->load->view(
			'homepage.phtml', array('title' => 'Edit Medication Information', 
			'view' => 'register/edit-information', 'cardData' =>  $cardData, 'user_profile' => $user_profile,'customCss' => $customCss, 'customJs' => $customJs));
	}

	public function createByFacebook()
	{
		$user_profile = null;
		$redirectUrl = base_url().'register/createAccountSecond';
		$user = $this->facebook->getUser();
		$errorMessage = '';
		$error = FALSE;
		$start_new_session = FALSE;
		$email = '';
		$firstname = '';
		$middlename = '';
		$lastname = '';

		if(!empty($this->facebook_session['id'])){
			$firstname = $this->facebook_session['first_name'];
			$lastname = $this->facebook_session['last_name'];
		}

		# i - sure balik if tinood bah jud nga wala pa mo exist
		if ($this->fblogin->checkID($this->facebook_session['id'])) {
			$redirectUrl = base_url().'register/createAccountSecond';
            redirect($redirectUrl, 'refresh');
        }
		# end of i sure balik

		
		if ($_POST)
		{
			$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
			$this->form_validation->set_rules('firstname','First Name', 'trim|xss_clean|required');
			$this->form_validation->set_rules('middlename','Middle name', 'trim|xss_clean');
			$this->form_validation->set_rules('lastname','Last name or surname', 'trim|xss_clean|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$error = TRUE;
			}

			$email = filter_var($this->input->post('email'), FILTER_SANITIZE_EMAIL);
			$firstname = filter_var($this->input->post('firstname'), FILTER_SANITIZE_STRING);
			$middlename = filter_var($this->input->post('middlename'), FILTER_SANITIZE_STRING);
			$lastname = filter_var($this->input->post('lastname'), FILTER_SANITIZE_STRING);		

			if ($error == FALSE)
			{
				$FB_SessionData = array (
					'id' => $this->facebook_session['id'],
				    'first_name' => $firstname,
				    'last_name' => $lastname,
				    'middlename' => @$middlename,
				    'email' => $email,
				    'birthday' => $this->facebook_session['birthday'],
				    'gender' => $this->facebook_session['gender'],
				    'username' => $this->facebook_session['username']
				);
				if (!$this->signup->check_email($email))
				{
					# register the facebook user
					$IDsafe_user = $this->fblogin->createNewFB($FB_SessionData);
					$IDsafe_personalInfo = $this->fblogin->getPersonalInfoData();
					$usernameNew = $this->fblogin->getUsername();
					# set the nambal session

					# set session
					$this->session->set_userdata('fb_profile', $FB_SessionData);
					$start_new_session = true;
					# end setting the session
				}
				else
				{
					$errorMessage .= 'You already used this email to register before.  Please check your email. <a href="'.base_url().'forgotPassword">Did you forgot your password?</a>.  You may also continue below using different email address.';
				}				
			}
			
		}
		elseif(!empty($this->facebook_session['email']))
		{
			if ($this->fblogin->checkID($this->facebook_session['id']))
	        {
	        	# fb account already existing
	        	redirect($redirectUrl, 'refresh');
			}
			else
			{
				if (!$this->signup->check_email($this->facebook_session['email']))
				{			
					# register the facebook user
					$IDsafe_user = $this->fblogin->createNewFB($this->facebook_session);
					$IDsafe_personalInfo = $this->fblogin->getPersonalInfoData();
					$usernameNew = $this->fblogin->getUsername();
					# set the nambal session

					# set session
					$start_new_session = true;
					# end setting the session
				}
				else
				{
					$email = $this->facebook_session['email'];
					$errorMessage .= 'You already used this email to register before.  Please check your email. <a href="'.base_url().'forgotPassword">Did you forgot your password?</a>.  You may also continue below using different email address.';
				}	
			}			
		}

		# set new session
		if ($start_new_session == true)
		{
			if ($_POST && (!empty($email) ) )
			{
				$email = $email;
			}
			else
			{
				$email = $this->facebook_session['email'];
			}

			$birthday = explode('/', @$this->facebook_session['birthday']);

			$data = array(
                'sessionName' => $this->nagkamoritsing->bungkag($usernameNew),
                'firstname' => $this->nagkamoritsing->bungkag(@$firstname),
                'middlename' => $this->nagkamoritsing->bungkag(@$middlename),
                'lastname' => $this->nagkamoritsing->bungkag(@$lastname),
                'fullname' => $this->nagkamoritsing->bungkag(@$lastname.', '.@$firstname),
                'default' => TRUE,
                'sex' => @$this->facebook_session['gender'],
                'dateofbirth' => @$birthday[2].'/'.@$birthday[0].'/'.@$birthday[1],
                'bloodtype' => '',
                'IDsafe_personalInfo' => $this->nagkamoritsing->bungkag(@$IDsafe_personalInfo->IDsafe_personalInfo),
                'IDsafe_user' => $this->nagkamoritsing->bungkag(@$IDsafe_user),
                'street' => '',
                'city' => '',
                'state' => '',
                'postcode' => '',
                'country' => '',
                'sessionAddress' => $this->ipAddress
            );
			# send the email
			$this->email->from('no-reply@nambal.com', 'Nambal Information');
			$this->email->to($email);
			// $this->email->cc('edisonq@yahoo.com');
			// $this->email->bcc('edisonq@hotmail.com');
			 
			$this->email->subject('Username and Password in nambal.com');
			$emailData = array(
				'username' => $usernameNew,
				'firstname' => $firstname,
				'lastname' => $lastname
			);
			$this->email->message(
				$this->load->view(
		            'for-email.phtml', array('title' => 'Forgot Password', 
		            'view' => 'emails/new-user-facebook', 'emailData' => $emailData, 'footerEmail' => $email), true));
			$this->email->send();
			# end of sending the email
            
			$this->session->set_userdata('logged_in', $data);
			$this->sessionModel->setSession($data);
			$dataSession = array(
                'IDsafe_user' => $IDsafe_user,
                'activity' => 'Register using facebook',
                'activityDescription' => 'Register using facebook',
                'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
            );
            $this->safeuserlog->logUser($dataSession);
			redirect($redirectUrl, 'refresh');
		}
		# end of setting new session

		$this->load->view(
				'homepage.phtml', array('title' => 'Insert Medication Information', 
				'view' => 'register/facebook', 
				'user_profile' => $user_profile,
				'email' => $email, 
				'firstname' => $firstname, 
				'lastname' => $lastname, 
				'middlename' => $middlename, 
				'errorMessage' => $errorMessage  
			));
	}

	public function createByTwitter(){
		$userProfile = $this->session->userdata('user_profile');
		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Medication Information', 
			'view' => 'register/third', 
			'user_profile' =>  $userProfile, 
			'firstname' => $firstname, 
			'lastname' => $lastname ));
	}

	public function createByGoogle(){
		
	}

	public function nambalCardImg()
	{
		$config['source_image']	= base_url().'/public/img/nambal-card-fold-cut.jpg';
		$config['new_image'] = base_url().'/public/img/newimage/';
		$config['wm_text'] = 'Copyright 2006 - John Doe';
		$config['wm_type'] = 'text';
		$config['wm_font_size']	= '16';
		$config['wm_font_color'] = '000000';
		$config['wm_vrt_alignment'] = 'bottom';
		$config['wm_hor_alignment'] = 'center';
		$config['wm_padding'] = '20';

		$this->image_lib->clear();
		$this->image_lib->initialize($config); 
		$this->image_lib->watermark();

	}

	public function justestemail()
	{
		$emailData = '';
		$email = '';
		$message = '';

		$patientinfo = $this->safeuser->getSafeUserInfo($this->nambal_session['IDsafe_user']);
		$IDsafe_user = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']);

		$emailData = array(
			'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
			'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
		);
		$message = $this->load->view(
			            'for-email.phtml', array('title' => 'Notify patient', 
			            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $email), true);
		
		# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
		$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
		# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
		
		$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
		
		// print_r($this->nagkamoritsing->ibalik($patientinfo->emailAddress));

		print_r($patientinfoemail);
		echo '<br><br>';
		print_r($patientinfo);
		echo '<br><br>';

		$this->output->enable_profiler(TRUE);

		return;
	}

}

?>