<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class HealthCard extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

	}

	public function index()
	{
		$this->load->view(
			'homepage.phtml', array('title' => 'Frequently Ask Question', 
			'view' => 'health-card/index'));
		return;
	}

	public function printArea()
	{
		$customCss = array('custom-dashboard');
        $this->load->model('Emruser_model', 'emrusermodel');

        $arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		if (!empty($this->nambal_session['sessionName']))
        {
        	# check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  
        }

        $this->load->view(
			'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
			'view' => 'health-card/print-area', 
            'customCss' => $customCss,
            'arrayGetDocInfo' => $arrayGetDocInfo,
			'css' => 'dashboard-custom'));
	}
}

?>