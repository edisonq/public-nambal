<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ConnectLab extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Laboratory List',
				'view' => 'connect-to-laboratory/index'));
		$pharmaID = $this->input->get('pharmaID');
		echo 'profileID:'.$pharmaID;

		return;
	}
}
?>