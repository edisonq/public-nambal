<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class RegisterPharma extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	}

	public function index()
	{
		$errorMessage = '';
		$customCss = array('custom-register');

		$this->load->view(
			'homepage.phtml', array('title' => 'Register Pharmacy', 
			'view' => 'register/register-pharma',
			'customCss' => $customCss,
			'errorMessage' => $errorMessage
		));
	}
}

?>