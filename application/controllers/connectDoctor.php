<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ConnectDoctor extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_model', 'emrusermodel');
        $this->load->library('form_validation');
    	$this->load->model('Facebook_model', 'fblogin');
        $this->load->model('Emruser_model', 'emrUser');
		$this->load->helper(array('form', 'url'));
        $this->activeMainMenu = "connect-with-professionals";
        $this->navOpen = "connect with Professionals";

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }


        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

        }
        # end of redirecting users to dashboard if logged in

        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

	public function index(){
		// $this->load->view(
		// 	'homepage.phtml', array('title' => 'Connected doctor',
		// 		'view' => 'connect-to-doctor/index'));
		// return;
		redirect(base_url().'dashboard','refresh');
	}

	public function search()
	{
		$this->load->model('Emrspecialty_model', 'emrspecialty');
        $this->load->model('PatientDoctorRelationship_model', 'PatientDoctorRelationship');
		$customCss = array('custom-dashboard', 'custom-doctor-search','custom-patient-dashboard');
		// $customJs = array('custom-connect-doctor-search');
        // $customJsTop = array('countries');
        $customJs = array('jquery.flot.min','jquery.flot.pie','jquery.flot.resize.min', 'jquery.stateselect', 'jquery.selectboxes.pack', 'custom-connect-doctor-search-short', 'jquery-ui-1.10.3.custom.min','../lib/select2', 'forms-elemets','countries','custom-dashboard');

		$error = FALSE;
        $errorMessage = '';
        $addressClinic = '';
        $specialtyArray = array();
        $searchResult = array();

        $allEmrSpecialty = $this->emrspecialty->getAll();
		$this->form_validation->set_rules('specialty','Specialty', 'trim|required|xss_clean');
		$this->form_validation->set_rules('state', 'Province or State',  'trim|xss_clean');
		$this->form_validation->set_rules('country', 'Country',  'trim|required|xss_clean');
        $this->form_validation->set_rules('sortRecommendation','Sort recommendation','trim|xss_clean');
        $this->form_validation->set_rules('sortPopularChat','Sort most chatted','trim|xss_clean');
        $this->form_validation->set_rules('sortPopularAppointment', 'Sort by most number of appointment', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}
	
		$specialty = $this->input->get('specialty');
		$province =  filter_var($this->input->get('state'), FILTER_SANITIZE_STRING);
		$country =   filter_var($this->input->get('country'), FILTER_SANITIZE_STRING);
        $this->sortRecommendation = filter_var($this->input->get('sortRecommendation'), FILTER_SANITIZE_STRING);
        $this->sortPopularChat = filter_var($this->input->get('sortPopularChat'), FILTER_SANITIZE_STRING);
        $this->sortPopularAppointment = filter_var($this->input->get('sortPopularAppointment'),FILTER_SANITIZE_STRING);
		//$country = preg_replace('#[^a-z0-9_]#', '', $country);

		$arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $arraySearch = $this->emrusermodel->search($specialty, $country, $province, $this->sortRecommendation, $this->sortPopularChat, $this->sortPopularAppointment);
        # for testing only
        // $specialty = array(2,3);
        // $arraySearch = $this->emrusermodel->search($specialty,'philippines', NULL);
        // $arraySearch = $this->emrusermodel->search($specialty,NULL, 'cebu');
        // $arraySearch = $this->emrusermodel->search($specialty,NULL, NULL);
        // print_r($arraySearch);
        // $this->output->enable_profiler(TRUE);
        # end for testing only

        

        if ($_GET)
        {
            foreach ($arraySearch as $searcher) 
            {
                $addressClinic = '';
                // print_r($searcher['doctorInfo']);
                // print_r($searcher['doctorInfo']->IDsafe_user);
                foreach ($searcher['specialty'] as $specs) 
                {
                   foreach ($specs as $key) 
                   {
                        // print_r($key);
                        // echo '<br>';
                   }
                } 
                if (!empty($searcher['clinicAddress'][0]->street))
                {
                    $addressClinic = ucwords($searcher['clinicAddress'][0]->street);
                    // print_r($searcher['clinicAddress'][0]->IDemr_addresses);
                }
                array_push($searchResult, array(
                    'title' => 'Dr', 
                    'name' => ucwords($this->nagkamoritsing->ibalik($searcher['doctorInfo']->firstname)).' '.ucwords($this->nagkamoritsing->ibalik($searcher['doctorInfo']->lastname)), 
                    'online' => TRUE, 
                    'specialty' => $searcher['specialty'],//array('Anaesthetics', 'Family Doctor'), 
                    'address' => $addressClinic, 
                    'avatar' => '',
                    'clinicCount' => $searcher['clinicCount'],
                    'IDsafe_user' => $this->nagkamoritsing->bungkag($searcher['doctorInfo']->IDsafe_user), 
                    'recommendation' => $searcher['countRecommend']));
            }
            
        }
        
        # getting the connected doctors

        # end of getting the connected doctors

		if (!empty($this->nambal_session['sessionName']))
        {
        	# check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  
        }

        $this->load->view(
			'patient-dashboard-light-blue.phtml', array(
                // 'dashboard-template.phtml', array('title' => 'Connect to your doctor', 
            'title' => 'Connect to your doctor',
			'view' => 'connect-to-doctor/search', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'specialty' => $specialty,
            'province' => $province,
            'specialtyArray' => $specialtyArray,
            'allEmrSpecialty' => $allEmrSpecialty,
            'country' => $country,
            'searchResult' => $searchResult,
			'css' => 'dashboard-custom'));
        
		return;
	}

	public function pending(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Pending list of doctor',
				'view' => 'connect-to-doctor/pending'));
		return;
	}

    public function recommend()
    {
        $this->load->model('recommendation_model', 'recommendationModel');
        $customCss = array('custom-dashboard','custom-patient-dashboard');
        $customJs = array(
            'jquery.flot.min',
            'jquery.flot.pie',
            'jquery.flot.resize.min', 
            'jquery.stateselect', 
            'jquery.selectboxes.pack', 
            'jquery-ui-1.10.3.custom.min',
            '../lib/select2', 
            '../lib/jquery.autogrow-textarea',
            'custom-dashboard',
            '../lib/jquery-maskedinput/jquery.maskedinput',
            'forms-elemets-OLD'
        );

        $error = FALSE;
        $this->errorMessage = '';
        $this->dr = filter_var($this->input->get('dr'), FILTER_SANITIZE_STRING);
        $dr = $this->nagkamoritsing->ibalik($this->dr);

        if (empty($dr))
        {
            redirect(base_url().'connectDoctor/search','refresh');
        }

        $this->doctorInfo = $this->emrUser->getDocInfoComplete($dr);

        if (empty($this->doctorInfo))
        {
            redirect(base_url().'connectDoctor/search', 'refresh');
        }

        $this->doctorSpecialty = $this->emrUser->getDocSpecialty($this->doctorInfo[0]->IDemr_user);       
        $this->clinicInfo = $this->emrusermodel->getDocClinicInfo($dr);

        if ($_POST)
        {
            $this->form_validation->set_rules('recommendationNotes', 'comments and suggestions', 'trim|xss_clean|required');

            if ($this->form_validation->run() == FALSE)
            {
                $error = true;
            }
            else
            {
                $error = false;
            }

            if ($error == false)
            {
                $this->recommendationNotes = filter_var($this->input->post('recommendationNotes'), FILTER_SANITIZE_STRING);
                $this->recommendationModel->recommendDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']), $this->doctorInfo[0]->IDemr_user, $this->recommendationNotes);

                $this->errorMessage = "Thank you for your feedback. It would need your doctor's approval for it to be displayed.";
            }

        }

        $this->recommends = $this->recommendationModel->getPublicRecommendation_doctors($this->doctorInfo[0]->IDemr_user);

       


        $this->load->view(
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Recommend this doctor',
            'view' => 'connect-to-doctor/recommend', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'css' => 'dashboard-custom'));
        // $this->output->enable_profiler(TRUE);
        return;
    }

    public function clinicInfo()
    {
        
    }

    public function doctorInfo()
    {
        $customCss = array(
            'custom-dashboard', 
            'custom-patient-dashboard'
        );
        // $customJs = array('custom-connect-doctor-search');
        // $customJsTop = array('countries');
        $customJs = array(
            'custom-connect-doctor-search-short', 
            'custom-dashboard'
        );
        $error = FALSE;
        $errorMessage = '';

        $this->form_validation->set_rules('dr','doctor', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
        }
        else
        {
            $error = false;
        }

        $dr =  filter_var($this->input->get('dr'), FILTER_SANITIZE_STRING);
        $dr = $this->nagkamoritsing->ibalik($dr);

        if ($_GET && (!empty($dr)))
        {
            $this->clinicInfo = $this->emrusermodel->getDocClinicInfo($dr);
            if(empty($this->clinicInfo))
            {
                redirect(base_url().'connectDoctor/search', 'refresh');
            }
            // echo 'Clinic Info:';
            // print_r($clinicInfo);
        }
        else
        {
            redirect(base_url().'connectDoctor/search', 'refresh');
        }

        $this->load->view(
            'patient-dashboard-light-blue.phtml', array(
                // 'dashboard-template.phtml', array('title' => 'Connect to your doctor', 
            'title' => 'Show clinic information',
            'view' => 'connect-to-doctor/doctor-info', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'css' => 'dashboard-custom'));

        // $this->output->enable_profiler(TRUE);
    }
}

?>