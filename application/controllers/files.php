<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Files extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Files Information', 
			'view' => 'files/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Files Information', 
			'view' => 'files/insert'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function upload(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Upload Files Information', 
			'view' => 'files/upload'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function update(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Update Files Information', 
			'view' => 'files/update'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function delete(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Delete Files Information', 
			'view' => 'files/delete'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}
}
?>