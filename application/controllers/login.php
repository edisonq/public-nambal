<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends CI_Controller 
{

	public function __construct() 
    {
		parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Login_model', 'login');
        $this->load->model('Facebook_model', 'fblogin');
        $this->load->model('Addresses_model', 'addresses');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('SafeuserLog_model', 'safeuserlog');
        $this->load->model('Safeuseremail_model', 'safeuseremail');
        $this->load->model('Personalprofile_model', 'personalProfile');
        $this->load->model('ContactInformation_model', 'contactinfo');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Barangaylist_model', 'barangay');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->helper(array('form', 'url'));

        
        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->redirector = $this->session->userdata('urldirect');

        # check if logged in to nambal using any social network account
        if (!empty($this->nambal_session['sessionName']))
        {
            # if true, redirect to dashboard
        }
        // elseif (!empty($this->facebook_session['id'])) {
        //     # code...
        //     # check if the user is still logged-in in facebook
        //     $params = array(
        //       'no_user' => base_url().'login/facebook');
        //     $facebook->getLoginStatusUrl($params);
        // }
        # else if logged in to nambal account w/o using social network account, redirect to dashboard
        # else continue

        $this->errorMessage = array();
        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

        $this->load->library('recaptcha');
	}


	public function index()
    {
        $errorMessage = '';
        $validationCode = '';
        $validationUrl = '';
        $error = FALSE;     
        $retUrl = '';
        # redirect users to dashboard who are logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if($this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
               # check if session and in the database are same
                if ($this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
                {
                    # check if the session IP address is same in users IP address
                    # you can use this for quicker load in small CRUD request
                    if ($this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
                    {
                        # check if the user has address, 
                        if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
                        {
                            redirect(base_url().'register/createAccountSecond', 'refresh');
                        }

                        # check if the user has primary emergency contact person
                        if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
                        {
                            redirect(base_url().'register/createAccountThird', 'refresh');
                        }

                       
                        redirect(base_url().'dashboard', 'refresh');
                    }
                    else
                    {
                        $errorMessage = 'Session Expired. Please login again.';
                    }
                    # end of checiing if the usersIP is same with session IP                     
                }
                else
                {
                    $errorMessage = 'Session Expired. Please login again.';
                }
                # end of checking if the session and in the database are same
            }
            else
            {
                $errorMessage = 'Session Expired. Please login again.';
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches             
        }
        # end of redirecting users to dashboard if logged in
       
		
        if($_POST)
        {
            $this->form_validation->set_rules('username','Username or Email address', 'trim|xss_clean|required');
            $this->form_validation->set_rules('password','Password', 'trim|xss_clean|required');
            $this->form_validation->set_rules('retUrl','Return Url', 'trim|xss_clean');
            
            if ($this->form_validation->run() == FALSE)
            {
                $error = TRUE;
            } else {
                $error = FALSE;
            }

            $username = filter_var($this->input->post('username'), FILTER_SANITIZE_STRING);
            $password = filter_var($this->input->post('password'), FILTER_SANITIZE_STRING);
            $retUrl = filter_var($this->input->post('retUrl'), FILTER_SANITIZE_STRING);
            $barangay = filter_var($this->input->post('barangay'), FILTER_SANITIZE_STRING);


            if (!$error)
            {
                # login
                $secret_key = config_item('secret_key_nambal');
                $dataLogin = array(
                    'username' => $username,
                    'password' => $password
                );

                $login = $this->login->login($dataLogin, $secret_key);
                
               if ($login)
                {
                    $loginInformation = $this->login->getData();
                    $safePersonalInfo = $this->login->getPersonalInfoData();
                    $safeAddressInfo = $this->login->getSafeAddressData();
                    $data = array(
                        'sessionName' => $this->nagkamoritsing->bungkag($username),
                        'firstname' => @$loginInformation->firstname,
                        'middlename' =>  @$loginInformation->middlename,
                        'lastname' =>  @$loginInformation->lastname,
                        'fullname' =>  @$safePersonalInfo->fullname,
                        'default' => @$safePersonalInfo->default,
                        'sex' => @$safePersonalInfo->sex,
                        'dateofbirth' => @$safePersonalInfo->dateofbirth,
                        'bloodtype' => @$safePersonalInfo->bloodtype,
                        'IDsafe_personalInfo' =>  $this->nagkamoritsing->bungkag(@$safePersonalInfo->IDsafe_personalInfo),
                        'IDsafe_user' =>  $this->nagkamoritsing->bungkag(@$loginInformation->IDsafe_user),
                        'street' => @$safeAddressInfo->street,
                        'city' => @$safeAddressInfo->city,
                        'state' => @$safeAddressInfo->state,
                        'postcode' => @$safeAddressInfo->postcode,
                        'country' => @$safeAddressInfo->country,
                        'sessionAddress' => $this->ipAddress
                    );
                    $this->session->unset_userdata('logged_in');
                    $this->session->unset_userdata('fb_profile');
                    $this->session->unset_userdata('barangay_login');

                    $this->sessionModel->setSession($data);
                    $this->session->set_userdata('logged_in', $data);

                    $barangayInfo = $this->barangay->getBarangayInfoString(@$barangay);

                    if (!empty(@$barangayInfo->idbarangaylist) || (@$barangayInfo->barangayInfo != 0))
                    {
                        $this->session->set_userdata('barangay_login', $barangayInfo);
                    }

                    $data = array(
                        'IDsafe_user' => @$loginInformation->IDsafe_user,
                        'activity' => 'LOG in using username or email',
                        'activityDescription' => 'LOG in using username or email',
                        'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
                    );

                    $this->safeuserlog->logUser($data);
                    

                    # check if the user is already verified using the session
                    if(!$this->login->checkVerified())
                    {
                        redirect(base_url().'register/verifyEmail', 'refresh');
                    }

                    # check if the user has address
                    if ((empty($safeAddressInfo->street)) || (empty($safeAddressInfo->country)) || (empty($safeAddressInfo->city)) )
                    {
                        redirect(base_url().'register/createAccountSecond', 'refresh');
                    }

                    # check if the user has primary emergency contact person
                    if (!$this->login->checkPrimaryContact($safePersonalInfo->IDsafe_personalInfo))
                    {
                        redirect(base_url().'register/createAccountThird', 'refresh');
                    }   

                    
                    # it should go to the last redirection
                    if (empty($retUrl))
                    {
                        redirect(base_url().'dashboard', 'refresh');
                    }
                    else
                    {
                        redirect(base_url().$retUrl, 'refresh');
                    }
                }
                else
                {
                    $errorMessage = '<ul>';
                    foreach ($this->login->getErrors() as $errorMsg) {
                        $errorMessage .= '<li>'.$errorMsg.'</li>';
                    }
                    $errorMessage .= '</ul>';
                    
                }
                # end of logging in                
            }
        } else {


            $_POST = $_GET;
            $this->form_validation->set_rules('retUrl','Return URL', 'trim|xss_clean');
            if ($this->form_validation->run() != FALSE)
            {
                $retUrl = filter_var($this->input->get('retUrl'), FILTER_SANITIZE_STRING);
            }
        }

        # captcha test 
          // $data = array();
          //   $data['recaptcha'] = $this->recaptcha->get_html();
          //   $this->form_validation->set_rules('recaptcha_response_field', 'Captcha', 'required|callback_validateCaptcha');
          //   if($this->form_validation->run()) 
          //   {
          //       $data['response'] = "Correct Captcha!!!!!";
          //   }
          //   else
          //   {
                
          //   }

          //   $this->load->view('/recaptcha/demo',$data);
        # captcha test
        $customCss = array('custom-login','signup');


        $this->load->view(
            'homepage.phtml', array('title' => 'Login Area', 
            'view' => 'login/index', 'errorMessage' => $errorMessage, 'retUrl' => $retUrl, 'customCss' => $customCss));
    }

    public function barangay()
    {
        $errorMessage = '';
        $retUrl = '';
        $customCss = array('custom-login','signup');

        $this->load->view(
            'homepage.phtml', array('title' => 'Login Area', 
            'view' => 'login/barangay', 'errorMessage' => $errorMessage, 'retUrl' => $retUrl, 'customCss' => $customCss));
    }

	public function facebook($logout_url = null, $login_url = null, $user_profile = null)
    {
        # this controller 
        # set the session fb
        # if fb user is not yet in the database redirect to register/createByFacebook
        # if already registered redirect to right URL
        # if safe personal info is not yet set redirect to createAccountSecond
        # if safe personal info set but emergency contact informaion is not yet set redirect to createAccountThird
        # if both safe personal and emergency contact information is set redirect to dashboard
        # set the session in FB

        // error_reporting(E_ALL);
        $retUrl = '';
        $error = false;
        $errorMessage = '';
        $logout_url = '';
        $data = array();


        $this->form_validation->set_rules('retUrl','Return URL', 'trim|xss_clean');
        
        if ($this->form_validation->run() == TRUE)
        {
            $error = true;
            $errorMessage = 'Please check your input.';
        }
        else
        {
            $error = false;
        }

        $retUrl =  filter_var($this->input->post('retUrl'), FILTER_SANITIZE_STRING);

        # destroy the nambal session and facebook session
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('fb_profile');
        # end of destroying nambal session
    
        #-----------------------------------
        #--- all facebook stuffs here-------
        
        # check if nag login bah sa facebook
        # mao ni ibutang sa other pages 
        # para makita nato nga naka logged ba jud siya sa fb
        $user_profile = $this->facebook->get_user();
        $this->session->set_userdata('fb_profile', @$user_profile);
        if (empty($user_profile['id']))
        {
            $loginUrl = $this->facebook->login_url();
            redirect($loginUrl, 'refresh');
        } 

        $this->session->set_userdata('fb_profile', @$user_profile);
        // $this->session->set_userdata('logoutURL', urldecode($logout_url));

        $this->load->view(
            'homepage.phtml', array('title' => 'Facebook login_url', 
            'view' => 'login/facebook', 'logout_url' =>  $logout_url , 'login_url' => $login_url, 'user_profile' =>  $user_profile ));

        # check if the facebook user is already registered in nambal
        if ($this->fblogin->checkID($user_profile['id'])) {
            $IDsafe_user = $this->fblogin->getIDSafeUser($user_profile['id']);
            $IDsafe_personalInfo = $this->personalProfile->getIDSafePersonalInfo($IDsafe_user);
            $IDsafe_userInfo = $this->fblogin->getSafeUserInfo();
            $safePersonalInfoData = $this->fblogin->getPersonalInfoData();
            $safeAddressInfo = $this->fblogin->getSafeAddressInfo();
            $fullname = @$IDsafe_userInfo['lastname'].', '.@$IDsafe_userInfo['firstname'];           

            # set nambal session
            # --------------[ NEED ATTENTION ]-------------
            $data = array(
                'sessionName' => $this->nagkamoritsing->bungkag(@$IDsafe_userInfo['username']),
                'firstname' => $this->nagkamoritsing->bungkag(@$IDsafe_userInfo['firstname']),
                'middlename' => $this->nagkamoritsing->bungkag(@$IDsafe_userInfo['middlename']),
                'lastname' => $this->nagkamoritsing->bungkag(@$IDsafe_userInfo['lastname']),
                'default' => @$safePersonalInfoData->default,
                'sex' => @$safePersonalInfoData->sex,
                'dateofbirth' => @$safePersonalInfoData->dateofbirth,
                'bloodtype' => @$safePersonalInfoData->bloodtype,
                'IDsafe_personalInfo' =>  $this->nagkamoritsing->bungkag(@$IDsafe_personalInfo),
                'IDsafe_user' =>  $this->nagkamoritsing->bungkag(@$IDsafe_user),
                'fullname' =>  $this->nagkamoritsing->bungkag(@$fullname),
                'street' => @$safeAddressInfo->street,
                'city' => @$safeAddressInfo->city,
                'state' => @$safeAddressInfo->state,
                'postcode' => @$safeAddressInfo->postcode,
                'country' => @$safeAddressInfo->country,  
                'sessionAddress' => $this->ipAddress
            );
            $this->session->set_userdata('logged_in', $data);
            # end of setting nambal session
            # --------------[ NEED ATTENTION above ]-------------
            $this->sessionModel->setSession($data);

            $dataSession = array(
                'IDsafe_user' => $IDsafe_user,
                'activity' => 'LOG in using facebook',
                'activityDescription' => 'LOG in using facebook',
                'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
            );
            $this->safeuserlog->logUser($dataSession);

            if($this->personalProfile->checkComplete($IDsafe_user)){
                if($this->addresses->checkComplete($IDsafe_personalInfo))
                {
                    if($this->contactinfo->checkComplete($IDsafe_personalInfo)){
                        # if both safe personal and emergency contact information is set redirect to dashboard
                       redirect(base_url().'dashboard','refresh');
                    } else {
                        # if safe personal info set but emergency contact informaion is not yet set redirect to createAccountThird
                        redirect(base_url().'register/createAccountThird','refresh');
                    }
                } else {
                    # if the user has not yet set default address
                    redirect(base_url().'register/createAccountSecond','refresh');
                }
            } else {
                # if safe personal info is not yet complete redirect to createAccountSecond
                redirect(base_url().'register/createAccountSecond','refresh');
            }            
        } else {
            # if fb user is not yet in the database redirect to register/createByFacebook
            // echo 'id: '.$user_profile['id'].'<br>';
            // print_r($this->fblogin->checkID($user_profile['id']));
            redirect(base_url().'register/createByFacebook','refresh');
        }
      
        # check if the fb is connected to this nambal session
            # if not set the fb session to the right nambal session
        # end of checking if the fb is connected to this nambal session

        
	}

    public function auto()
    {
        # --------------[ NEED ATTENTION ]-------------
        # need to monitor wrong attempt to prevent spam
        # use session and same user attempt
        # put in the database number of wrong attempt in specific user.

        $retUrl = '';
        $email = '';
        $password = '';
        $error = false;

        $_POST = $_GET;
        $this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required');
        $this->form_validation->set_rules('retUrl', 'Return URL', 'trim|xss_clean|required');

        
        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
        }

        $email = filter_var($this->input->post('email'), FILTER_SANITIZE_STRING);
        $password = filter_var($this->input->post('password'), FILTER_SANITIZE_STRING);
        $retUrl = filter_var($this->input->post('retUrl'), FILTER_SANITIZE_STRING);

        if ($error != true)
        {
            # login
            $secret_key = config_item('secret_key_nambal');
            $dataLogin = array(
                'username' => $email,
                'password' => $password
            );

            $login = $this->login->login($dataLogin, $secret_key);
            $loginInformation = $this->login->getData();
            $safePersonalInfo = $this->login->getPersonalInfoData();
            $safeAddressInfo = $this->login->getSafeAddressData();
            $fullname = $this->nagkamoritsing->ibalik($safePersonalInfo->fullname);

           if ($login)
            {
                $data = array(
                    'sessionName' => $this->nagkamoritsing->bungkag($email),
                    'firstname' => $this->nagkamoritsing->bungkag(@$loginInformation->firstname),
                    'middlename' =>  $this->nagkamoritsing->bungkag(@$loginInformation->middlename),
                    'lastname' =>  $this->nagkamoritsing->bungkag(@$loginInformation->lastname),
                    'fullname' =>  $this->nagkamoritsing->bungkag(@$fullname),
                    'default' => @$safePersonalInfo->default,
                    'sex' => @$safePersonalInfo->sex,
                    'dateofbirth' => @$safePersonalInfo->dateofbirth,
                    'bloodtype' => @$safePersonalInfo->bloodtype,
                    'IDsafe_personalInfo' => $this->nagkamoritsing->bungkag(@$safePersonalInfo->IDsafe_personalInfo),
                    'IDsafe_user' => $this->nagkamoritsing->bungkag(@$loginInformation->IDsafe_user),
                    'street' => @$safeAddressInfo->street,
                    'city' => @$safeAddressInfo->city,
                    'state' => @$safeAddressInfo->state,
                    'postcode' => @$safeAddressInfo->postcode,
                    'country' => @$safeAddressInfo->country
                );
                $this->session->unset_userdata('logged_in');
                $this->session->unset_userdata('fb_profile');

                $this->session->set_userdata('logged_in', $data);
            }
            # end of logging in
            
            # it should go to the last redirection
            redirect(base_url().$retUrl, 'refresh');
           
        }
        else
        {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function checkLogin()
    {
        $data = array('IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
        $contactinformation = $this->contactinfo->getContact($data, NULL );

        echo '<h1>version of codeigniter</h1>'.CI_VERSION;
        print_r($contactinformation);
        echo '<br><br>';
        $session_data = $this->session->userdata('logged_in');
        echo 'session data: '.$session_data['sessionName'];
        echo '<br>';
        print_r ($session_data);
        echo '<br>';
        echo '<br>';
        $session_data2 = $this->session->userdata('user_profile');
        echo 'session data2: '.$session_data2['username'];
        echo '<br>';
        print_r ($session_data2);
        echo '<br>';
        echo '<br>';
        $session_data3 = $this->session->userdata('fb_profile');
        echo 'session data3: '.$session_data2['username'];
        echo '<br>';
        print_r ($session_data3);

        $session_data4 = $this->session->userdata('barangay_login');
        print_r ($session_data4);


        $addresssession = $this->addresses->getPersonalInfoAddress($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
        $emailsession = $this->safeuseremail->getuseremaildefault($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $doctorPhone = $this->emrUser->getDefaultNumber(@$doctorInformation->IDemr_user);
        echo '<br><br>Contact information:<br>';
        print_r($emailsession);
        echo '<br><br>';
        print_r($addresssession);
        echo '<br><br>Doctor number:<br>';
        print_r($doctorPhone[0]);
     
        echo '<br><br>';
        echo '<br>';
        echo 'Logout URL: '.$this->logoutURL;
                
        echo '<br><br>';
        echo json_encode(@$array);


        $session_data3 = $_SESSION['urldirect'];//$this->session->userdata('urldirect');;
        echo '<br><br>';
        echo '<br>';
        echo json_encode(@$session_data3);
        $this->output->enable_profiler(TRUE);
    }

	public function twitter()
    {
         $tw_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
        $this->load->library('twitteroauth',$tw_config);  

        // The TwitterOAuth instance
        $twitteroauth = new TwitterOAuth(config_item('twitter_consumer_key'), config_item('twitter_consumer_secret'));
        // Requesting authentication tokens, the parameter is the URL we will be redirected to
        $request_token = $twitteroauth->getRequestToken('http://localhost/twitter-test/twitteroauth/twitteroauth.php');

        // Saving them into the session
       // $_SESSION['oauth_token'] = $request_token['oauth_token'];
        //$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

        // If everything goes well..
        if($twitteroauth->http_code==200){
            // Let's generate the URL and redirect
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            header('Location: '. $url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened.');
        }
	}

    public function successFacebook()
    {
        if ($this->session->userdata('is_fb_logged')){
            if ($this->facebook->getUser()){
                try
                {
                    $me = $this->facebook->api('/me');
                }
                catch(FacebookApiException $e){
                    $this->facebook->destroySession();
                }
                echo $this->session->userdata('username');
                echo '<br/>';
                echo $this->session->userdata('id');
                echo $this->facebook->getUser();
            }
            else{
                echo 'wala na login facebook';
            }
        }
        
    }

    public function successTwitter()
    {

    }
    public function sessionTest()
    {
        $newArray = array( 
            'fullname' => '0910162013090947485667204748566720474856672',
            'street' => '7455 France Avenue South Suite #416',
            'city' => 'Edina',
            'state' => 'Edina', 
            'postcode' => '55435', 'country' => 'Philippines',
            'dateofbirth' => '1993-03-09',
            'sex' => 'Female',
            'bloodtype' => 'DKY',
            'IDsafe_personalInfo' => '091016201309090343', 
            'IDsafe_user' => $this->nagkamoritsing->bungkag(4), // 4 unta ni
            'sessionName' => '0910162013090904e06f05802e0470480560470400330470480560473',
            'firstname' => '091016201309090000470000480000560000476',
            'middlename' => '09101620130909474856472',
            'lastname' => '09101620130909000470004800056000475',
            'default' => TRUE,
            'sessionAddress' => $this->nagkamoritsing->bungkag('127.0.0.1')
            );
        $this->session->unset_userdata('logged_in');
        $this->session->set_userdata('logged_in', $newArray);
    }

    public function validateCaptcha($value) 
    {
        if ($this->recaptcha->check_answer($_SERVER['REMOTE_ADDR'],$this->input->post('recaptcha_challenge_field'),$value)) 
        {
            return TRUE;
        } else 
        {
            $this->form_validation->set_message(__FUNCTION__, lang('recaptcha_incorrect_response'));
            return FALSE;
        }
    }

    public function justest()
    {
        $loginUrl = $this->facebook->login_url();
        // // print_r($loginUrl);
        // // echo '<br><br>TEST';
        $get_user = $this->facebook->get_user();
        if (!empty($get_user['email']))
        {
            // print_r($get_user['email']);
            print_r($get_user);
        }
        else {
             echo '<a href="' . $loginUrl. '">Login with Facebook</a>';
        }

        // redirect($loginUrl, 'refresh');
        // http://localhost:4545/login
        //?code=AQCVeHkdHItjgjpRDf7-h5zyVdRntsxZSBr8PCaDr1XvcgRTCiqxs5ydv3PcdnWXFvQJprcfUS7zsAUINd-97j-Ax-Fib8DcqEOlkU33HpTOJe9vyDHMK2aZ22_5FnZ7xNgjpvUGA6tWEdZO6eial_otOQWOt23Td5q4ee74_0sGp5FsT3L95k7mzZtCko7W75614SkmDIa8fIDjFsWhvjGOkJpvIlDFeas5VKUDVZ4spmOEWbPApHmmCAKRr661vyTpgC4FU2p_G0iO40qVraXbC5gFr4bMvAjC_Z5nwlfZtkf0C092IdPWoJgyuEp19GPG_M3rXUgkYCbN3XPUO_oe
        // &state=9c35e089ce9c1d2644aa0251644c5666#_=_

        // $helper = new FacebookRedirectLoginHelper($redirect_url, $appId = NULL, $appSecret = NULL);
       

    }

}

?>