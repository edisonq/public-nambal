<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Privacy extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml',array('title' => 'Privacy',
				'view' => 'privacy/index'));
		return;
	}

	public function edit(){
		// /profile/update?profileID=2

		$this->load->view(
			'homepage.phtml', array('title' => 'Update privacy settings',
				'view' => 'privacy/edit'));

		$profileID = $this->input->get('profileID');
		echo 'profileID:'.$profileID;

		return;
	}
}

?>