<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ProfileViewer extends CI_Controller 
{
	// private $nambal_session = array();  # ngano gi butang man ni nimo?
	private $facebook_session = array();
	private $logoutURL = array();
	private $fb_config = array();

	public function __construct() 
	{
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->redirector = $this->session->userdata('urldirect');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('ContactInformation_model', 'contactinformation');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->model('Error_model', 'errordisplay');
        $this->load->model('Personalprofile_model', 'personalinfo');
        $this->load->model('Addresses_model','addressinfo');
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->model('Patientsearch_model', 'patientSearch');
        $this->load->helper('s3');
        $this->load->helper('text');
		
        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        // if (empty($this->nambal_session['sessionName']))
        // {
        // 	redirect(base_url().'login', 'refresh');
        // }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
        # get the profile picture
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

	public function index()
	{
        $this->load->model('Emruser_model', 'emrusermodel');
        $profileID = filter_var($this->input->post('profileID'), FILTER_SANITIZE_STRING);
        $arrayGetContact = array();
        $customCss = array('custom-dashboard','zocial', 'custom-profile-viewer');
        $customJs = array('jquery.flot.min','jquery.flot.pie','jquery.flot.resize.min', 'jquery.stateselect', 'jquery.selectboxes.pack', 'custom-connect-doctor-search-short');
        $arrayGetDocInfo = $this->emrusermodel->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        $viewoutput = filter_var($this->input->get('view'), FILTER_SANITIZE_STRING);

        $this->datafromurl['SN'] = filter_var(@$this->input->get('SN'), FILTER_SANITIZE_STRING);  // email or username
        $this->datafromurl['IDSU'] = filter_var(@$this->input->get('IDSU'), FILTER_SANITIZE_STRING); // IDsafe_user
        $this->datafromurl['IDSP'] = filter_var(@$this->input->get('IDSP'), FILTER_SANITIZE_STRING); // IDsafe_personalInfo 
        $this->datafromurl['FIL'] = filter_var(@$this->input->get('FIL'), FILTER_SANITIZE_STRING); // filename of QR in the system

        # mo check sa session if doctor ba jud
        # if doctor redirect to profileviewer ex: profileViewer/doctorViewing?sessionid=3826162015011632342
        if ($this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'profileViewer/doctorViewing?sessionid='.$this->datafromurl['IDSU'], 'refresh');
        }

        # else show the patient information base sa privacy settings
        # pero for now hatagan lang sa og instructions nga ingnan nga kailangan "Medical professional" ka para maka view ka 
        # tell medical professional to register at nambal
        # naka check if naka login
        # redirect to registerDoctor?errorNum=2001urlredirect=
        $_SESSION['urldirect'] = urlencode(base_url().'profileViewer/doctorViewing?sessionid='.$this->datafromurl['IDSU']); 

        if ($this->nambal_session['IDsafe_personalInfo'])
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        else {
            # if kung wala login redirect to login?urlredirect=
            redirect(base_url().'login', 'refresh');
        }


        
        # this is for self viewing
        $dataRetrieveGetContact = array(
            'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
        );

        $arrayGetContact = $this->contactinformation->getAllContact($dataRetrieveGetContact);

        if (!empty($profileID))
        {
            $dataRetrieveGetContact = array(
                'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
            );

            $arrayGetContact = $this->contactinformation->getAllContact($dataRetrieveGetContact);
        }
        # end of self viewing




		// $this->load->view(
		// 	'patient-dashboard-light-blue.phtml', array('title' => 'My Personal Profile', 
		// 	'view' => 'profile-viewer/index', 
  //           'customCss' => $customCss,
  //           'customJs' => $customJs,
  //           'viewoutput' => $viewoutput,
  //           'arrayGetDocInfo' => $arrayGetDocInfo,
  //           'arrayGetContact' => $arrayGetContact,
		// 	'css' => 'dashboard-custom'));
        $this->load->view(
            'homepage2014.phtml', array(
                'title' => 'Profile viewer', 
                'pageKeywords' => 'version 1, version 2, version 3, impressive consultants, thanks',
                'pageDescription' => 'Thanking GOD, families, friends and these amazing people.',
                'view' => 'profile-viewer/under-construction'));
		return;
	}

    public function doctorViewing()
    {
        $this->registrationInfo['redirectdownloadsessionid'] = ucwords(filter_var(@$this->input->get('redirectdownloadsessionid'), FILTER_SANITIZE_STRING));
        
        $this->activeMainMenu = "Patients";
        $customCss = array();
        $customJs = array();
        $urlAdd = '';
        $navOpen = 'Patient records';
        $this->breadcrumb[] = array(
            'link' => 'profileViewer/doctorViewing',
            'linkname' => 'Nambal&prime;s Safe'
        );

        $this->checksession();
        $this->checkdoctor();
        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);

        if (empty($this->sessionid))
        {
            redirect(base_url().'profileViewer/search', 'refresh');
        }


        $this->patientInformation = $this->safeuser->getSafeUserInfo($this->sessionid);
        $this->personalinformation = $this->personalinfo->getPersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));
        if (!empty($this->personalinformation))
            $this->personalinformationAddress = $this->addressinfo->getPersonalInfoAddress($this->personalinformation->IDsafe_personalInfo);

        $this->profilepicPatient = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->sessionid)); 


        if (empty($this->patientInformation))
        {
            redirect(base_url().'profileViewer/search?errornum=1000', 'refresh');
        }

        
        
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Nambal safe - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'profile-viewer/index-doctor'
        ));
        return;
    }
	
	# search patient
    public function search()
    {
		$baseLink = base_url().lcfirst(get_class($this)).'/';
        $navOpen = 'Patient records';
        $defaultDatas = $this->defaultDatas();
        $this->errorMessage = $urlAdd = $onlyKeyword = $moreoptions = $firstname = $middlename = $lastname = $bloodtype = $gender = $dateofbirth = $emailaddress = $country = $state = $city = $postal = $streetaddress = $searchmMyPatientOnly = '';
		$withOptions = $searchedPatientsIDs = $getSearchedPatientsInformation = $data = $dataError = $searchedPatients = array();
		$error = $changeUI = $check_dateofbirth_numeric = $validateEmail = $validateGender = $validateBloodtype = false;
		$totalPage = 0;
		$totalSearchCount = 0;				# count total search result
		$resultsPerPage = 10; 				# number of results per page
		$limit = $resultsPerPage; 			# default limit value
		$offset = 0;						# default offset value
		$page = 1;							# default page number
		$outOfTotalSearchCount = 0;			# count of search result while on page
		$maxPaginationCount = 5;			# default max pagination count to be shown on page
    	$searchAgeLimit = 110; 				# default age value of patients to be considered for search, regarding the year limit 
        $customJs = array();
        $customJsTop = array('countriesV2');
        $customCss = array('custom-search-patient-doctor');
		$this->activeMainMenu = "Patients";
		$this->breadcrumb[] = array(
            'link' => 'profileViewer/doctorViewing',
            'linkname' => 'Nambal&prime;s Safe'
        );
        
		
        $this->checksession();
        $this->checkdoctor();

        # mag sugod ta diri para text search og dili QR
		if ($_GET)
		{
			$errornum 				= filter_var($this->input->get('errornum'), FILTER_SANITIZE_STRING);
			$moreoptions 			= filter_var($this->input->get('moreoptions'), FILTER_SANITIZE_STRING);
			$onlyKeyword 			= filter_var($this->input->get('search-patient-keyword'), FILTER_SANITIZE_STRING);
			$firstname 				= filter_var($this->input->get('firstname'), FILTER_SANITIZE_STRING);
			$middlename 			= filter_var($this->input->get('middlename'), FILTER_SANITIZE_STRING);
			$lastname 				= filter_var($this->input->get('lastname'), FILTER_SANITIZE_STRING);
			$dateofbirth 			= filter_var($this->input->get('dateofbirth'), FILTER_SANITIZE_STRING);
			$emailaddress 			= filter_var($this->input->get('emailaddress'), FILTER_SANITIZE_STRING);
			$gender 				= filter_var($this->input->get('gender'), FILTER_SANITIZE_STRING);
			$bloodtype 				= filter_var($this->input->get('bloodtype'), FILTER_SANITIZE_STRING);
			$country 				= filter_var($this->input->get('country'), FILTER_SANITIZE_STRING);
			$state 					= filter_var($this->input->get('state'), FILTER_SANITIZE_STRING);
			$city 					= filter_var($this->input->get('city'), FILTER_SANITIZE_STRING);
			$postal 				= filter_var($this->input->get('postal'), FILTER_SANITIZE_STRING);
			$streetaddress 			= filter_var($this->input->get('streetaddress'), FILTER_SANITIZE_STRING);
			$searchmMyPatientOnly 	= filter_var($this->input->get('search-my-patient-only'), FILTER_SANITIZE_STRING);

			# check for page redirection
			$errornum = preg_replace('#[^a-z0-9_]#', '', $errornum);
	        if (!empty($errornum))
	        {
	            $errorD = $this->errordisplay->identifyError($errornum);
	            $this->errorMessage .= $errorD['userdisplaytext'];
	        }

			# since page's default value is not set as a $_get data
			# set page data as page $_GET value if exist in $_GET
			if (filter_var($this->input->get('page'), FILTER_SANITIZE_STRING) && array_key_exists('page', $_GET))
			{
				$page = filter_var($this->input->get('page'), FILTER_SANITIZE_STRING);

				# check if page is a numeric character
				if (!is_numeric($page))
				{
					if ($moreoptions && array_key_exists('moreoptions', $_GET) && !array_key_exists('search-patient-keyword', $_GET))
					{
						redirect($baseLink.'search?moreoptions=1&firstname='.urlencode($firstname).'&middlename='.urlencode($middlename).'&lastname='.urlencode($lastname).'&dateofbirth='.urlencode($dateofbirth).'&emailaddress='.urlencode($emailaddress).'&gender='.urlencode($gender).'&bloodtype='.urlencode($bloodtype).'&country='.urlencode($country).'&state='.urlencode($state).'&city='.urlencode($city).'&postal='.urlencode($postal).'&streetaddress='.urlencode($streetaddress).'&search-my-patient-only='.urlencode($searchmMyPatientOnly), 'refresh');
					}
					else if ($onlyKeyword && array_key_exists('search-patient-keyword', $_GET) && !array_key_exists('moreoptions', $_GET))
					{
						redirect($baseLink.'search?search-patient-keyword='.urlencode($onlyKeyword), 'refresh');
					}
				}
			}

			
			if($page > 1)
			{
				$limit = $page * $resultsPerPage;
				$offset = $limit - $resultsPerPage;
			}

			$outOfTotalSearchCount = $resultsPerPage * ($page - 1);

			#------------------------------ more options -------------------------------------

				if ($moreoptions && array_key_exists('moreoptions', $_GET) && !array_key_exists('search-patient-keyword', $_GET))
				{
					# check if moreoptions is true
					if (is_numeric($moreoptions) &&  $moreoptions == 1)
					{	
						# check firstname
						if (!empty($firstname))
						{
							$withOptions['firstname'] = $firstname;
						}

						# check middlename
						if (!empty($middlename))
						{
							$withOptions['middlename'] = $middlename;
						}
						
						# check lastname
						if (!empty($lastname))
						{
							$withOptions['lastname'] = $lastname;
						}
						
						# check date of birth
				        if ($dateofbirth && array_key_exists('dateofbirth', $_GET))
				        {
				        	$dob = explode('/', $dateofbirth);
				        	if (sizeof($dob) == 3)
				        	{
				        		foreach ($dob as $dobKey => $dobValue )
								{
									if (!is_numeric($dobValue))
									{
										break;
									}
									else
									{
										$check_dateofbirth_numeric = true;
									}
								}

								# validate date of birth
								if ($check_dateofbirth_numeric)
								{
									if ($this->checkDateofbirth_segment($dob, $searchAgeLimit))
									{
										$withOptions['dateofbirth'] = $dateofbirth = $dateofbirth;
									}
								}
				        	}
				        }

						# check email
						if (filter_var($emailaddress, FILTER_SANITIZE_EMAIL))
						{
							$withOptions['emailaddress'] = $emailaddress = $emailaddress;
						}

				        # check gender
				        foreach ($defaultDatas['genders'] as $g)
				        {
				        	if(strcmp(strtolower($g), strtolower($gender)) == 0)
				        	{
				        		$withOptions['gender'] = $gender = $g;
				        		$validateGender = true;
				        	}
				        }

				        # check bloodtype
				        foreach ($defaultDatas['bloodtypes'] as $bt)
				        {
				        	if(strcmp(strtolower($bt), strtolower($bloodtype)) == 0)
				        	{
				        		$withOptions['bloodtype'] = $bloodtype = $bt;
				        		$validateBloodtype = true;
				        	}
				        }

				        # check country and state
				        foreach ($defaultDatas['countries'] as $cKey => $cValue)
				        {
				        	$countries[$cKey] = strtolower($cValue);
				        	if (strcmp(strtolower($country), strtolower($cValue)) == 0)
				        	{	
				        		$withOptions['country'] = $country = $cValue;

				       			# get selected state
						        foreach (explode('|', $defaultDatas['states'][$cKey]) as $scs)
						        {
						        	if(strcmp(strtolower($state), strtolower($scs)) == 0)
						        	{
						        		$withOptions['state'] = $state = $scs;
						        		break;
						        	}
						        }

				       			break;
				        	}
				        }

				        # check city
						if (!empty($city))
						{
							$withOptions['city'] = $city;
						}
						
						# check postal
						if (!empty($postal))
						{
							$withOptions['postal'] = $postal;
						}
						
						# check streetaddress
						if (!empty($streetaddress))
						{
							$withOptions['streetaddress'] = $streetaddress;
						}
						


						# start search
						if(!empty($withOptions))
						{
					        $searchedPatientsIDs = $this->searchPatient($onlyKeyword, $withOptions, $searchmMyPatientOnly);
							$totalSearchCount = sizeof($searchedPatientsIDs);

							$getSearchedPatientsInformation = $this->getSearchedPatientsInformation($searchedPatientsIDs, $offset, $limit, $outOfTotalSearchCount);
							$searchedPatients 		= $getSearchedPatientsInformation['searchedPatients'];
							$outOfTotalSearchCount	= $getSearchedPatientsInformation['outOfTotalSearchCount'];

							# if page count is greater than the total page count regarding the search result
							$totalPage = ceil($totalSearchCount  / $resultsPerPage);
							if ($page > 1 && $page > $totalPage)
							{
								redirect($baseLink.'search?moreoptions=1&page='.$totalPage.'&firstname='.urlencode($firstname).'&middlename='.urlencode($middlename).'&lastname='.urlencode($lastname).'&dateofbirth='.urlencode($dateofbirth).'&emailaddress='.urlencode($emailaddress).'&gender='.urlencode($gender).'&bloodtype='.urlencode($bloodtype).'&country='.urlencode($country).'&state='.urlencode($state).'&city='.urlencode($city).'&postal='.urlencode($postal).'&streetaddress='.urlencode($streetaddress).'&search-my-patient-only='.urlencode($searchmMyPatientOnly), 'refresh');
							}
							else if ($page < 1)
							{
								redirect($baseLink.'search?moreoptions=1&firstname='.urlencode($firstname).'&middlename='.urlencode($middlename).'&lastname='.urlencode($lastname).'&dateofbirth='.urlencode($dateofbirth).'&emailaddress='.urlencode($emailaddress).'&gender='.urlencode($gender).'&bloodtype='.urlencode($bloodtype).'&country='.urlencode($country).'&state='.urlencode($state).'&city='.urlencode($city).'&postal='.urlencode($postal).'&streetaddress='.urlencode($streetaddress).'&search-my-patient-only='.urlencode($searchmMyPatientOnly), 'refresh');
							}
							
							# change form postion
							$changeUI = true;
						}
					}
					else
					{
						redirect($baseLink.'search?moreoptions=1', 'refresh');
					}
				}

			#--------------------- end more options ----- use only keywords -------------------

				else if ($onlyKeyword && array_key_exists('search-patient-keyword', $_GET) && !array_key_exists('moreoptions', $_GET))
				{

					# select patient that have the keyword
					$searchedPatientsIDs = $this->searchPatient($onlyKeyword);
					$totalSearchCount = sizeof($searchedPatientsIDs);
					
					$getSearchedPatientsInformation = $this->getSearchedPatientsInformation($searchedPatientsIDs, $offset, $limit, $outOfTotalSearchCount);
					$searchedPatients 		= $getSearchedPatientsInformation['searchedPatients'];
					$outOfTotalSearchCount	= $getSearchedPatientsInformation['outOfTotalSearchCount'];

					# if page count is greater than the total page count regarding the search result
					$totalPage = ceil($totalSearchCount  / $resultsPerPage);
					if ($page > 1 && $page > $totalPage)
					{
						redirect($baseLink.'search?page='.$totalPage.'&search-patient-keyword='.urlencode($onlyKeyword), 'refresh');
					}
					else if ($page < 1)
					{
						redirect($baseLink.'search?search-patient-keyword='.urlencode($onlyKeyword), 'refresh');
					}
					
					# change form postiion
					$changeUI = true;
				}

			#--------------------- end use only keywords --------------------------
				else
				{
					redirect($baseLink.'search', 'refresh');
				}

		}
        # end of text search og dili QR

        #---> gi tuyo sa frontend i generate ang pagination for future migration to ajax or unsa pana nga frontend strategies and to lessen backend load 

         $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Nambal safe - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJsTop' => $customJsTop,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
			'data' => array(
				'moreoptions' 			=> $moreoptions,
				'onlyKeyword'			=> $onlyKeyword,
				'firstname'				=> $firstname,
				'middlename'			=> $middlename,
				'lastname'				=> $lastname,
				'dateofbirth' 			=> $dateofbirth,
				'emailaddress' 			=> $emailaddress,
				'bloodtype'				=> $bloodtype,
				'validateBloodtype'		=> $validateBloodtype,
				'bloodtypes'			=> $defaultDatas['bloodtypes'],
				'gender'				=> $gender,
				'validateGender'		=> $validateGender,
				'genders'				=> $defaultDatas['genders'],
				'country'				=> $country,
				'state'					=> $state,
				'city'					=> $city,
				'postal'				=> $postal,
				'streetaddress'			=> $streetaddress,
				'searchmMyPatientOnly'	=> $searchmMyPatientOnly,
				'baseLink'				=> $baseLink,
				'changeUI' 				=> $changeUI,
				'searchedPatients'		=> $searchedPatients, 
				'outOfTotalSearchCount' => $outOfTotalSearchCount,
				'totalSearchCount'		=> $totalSearchCount,
				'resultsPerPage'		=> $resultsPerPage,
				'currentPage'			=> $page,
				'maxPaginationCount'	=> $maxPaginationCount
			),
            'view' => 'profile-viewer/search-patient-doctor-dashboard'
        ));
		// $this->output->enable_profiler(TRUE);
    }

    #----> check date of birth segments
    private function checkDateofbirth_segment($dateofbirth = array(), $ageLimit = 0)
    {
    	$valid_dateofbith = false;
    	$month = $dateofbirth[0];
    	$day = $dateofbirth[1];
    	$year = $dateofbirth[2];

    	# check year
    	if (($year >= (date('Y') - $ageLimit)) && $year <= date('Y'))
    	{
    		# check month 
    		# February
    		# check days for leap and non leap year
    		if (((($month >= 1 && $month <= 12) && $month == 2) && ($year % 4 == 0 && ($day >= 1 && $day <= 29))) ||
    		   ((($month >= 1 && $month <= 12) && $month == 2) && ($year % 4 != 0 && ($day >= 1 && $day <= 28))))
    		{
    			$valid_dateofbith = true;
    		}
    		
    		# not February but is before August
    		else if (((($month >= 1 && $month <= 7) && $month != 2) && ($month % 2 == 0 && ($day >= 1 && $day <= 30))) ||
    				((($month >= 1 && $month <= 7) && $month != 2) && ($month % 2 != 0 && ($day >= 1 && $day <= 31))))
    		{
    			$valid_dateofbith = true;
    		}

    		# August to December
    		else if (((($month >= 8 && $month <= 12)) && ($month % 2 != 0 && ($day >= 1 && $day <= 30))) ||
    				((($month >= 8 && $month <= 12)) && ($month % 2 == 0 && ($day >= 1 && $day <= 31))))
    		{
    			$valid_dateofbith = true;
    		}
    	}

    	return $valid_dateofbith;
    }
	 
	#-----> search safe user and make ranking for search keywords though there will be trash results due to the occurence of some encrypted data in some encrypted data from the database which is used for matching
	private function searchPatient($onlyKeyword = '', $withOptions = array(), $searchmMyPatientOnly = '')
	{
		$encryptedKeyword = array();
		$searchPatient_safeuserIDs = $searchFirstname = $searchMiddlename = $searchLastname = $searchSex = $searchDateOfBirth = $searchBloodType = $searchEmails = $searchStreet = $searchState = $searchCity = $searchCountry = $searchPostcode = $mergedArray = $dummyArray = $newArray = $finalArray = array();
		$counter = $value = $value2 = $inArrayCounter = $inArrayCounter2 = 0;
		$checkValues = false;
		
		if (!empty($onlyKeyword) && empty($withOptions))
		{
			$encryptedKeyword =  $this->encryptKeyword($onlyKeyword);

			# search instances ni occur ang keyword sa kada field for ranking sa search purposses
			# search safeuser
				$searchFirstname	= $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'firstname');
				$searchMiddlename	= $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'middlename');
				$searchLastname		= $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'lastname');
			
			# search emails
				$searchEmails		= $this->patientSearch->searchPatient_useremails($encryptedKeyword, 'emailAddress');
			
			# search personalinfo
				$searchSex			= $this->patientSearch->searchPatient_personalinfo($onlyKeyword, 'sex');
				$searchBloodType	= $this->patientSearch->searchPatient_personalinfo($onlyKeyword, 'bloodtype');
			
			# search personalinfo dateofbirth
				$searchDateOfBirth	= $this->patientSearch->searchPatient_personalinfo_dateofbirth($onlyKeyword);
			
			# search adresses
				$searchStreet		= $this->patientSearch->searchPatient_addresses($onlyKeyword, 'street');
				$searchState		= $this->patientSearch->searchPatient_addresses($onlyKeyword, 'state');
				$searchCity			= $this->patientSearch->searchPatient_addresses($onlyKeyword, 'city');
				$searchCountry		= $this->patientSearch->searchPatient_addresses($onlyKeyword, 'country');
				$searchPostcode		= $this->patientSearch->searchPatient_addresses($onlyKeyword, 'postcode');
		}
		else if (empty($onlyKeyword) && !empty($withOptions))
		{
			# segregate each search for these keywords in "if" statements to avoid accessing database and querying unset keywords and to lessen server traffic
			# search instances ni occur and keyword sa kada field for ranking sa search purposses
			# search safeuser
				if (array_key_exists('firstname', $withOptions))
				{
					$encryptedKeyword =  $this->encryptKeyword($withOptions['firstname']);
					$searchFirstname = $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'firstname', $searchmMyPatientOnly);
				}
				
				if (array_key_exists('middlename', $withOptions))
				{
					$encryptedKeyword =  $this->encryptKeyword($withOptions['middlename']);
					$searchMiddlename = $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'middlename', $searchmMyPatientOnly);
				}

				if (array_key_exists('lastname', $withOptions))
				{
					$encryptedKeyword =  $this->encryptKeyword($withOptions['lastname']);
					$searchLastname = $this->patientSearch->searchPatient_safeuser($encryptedKeyword, 'lastname', $searchmMyPatientOnly);
				}

			# search emails
				if (array_key_exists('emailaddress', $withOptions))
				{
					$encryptedKeyword =  $this->encryptKeyword($withOptions['emailaddress']);
					$searchEmails = $this->patientSearch->searchPatient_useremails($encryptedKeyword, 'emailAddress', $searchmMyPatientOnly);
				}

			# search personalinfo
				if (array_key_exists('gender', $withOptions))
				{
					$searchSex = $this->patientSearch->searchPatient_personalinfo($withOptions['gender'], 'sex', $searchmMyPatientOnly);
				}

				if (array_key_exists('bloodtype', $withOptions))
				{
					$searchBloodType = $this->patientSearch->searchPatient_personalinfo($withOptions['bloodtype'], 'bloodtype', $searchmMyPatientOnly);
				}

			# search personalinfo dateofbirth
				if (array_key_exists('dateofbirth', $withOptions))
				{
					$searchDateOfBirth	= $this->patientSearch->searchPatient_personalinfo_dateofbirth($withOptions['dateofbirth'], $searchmMyPatientOnly);
				}
			
			# search adresses
				if (array_key_exists('streetaddress', $withOptions))
				{
					$searchStreet = $this->patientSearch->searchPatient_addresses($withOptions['streetaddress'], 'street', $searchmMyPatientOnly);
				}

				if (array_key_exists('state', $withOptions))
				{
					$searchState = $this->patientSearch->searchPatient_addresses($withOptions['state'], 'state', $searchmMyPatientOnly);
				}

				if (array_key_exists('city', $withOptions))
				{
					$searchCity	= $this->patientSearch->searchPatient_addresses($withOptions['city'], 'city', $searchmMyPatientOnly);
				}

				if (array_key_exists('country', $withOptions))
				{
					$searchCountry = $this->patientSearch->searchPatient_addresses($withOptions['country'], 'country', $searchmMyPatientOnly);
				}

				if (array_key_exists('postal', $withOptions))
				{
					$searchPostcode	= $this->patientSearch->searchPatient_addresses($withOptions['postal'], 'postcode', $searchmMyPatientOnly);
				}
		}
		
		$mergedArray = array_merge($searchFirstname, $searchMiddlename, $searchLastname, $searchSex, $searchDateOfBirth, $searchBloodType, $searchEmails, $searchStreet, $searchState, $searchCity, $searchCountry, $searchPostcode);
		
		sort($mergedArray);
		
		# count occurence of keyword in an user id (used as array index) 
		foreach($mergedArray as $mk => $mv)
		{
			if($mk > 0)
			{
				$value = $mergedArray[$mk - 1];
			}
			else
			{
				$value = $mergedArray[0];
			}	
			
			if($mv == $value)
			{
				$counter++;
			}
			else
			{
				$counter = 1;
			}
			
			$searchPatient_safeuserIDs[$mv] = $counter;
		} 
		
		# reverse sort (biggest to smallest) the values of searchPatient_safeuserIDs (count occurence of keyword in an user id)
		arsort($searchPatient_safeuserIDs);
		
		# arrange searchPatient_safeuserIDs array in accordance sa value (biggest to smallest) since ang arsort() function kay gub.on ang sequence sa indexing from smallest to biggest though gi arrange ang value from biggest to smallest
		
			# store searchPatient_safeuserIDs array keys and values for matching
			foreach($searchPatient_safeuserIDs as $sk => $sv)
			{
				$dummyArray[$inArrayCounter] = [$sk, $sv];
				$inArrayCounter++;
			}
			
			# use dummyArray values (searchPatient_safeuserIDs array keys and values) to arrange ids with the same count of keyword occurence form smallest to biggest
			foreach($dummyArray as $dk => $dv)
			{
				if($dk > 0)
				{
					$value2 = $dummyArray[$dk - 1][1];
					if($dummyArray[$dk][1] == $value2)
					{
						$newArray[$inArrayCounter2] =  $dummyArray[$dk][0];
						$inArrayCounter2++;
					}
					else
					{
						sort($newArray);
						foreach($newArray as $na)
						{
							array_push($finalArray, $na);
						}
						unset($newArray);
						$inArrayCounter2 = 0;
						$newArray[$inArrayCounter2] =  $dummyArray[$dk][0];
					}
				}
				else
				{
					$newArray[$inArrayCounter2] =  $dummyArray[$dk][0];
					$inArrayCounter2++;
				}
				
				# if end of array
				if($dk == sizeof($dummyArray) - 1)
				{
					sort($newArray);
					foreach($newArray as $na)
					{
						array_push($finalArray, $na);
					}
					unset($newArray);
				}
				
			}

		return $finalArray;
		
	}
	
	#-----> convert keyword into encrypted datas
	private function encryptKeyword($keyword = '')
	{
		$explodedKeyword = $newArray2 = $encryptedKeyword = array();
		$i = 0;
		
		# original words
		$explodedKeyword = explode(' ',$keyword);
		
		# create new set of word transformation for there are different encryption datas produce in every different word case formation
		foreach($explodedKeyword as $ekKey => $ekValue)
		{
			$newArray = array();
			
			array_push($newArray, strtoupper($ekValue));
			array_push($newArray, strtolower($ekValue));
			array_push($newArray, ucfirst(strtolower($ekValue)));
			
			$newArray2[$ekKey] = $newArray;
		}
		
		array_unique($newArray2, SORT_REGULAR);
		
		# encrypt new array of keywords 
		foreach($newArray2 as $na2Key => $na2Value)
		{
			foreach($na2Value as $na2v)
			{
				$encryptedKeyword[$i] = $this->nagkamoritsing->useForSearch($na2v);
				$i++;
			}
		}
		
		return $encryptedKeyword;
	}

	private function getSearchedPatientsInformation($searchedPatientsIDs = array(), $offset = 0, $limit = 0, $outOfTotalSearchCount = 0)
	{
		$personalInfo = $profilepic = $searchedPatients = $searchedPatientsInformation = array();
		$searchedPatients_defaultIndex = 0;

		if (!empty($searchedPatientsIDs))
		{	
			foreach ($searchedPatientsIDs as $sKey => $sValue)
			{
				if (($sKey >= $offset) && ($sKey < $limit))
				{
					$personalInfo = $this->patientSearch->getSearchPatientInfo($sValue);
					
					foreach ($personalInfo as $pInfo)
					{
						$searchedPatients[$searchedPatients_defaultIndex] = array(
						"patient_id" 			=> $pInfo->IDsafe_user,
						"patient_fullname"		=> $pInfo->fullname,
						"patient_emailAddress" 	=> $pInfo->emailAddress,
						"patient_sex" 			=> $pInfo->sex,
						"patient_dateofbirth"	=> $pInfo->dateofbirth,
						"patient_bloodtype" 	=> $pInfo->bloodtype);
					}
					
					$profilepic = $this->profilepic->getprofilepicinfo($sValue);
					
					if ($profilepic)
					{
						$searchedPatients[$searchedPatients_defaultIndex]['patient_profilepic_type'] = $profilepic->type;
						$searchedPatients[$searchedPatients_defaultIndex]['patient_profilepic_filename'] = $profilepic->filename;
					}
					else
					{
						$searchedPatients[$searchedPatients_defaultIndex]['patient_profilepic_type'] = '';
						$searchedPatients[$searchedPatients_defaultIndex]['patient_profilepic_filename'] = '';
					}
					
					$searchedPatients_defaultIndex++;
					$outOfTotalSearchCount++;
				}
			}
		}

		$searchedPatientsInformation = array(
			'searchedPatients' => $searchedPatients,
			'outOfTotalSearchCount' => $outOfTotalSearchCount
		);

		return $searchedPatientsInformation;
	}

    public function searchBackupCode()
    {
        $navOpen = 'Patient records';
        $urlAdd = '';
        $this->activeMainMenu = "Patients";
        $customCss = array();
        $customJs = array();
        $error = FALSE;
        $this->errorMessage = '';

        $this->form_validation->set_rules('backupcode','Back up code', 'trim|xss_clean|required');

        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
            $this->errorMessage = validation_errors();
        }
        else
        {
            $error = FALSE;
        }


        if ($error == FALSE)
        {
            $this->backupcode = filter_var($this->input->post('backupcode'), FILTER_SANITIZE_STRING);
            $this->searchResult = $this->patientSearch->backupCodeSearch($this->backupcode);
         
            // naay view if sobra duha ang result
            // or walay tag-iya ana nga code

            if (empty($this->searchResult))
            {
                $this->searchResult = array();
            }


            if (count($this->searchResult) == 1)
            {
                foreach ($this->searchResult as $result) 
                {
                    redirect(base_url().'profileViewer/doctorViewing?sessionid='.$this->nagkamoritsing->bungkag($result->IDsafe_user), 'refresh');
                }
            }

           
        }
        if (empty($this->searchResult))
        {
            $this->searchResult = array();
        }

         $this->load->view(
                'doctor-dashboard-light-blue.phtml', array(
                'title' => 'Nambal safe - Doctor Dashboard', 
                'customCss' => $customCss,
                'customJs' => $customJs,
                'urlAdd' => $urlAdd,
                'navOpen' => $navOpen,
                'view' => 'profile-viewer/search-patient-backupcode'
            ));
    }

    public function howtoscan()
    {
        $this->load->view('profile-viewer/howtoscan.phtml', array());
    }

    public function wheretofindqr()
    {
        $this->load->view('profile-viewer/wheretofindqr.phtml', array());
    }

    private function checksession()
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
    }

    private function checkdoctor()
    {
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }

    private function defaultDatas()
    {
    	$data = $data['states'] = array();


        $data['bloodtypes'] = array('DKY', 'O+', 'O-', 'O?', 'A+', 'A-', 'A?', 'B+', 'B-', 'B?', 'AB+', 'AB-', 'AB?');

		$data['genders'] = array('Male', 'Female', 'Transgender');

    	$data['countries'] = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ascension", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint-Barthélemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Sint Maarten", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "South Sudan", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "United States of America", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

		$data['states'][0]="Badakhshan|Badghis|Baghlan|Balkh|Bamyan|Daykundi|Farah|Faryab|Ghazni|Ghor|Helmand|Herat|Jowzjan|Kabul|Kandahar|Kapisa|Khost|Kunar|Kunduz|Laghman|Logar|Nangarhar|Nimruz|Nurestan|Oruzgan|Paktia|Paktika|Panjshir|Parvan|Samangan|Sare Pol|Takhar|Wardak|Zabul";
		// $data['states'][2]="Berat|Bulqize|Delvine|Devoll (Bilisht)|Dibër (Peshkopi)|Durrës|Elbasan|Fier|Gjirokastër|Gramsh|Has (Krume)|Kavaje|Kolonje (Erseke)|Korçë|Kruje|Kucove|Kukës|Kurbin|Lezhë|Librazhd|Lushnje|Malesi e Madhe (Koplik)|Mallakaster (Ballsh)|Mat (Burrel)|Mirdite (Rreshen)|Peqin|Permet|Pogradec|Puke|Sarande|Shkodër|Skrapar (Corovode)|Tepelene|Tirane (Tirana)|Tirane (Tirana)|Tropoje (Bajram Curri)|Vlorë";
		$data['states'][1]="Berat|Dibër|Durrës|Elbasan|Fier|Gjirokastër|Korçë|Kukës|Lezhë|Shkodër|Tirana|Vlorë";
		$data['states'][2]="Adrar|Ain Defla|Ain Timouchent|Algier|Annaba|Batna|Béchar|Béjaïa|Biskra|Blida|Bordj Bou Arréridj|Bouïra|Boumerdes|Chlef|Constantine|Djelfa|El Bayadh|El Oued|El Tarf|Ghardaia|Guelma|Illizi|Jijel|Khenchela|Laghouat|M'Sila|Mascara|Médéa|Mila|Mostaganem|Naama|Oran|Ouargla|Oum el Bouaghi|Relizane|Saida|Setif|Sidi Bel Abbès|Skikda|Souk Ahras|Tamanghasset|Tébessa|Tiaret|Tindouf|Tipaza|Tissemsilt|Tizi Ouzou|Tlemcen";
		$data['states'][3]="Eastern District|Manu'a District|Rose Atoll|Swains Island|Western District";
		$data['states'][4]="Andorra la Vella|Canillo|Encamp|Escaldes-Engordany|La Massana|Ordino|Sant Julià de Lòria";
		$data['states'][5]="Bengo|Benguela|Bié|Cabinda|Cuando Cubango|Cuanza Norte|Cuanza Sul|Cunene|Huambo|Huila|La Massana|Luanda|Lunda Norte|Lunda Sul|Malanje|Moxico|Namibe|Ordino|Sant Julia de Loria|Uige|Zaire";
		$data['states'][6]="Anguilla";
		$data['states'][7]="Antartica";
		$data['states'][8]="Barbuda|Redonda|Saint George|Saint John|Saint Mary|Saint Paul|Saint Peter|Saint Philip";
		$data['states'][9]="Antartica e Islas del Atlantico Sur|Autonomous City of Buenos Aires|Buenos Aires Province|Catamarca|Chaco|Chubut|Córdoba|Corrientes|Entre Rios|Formosa|Jujuy|La Pampa|La Rioja|Mendoza|Misiones|Neuquen|Rio Negro|Salta|San Juan|San Luis|Santa Cruz|Santa Fe|Santiago del Estero|Tierra del Fuego|Tucuman";
		$data['states'][10]="Aragatsotn|Ararat|Armavir|Gegharkunik|Kotayk|Lori|Shirak|Syunik|Tavush|Vayots Dzor|Yerevan";
		$data['states'][11]="Aruba";
		$data['states'][12]="Ascension Island";
		$data['states'][13]="Ashmore and Cartier Island";
		$data['states'][14]="Australian Capital Territory|New South Wales|Northern Territory|Queensland|South Australia|Tasmania|Victoria|Western Australia";
		// $data['states'][16]="Burgenland|Kaernten|Niederoesterreich|Oberoesterreich|Salzburg|Steiermark|Tirol|Vorarlberg|Wien";
		$data['states'][15]="Burgenland|Carinthia|Lower Austria|Salzburg|Styria|Tyrol|Upper Austria|Vienna|Vorarlberg";
		// $data['states'][17]="Abseron Rayonu|Agcabadi Rayonu|Agdam Rayonu|Agdas Rayonu|Agstafa Rayonu|Agsu Rayonu|Ali Bayramli Sahari|Astara Rayonu|Baki Sahari|Balakan Rayonu|Barda Rayonu|Beylaqan Rayonu|Bilasuvar Rayonu|Cabrayil Rayonu|Calilabad Rayonu|Daskasan Rayonu|Davaci Rayonu|Fuzuli Rayonu|Gadabay Rayonu|Ganca Sahari|Goranboy Rayonu|Goycay Rayonu|Haciqabul Rayonu|Imisli Rayonu|Ismayilli Rayonu|Kalbacar Rayonu|Kurdamir Rayonu|Lacin Rayonu|Lankaran Rayonu|Lankaran Sahari|Lerik Rayonu|Masalli Rayonu|Mingacevir Sahari|Naftalan Sahari|Naxcivan Muxtar Respublikasi|Neftcala Rayonu|Oguz Rayonu|Qabala Rayonu|Qax Rayonu|Qazax Rayonu|Qobustan Rayonu|Quba Rayonu|Qubadli Rayonu|Qusar Rayonu|Saatli Rayonu|Sabirabad Rayonu|Saki Rayonu|Saki Sahari|Salyan Rayonu|Samaxi Rayonu|Samkir Rayonu|Samux Rayonu|Siyazan Rayonu|Sumqayit Sahari|Susa Rayonu|Susa Sahari|Tartar Rayonu|Tovuz Rayonu|Ucar Rayonu|Xacmaz Rayonu|Xankandi Sahari|Xanlar Rayonu|Xizi Rayonu|Xocali Rayonu|Xocavand Rayonu|Yardimli Rayonu|Yevlax Rayonu|Yevlax Sahari|Zangilan Rayonu|Zaqatala Rayonu|Zardab Rayonu";
		$data['states'][16]="Absheron (Abşeron)|Aghdam (Ağdam)|Aghdash (Ağdaş)|Aghjabadi (Ağcabədi)|Aghstafa (Ağstafa)|Aghsu (Ağsu)|Astara (Astara)|Babek (Babək)|Balakan (Balakən)|Baku (Bakı)|Barda (Bərdə)|Beylagan (Beyləqan)|Bilasuvar (Biləsuvar)|Dashkasan (Daşkəsən)|Fuzuli (Füzuli)|Gabala (Qəbələ)|Gadabay (Gədəbəy)|Ganja (Gəncə)|Gakh (Qax)|Gazakh (Qazax)|Gobustan (Qobustan)|Goychay (Göyçay)|Goranboy (Goranboy)|Goygol (Göygöl)|Guba (Quba)|Gubadly (Qubadlı)|Gusar (Qusar)|Hajigabul (Hacıqabul)|Imishli (İmişli)|Ismailly (İsmayıllı)|Jabrayil (Cəbrayıl)|Jalilabad (Cəlilabad)|Julfa (Culfa)|Kalbajar (Kəlbəcər)|Kangarli (Kəngərli)|Khachmaz (Xaçmaz)|Khankendi (Xankəndi)|Khizi (Xızı)|Khojaly (Xocalı)|Khojavend (Xocavənd)|Kurdamir (Kürdəmir)|Lachin (Laçın)|Lankaran (Lənkəran)|Lankaran (Lənkəran)|Lerik (Lerik)|Masally (Masallı)|Mingachevir (Mingəçevir)|Naftalan (Naftalan)|Nakhchivan (Naxçıvan)|Neftchala (Neftçala)|Oghuz (Oğuz)|Ordubad (Ordubad)|Saatly (Saatlı)|Sabirabad (Sabirabad)|Sadarak (Sədərək)|Salyan (Salyan)|Samukh (Samux)|Shabran (Şabran)|Shahbuz (Şahbuz)|Shaki (Şəki)|Shamakhy (Şamaxı)|Shamkir (Şəmkir)|Sharur (Şərur)|Shirvan (Şirvan)|Shusha (Şuşa)|Siyazan (Siyəzən)|Sumqayit (Sumqayıt)|Tartar (Tərtər)|Tovuz (Tovuz)|Ujar (Ucar)|Yardimly (Yardımlı)|Yevlakh (Yevlax)|Zangilan (Zəngilan)|Zaqatala (Zaqatala)|Zardab (Zərdab)";
		$data['states'][17]="Acklins and Crooked Islands|Bimini|Cat Island|Exuma|Freeport|Fresh Creek|Governor's Harbour|Green Turtle Cay|Harbour Island|High Rock|Inagua|Kemps Bay|Long Island|Marsh Harbour|Mayaguana|New Providence|Nicholls Town and Berry Islands|Ragged Island|Rock Sound|San Salvador and Rum Cay|Sandy Point";
		$data['states'][18]="Al Hadd|Al Manamah|Al Mintaqah al Gharbiyah|Al Mintaqah al Wusta|Al Mintaqah ash Shamaliyah|Al Muharraq|Ar Rifa' wa al Mintaqah al Janubiyah|Jidd Hafs|Juzur Hawar|Madinat 'Isa|Madinat Hamad|Sitrah";
		$data['states'][19]="Barguna|Barisal|Bhola|Jhalokati|Patuakhali|Pirojpur|Bandarban|Brahmanbaria|Chandpur|Chittagong|Comilla|Cox's Bazar|Feni|Khagrachari|Lakshmipur|Noakhali|Rangamati|Dhaka|Faridpur|Gazipur|Gopalganj|Jamalpur|Kishoreganj|Madaripur|Manikganj|Munshiganj|Mymensingh|Narayanganj|Narsingdi|Netrokona|Rajbari|Shariatpur|Sherpur|Tangail|Bagerhat|Chuadanga|Jessore|Jhenaidah|Khulna|Kushtia|Magura|Meherpur|Narail|Satkhira|Bogra|Dinajpur|Gaibandha|Jaipurhat|Kurigram|Lalmonirhat|Naogaon|Natore|Nawabganj|Nilphamari|Pabna|Panchagarh|Rajshahi|Rangpur|Sirajganj|Thakurgaon|Habiganj|Maulvi bazar|Sunamganj|Sylhet";
		$data['states'][20]="Bridgetown|Christ Church|Saint Andrew|Saint George|Saint James|Saint John|Saint Joseph|Saint Lucy|Saint Michael|Saint Peter|Saint Philip|Saint Thomas";
		$data['states'][21]="Brestskaya (Brest)|Homyel'skaya (Homyel')|Horad Minsk|Hrodzyenskaya (Hrodna)|Mahilyowskaya (Mahilyow)|Minskaya|Vitsyebskaya (Vitsyebsk)";
		$data['states'][22]="Antwerpen|Brabant Wallon|Brussels Capitol Region|Hainaut|Liege|Limburg|Luxembourg|Namur|Oost-Vlaanderen|Vlaams Brabant|West-Vlaanderen";
		$data['states'][23]="Belize|Cayo|Corozal|Orange Walk|Stann Creek|Toledo";
		$data['states'][24]="Alibori|Atakora|Atlantique|Borgou|Collines|Couffo|Donga|Littoral|Mono|Oueme|Plateau|Zou";
		$data['states'][25]="Devonshire|Hamilton|Hamilton|Paget|Pembroke|Saint George|Saint Georges|Sandys|Smiths|Southampton|Warwick";
		$data['states'][26]="Bumthang|Chhukha|Chirang|Daga|Geylegphug|Ha|Lhuntshi|Mongar|Paro|Pemagatsel|Punakha|Samchi|Samdrup Jongkhar|Shemgang|Tashigang|Thimphu|Tongsa|Wangdi Phodrang";
		$data['states'][27]="Beni|Chuquisaca|Cochabamba|La Paz|Oruro|Pando|Potosi|Santa Cruz|Tarija";
		$data['states'][28]="Federation of Bosnia and Herzegovina|Republika Srpska";
		$data['states'][29]="Central|Chobe|Francistown|Gaborone|Ghanzi|Kgalagadi|Kgatleng|Kweneng|Lobatse|Ngamiland|North-East|Selebi-Pikwe|South-East|Southern";
		$data['states'][30]="Acre|Alagoas|Amapa|Amazonas|Bahia|Ceara|Distrito Federal|Espirito Santo|Goias|Maranhao|Mato Grosso|Mato Grosso do Sul|Minas Gerais|Para|Paraiba|Parana|Pernambuco|Piaui|Rio de Janeiro|Rio Grande do Norte|Rio Grande do Sul|Rondonia|Roraima|Santa Catarina|Sao Paulo|Sergipe|Tocantins";
		$data['states'][31]="British Indian Ocean Territory";
		$data['states'][32]="Anegada|Jost Van Dyke|Tortola|Virgin Gorda";
		$data['states'][33]="Belait|Brunei and Muara|Temburong|Tutong";
		$data['states'][34]="Blagoevgrad|Burgas|Dobrich|Gabrovo|Khaskovo|Kurdzhali|Kyustendil|Lovech|Montana|Pazardzhik|Pernik|Pleven|Plovdiv|Razgrad|Ruse|Shumen|Silistra|Sliven|Smolyan|Sofiya|Sofiya-Grad|Stara Zagora|Turgovishte|Varna|Veliko Turnovo|Vidin|Vratsa|Yambol";
		$data['states'][35]="Bale|Bam|Banwa|Bazega|Bougouriba|Boulgou|Boulkiemde|Comoe|Ganzourgou|Gnagna|Gourma|Houet|Ioba|Kadiogo|Kenedougou|Komandjari|Kompienga|Kossi|Koupelogo|Kouritenga|Kourweogo|Leraba|Loroum|Mouhoun|Nahouri|Namentenga|Naumbiel|Nayala|Oubritenga|Oudalan|Passore|Poni|Samentenga|Sanguie|Seno|Sissili|Soum|Sourou|Tapoa|Tuy|Yagha|Yatenga|Ziro|Zondomo|Zoundweogo";
		$data['states'][36]="Ayeyarwady|Bago|Chin State|Kachin State|Kayah State|Kayin State|Magway|Mandalay|Mon State|Rakhine State|Sagaing|Shan State|Tanintharyi|Yangon";
		$data['states'][37]="Bubanza|Bujumbura|Bururi|Cankuzo|Cibitoke|Gitega|Karuzi|Kayanza|Kirundo|Makamba|Muramvya|Muyinga|Mwaro|Ngozi|Rutana|Ruyigi";
		$data['states'][38]="Banteay Mean Cheay|Batdambang|Kampong Cham|Kampong Chhnang|Kampong Spoe|Kampong Thum|Kampot|Kandal|Kaoh Kong|Keb|Kracheh|Mondol Kiri|Otdar Mean Cheay|Pailin|Phnum Penh|Pouthisat|Preah Seihanu (Sihanoukville)|Preah Vihear|Prey Veng|Rotanah Kiri|Siem Reab|Stoeng Treng|Svay Rieng|Takev";
		$data['states'][39]="Adamaoua|Centre|Est|Extreme-Nord|Littoral|Nord|Nord-Ouest|Ouest|Sud|Sud-Ouest";
		$data['states'][40]="Alberta|British Columbia|Manitoba|New Brunswick|Newfoundland|Northwest Territories|Nova Scotia|Nunavut|Ontario|Prince Edward Island|Quebec|Saskatchewan|Yukon Territory";
		$data['states'][41]="Boa Vista|Brava|Maio|Mosteiros|Paul|Porto Novo|Praia|Ribeira Grande|Sal|Santa Catarina|Santa Cruz|Sao Domingos|Sao Filipe|Sao Nicolau|Sao Vicente|Tarrafal";
		$data['states'][42]="Creek|Eastern|Midland|South Town|Spot Bay|Stake Bay|West End|Western";
		$data['states'][43]="Bamingui-Bangoran|Bangui|Basse-Kotto|Gribingui|Haut-Mbomou|Haute-Kotto|Haute-Sangha|Kemo-Gribingui|Lobaye|Mbomou|Nana-Mambere|Ombella-Mpoko|Ouaka|Ouham|Ouham-Pende|Sangha|Vakaga";
		$data['states'][44]="Batha|Biltine|Borkou-Ennedi-Tibesti|Chari-Baguirmi|Guera|Kanem|Lac|Logone Occidental|Logone Oriental|Mayo-Kebbi|Moyen-Chari|Ouaddai|Salamat|Tandjile";
		$data['states'][45]="Aisen del General Carlos Ibanez del Campo|Antofagasta|Araucania|Atacama|Bio-Bio|Coquimbo|Libertador General Bernardo O'Higgins|Los Lagos|Magallanes y de la Antartica Chilena|Maule|Region Metropolitana (Santiago)|Tarapaca|Valparaiso";
		$data['states'][46]="Anhui|Beijing|Chongqing|Fujian|Gansu|Guangdong|Guangxi|Guizhou|Hainan|Hebei|Heilongjiang|Henan|Hubei|Hunan|Jiangsu|Jiangxi|Jilin|Liaoning|Nei Mongol|Ningxia|Qinghai|Shaanxi|Shandong|Shanghai|Shanxi|Sichuan|Tianjin|Xinjiang|Xizang (Tibet)|Yunnan|Zhejiang";
		$data['states'][47]="Christmas Island";
		$data['states'][48]="Clipperton Island";
		$data['states'][49]="Direction Island|Home Island|Horsburgh Island|North Keeling Island|South Island|West Island";
		$data['states'][50]="Amazonas|Antioquia|Arauca|Atlantico|Bolivar|Boyaca|Caldas|Caqueta|Casanare|Cauca|Cesar|Choco|Cordoba|Cundinamarca|Distrito Capital de Santa Fe de Bogota|Guainia|Guaviare|Huila|La Guajira|Magdalena|Meta|Narino|Norte de Santander|Putumayo|Quindio|Risaralda|San Andres y Providencia|Santander|Sucre|Tolima|Valle del Cauca|Vaupes|Vichada";
		// <!-- -->
		$data['states'][51]="Anjouan (Nzwani)|Domoni|Fomboni|Grande Comore (Njazidja)|Moheli (Mwali)|Moroni|Moutsamoudou";
		$data['states'][52]="Bandundu|Bas-Congo|Equateur|Kasai-Occidental|Kasai-Oriental|Katanga|Kinshasa|Maniema|Nord-Kivu|Orientale|Sud-Kivu";
		$data['states'][53]="Bouenza|Brazzaville|Cuvette|Kouilou|Lekoumou|Likouala|Niari|Plateaux|Pool|Sangha";
		$data['states'][54]="Aitutaki|Atiu|Avarua|Mangaia|Manihiki|Manuae|Mauke|Mitiaro|Nassau Island|Palmerston|Penrhyn|Pukapuka|Rakahanga|Rarotonga|Suwarrow|Takutea";
		$data['states'][55]="Alajuela|Cartago|Guanacaste|Heredia|Limon|Puntarenas|San Jose";
		$data['states'][56]="Abengourou|Abidjan|Aboisso|Adiake'|Adzope|Agboville|Agnibilekrou|Ale'pe'|Bangolo|Beoumi|Biankouma|Bocanda|Bondoukou|Bongouanou|Bouafle|Bouake|Bouna|Boundiali|Dabakala|Dabon|Daloa|Danane|Daoukro|Dimbokro|Divo|Duekoue|Ferkessedougou|Gagnoa|Grand Bassam|Grand-Lahou|Guiglo|Issia|Jacqueville|Katiola|Korhogo|Lakota|Man|Mankono|Mbahiakro|Odienne|Oume|Sakassou|San-Pedro|Sassandra|Seguela|Sinfra|Soubre|Tabou|Tanda|Tiassale|Tiebissou|Tingrela|Touba|Toulepleu|Toumodi|Vavoua|Yamoussoukro|Zuenoula";
		$data['states'][57]="Bjelovarsko-Bilogorska Zupanija|Brodsko-Posavska Zupanija|Dubrovacko-Neretvanska Zupanija|Istarska Zupanija|Karlovacka Zupanija|Koprivnicko-Krizevacka Zupanija|Krapinsko-Zagorska Zupanija|Licko-Senjska Zupanija|Medimurska Zupanija|Osjecko-Baranjska Zupanija|Pozesko-Slavonska Zupanija|Primorsko-Goranska Zupanija|Sibensko-Kninska Zupanija|Sisacko-Moslavacka Zupanija|Splitsko-Dalmatinska Zupanija|Varazdinska Zupanija|Viroviticko-Podravska Zupanija|Vukovarsko-Srijemska Zupanija|Zadarska Zupanija|Zagreb|Zagrebacka Zupanija";
		$data['states'][58]="Camaguey|Ciego de Avila|Cienfuegos|Ciudad de La Habana|Granma|Guantanamo|Holguin|Isla de la Juventud|La Habana|Las Tunas|Matanzas|Pinar del Rio|Sancti Spiritus|Santiago de Cuba|Villa Clara";
		$data['states'][59]="Famagusta|Kyrenia|Larnaca|Limassol|Nicosia|Paphos";
		$data['states'][60]="Brnensky|Budejovicky|Jihlavsky|Karlovarsky|Kralovehradecky|Liberecky|Olomoucky|Ostravsky|Pardubicky|Plzensky|Praha|Stredocesky|Ustecky|Zlinsky";
		$data['states'][61]="Arhus|Bornholm|Fredericksberg|Frederiksborg|Fyn|Kobenhavn|Kobenhavns|Nordjylland|Ribe|Ringkobing|Roskilde|Sonderjylland|Storstrom|Vejle|Vestsjalland|Viborg";
		$data['states'][62]="'Ali Sabih|Dikhil|Djibouti|Obock|Tadjoura";
		$data['states'][63]="Saint Andrew|Saint David|Saint George|Saint John|Saint Joseph|Saint Luke|Saint Mark|Saint Patrick|Saint Paul|Saint Peter";
		$data['states'][64]="Azua|Baoruco|Barahona|Dajabon|Distrito Nacional|Duarte|El Seibo|Elias Pina|Espaillat|Hato Mayor|Independencia|La Altagracia|La Romana|La Vega|Maria Trinidad Sanchez|Monsenor Nouel|Monte Cristi|Monte Plata|Pedernales|Peravia|Puerto Plata|Salcedo|Samana|San Cristobal|San Juan|San Pedro de Macoris|Sanchez Ramirez|Santiago|Santiago Rodriguez|Valverde";
		// <!-- -->
		$data['states'][65]="Aileu|Ainaro|Baucau|Bobonaro|Cova Lima|Dili|Ermera|Lautém|Liquiçá|Manatuto|Manufahi|Oecusse|Viqueque";
		$data['states'][66]="Azuay|Bolivar|Canar|Carchi|Chimborazo|Cotopaxi|El Oro|Esmeraldas|Galapagos|Guayas|Imbabura|Loja|Los Rios|Manabi|Morona-Santiago|Napo|Orellana|Pastaza|Pichincha|Sucumbios|Tungurahua|Zamora-Chinchipe";
		$data['states'][67]="Alexandria|Al Sharqia|Aswan|Asyut|Beheira|Beni Suef|Cairo|Dakahlia|Damietta|Faiyum|Gharbia|Giza|Ismailia|Kafr el-Sheikh|Luxor|Minya|Monufia|New Valley|North Sinai|Port Said|Qalyubia|Qena|Red Sea|Sohag|South Sinai|Suez";
		$data['states'][68]="Ahuachapan|Cabanas|Chalatenango|Cuscatlan|La Libertad|La Paz|La Union|Morazan|San Miguel|San Salvador|San Vicente|Santa Ana|Sonsonate|Usulutan";
		$data['states'][69]="Annobon|Bioko Norte|Bioko Sur|Centro Sur|Kie-Ntem|Litoral|Wele-Nzas";
		$data['states'][70]="Akale Guzay|Barka|Denkel|Hamasen|Sahil|Semhar|Senhit|Seraye";
		$data['states'][71]="Harjumaa (Tallinn)|Hiiumaa (Kardla)|Ida-Virumaa (Johvi)|Jarvamaa (Paide)|Jogevamaa (Jogeva)|Laane-Virumaa (Rakvere)|Laanemaa (Haapsalu)|Parnumaa (Parnu)|Polvamaa (Polva)|Raplamaa (Rapla)|Saaremaa (Kuessaare)|Tartumaa (Tartu)|Valgamaa (Valga)|Viljandimaa (Viljandi)|Vorumaa (Voru)";
		$data['states'][72]="Adis Abeba (Addis Ababa)|Afar|Amara|Dire Dawa|Gambela Hizboch|Hareri Hizb|Oromiya|Sumale|Tigray|YeDebub Biheroch Bihereseboch na Hizboch";
		$data['states'][73]="Europa Island";
		$data['states'][74]="Falkland Islands (Islas Malvinas)";
		$data['states'][75]="Bordoy|Eysturoy|Mykines|Sandoy|Skuvoy|Streymoy|Suduroy|Tvoroyri|Vagar";
		$data['states'][76]="Central|Eastern|Northern|Rotuma|Western";
		$data['states'][77]="Aland|Etela-Suomen Laani|Ita-Suomen Laani|Lansi-Suomen Laani|Lappi|Oulun Laani";
		$data['states'][78]="Alsace|Aquitaine|Auvergne|Basse-Normandie|Bourgogne|Bretagne|Centre|Champagne-Ardenne|Corse|Franche-Comte|Haute-Normandie|Ile-de-France|Languedoc-Roussillon|Limousin|Lorraine|Midi-Pyrenees|Nord-Pas-de-Calais|Pays de la Loire|Picardie|Poitou-Charentes|Provence-Alpes-Cote d'Azur|Rhone-Alpes";
		$data['states'][79]="French Guiana";
		$data['states'][80]="Archipel des Marquises|Archipel des Tuamotu|Archipel des Tubuai|Iles du Vent|Iles Sous-le-Vent";
		$data['states'][81]="Adelie Land|Ile Crozet|Iles Kerguelen|Iles Saint-Paul et Amsterdam";
		$data['states'][82]="Estuaire|Haut-Ogooue|Moyen-Ogooue|Ngounie|Nyanga|Ogooue-Ivindo|Ogooue-Lolo|Ogooue-Maritime|Woleu-Ntem";
		$data['states'][83]="Banjul|Central River|Lower River|North Bank|Upper River|Western";
		$data['states'][84]="Gaza Strip";
		$data['states'][85]="Abashis|Abkhazia or Ap'khazet'is Avtonomiuri Respublika (Sokhumi)|Adigenis|Ajaria or Acharis Avtonomiuri Respublika (Bat'umi)|Akhalgoris|Akhalk'alak'is|Akhalts'ikhis|Akhmetis|Ambrolauris|Aspindzis|Baghdat'is|Bolnisis|Borjomis|Ch'khorotsqus|Ch'okhatauris|Chiat'ura|Dedop'listsqaros|Dmanisis|Dushet'is|Gardabanis|Gori|Goris|Gurjaanis|Javis|K'arelis|K'ut'aisi|Kaspis|Kharagaulis|Khashuris|Khobis|Khonis|Lagodekhis|Lanch'khut'is|Lentekhis|Marneulis|Martvilis|Mestiis|Mts'khet'is|Ninotsmindis|Onis|Ozurget'is|P'ot'i|Qazbegis|Qvarlis|Rust'avi|Sach'kheris|Sagarejos|Samtrediis|Senakis|Sighnaghis|T'bilisi|T'elavis|T'erjolis|T'et'ritsqaros|T'ianet'is|Tqibuli|Ts'ageris|Tsalenjikhis|Tsalkis|Tsqaltubo|Vanis|Zestap'onis|Zugdidi|Zugdidis";
		$data['states'][86]="Baden-Wuerttemberg|Bayern|Berlin|Brandenburg|Bremen|Hamburg|Hessen|Mecklenburg-Vorpommern|Niedersachsen|Nordrhein-Westfalen|Rheinland-Pfalz|Saarland|Sachsen|Sachsen-Anhalt|Schleswig-Holstein|Thueringen";
		$data['states'][87]="Ashanti|Brong-Ahafo|Central|Eastern|Greater Accra|Northern|Upper East|Upper West|Volta|Western";
		$data['states'][88]="Gibraltar";
		$data['states'][89]="Ile du Lys|Ile Glorieuse";
		$data['states'][90]="Aitolia kai Akarnania|Akhaia|Argolis|Arkadhia|Arta|Attiki|Ayion Oros (Mt. Athos)|Dhodhekanisos|Drama|Evritania|Evros|Evvoia|Florina|Fokis|Fthiotis|Grevena|Ilia|Imathia|Ioannina|Irakleion|Kardhitsa|Kastoria|Kavala|Kefallinia|Kerkyra|Khalkidhiki|Khania|Khios|Kikladhes|Kilkis|Korinthia|Kozani|Lakonia|Larisa|Lasithi|Lesvos|Levkas|Magnisia|Messinia|Pella|Pieria|Preveza|Rethimni|Rodhopi|Samos|Serrai|Thesprotia|Thessaloniki|Trikala|Voiotia|Xanthi|Zakinthos";
		$data['states'][91]="Avannaa (Nordgronland)|Kitaa (Vestgronland)|Tunu (Ostgronland)";
		$data['states'][92]="Carriacou and Petit Martinique|Saint Andrew|Saint David|Saint George|Saint John|Saint Mark|Saint Patrick";
		$data['states'][93]="Basse-Terre|Grande-Terre|Iles de la Petite Terre|Iles des Saintes|Marie-Galante";
		$data['states'][94]="Guam";
		$data['states'][95]="Alta Verapaz|Baja Verapaz|Chimaltenango|Chiquimula|El Progreso|Escuintla|Guatemala|Huehuetenango|Izabal|Jalapa|Jutiapa|Peten|Quetzaltenango|Quiche|Retalhuleu|Sacatepequez|San Marcos|Santa Rosa|Solola|Suchitepequez|Totonicapan|Zacapa";
		$data['states'][96]="Castel|Forest|St. Andrew|St. Martin|St. Peter Port|St. Pierre du Bois|St. Sampson|St. Saviour|Torteval|Vale";
		$data['states'][97]="Beyla|Boffa|Boke|Conakry|Coyah|Dabola|Dalaba|Dinguiraye|Dubreka|Faranah|Forecariah|Fria|Gaoual|Gueckedou|Kankan|Kerouane|Kindia|Kissidougou|Koubia|Koundara|Kouroussa|Labe|Lelouma|Lola|Macenta|Mali|Mamou|Mandiana|Nzerekore|Pita|Siguiri|Telimele|Tougue|Yomou";
		$data['states'][98]="Bafata|Biombo|Bissau|Bolama-Bijagos|Cacheu|Gabu|Oio|Quinara|Tombali";
		$data['states'][99]="Barima-Waini|Cuyuni-Mazaruni|Demerara-Mahaica|East Berbice-Corentyne|Essequibo Islands-West Demerara|Mahaica-Berbice|Pomeroon-Supenaam|Potaro-Siparuni|Upper Demerara-Berbice|Upper Takutu-Upper Essequibo";
		$data['states'][100]="Artibonite|Centre|Grand'Anse|Nord|Nord-Est|Nord-Ouest|Ouest|Sud|Sud-Est";
		$data['states'][101]="Heard Island and McDonald Islands";
		$data['states'][102]="Holy See (Vatican City)";
		$data['states'][103]="Atlantida|Choluteca|Colon|Comayagua|Copan|Cortes|El Paraiso|Francisco Morazan|Gracias a Dios|Intibuca|Islas de la Bahia|La Paz|Lempira|Ocotepeque|Olancho|Santa Barbara|Valle|Yoro";
		$data['states'][104]="Hong Kong";
		$data['states'][105]="Howland Island";
		$data['states'][106]="Bacs-Kiskun|Baranya|Bekes|Bekescsaba|Borsod-Abauj-Zemplen|Budapest|Csongrad|Debrecen|Dunaujvaros|Eger|Fejer|Gyor|Gyor-Moson-Sopron|Hajdu-Bihar|Heves|Hodmezovasarhely|Jasz-Nagykun-Szolnok|Kaposvar|Kecskemet|Komarom-Esztergom|Miskolc|Nagykanizsa|Nograd|Nyiregyhaza|Pecs|Pest|Somogy|Sopron|Szabolcs-Szatmar-Bereg|Szeged|Szekesfehervar|Szolnok|Szombathely|Tatabanya|Tolna|Vas|Veszprem|Veszprem|Zala|Zalaegerszeg";
		$data['states'][107]="Akranes|Akureyri|Arnessysla|Austur-Bardhastrandarsysla|Austur-Hunavatnssysla|Austur-Skaftafellssysla|Borgarfjardharsysla|Dalasysla|Eyjafjardharsysla|Gullbringusysla|Hafnarfjordhur|Husavik|Isafjordhur|Keflavik|Kjosarsysla|Kopavogur|Myrasysla|Neskaupstadhur|Nordhur-Isafjardharsysla|Nordhur-Mulasys-la|Nordhur-Thingeyjarsysla|Olafsfjordhur|Rangarvallasysla|Reykjavik|Saudharkrokur|Seydhisfjordhur|Siglufjordhur|Skagafjardharsysla|Snaefellsnes-og Hnappadalssysla|Strandasysla|Sudhur-Mulasysla|Sudhur-Thingeyjarsysla|Vesttmannaeyjar|Vestur-Bardhastrandarsysla|Vestur-Hunavatnssysla|Vestur-Isafjardharsysla|Vestur-Skaftafellssysla";
		$data['states'][108]="Andaman and Nicobar Islands|Andhra Pradesh|Arunachal Pradesh|Assam|Bihar|Chandigarh|Chhattisgarh|Dadra and Nagar Haveli|Daman and Diu|Delhi|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu and Kashmir|Jharkhand|Karnataka|Kerala|Lakshadweep|Madhya Pradesh|Maharashtra|Manipur|Meghalaya|Mizoram|Nagaland|Orissa|Pondicherry|Punjab|Rajasthan|Sikkim|Tamil Nadu|Tripura|Uttar Pradesh|Uttaranchal|West Bengal";
		$data['states'][109]="Aceh|Bali|Banten|Bengkulu|East Timor|Gorontalo|Irian Jaya|Jakarta Raya|Jambi|Jawa Barat|Jawa Tengah|Jawa Timur|Kalimantan Barat|Kalimantan Selatan|Kalimantan Tengah|Kalimantan Timur|Kepulauan Bangka Belitung|Lampung|Maluku|Maluku Utara|Nusa Tenggara Barat|Nusa Tenggara Timur|Riau|Sulawesi Selatan|Sulawesi Tengah|Sulawesi Tenggara|Sulawesi Utara|Sumatera Barat|Sumatera Selatan|Sumatera Utara|Yogyakarta";
		$data['states'][110]="Ardabil|Azarbayjan-e Gharbi|Azarbayjan-e Sharqi|Bushehr|Chahar Mahall va Bakhtiari|Esfahan|Fars|Gilan|Golestan|Hamadan|Hormozgan|Ilam|Kerman|Kermanshah|Khorasan|Khuzestan|Kohgiluyeh va Buyer Ahmad|Kordestan|Lorestan|Markazi|Mazandaran|Qazvin|Qom|Semnan|Sistan va Baluchestan|Tehran|Yazd|Zanjan";
		$data['states'][111]="Al Anbar|Al Basrah|Al Muthanna|Al Qadisiyah|An Najaf|Arbil|As Sulaymaniyah|At Ta'mim|Babil|Baghdad|Dahuk|Dhi Qar|Diyala|Karbala'|Maysan|Ninawa|Salah ad Din|Wasit";
		$data['states'][112]="Carlow|Cavan|Clare|Cork|Donegal|Dublin|Galway|Kerry|Kildare|Kilkenny|Laois|Leitrim|Limerick|Longford|Louth|Mayo|Meath|Monaghan|Offaly|Roscommon|Sligo|Tipperary|Waterford|Westmeath|Wexford|Wicklow";
		$data['states'][113]="Antrim|Ards|Armagh|Ballymena|Ballymoney|Banbridge|Belfast|Carrickfergus|Castlereagh|Coleraine|Cookstown|Craigavon|Derry|Down|Dungannon|Fermanagh|Larne|Limavady|Lisburn|Magherafelt|Moyle|Newry and Mourne|Newtownabbey|North Down|Omagh|Strabane";
		$data['states'][114]="Central|Haifa|Jerusalem|Northern|Southern|Tel Aviv";
		$data['states'][115]="Abruzzo|Basilicata|Calabria|Campania|Emilia-Romagna|Friuli-Venezia Giulia|Lazio|Liguria|Lombardia|Marche|Molise|Piemonte|Puglia|Sardegna|Sicilia|Toscana|Trentino-Alto Adige|Umbria|Valle d'Aosta|Veneto";
		$data['states'][116]="Clarendon|Hanover|Kingston|Manchester|Portland|Saint Andrew|Saint Ann|Saint Catherine|Saint Elizabeth|Saint James|Saint Mary|Saint Thomas|Trelawny|Westmoreland";
		$data['states'][117]="Jan Mayen";
		$data['states'][118]="Aichi|Akita|Aomori|Chiba|Ehime|Fukui|Fukuoka|Fukushima|Gifu|Gumma|Hiroshima|Hokkaido|Hyogo|Ibaraki|Ishikawa|Iwate|Kagawa|Kagoshima|Kanagawa|Kochi|Kumamoto|Kyoto|Mie|Miyagi|Miyazaki|Nagano|Nagasaki|Nara|Niigata|Oita|Okayama|Okinawa|Osaka|Saga|Saitama|Shiga|Shimane|Shizuoka|Tochigi|Tokushima|Tokyo|Tottori|Toyama|Wakayama|Yamagata|Yamaguchi|Yamanashi";
		$data['states'][119]="Jarvis Island";
		$data['states'][120]="Jersey";
		$data['states'][121]="Johnston Atoll";
		$data['states'][122]="'Amman|Ajlun|Al 'Aqabah|Al Balqa'|Al Karak|Al Mafraq|At Tafilah|Az Zarqa'|Irbid|Jarash|Ma'an|Madaba";
		$data['states'][123]="Juan de Nova Island";
		$data['states'][124]="Almaty|Aqmola|Aqtobe|Astana|Atyrau|Batys Qazaqstan|Bayqongyr|Mangghystau|Ongtustik Qazaqstan|Pavlodar|Qaraghandy|Qostanay|Qyzylorda|Shyghys Qazaqstan|Soltustik Qazaqstan|Zhambyl";
		$data['states'][125]="Central|Coast|Eastern|Nairobi Area|North Eastern|Nyanza|Rift Valley|Western";
		$data['states'][126]="Abaiang|Abemama|Aranuka|Arorae|Banaba|Banaba|Beru|Butaritari|Central Gilberts|Gilbert Islands|Kanton|Kiritimati|Kuria|Line Islands|Line Islands|Maiana|Makin|Marakei|Nikunau|Nonouti|Northern Gilberts|Onotoa|Phoenix Islands|Southern Gilberts|Tabiteuea|Tabuaeran|Tamana|Tarawa|Tarawa|Teraina";
		$data['states'][127]="Chagang-do (Chagang Province)|Hamgyong-bukto (North Hamgyong Province)|Hamgyong-namdo (South Hamgyong Province)|Hwanghae-bukto (North Hwanghae Province)|Hwanghae-namdo (South Hwanghae Province)|Kaesong-si (Kaesong City)|Kangwon-do (Kangwon Province)|Namp'o-si (Namp'o City)|P'yongan-bukto (North P'yongan Province)|P'yongan-namdo (South P'yongan Province)|P'yongyang-si (P'yongyang City)|Yanggang-do (Yanggang Province)";
		$data['states'][128]="Ch'ungch'ong-bukto|Ch'ungch'ong-namdo|Cheju-do|Cholla-bukto|Cholla-namdo|Inch'on-gwangyoksi|Kangwon-do|Kwangju-gwangyoksi|Kyonggi-do|Kyongsang-bukto|Kyongsang-namdo|Pusan-gwangyoksi|Soul-t'ukpyolsi|Taegu-gwangyoksi|Taejon-gwangyoksi|Ulsan-gwangyoksi";
		$data['states'][129]="Al 'Asimah|Al Ahmadi|Al Farwaniyah|Al Jahra'|Hawalli";
		$data['states'][130]="Batken Oblasty|Bishkek Shaary|Chuy Oblasty (Bishkek)|Jalal-Abad Oblasty|Naryn Oblasty|Osh Oblasty|Talas Oblasty|Ysyk-Kol Oblasty (Karakol)";
		$data['states'][131]="Attapu|Bokeo|Bolikhamxai|Champasak|Houaphan|Khammouan|Louangnamtha|Louangphabang|Oudomxai|Phongsali|Salavan|Savannakhet|Viangchan|Viangchan|Xaignabouli|Xaisomboun|Xekong|Xiangkhoang";
		$data['states'][132]="Aizkraukles Rajons|Aluksnes Rajons|Balvu Rajons|Bauskas Rajons|Cesu Rajons|Daugavpils|Daugavpils Rajons|Dobeles Rajons|Gulbenes Rajons|Jekabpils Rajons|Jelgava|Jelgavas Rajons|Jurmala|Kraslavas Rajons|Kuldigas Rajons|Leipaja|Liepajas Rajons|Limbazu Rajons|Ludzas Rajons|Madonas Rajons|Ogres Rajons|Preilu Rajons|Rezekne|Rezeknes Rajons|Riga|Rigas Rajons|Saldus Rajons|Talsu Rajons|Tukuma Rajons|Valkas Rajons|Valmieras Rajons|Ventspils|Ventspils Rajons";
		$data['states'][133]="Beyrouth|Ech Chimal|Ej Jnoub|El Bekaa|Jabal Loubnane";
		$data['states'][134]="Berea|Butha-Buthe|Leribe|Mafeteng|Maseru|Mohales Hoek|Mokhotlong|Qacha's Nek|Quthing|Thaba-Tseka";
		$data['states'][135]="Bomi|Bong|Grand Bassa|Grand Cape Mount|Grand Gedeh|Grand Kru|Lofa|Margibi|Maryland|Montserrado|Nimba|River Cess|Sinoe";
		$data['states'][136]="Ajdabiya|Al 'Aziziyah|Al Fatih|Al Jabal al Akhdar|Al Jufrah|Al Khums|Al Kufrah|An Nuqat al Khams|Ash Shati'|Awbari|Az Zawiyah|Banghazi|Darnah|Ghadamis|Gharyan|Misratah|Murzuq|Sabha|Sawfajjin|Surt|Tarabulus|Tarhunah|Tubruq|Yafran|Zlitan";
		$data['states'][137]="Balzers|Eschen|Gamprin|Mauren|Planken|Ruggell|Schaan|Schellenberg|Triesen|Triesenberg|Vaduz";
		$data['states'][138]="Akmenes Rajonas|Alytaus Rajonas|Alytus|Anyksciu Rajonas|Birstonas|Birzu Rajonas|Druskininkai|Ignalinos Rajonas|Jonavos Rajonas|Joniskio Rajonas|Jurbarko Rajonas|Kaisiadoriu Rajonas|Kaunas|Kauno Rajonas|Kedainiu Rajonas|Kelmes Rajonas|Klaipeda|Klaipedos Rajonas|Kretingos Rajonas|Kupiskio Rajonas|Lazdiju Rajonas|Marijampole|Marijampoles Rajonas|Mazeikiu Rajonas|Moletu Rajonas|Neringa Pakruojo Rajonas|Palanga|Panevezio Rajonas|Panevezys|Pasvalio Rajonas|Plunges Rajonas|Prienu Rajonas|Radviliskio Rajonas|Raseiniu Rajonas|Rokiskio Rajonas|Sakiu Rajonas|Salcininku Rajonas|Siauliai|Siauliu Rajonas|Silales Rajonas|Silutes Rajonas|Sirvintu Rajonas|Skuodo Rajonas|Svencioniu Rajonas|Taurages Rajonas|Telsiu Rajonas|Traku Rajonas|Ukmerges Rajonas|Utenos Rajonas|Varenos Rajonas|Vilkaviskio Rajonas|Vilniaus Rajonas|Vilnius|Zarasu Rajonas";
		$data['states'][139]="Diekirch|Grevenmacher|Luxembourg";
		$data['states'][140]="Macau";
		$data['states'][141]="Aracinovo|Bac|Belcista|Berovo|Bistrica|Bitola|Blatec|Bogdanci|Bogomila|Bogovinje|Bosilovo|Brvenica|Cair (Skopje)|Capari|Caska|Cegrane|Centar (Skopje)|Centar Zupa|Cesinovo|Cucer-Sandevo|Debar|Delcevo|Delogozdi|Demir Hisar|Demir Kapija|Dobrusevo|Dolna Banjica|Dolneni|Dorce Petrov (Skopje)|Drugovo|Dzepciste|Gazi Baba (Skopje)|Gevgelija|Gostivar|Gradsko|Ilinden|Izvor|Jegunovce|Kamenjane|Karbinci|Karpos (Skopje)|Kavadarci|Kicevo|Kisela Voda (Skopje)|Klecevce|Kocani|Konce|Kondovo|Konopiste|Kosel|Kratovo|Kriva Palanka|Krivogastani|Krusevo|Kuklis|Kukurecani|Kumanovo|Labunista|Lipkovo|Lozovo|Lukovo|Makedonska Kamenica|Makedonski Brod|Mavrovi Anovi|Meseista|Miravci|Mogila|Murtino|Negotino|Negotino-Poloska|Novaci|Novo Selo|Oblesevo|Ohrid|Orasac|Orizari|Oslomej|Pehcevo|Petrovec|Plasnia|Podares|Prilep|Probistip|Radovis|Rankovce|Resen|Rosoman|Rostusa|Samokov|Saraj|Sipkovica|Sopiste|Sopotnika|Srbinovo|Star Dojran|Staravina|Staro Nagoricane|Stip|Struga|Strumica|Studenicani|Suto Orizari (Skopje)|Sveti Nikole|Tearce|Tetovo|Topolcani|Valandovo|Vasilevo|Veles|Velesta|Vevcani|Vinica|Vitoliste|Vranestica|Vrapciste|Vratnica|Vrutok|Zajas|Zelenikovo|Zileno|Zitose|Zletovo|Zrnovci";
		$data['states'][142]="Antananarivo|Antsiranana|Fianarantsoa|Mahajanga|Toamasina|Toliara";
		$data['states'][143]="Balaka|Blantyre|Chikwawa|Chiradzulu|Chitipa|Dedza|Dowa|Karonga|Kasungu|Likoma|Lilongwe|Machinga (Kasupe)|Mangochi|Mchinji|Mulanje|Mwanza|Mzimba|Nkhata Bay|Nkhotakota|Nsanje|Ntcheu|Ntchisi|Phalombe|Rumphi|Salima|Thyolo|Zomba";
		$data['states'][144]="Johor|Kedah|Kelantan|Labuan|Melaka|Negeri Sembilan|Pahang|Perak|Perlis|Pulau Pinang|Sabah|Sarawak|Selangor|Terengganu|Wilayah Persekutuan";
		$data['states'][145]="Alifu|Baa|Dhaalu|Faafu|Gaafu Alifu|Gaafu Dhaalu|Gnaviyani|Haa Alifu|Haa Dhaalu|Kaafu|Laamu|Lhaviyani|Maale|Meemu|Noonu|Raa|Seenu|Shaviyani|Thaa|Vaavu";
		$data['states'][146]="Gao|Kayes|Kidal|Koulikoro|Mopti|Segou|Sikasso|Tombouctou";
		$data['states'][147]="Valletta";
		$data['states'][148]="Man, Isle of";
		$data['states'][149]="Ailinginae|Ailinglaplap|Ailuk|Arno|Aur|Bikar|Bikini|Bokak|Ebon|Enewetak|Erikub|Jabat|Jaluit|Jemo|Kili|Kwajalein|Lae|Lib|Likiep|Majuro|Maloelap|Mejit|Mili|Namorik|Namu|Rongelap|Rongrik|Toke|Ujae|Ujelang|Utirik|Wotho|Wotje";
		$data['states'][150]="Martinique";
		$data['states'][151]="Adrar|Assaba|Brakna|Dakhlet Nouadhibou|Gorgol|Guidimaka|Hodh Ech Chargui|Hodh El Gharbi|Inchiri|Nouakchott|Tagant|Tiris Zemmour|Trarza";
		$data['states'][152]="Agalega Islands|Black River|Cargados Carajos Shoals|Flacq|Grand Port|Moka|Pamplemousses|Plaines Wilhems|Port Louis|Riviere du Rempart|Rodrigues|Savanne";
		$data['states'][153]="Mayotte";
		$data['states'][154]="Aguascalientes|Baja California|Baja California Sur|Campeche|Chiapas|Chihuahua|Coahuila de Zaragoza|Colima|Distrito Federal|Durango|Guanajuato|Guerrero|Hidalgo|Jalisco|Mexico|Michoacan de Ocampo|Morelos|Nayarit|Nuevo Leon|Oaxaca|Puebla|Queretaro de Arteaga|Quintana Roo|San Luis Potosi|Sinaloa|Sonora|Tabasco|Tamaulipas|Tlaxcala|Veracruz-Llave|Yucatan|Zacatecas";
		$data['states'][155]="Chuuk (Truk)|Kosrae|Pohnpei|Yap";
		$data['states'][156]="Midway Islands";
		$data['states'][157]="Balti|Cahul|Chisinau|Chisinau|Dubasari|Edinet|Gagauzia|Lapusna|Orhei|Soroca|Tighina|Ungheni";
		$data['states'][158]="Fontvieille|La Condamine|Monaco-Ville|Monte-Carlo";
		$data['states'][159]="Arhangay|Bayan-Olgiy|Bayanhongor|Bulgan|Darhan|Dornod|Dornogovi|Dundgovi|Dzavhan|Erdenet|Govi-Altay|Hentiy|Hovd|Hovsgol|Omnogovi|Ovorhangay|Selenge|Suhbaatar|Tov|Ulaanbaatar|Uvs";
		$data['states'][160]="Saint Anthony|Saint Georges|Saint Peter's";
		$data['states'][161]="Agadir|Al Hoceima|Azilal|Ben Slimane|Beni Mellal|Boulemane|Casablanca|Chaouen|El Jadida|El Kelaa des Srarhna|Er Rachidia|Essaouira|Fes|Figuig|Guelmim|Ifrane|Kenitra|Khemisset|Khenifra|Khouribga|Laayoune|Larache|Marrakech|Meknes|Nador|Ouarzazate|Oujda|Rabat-Sale|Safi|Settat|Sidi Kacem|Tan-Tan|Tanger|Taounate|Taroudannt|Tata|Taza|Tetouan|Tiznit";
		$data['states'][162]="Cabo Delgado|Gaza|Inhambane|Manica|Maputo|Nampula|Niassa|Sofala|Tete|Zambezia";
		$data['states'][163]="Caprivi|Erongo|Hardap|Karas|Khomas|Kunene|Ohangwena|Okavango|Omaheke|Omusati|Oshana|Oshikoto|Otjozondjupa";
		$data['states'][164]="Aiwo|Anabar|Anetan|Anibare|Baiti|Boe|Buada|Denigomodu|Ewa|Ijuw|Meneng|Nibok|Uaboe|Yaren";
		$data['states'][165]="Bagmati|Bheri|Dhawalagiri|Gandaki|Janakpur|Karnali|Kosi|Lumbini|Mahakali|Mechi|Narayani|Rapti|Sagarmatha|Seti";
		$data['states'][166]="Drenthe|Flevoland|Friesland|Gelderland|Groningen|Limburg|Noord-Brabant|Noord-Holland|Overijssel|Utrecht|Zeeland|Zuid-Holland";
		$data['states'][167]="Netherlands Antilles";
		$data['states'][168]="Iles Loyaute|Nord|Sud";
		$data['states'][169]="Akaroa|Amuri|Ashburton|Bay of Islands|Bruce|Buller|Chatham Islands|Cheviot|Clifton|Clutha|Cook|Dannevirke|Egmont|Eketahuna|Ellesmere|Eltham|Eyre|Featherston|Franklin|Golden Bay|Great Barrier Island|Grey|Hauraki Plains|Hawera|Hawke's Bay|Heathcote|Hikurangi|Hobson|Hokianga|Horowhenua|Hurunui|Hutt|Inangahua|Inglewood|Kaikoura|Kairanga|Kiwitea|Lake|Mackenzie|Malvern|Manaia|Manawatu|Mangonui|Maniototo|Marlborough|Masterton|Matamata|Mount Herbert|Ohinemuri|Opotiki|Oroua|Otamatea|Otorohanga|Oxford|Pahiatua|Paparua|Patea|Piako|Pohangina|Raglan|Rangiora|Rangitikei|Rodney|Rotorua|Runanga|Saint Kilda|Silverpeaks|Southland|Stewart Island|Stratford|Strathallan|Taranaki|Taumarunui|Taupo|Tauranga|Thames-Coromandel|Tuapeka|Vincent|Waiapu|Waiheke|Waihemo|Waikato|Waikohu|Waimairi|Waimarino|Waimate|Waimate West|Waimea|Waipa|Waipawa|Waipukurau|Wairarapa South|Wairewa|Wairoa|Waitaki|Waitomo|Waitotara|Wallace|Wanganui|Waverley|Westland|Whakatane|Whangarei|Whangaroa|Woodville";
		$data['states'][170]="Atlantico Norte|Atlantico Sur|Boaco|Carazo|Chinandega|Chontales|Esteli|Granada|Jinotega|Leon|Madriz|Managua|Masaya|Matagalpa|Nueva Segovia|Rio San Juan|Rivas";
		$data['states'][171]="Agadez|Diffa|Dosso|Maradi|Niamey|Tahoua|Tillaberi|Zinder";
		$data['states'][172]="Abia|Abuja Federal Capital Territory|Adamawa|Akwa Ibom|Anambra|Bauchi|Bayelsa|Benue|Borno|Cross River|Delta|Ebonyi|Edo|Ekiti|Enugu|Gombe|Imo|Jigawa|Kaduna|Kano|Katsina|Kebbi|Kogi|Kwara|Lagos|Nassarawa|Niger|Ogun|Ondo|Osun|Oyo|Plateau|Rivers|Sokoto|Taraba|Yobe|Zamfara";
		$data['states'][173]="Niue";
		$data['states'][174]="Norfolk Island";
		$data['states'][175]="Northern Islands|Rota|Saipan|Tinian";
		$data['states'][176]="Akershus|Aust-Agder|Buskerud|Finnmark|Hedmark|Hordaland|More og Romsdal|Nord-Trondelag|Nordland|Oppland|Oslo|Ostfold|Rogaland|Sogn og Fjordane|Sor-Trondelag|Telemark|Troms|Vest-Agder|Vestfold";
		$data['states'][177]="Ad Dakhiliyah|Al Batinah|Al Wusta|Ash Sharqiyah|Az Zahirah|Masqat|Musandam|Zufar";
		$data['states'][178]="Balochistan|Federally Administered Tribal Areas|Islamabad Capital Territory|North-West Frontier Province|Punjab|Sindh";
		$data['states'][179]="Aimeliik|Airai|Angaur|Hatobohei|Kayangel|Koror|Melekeok|Ngaraard|Ngarchelong|Ngardmau|Ngatpang|Ngchesar|Ngeremlengui|Ngiwal|Palau Island|Peleliu|Sonsoral|Tobi";
		$data['states'][180]="Bocas del Toro|Chiriqui|Cocle|Colon|Darien|Herrera|Los Santos|Panama|San Blas|Veraguas";
		$data['states'][181]="Bougainville|Central|Chimbu|East New Britain|East Sepik|Eastern Highlands|Enga|Gulf|Madang|Manus|Milne Bay|Morobe|National Capital|New Ireland|Northern|Sandaun|Southern Highlands|West New Britain|Western|Western Highlands";
		$data['states'][182]="Alto Paraguay|Alto Parana|Amambay|Asuncion (city)|Boqueron|Caaguazu|Caazapa|Canindeyu|Central|Concepcion|Cordillera|Guaira|Itapua|Misiones|Neembucu|Paraguari|Presidente Hayes|San Pedro";
		$data['states'][183]="Amazonas|Ancash|Apurimac|Arequipa|Ayacucho|Cajamarca|Callao|Cusco|Huancavelica|Huanuco|Ica|Junin|La Libertad|Lambayeque|Lima|Loreto|Madre de Dios|Moquegua|Pasco|Piura|Puno|San Martin|Tacna|Tumbes|Ucayali";
		$data['states'][184]="Abra|Agusan del Norte|Agusan del Sur|Aklan|Albay|Angeles|Antique|Aurora|Bacolod|Bago|Baguio|Bais|Basilan|Basilan City|Bataan|Batanes|Batangas|Batangas City|Benguet|Bohol|Bukidnon|Bulacan|Butuan|Cabanatuan|Cadiz|Cagayan|Cagayan de Oro|Calbayog|Caloocan|Camarines Norte|Camarines Sur|Camiguin|Canlaon|Capiz|Catanduanes|Cavite|Cavite City|Cebu|Cebu City|Cotabato|Dagupan|Danao|Dapitan|Davao City Davao|Davao del Sur|Davao Oriental|Dipolog|Dumaguete|Eastern Samar|General Santos|Gingoog|Ifugao|Iligan|Ilocos Norte|Ilocos Sur|Iloilo|Iloilo City|Iriga|Isabela|Kalinga-Apayao|La Carlota|La Union|Laguna|Lanao del Norte|Lanao del Sur|Laoag|Lapu-Lapu|Legaspi|Leyte|Lipa|Lucena|Maguindanao|Mandaue|Manila|Marawi|Marinduque|Masbate|Mindoro Occidental|Mindoro Oriental|Misamis Occidental|Misamis Oriental|Mountain|Naga|Negros Occidental|Negros Oriental|North Cotabato|Northern Samar|Nueva Ecija|Nueva Vizcaya|Olongapo|Ormoc|Oroquieta|Ozamis|Pagadian|Palawan|Palayan|Pampanga|Pangasinan|Pasay|Puerto Princesa|Quezon|Quezon City|Quirino|Rizal|Romblon|Roxas|Samar|San Carlos (in Negros Occidental)|San Carlos (in Pangasinan)|San Jose|San Pablo|Silay|Siquijor|Sorsogon|South Cotabato|Southern Leyte|Sultan Kudarat|Sulu|Surigao|Surigao del Norte|Surigao del Sur|Tacloban|Tagaytay|Tagbilaran|Tangub|Tarlac|Tawitawi|Toledo|Trece Martires|Zambales|Zamboanga|Zamboanga del Norte|Zamboanga del Sur";
		$data['states'][185]="Pitcaim Islands";
		$data['states'][186]="Dolnoslaskie|Kujawsko-Pomorskie|Lodzkie|Lubelskie|Lubuskie|Malopolskie|Mazowieckie|Opolskie|Podkarpackie|Podlaskie|Pomorskie|Slaskie|Swietokrzyskie|Warminsko-Mazurskie|Wielkopolskie|Zachodniopomorskie";
		$data['states'][187]="Acores (Azores)|Aveiro|Beja|Braga|Braganca|Castelo Branco|Coimbra|Evora|Faro|Guarda|Leiria|Lisboa|Madeira|Portalegre|Porto|Santarem|Setubal|Viana do Castelo|Vila Real|Viseu";
		$data['states'][188]="Adjuntas|Aguada|Aguadilla|Aguas Buenas|Aibonito|Anasco|Arecibo|Arroyo|Barceloneta|Barranquitas|Bayamon|Cabo Rojo|Caguas|Camuy|Canovanas|Carolina|Catano|Cayey|Ceiba|Ciales|Cidra|Coamo|Comerio|Corozal|Culebra|Dorado|Fajardo|Florida|Guanica|Guayama|Guayanilla|Guaynabo|Gurabo|Hatillo|Hormigueros|Humacao|Isabela|Jayuya|Juana Diaz|Juncos|Lajas|Lares|Las Marias|Las Piedras|Loiza|Luquillo|Manati|Maricao|Maunabo|Mayaguez|Moca|Morovis|Naguabo|Naranjito|Orocovis|Patillas|Penuelas|Ponce|Quebradillas|Rincon|Rio Grande|Sabana Grande|Salinas|San German|San Juan|San Lorenzo|San Sebastian|Santa Isabel|Toa Alta|Toa Baja|Trujillo Alto|Utuado|Vega Alta|Vega Baja|Vieques|Villalba|Yabucoa|Yauco";
		$data['states'][189]="Ad Dawhah|Al Ghuwayriyah|Al Jumayliyah|Al Khawr|Al Wakrah|Ar Rayyan|Jarayan al Batinah|Madinat ash Shamal|Umm Salal";
		$data['states'][190]="Reunion";
		$data['states'][191]="Alba|Arad|Arges|Bacau|Bihor|Bistrita-Nasaud|Botosani|Braila|Brasov|Bucuresti|Buzau|Calarasi|Caras-Severin|Cluj|Constanta|Covasna|Dimbovita|Dolj|Galati|Giurgiu|Gorj|Harghita|Hunedoara|Ialomita|Iasi|Maramures|Mehedinti|Mures|Neamt|Olt|Prahova|Salaj|Satu Mare|Sibiu|Suceava|Teleorman|Timis|Tulcea|Vaslui|Vilcea|Vrancea";
		$data['states'][192]="Adygeya (Maykop)|Aginskiy Buryatskiy (Aginskoye)|Altay (Gorno-Altaysk)|Altayskiy (Barnaul)|Amurskaya (Blagoveshchensk)|Arkhangel'skaya|Astrakhanskaya|Bashkortostan (Ufa)|Belgorodskaya|Bryanskaya|Buryatiya (Ulan-Ude)|Chechnya (Groznyy)|Chelyabinskaya|Chitinskaya|Chukotskiy (Anadyr')|Chuvashiya (Cheboksary)|Dagestan (Makhachkala)|Evenkiyskiy (Tura)|Ingushetiya (Nazran')|Irkutskaya|Ivanovskaya|Kabardino-Balkariya (Nal'chik)|Kaliningradskaya|Kalmykiya (Elista)|Kaluzhskaya|Kamchatskaya (Petropavlovsk-Kamchatskiy)|Karachayevo-Cherkesiya (Cherkessk)|Kareliya (Petrozavodsk)|Kemerovskaya|Khabarovskiy|Khakasiya (Abakan)|Khanty-Mansiyskiy (Khanty-Mansiysk)|Kirovskaya|Komi (Syktyvkar)|Komi-Permyatskiy (Kudymkar)|Koryakskiy (Palana)|Kostromskaya|Krasnodarskiy|Krasnoyarskiy|Kurganskaya|Kurskaya|Leningradskaya|Lipetskaya|Magadanskaya|Mariy-El (Yoshkar-Ola)|Mordoviya (Saransk)|Moskovskaya|Moskva (Moscow)|Murmanskaya|Nenetskiy (Nar'yan-Mar)|Nizhegorodskaya|Novgorodskaya|Novosibirskaya|Omskaya|Orenburgskaya|Orlovskaya (Orel)|Penzenskaya|Permskaya|Primorskiy (Vladivostok)|Pskovskaya|Rostovskaya|Ryazanskaya|Sakha (Yakutsk)|Sakhalinskaya (Yuzhno-Sakhalinsk)|Samarskaya|Sankt-Peterburg (Saint Petersburg)|Saratovskaya|Severnaya Osetiya-Alaniya [North Ossetia] (Vladikavkaz)|Smolenskaya|Stavropol'skiy|Sverdlovskaya (Yekaterinburg)|Tambovskaya|Tatarstan (Kazan')|Taymyrskiy (Dudinka)|Tomskaya|Tul'skaya|Tverskaya|Tyumenskaya|Tyva (Kyzyl)|Udmurtiya (Izhevsk)|Ul'yanovskaya|Ust'-Ordynskiy Buryatskiy (Ust'-Ordynskiy)|Vladimirskaya|Volgogradskaya|Vologodskaya|Voronezhskaya|Yamalo-Nenetskiy (Salekhard)|Yaroslavskaya|Yevreyskaya";
		$data['states'][193]="Butare|Byumba|Cyangugu|Gikongoro|Gisenyi|Gitarama|Kibungo|Kibuye|Kigali Rurale|Kigali-ville|Ruhengeri|Umutara";
		$data['states'][194]="Saint-Barthélemy";
		$data['states'][195]="Saint Helena";
		$data['states'][196]="Christ Church Nichola Town|Saint Anne Sandy Point|Saint George Basseterre|Saint George Gingerland|Saint James Windward|Saint John Capisterre|Saint John Figtree|Saint Mary Cayon|Saint Paul Capisterre|Saint Paul Charlestown|Saint Peter Basseterre|Saint Thomas Lowland|Saint Thomas Middle Island|Trinity Palmetto Point";
		$data['states'][197]="Anse-la-Raye|Castries|Choiseul|Dauphin|Dennery|Gros Islet|Laborie|Micoud|Praslin|Soufriere|Vieux Fort";
		$data['states'][198]="Saint Martin";
		$data['states'][199]="Miquelon|Saint Pierre";
		$data['states'][200]="Charlotte|Grenadines|Saint Andrew|Saint David|Saint George|Saint Patrick";
		$data['states'][201]="A'ana|Aiga-i-le-Tai|Atua|Fa'asaleleaga|Gaga'emauga|Gagaifomauga|Palauli|Satupa'itea|Tuamasaga|Va'a-o-Fonoti|Vaisigano";
		$data['states'][202]="Acquaviva|Borgo Maggiore|Chiesanuova|Domagnano|Faetano|Fiorentino|Monte Giardino|San Marino|Serravalle";
		$data['states'][203]="Principe|Sao Tome";
		$data['states'][204]="'Asir|Al Bahah|Al Hudud ash Shamaliyah|Al Jawf|Al Madinah|Al Qasim|Ar Riyad|Ash Sharqiyah (Eastern Province)|Ha'il|Jizan|Makkah|Najran|Tabuk";
		$data['states'][205]="Aberdeen City|Aberdeenshire|Angus|Argyll and Bute|City of Edinburgh|Clackmannanshire|Dumfries and Galloway|Dundee City|East Ayrshire|East Dunbartonshire|East Lothian|East Renfrewshire|Eilean Siar (Western Isles)|Falkirk|Fife|Glasgow City|Highland|Inverclyde|Midlothian|Moray|North Ayrshire|North Lanarkshire|Orkney Islands|Perth and Kinross|Renfrewshire|Shetland Islands|South Ayrshire|South Lanarkshire|Stirling|The Scottish Borders|West Dunbartonshire|West Lothian";
		$data['states'][206]="Dakar|Diourbel|Fatick|Kaolack|Kolda|Louga|Saint-Louis|Tambacounda|Thies|Ziguinchor";
		$data['states'][207]="Anse aux Pins|Anse Boileau|Anse Etoile|Anse Louis|Anse Royale|Baie Lazare|Baie Sainte Anne|Beau Vallon|Bel Air|Bel Ombre|Cascade|Glacis|Grand' Anse (on Mahe)|Grand' Anse (on Praslin)|La Digue|La Riviere Anglaise|Mont Buxton|Mont Fleuri|Plaisance|Pointe La Rue|Port Glaud|Saint Louis|Takamaka";
		$data['states'][208]="Eastern|Northern|Southern|Western";
		$data['states'][209]="Sint Maarten";
		$data['states'][210]="Singapore";
		$data['states'][211]="Banskobystricky|Bratislavsky|Kosicky|Nitriansky|Presovsky|Trenciansky|Trnavsky|Zilinsky";
		$data['states'][212]="Ajdovscina|Beltinci|Bled|Bohinj|Borovnica|Bovec|Brda|Brezice|Brezovica|Cankova-Tisina|Celje|Cerklje na Gorenjskem|Cerknica|Cerkno|Crensovci|Crna na Koroskem|Crnomelj|Destrnik-Trnovska Vas|Divaca|Dobrepolje|Dobrova-Horjul-Polhov Gradec|Dol pri Ljubljani|Domzale|Dornava|Dravograd|Duplek|Gorenja Vas-Poljane|Gorisnica|Gornja Radgona|Gornji Grad|Gornji Petrovci|Grosuplje|Hodos Salovci|Hrastnik|Hrpelje-Kozina|Idrija|Ig|Ilirska Bistrica|Ivancna Gorica|Izola|Jesenice|Jursinci|Kamnik|Kanal|Kidricevo|Kobarid|Kobilje|Kocevje|Komen|Koper|Kozje|Kranj|Kranjska Gora|Krsko|Kungota|Kuzma|Lasko|Lenart|Lendava|Litija|Ljubljana|Ljubno|Ljutomer|Logatec|Loska Dolina|Loski Potok|Luce|Lukovica|Majsperk|Maribor|Medvode|Menges|Metlika|Mezica|Miren-Kostanjevica|Mislinja|Moravce|Moravske Toplice|Mozirje|Murska Sobota|Muta|Naklo|Nazarje|Nova Gorica|Novo Mesto|Odranci|Ormoz|Osilnica|Pesnica|Piran|Pivka|Podcetrtek|Podvelka-Ribnica|Postojna|Preddvor|Ptuj|Puconci|Race-Fram|Radece|Radenci|Radlje ob Dravi|Radovljica|Ravne-Prevalje|Ribnica|Rogasevci|Rogaska Slatina|Rogatec|Ruse|Semic|Sencur|Sentilj|Sentjernej|Sentjur pri Celju|Sevnica|Sezana|Skocjan|Skofja Loka|Skofljica|Slovenj Gradec|Slovenska Bistrica|Slovenske Konjice|Smarje pri Jelsah|Smartno ob Paki|Sostanj|Starse|Store|Sveti Jurij|Tolmin|Trbovlje|Trebnje|Trzic|Turnisce|Velenje|Velike Lasce|Videm|Vipava|Vitanje|Vodice|Vojnik|Vrhnika|Vuzenica|Zagorje ob Savi|Zalec|Zavrc|Zelezniki|Ziri|Zrece";
		$data['states'][213]="Bellona|Central|Choiseul (Lauru)|Guadalcanal|Honiara|Isabel|Makira|Malaita|Rennell|Temotu|Western";
		$data['states'][214]="Awdal|Bakool|Banaadir|Bari|Bay|Galguduud|Gedo|Hiiraan|Jubbada Dhexe|Jubbada Hoose|Mudug|Nugaal|Sanaag|Shabeellaha Dhexe|Shabeellaha Hoose|Sool|Togdheer|Woqooyi Galbeed";
		$data['states'][215]="Eastern Cape|Free State|Gauteng|KwaZulu-Natal|Mpumalanga|North-West|Northern Cape|Northern Province|Western Cape";
		$data['states'][216]="Bird Island|Bristol Island|Clerke Rocks|Montagu Island|Saunders Island|South Georgia|Southern Thule|Traversay Islands";
		$data['states'][217]="Eastern Equatoria|Central Equatoria (containing the national capital city of Juba)|Jonglei|Lakes|Northern Bahr el Ghazal|Warrap|Western Bahr el Ghazal|Western Equatoria|Unity|Upper Nile";
		$data['states'][218]="Andalucia|Aragon|Asturias|Baleares (Balearic Islands)|Canarias (Canary Islands)|Cantabria|Castilla y Leon|Castilla-La Mancha|Cataluna|Ceuta|Communidad Valencian|Extremadura|Galicia|Islas Chafarinas|La Rioja|Madrid|Melilla|Murcia|Navarra|Pais Vasco (Basque Country)|Penon de Alhucemas|Penon de Velez de la Gomera";
		$data['states'][219]="Spratly Islands";
		$data['states'][220]="Central|Eastern|North Central|North Eastern|North Western|Northern|Sabaragamuwa|Southern|Uva|Western";
		$data['states'][221]="A'ali an Nil|Al Bahr al Ahmar|Al Buhayrat|Al Jazirah|Al Khartum|Al Qadarif|Al Wahdah|An Nil al Abyad|An Nil al Azraq|Ash Shamaliyah|Bahr al Jabal|Gharb al Istiwa'iyah|Gharb Bahr al Ghazal|Gharb Darfur|Gharb Kurdufan|Janub Darfur|Janub Kurdufan|Junqali|Kassala|Nahr an Nil|Shamal Bahr al Ghazal|Shamal Darfur|Shamal Kurdufan|Sharq al Istiwa'iyah|Sinnar|Warab";
		$data['states'][222]="Brokopondo|Commewijne|Coronie|Marowijne|Nickerie|Para|Paramaribo|Saramacca|Sipaliwini|Wanica";
		$data['states'][223]="Barentsoya|Bjornoya|Edgeoya|Hopen|Kvitoya|Nordaustandet|Prins Karls Forland|Spitsbergen";
		$data['states'][224]="Hhohho|Lubombo|Manzini|Shiselweni";
		$data['states'][225]="Blekinge|Dalarnas|Gavleborgs|Gotlands|Hallands|Jamtlands|Jonkopings|Kalmar|Kronobergs|Norrbottens|Orebro|Ostergotlands|Skane|Sodermanlands|Stockholms|Uppsala|Varmlands|Vasterbottens|Vasternorrlands|Vastmanlands|Vastra Gotalands";
		$data['states'][226]="Aargau|Ausser-Rhoden|Basel-Landschaft|Basel-Stadt|Bern|Fribourg|Geneve|Glarus|Graubunden|Inner-Rhoden|Jura|Luzern|Neuchatel|Nidwalden|Obwalden|Sankt Gallen|Schaffhausen|Schwyz|Solothurn|Thurgau|Ticino|Uri|Valais|Vaud|Zug|Zurich";
		$data['states'][227]="Al Hasakah|Al Ladhiqiyah|Al Qunaytirah|Ar Raqqah|As Suwayda'|Dar'a|Dayr az Zawr|Dimashq|Halab|Hamah|Hims|Idlib|Rif Dimashq|Tartus";
		$data['states'][228]="Chang-hua|Chi-lung|Chia-i|Chia-i|Chung-hsing-hsin-ts'un|Hsin-chu|Hsin-chu|Hua-lien|I-lan|Kao-hsiung|Kao-hsiung|Miao-li|Nan-t'ou|P'eng-hu|P'ing-tung|T'ai-chung|T'ai-chung|T'ai-nan|T'ai-nan|T'ai-pei|T'ai-pei|T'ai-tung|T'ao-yuan|Yun-lin";
		$data['states'][229]="Viloyati Khatlon|Viloyati Leninobod|Viloyati Mukhtori Kuhistoni Badakhshon";
		$data['states'][230]="Arusha|Dar es Salaam|Dodoma|Iringa|Kagera|Kigoma|Kilimanjaro|Lindi|Mara|Mbeya|Morogoro|Mtwara|Mwanza|Pemba North|Pemba South|Pwani|Rukwa|Ruvuma|Shinyanga|Singida|Tabora|Tanga|Zanzibar Central/South|Zanzibar North|Zanzibar Urban/West";
		$data['states'][231]="Amnat Charoen|Ang Thong|Buriram|Chachoengsao|Chai Nat|Chaiyaphum|Chanthaburi|Chiang Mai|Chiang Rai|Chon Buri|Chumphon|Kalasin|Kamphaeng Phet|Kanchanaburi|Khon Kaen|Krabi|Krung Thep Mahanakhon (Bangkok)|Lampang|Lamphun|Loei|Lop Buri|Mae Hong Son|Maha Sarakham|Mukdahan|Nakhon Nayok|Nakhon Pathom|Nakhon Phanom|Nakhon Ratchasima|Nakhon Sawan|Nakhon Si Thammarat|Nan|Narathiwat|Nong Bua Lamphu|Nong Khai|Nonthaburi|Pathum Thani|Pattani|Phangnga|Phatthalung|Phayao|Phetchabun|Phetchaburi|Phichit|Phitsanulok|Phra Nakhon Si Ayutthaya|Phrae|Phuket|Prachin Buri|Prachuap Khiri Khan|Ranong|Ratchaburi|Rayong|Roi Et|Sa Kaeo|Sakon Nakhon|Samut Prakan|Samut Sakhon|Samut Songkhram|Sara Buri|Satun|Sing Buri|Sisaket|Songkhla|Sukhothai|Suphan Buri|Surat Thani|Surin|Tak|Trang|Trat|Ubon Ratchathani|Udon Thani|Uthai Thani|Uttaradit|Yala|Yasothon";
		$data['states'][232]="De La Kara|Des Plateaux|Des Savanes|Du Centre|Maritime";
		$data['states'][233]="Atafu|Fakaofo|Nukunonu";
		$data['states'][234]="Ha'apai|Tongatapu|Vava'u";
		// $data['states'][235]="Arima|Caroni|Mayaro|Nariva|Port-of-Spain|Saint Andrew|Saint David|Saint George|Saint Patrick|San Fernando|Victoria";
		$data['states'][235]="Arima|Chaguanas|Couva-Tabaquite-Talpari|Diego Martin|Penal-Debe|Point Fortin|Port of Spain|Princes Town|Rio Claro-Mayaro|Sangre Grande|San Fernando|San Juan-Laventille|Separia|Tobago|Tunapuna-Piarco";
		$data['states'][236]="Ariana|Beja|Ben Arous|Bizerte|El Kef|Gabes|Gafsa|Jendouba|Kairouan|Kasserine|Kebili|Mahdia|Medenine|Monastir|Nabeul|Sfax|Sidi Bou Zid|Siliana|Sousse|Tataouine|Tozeur|Tunis|Zaghouan";
		// $data['states'][236]="";
		$data['states'][237]="Adana|Adiyaman|Afyon|Agri|Aksaray|Amasya|Ankara|Antalya|Ardahan|Artvin|Aydin|Balikesir|Bartin|Batman|Bayburt|Bilecik|Bingol|Bitlis|Bolu|Burdur|Bursa|Canakkale|Cankiri|Corum|Denizli|Diyarbakir|Duzce|Edirne|Elazig|Erzincan|Erzurum|Eskisehir|Gaziantep|Giresun|Gumushane|Hakkari|Hatay|Icel|Igdir|Isparta|Istanbul|Izmir|Kahramanmaras|Karabuk|Karaman|Kars|Kastamonu|Kayseri|Kilis|Kirikkale|Kirklareli|Kirsehir|Kocaeli|Konya|Kutahya|Malatya|Manisa|Mardin|Mugla|Mus|Nevsehir|Nigde|Ordu|Osmaniye|Rize|Sakarya|Samsun|Sanliurfa|Siirt|Sinop|Sirnak|Sivas|Tekirdag|Tokat|Trabzon|Tunceli|Usak|Van|Yalova|Yozgat|Zonguldak";
		$data['states'][238]="Ahal Welayaty|Balkan Welayaty|Dashhowuz Welayaty|Lebap Welayaty|Mary Welayaty";
		$data['states'][239]="Tuvalu";
		$data['states'][240]="Adjumani|Apac|Arua|Bugiri|Bundibugyo|Bushenyi|Busia|Gulu|Hoima|Iganga|Jinja|Kabale|Kabarole|Kalangala|Kampala|Kamuli|Kapchorwa|Kasese|Katakwi|Kibale|Kiboga|Kisoro|Kitgum|Kotido|Kumi|Lira|Luwero|Masaka|Masindi|Mbale|Mbarara|Moroto|Moyo|Mpigi|Mubende|Mukono|Nakasongola|Nebbi|Ntungamo|Pallisa|Rakai|Rukungiri|Sembabule|Soroti|Tororo";
		$data['states'][241]="Avtonomna Respublika Krym (Simferopol')|Cherkas'ka (Cherkasy)|Chernihivs'ka (Chernihiv)|Chernivets'ka (Chernivtsi)|Dnipropetrovs'ka (Dnipropetrovs'k)|Donets'ka (Donets'k)|Ivano-Frankivs'ka (Ivano-Frankivs'k)|Kharkivs'ka (Kharkiv)|Khersons'ka (Kherson)|Khmel'nyts'ka (Khmel'nyts'kyy)|Kirovohrads'ka (Kirovohrad)|Kyyiv|Kyyivs'ka (Kiev)|L'vivs'ka (L'viv)|Luhans'ka (Luhans'k)|Mykolayivs'ka (Mykolayiv)|Odes'ka (Odesa)|Poltavs'ka (Poltava)|Rivnens'ka (Rivne)|Sevastopol'|Sums'ka (Sumy)|Ternopil's'ka (Ternopil')|Vinnyts'ka (Vinnytsya)|Volyns'ka (Luts'k)|Zakarpats'ka (Uzhhorod)|Zaporiz'ka (Zaporizhzhya)|Zhytomyrs'ka (Zhytomyr)";
		$data['states'][242]="'Ajman|Abu Zaby (Abu Dhabi)|Al Fujayrah|Ash Shariqah (Sharjah)|Dubayy (Dubai)|Ra's al Khaymah|Umm al Qaywayn";
		$data['states'][243]="Barking and Dagenham|Barnet|Barnsley|Bath and North East Somerset|Bedfordshire|Bexley|Birmingham|Blackburn with Darwen|Blackpool|Bolton|Bournemouth|Bracknell Forest|Bradford|Brent|Brighton and Hove|Bromley|Buckinghamshire|Bury|Calderdale|Cambridgeshire|Camden|Cheshire|City of Bristol|City of Kingston upon Hull|City of London|Cornwall|Coventry|Croydon|Cumbria|Darlington|Derby|Derbyshire|Devon|Doncaster|Dorset|Dudley|Durham|Ealing|East Riding of Yorkshire|East Sussex|Enfield|Essex|Gateshead|Gloucestershire|Greenwich|Hackney|Halton|Hammersmith and Fulham|Hampshire|Haringey|Harrow|Hartlepool|Havering|Herefordshire|Hertfordshire|Hillingdon|Hounslow|Isle of Wight|Islington|Kensington and Chelsea|Kent|Kingston upon Thames|Kirklees|Knowsley|Lambeth|Lancashire|Leeds|Leicester|Leicestershire|Lewisham|Lincolnshire|Liverpool|Luton|Manchester|Medway|Merton|Middlesbrough|Milton Keynes|Newcastle upon Tyne|Newham|Norfolk|North East Lincolnshire|North Lincolnshire|North Somerset|North Tyneside|North Yorkshire|Northamptonshire|Northumberland|Nottingham|Nottinghamshire|Oldham|Oxfordshire|Peterborough|Plymouth|Poole|Portsmouth|Reading|Redbridge|Redcar and Cleveland|Richmond upon Thames|Rochdale|Rotherham|Rutland|Salford|Sandwell|Sefton|Sheffield|Shropshire|Slough|Solihull|Somerset|South Gloucestershire|South Tyneside|Southampton|Southend-on-Sea|Southwark|St. Helens|Staffordshire|Stockport|Stockton-on-Tees|Stoke-on-Trent|Suffolk|Sunderland|Surrey|Sutton|Swindon|Tameside|Telford and Wrekin|Thurrock|Torbay|Tower Hamlets|Trafford|Wakefield|Walsall|Waltham Forest|Wandsworth|Warrington|Warwickshire|West Berkshire|West Sussex|Westminster|Wigan|Wiltshire|Windsor and Maidenhead|Wirral|Wokingham|Wolverhampton|Worcestershire|York";
		$data['states'][244]="Artigas|Canelones|Cerro Largo|Colonia|Durazno|Flores|Florida|Lavalleja|Maldonado|Montevideo|Paysandu|Rio Negro|Rivera|Rocha|Salto|San Jose|Soriano|Tacuarembo|Treinta y Tres";
		$data['states'][245]="Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|District of Columbia|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming";
		$data['states'][246]="Andijon Wiloyati|Bukhoro Wiloyati|Farghona Wiloyati|Jizzakh Wiloyati|Khorazm Wiloyati (Urganch)|Namangan Wiloyati|Nawoiy Wiloyati|Qashqadaryo Wiloyati (Qarshi)|Qoraqalpoghiston (Nukus)|Samarqand Wiloyati|Sirdaryo Wiloyati (Guliston)|Surkhondaryo Wiloyati (Termiz)|Toshkent Shahri|Toshkent Wiloyati";
		$data['states'][247]="Malampa|Penama|Sanma|Shefa|Tafea|Torba";
		$data['states'][248]="Amazonas|Anzoategui|Apure|Aragua|Barinas|Bolivar|Carabobo|Cojedes|Delta Amacuro|Dependencias Federales|Distrito Federal|Falcon|Guarico|Lara|Merida|Miranda|Monagas|Nueva Esparta|Portuguesa|Sucre|Tachira|Trujillo|Vargas|Yaracuy|Zulia";
		$data['states'][249]="An Giang|Ba Ria-Vung Tau|Bac Giang|Bac Kan|Bac Lieu|Bac Ninh|Ben Tre|Binh Dinh|Binh Duong|Binh Phuoc|Binh Thuan|Ca Mau|Can Tho|Cao Bang|Da Nang|Dac Lak|Dong Nai|Dong Thap|Gia Lai|Ha Giang|Ha Nam|Ha Noi|Ha Tay|Ha Tinh|Hai Duong|Hai Phong|Ho Chi Minh|Hoa Binh|Hung Yen|Khanh Hoa|Kien Giang|Kon Tum|Lai Chau|Lam Dong|Lang Son|Lao Cai|Long An|Nam Dinh|Nghe An|Ninh Binh|Ninh Thuan|Phu Tho|Phu Yen|Quang Binh|Quang Nam|Quang Ngai|Quang Ninh|Quang Tri|Soc Trang|Son La|Tay Ninh|Thai Binh|Thai Nguyen|Thanh Hoa|Thua Thien-Hue|Tien Giang|Tra Vinh|Tuyen Quang|Vinh Long|Vinh Phuc|Yen Bai";
		$data['states'][250]="Saint Croix|Saint John|Saint Thomas";
		$data['states'][251]="Blaenau Gwent|Bridgend|Caerphilly|Cardiff|Carmarthenshire|Ceredigion|Conwy|Denbighshire|Flintshire|Gwynedd|Isle of Anglesey|Merthyr Tydfil|Monmouthshire|Neath Port Talbot|Newport|Pembrokeshire|Powys|Rhondda Cynon Taff|Swansea|The Vale of Glamorgan|Torfaen|Wrexham";
		$data['states'][252]="Alo|Sigave|Wallis";
		$data['states'][253]="West Bank";
		$data['states'][254]="Western Sahara";
		$data['states'][255]="'Adan|'Ataq|Abyan|Al Bayda'|Al Hudaydah|Al Jawf|Al Mahrah|Al Mahwit|Dhamar|Hadhramawt|Hajjah|Ibb|Lahij|Ma'rib|Sa'dah|San'a'|Ta'izz";
		$data['states'][256]="Kosovo|Montenegro|Serbia|Vojvodina";
		$data['states'][257]="Central|Copperbelt|Eastern|Luapula|Lusaka|North-Western|Northern|Southern|Western";
		$data['states'][258]="Bulawayo|Harare|ManicalandMashonaland Central|Mashonaland East|Mashonaland West|Masvingo|Matabeleland North|Matabeleland South|Midlands";

    	return $data;
    }
}

?>