<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class SetAsDefaultDashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$customJs = array('fullcalendar.min','calendar','tables-dynamic');
        
        $urlAdd = 'doctorDashboard';
		$customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';        


		 # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in

		$this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Clinic Settings - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/index'
        ));
	}
}
?>