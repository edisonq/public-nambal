<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class NonNambal extends CI_Controller {

	public function __construct() 
    {
		parent::__construct();
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->barangayLogin = $this->session->userdata('barangay_login');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('Personalprofile_model', 'personalProfile');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Messages_model', 'messagesModel');
        $this->load->model('Emruser_model', 'emrUser');
        $this->navOpen = 'Patient records';

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
    
        # check user's if properly logged-in
        if (empty($this->nambal_session['sessionName']))
        {
            redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        //     $fbUser = $this->facebook->getUser();
        //     if (!empty($fbUser))
        //     {
        //         if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        //         {
        //             redirect(base_url().'logout', 'refresh');
        //         }
        //     } else {
        //         redirect(base_url().'login/facebook', 'refresh');
        //     }
        // }
        
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

            # check if the user is a registered doctor in nambal
            if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
            }
            # end of checking if the user is a registered doctor in nambal 
        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in
	}

	public function index()
	{
        $this->activeMainMenu = "Patients";
        $urlAdd = 'nonNambal';
		// $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        // $customJs = array('fullcalendar.min','calendar','tables-dynamic');
        $customCss = '';
        $customJs = '';
        $navOpen = $this->navOpen;
        $this->breadcrumb[] = array(
            'link' => 'nonNambal',
            'linkname' => 'Non-Nambal.com patients'
        );        


		$this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Non Nambal Patient  - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'profile-viewer/nonnambal-index'
        ));
        return;
	}

    public function create()
    {
        $this->activeMainMenu = "Patients";
        $urlAdd = 'nonNambal';
        $emailData = '';
        $customCss = '';
        $customJs = '';
        $errorMessage = '';
        $errorEmailMessage = '';
        $error = TRUE;
        $navOpen = $this->navOpen;
        $this->breadcrumb[] = array(
            'link' => 'nonNambal',
            'linkname' => 'Non-Nambal.com patients'
        );        

        $this->load->model('Barangaylist_model', 'barangays');
        $this->load->model('NonNambal_model', 'nonnambal');
        $this->load->model('ContactInformation_model', 'contactinfo');
        $this->load->model('Safeuseremail_model', 'safeuseremail');
        $this->load->model('Addresses_model', 'addresses');

        # get all barangays
        $this->barangayList = $this->barangays->getAllBarangay();

        # get the country in the nambal barangay session
        $this->country = @$this->barangayLogin->country;
        $this->province = @$this->barangayLogin->state;
        $this->registrationInfo['city'] = @$this->barangayLogin->city;
        $this->registrationInfo['postcode'] = @$this->barangayLogin->postcode;
        // $this->registrationInfo['street'] = @$this->barangayLogin->street;
       
        # for validation
        $this->form_validation->set_rules('firstname','First name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('middlename','Middle name', 'trim|xss_clean');
        $this->form_validation->set_rules('lastname','Last name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('sex','Gender', 'trim|xss_clean|required|max_length[45]|should_select');
        $this->form_validation->set_rules('emailaddress','Patient email address', 'trim|xss_clean|valid_email');
        $this->form_validation->set_rules('birthdate','Birthdate', 'trim|xss_clean|required');
        $this->form_validation->set_rules('bloodtype','Blood type', 'trim|xss_clean');
        $this->form_validation->set_rules('birthtime','Birthtime','trim|xss_clean');
        $this->form_validation->set_rules('country','Country','trim|xss_clean|required|should_select');
        $this->form_validation->set_rules('state','State or Province','trim|xss_clean|required|should_select');
        $this->form_validation->set_rules('street','Street','trim|xss_clean|required');
        $this->form_validation->set_rules('city','City','trim|xss_clean|required');
        $this->form_validation->set_rules('postcode','Postal or zip code','trim|xss_clean|required');
        $this->form_validation->set_rules('barangay','Barangay','trim|xss_clean');
        
        $this->form_validation->set_rules('addmothersname','Mothers name included','trim|xss_clean');
        $this->form_validation->set_rules('motherNambalmember','Mothers nambal member','trim|xss_clean');
        $this->form_validation->set_rules('motherNambalmemberName','Mothers name','trim|xss_clean');
        $this->form_validation->set_rules('doYouWantToBeContactInfoMother','Mothers name included','trim|xss_clean');
        $this->form_validation->set_rules('mothersemail','Mothers email address','trim|xss_clean|valid_email');
        $this->form_validation->set_rules('mothersnumber','Mothers contact number','trim|xss_clean');

        $this->form_validation->set_rules('addfathersname','Fathers nambal member','trim|xss_clean');
        $this->form_validation->set_rules('fatherNambalmember','Fathers name included','trim|xss_clean');
        $this->form_validation->set_rules('fatherNambalmemberName','Fathers name','trim|xss_clean');
        $this->form_validation->set_rules('doYouWantToBeContactInfoFather','Fathers name included','trim|xss_clean');
        $this->form_validation->set_rules('fathersemail','Fathers email address','trim|xss_clean|valid_email');
        $this->form_validation->set_rules('fathersnumber','Fathers contact number','trim|xss_clean');

        $this->form_validation->set_rules('doYouWantToBeContactInfo','Do you want to be contact info','trim|xss_clean');
        $this->form_validation->set_rules('generateNambalCard','Generate nambal card','trim|xss_clean');
        $this->registrationInfo['redirectdownloadsessionid'] = ucwords(filter_var(@$this->input->get('redirectdownloadsessionid'), FILTER_SANITIZE_STRING));

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }

        if (empty(filter_var($this->input->post('country'), FILTER_SANITIZE_STRING)))
        {
            $this->country = 'Philippines';
            $this->province = 'Cebu';
        }

         # get the patient information
        if ($_POST)
        {
            $this->registrationInfo['redirectdownloadsessionid'] = ucwords(filter_var(@$this->input->get('redirectdownloadsessionid'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['firstname'] = ucwords(filter_var(@$this->input->post('firstname'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['lastname'] = ucwords(filter_var(@$this->input->post('lastname'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['middlename'] = ucwords(filter_var(@$this->input->post('middlename'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['sex'] = ucwords(filter_var(@$this->input->post('sex'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['emailaddress'] = ucwords(filter_var(@$this->input->post('emailaddress'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['birthdate'] = ucwords(filter_var(@$this->input->post('birthdate'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['birthtime'] = ucwords(filter_var(@$this->input->post('birthtime'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['bloodtype'] = filter_var(@$this->input->post('bloodtype'), FILTER_SANITIZE_STRING);
            $this->country = ucwords(filter_var(@$this->input->post('country'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['country'] = $this->country;
            $this->province = ucwords(filter_var(@$this->input->post('state'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['state'] = $this->province;
            $this->registrationInfo['street'] = ucwords(filter_var(@$this->input->post('street'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['city'] = ucwords(filter_var(@$this->input->post('city'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['postcode'] = ucwords(filter_var(@$this->input->post('postcode'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['idbarangaylist'] = ucwords(filter_var(@$this->input->post('barangay'), FILTER_SANITIZE_STRING));
            
            $this->registrationInfo['addmothersname'] = filter_var(@$this->input->post('addmothersname'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['motherNambalmember'] = filter_var(@$this->input->post('motherNambalmember'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['motherNambalmemberName'] = ucwords(filter_var(@$this->input->post('motherNambalmemberName'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['doYouWantToBeContactInfoMother'] = filter_var(@$this->input->post('doYouWantToBeContactInfoMother'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['mothersemail'] = filter_var(@$this->input->post('mothersemail'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['countryCodeMother'] = filter_var(@$this->input->post('countryCodeMother'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['areaCodeMother'] = filter_var(@$this->input->post('areaCodeMother'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['mothersnumber'] = filter_var(@$this->input->post('mothersnumber'), FILTER_SANITIZE_STRING);

            $this->registrationInfo['addfathersname'] = filter_var(@$this->input->post('addfathersname'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['fatherNambalmember'] = filter_var(@$this->input->post('fatherNambalmember'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['fatherNambalmemberName'] = ucwords(filter_var(@$this->input->post('fatherNambalmemberName'), FILTER_SANITIZE_STRING));
            $this->registrationInfo['doYouWantToBeContactInfoFather'] = filter_var(@$this->input->post('doYouWantToBeContactInfoFather'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['fathersemail'] = filter_var(@$this->input->post('fathersemail'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['countryCodeFather'] = filter_var(@$this->input->post('countryCodeFather'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['areaCodeFather'] = filter_var(@$this->input->post('areaCodeFather'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['fathersnumber'] = filter_var(@$this->input->post('fathersnumber'), FILTER_SANITIZE_STRING);

            $this->registrationInfo['doYouWantToBeContactInfo'] = filter_var(@$this->input->post('doYouWantToBeContactInfo'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['generateNambalCard'] = filter_var(@$this->input->post('generateNambalCard'), FILTER_SANITIZE_STRING);
            $this->registrationInfo['addanotherpatient'] = filter_var(@$this->input->post('addanotherpatient'), FILTER_SANITIZE_STRING);
            
            if (!empty($this->registrationInfo['emailaddress']))
            {
                $this->registrationInfo['username'] = $this->registrationInfo['emailaddress'];
            }
            else
            {
                $this->registrationInfo['username'] = $this->registrationInfo['firstname'].$this->registrationInfo['lastname'].date('Y-m-d H:i:s');
            }
        }
        # check email of the patient exist ba sa database
        # if exist pa pilion ang user if i add ba sa existing db
        # or mag create og lain account using another email
        # $this->nonnambal->checkEmailExist($email);
        # or you can use this searchUsingEmailUsername($emailOrUsername) sa safeuser_model
        # or you can also use this check_email(username); sa signup_model
        if (!empty(@$this->registrationInfo['emailaddress']))
        {
            $emailexist = $this->nonnambal->checkEmailExist(@$this->registrationInfo['emailaddress']);
            if (@$emailexist == TRUE)
            {
                $error = TRUE;
                $errorEmailMessage .= '<br>Patient email address already exist<br>';
            }
        }

        # mother and father contact filtering.
        if ((!empty(@$this->registrationInfo['addmothersname'])) || (@$this->registrationInfo['addmothersname'] == TRUE))
        {
            if (    (!empty(@$this->registrationInfo['doYouWantToBeContactInfoMother'])) || 
                (@$this->registrationInfo['doYouWantToBeContactInfoMother'] == TRUE)
                )
            {
               if (   (empty($this->registrationInfo['countryCodeMother'])) || 
                        (empty($this->registrationInfo['areaCodeMother'])) ||
                        (empty($this->registrationInfo['mothersnumber']))
                ) {
                    $error = TRUE;
                }
            }
            if (empty($this->registrationInfo['motherNambalmemberName']))
            {
                $error = TRUE;
                $this->errorAddmothersName = 'Mothers name is empty';
            }
        }

        if ((!empty(@$this->registrationInfo['addfathersname'])) || (@$this->registrationInfo['addfathersname'] == TRUE))
        {
            
            if ( (!empty($this->registrationInfo['doYouWantToBeContactInfoFather'])) || 
                (@$this->registrationInfo['doYouWantToBeContactInfoFather'] == TRUE)
            )
            {
                
                if (   (empty($this->registrationInfo['countryCodeFather'])) || 
                        (empty($this->registrationInfo['areaCodeFather'])) ||
                        (empty($this->registrationInfo['fathersnumber']))
                ) {
                    $error = TRUE;
                }
            }
            if (empty($this->registrationInfo['fatherNambalmemberName']))
            {
                $error = TRUE;
                $this->errorAddfathersName = 'Fathers name is empty';
            }

        }


        # mother and father contact filtering.

        if ($error == FALSE)
        {
            $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
            $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

            #birthday segration
            $bdayArray = explode('/', $this->registrationInfo['birthdate']);
            $dateofbirth = $bdayArray[2].'-'.$bdayArray[0].'-'.$bdayArray[1];
            #end of birthday segration

            if (!empty($this->registrationInfo['birthtime']))
            {
                $this->registrationInfo['dateofbirth'] = $dateofbirth.' '.$this->registrationInfo['birthtime'].':00';
            }
            else
            {
                $this->registrationInfo['dateofbirth'] = $dateofbirth.' 00:00:00';
            }

            # add patient information here
            $returnNonnambal = $this->nonnambal->addPatient($this->registrationInfo);


            $this->registrationInfo['IDsafe_user'] = $returnNonnambal['IDsafe_user'];

            if (!empty($this->registrationInfo['emailaddress']))
            {
                $this->nonnambal->addPatientEmail($this->registrationInfo);
            }

            # add barangay if naa siya gi select "barangay
            # IDsafe_personalInfo
            $this->registrationInfo['IDsafe_personalInfo'] = $returnNonnambal['IDsafe_personalInfo'];
            $this->nonnambal->addBarangay($this->registrationInfo);

            # add mothers name if naka select "addmothersname"
            # connect to mother nambal account if naka check ang "motherNambalmember"
            # if wala maka check ang "motherNambalmember" .. use "motherNambalmemberName" in adding mother name
            # check if ipa add as contact information ang mother using "doYouWantToBeContactInfoMother"
            if ((!empty($this->registrationInfo['addmothersname'])) || ($this->registrationInfo['addmothersname'] == TRUE))
            {
                $this->registrationInfo['parentGender'] = 'mother';
                $this->registrationInfo['parentName'] = @$this->registrationInfo['motherNambalmemberName'];

                if (!empty($this->registrationInfo['addmothersname']))
                {
                    $this->registrationInfo['contactfullname'] = $this->registrationInfo['motherNambalmemberName'];
                }
                // if ((!empty($this->registrationInfo['motherNambalmemberName'])) || ($this->registrationInfo['motherNambalmemberName'] == TRUE))
                // {
                //     # get the IDsafe_user of the parents for parentSafeID
                //     // $this->registrationInfo['parentSafeID'] = $this->registrationInfo['addmothersname'];
                // }

                # add the parents
                $this->nonnambal->addParent($this->registrationInfo);

                if ((!empty($this->registrationInfo['doYouWantToBeContactInfoMother'])) || ($this->registrationInfo['doYouWantToBeContactInfoMother'] == TRUE))
                {
                    $this->registrationInfo['countrycode'] = $this->registrationInfo['countryCodeMother'];
                    $this->registrationInfo['contactAreacode'] = $this->registrationInfo['areaCodeMother'];
                    $this->registrationInfo['contactnumbers'] = $this->registrationInfo['mothersnumber'];
                    $this->registrationInfo['contactemail'] = $this->registrationInfo['mothersemail'];

                    # temporarily set mother as primary contact
                    # kung ni exist iya mother
                    if ((!empty($this->registrationInfo['doYouWantToBeContactInfoFather'])) || ($this->registrationInfo['doYouWantToBeContactInfoFather'] == TRUE))
                        $this->registrationInfo['primaryni'] = TRUE;
                    # end: temporarily set mother as primary contact

                    $this->nonnambal->addContact($this->registrationInfo);
                }
            }

            # add father name if naka select "addfathersname"
            # connect to father nambal account if naka check ang "fatherNambalmember"
            # if wala maka check ang "fatherNambalmember" .. use "fatherNambalmemberName" in adding mother name
            # check if ipa add as contact information ang father using "doYouWantToBeContactInfoFather"
            if ((!empty($this->registrationInfo['addfathersname'])) || ($this->registrationInfo['addfathersname'] == TRUE))
            {
                $this->registrationInfo['parentGender'] = 'father';
                $this->registrationInfo['parentName'] = @$this->registrationInfo['fatherNambalmemberName'];

                if (!empty($this->registrationInfo['addfathersname']))
                {
                    $this->registrationInfo['contactfullname'] = $this->registrationInfo['fatherNambalmemberName'];
                }
                // if ((!empty($this->registrationInfo['motherNambalmemberName'])) || ($this->registrationInfo['motherNambalmemberName'] == TRUE))
                // {
                //     # get the IDsafe_user of the parents for parentSafeID
                //     // $this->registrationInfo['parentSafeID'] = $this->registrationInfo['addfathersname'];
                // }

                # add the parents
                $this->nonnambal->addParent($this->registrationInfo);

                if (
                    (!empty($this->registrationInfo['doYouWantToBeContactInfoFather'])) || 
                    ($this->registrationInfo['doYouWantToBeContactInfoFather'] == TRUE)
                    )
                {
                    $this->registrationInfo['countrycode'] = $this->registrationInfo['countryCodeFather'];
                    $this->registrationInfo['contactAreacode'] = $this->registrationInfo['areaCodeFather'];
                    $this->registrationInfo['contactnumbers'] = $this->registrationInfo['fathersnumber'];
                    $this->registrationInfo['contactemail'] = $this->registrationInfo['fathersemail'];

                    # temporarily set mother as primary contact
                    # kung ni exist iya mother as contact
                    if (
                        ((@$this->registrationInfo['doYouWantToBeContactInfoMother'] == TRUE) && (@$this->registrationInfo['addmothersname']))
                        )
                        $this->registrationInfo['primaryni'] = FALSE;
                    else
                        $this->registrationInfo['primaryni'] = TRUE;
                    # end: temporarily set mother as primary contact

                    $this->nonnambal->addContact($this->registrationInfo);
                }
            }

            # if naka check ang "doYouWantToBeContactInfo", i add sa emergency contatc information ang user sa patient
            if (    (!empty(@$this->registrationInfo['doYouWantToBeContactInfo'])) || 
                (@$this->registrationInfo['doYouWantToBeContactInfo'] == TRUE))
            {
                $emailsession = $this->safeuseremail->getuseremaildefault($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
                $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
                $addresssession = $this->addresses->getPersonalInfoAddress($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
                $doctorPhone = $this->emrUser->getDefaultNumber($doctorInformation->IDemr_user);
                $dataDoctorAdder = array(
                    'IDemr_user' => $doctorInformation->IDemr_user,
                    'IDsafe_personalInfo' => $returnNonnambal['IDsafe_personalInfo']
                );
                $this->nonnambal->doctorAdder($dataDoctorAdder);

                # just retrieve any clinic number, for now
                $this->registrationInfo['countrycode'] = @$doctorPhone[0]->countrycode;
                $this->registrationInfo['contactAreacode'] = @$doctorPhone[0]->areacode;
                $this->registrationInfo['contactnumbers'] = @$doctorPhone[0]->phonenumber;
                $this->registrationInfo['contactfulladdress'] = $addresssession->street.', '.$addresssession->city.', '.$addresssession->state.', '.$addresssession->country.' '.$addresssession->postcode;
                $this->registrationInfo['contactemail'] = $this->nagkamoritsing->ibalik(@$emailsession->emailAddress);
                $this->registrationInfo['contactfullname'] = $this->nagkamoritsing->ibalik(@$this->nambal_session['firstname']).' '.$this->nagkamoritsing->ibalik(@$this->nambal_session['lastname']);
                
                # set false if naa na ang father or mother sa contact
                # pero kung wala set to true
                if (
                    ((@$this->registrationInfo['doYouWantToBeContactInfoMother'] == TRUE) && (@$this->registrationInfo['addmothersname'])) || 
                    ((@$this->registrationInfo['doYouWantToBeContactInfoFather'] == TRUE) && (@$this->registrationInfo['addfathersname'])) 
                    )
                    $this->registrationInfo['primaryni'] = FALSE;
                else
                    $this->registrationInfo['primaryni'] = TRUE;

                $this->nonnambal->addContact($this->registrationInfo);
            }

            # redirect to generate nambal card sa patient if naka check ang "generateNambalCard"
            if (@$this->registrationInfo['generateNambalCard'] == TRUE)
                $extendedurl = '&redirectdownloadsessionid='.$this->nagkamoritsing->bungkag($returnNonnambal['IDsafe_user']);
            else
                $extendedurl = '';

            # display add form if naka check ang "addanotherpatient" else display success page.
            // profileViewer/doctorViewing?sessionid=3826162015011632342
            if (@$this->registrationInfo['addanotherpatient'] == TRUE)
            {
                if (!empty($extendedurl))
                    redirect(base_url().'nonNambal/create?addanotherpatient=true'.$extendedurl, 'refresh');
                else
                    redirect(base_url().'nonNambal/create?', 'refresh');
            }
            else
                redirect(base_url().'profileViewer/doctorViewing?sessionid='.$this->nagkamoritsing->bungkag($returnNonnambal['IDsafe_user']).$extendedurl, 'refresh');

            # if naa ang email "emailaddress kay dili empty kay mo email sa patient
            # nga naa siya account sa nambal
            # send the $returnNonnambal['vcode'] to the patient
            # email sending
            if (!empty(@$this->registrationInfo['emailaddress']))
            {
                $validationUrl = base_url().'register/verifyEmail?email='.$this->registrationInfo['emailaddress'].'&validationCode='.$returnNonnambal['vcode'];
                $this->email->from('no-reply@nambal.com', 'Nambal Information');
                $this->email->to($this->registrationInfo['emailaddress']);

                // // $this->email->cc('edisonq@yahoo.com');
                // // $this->email->bcc('edisonq@hotmail.com');
                 
                $this->email->subject('Your doctor welcomes you to nambal');
                $emailData = array(
                    'firstname' => ucwords($this->registrationInfo['firstname']),
                    'lastname' => ucwords($this->registrationInfo['lastname']),
                    'vcode' => $returnNonnambal['vcode'],
                    'username' =>  $this->registrationInfo['username'],
                    'validationUrl' => $validationUrl
                );

                $this->email->message(
                    $this->load->view(
                            'for-email.phtml', 
                            array('title' => 'Appointment approved', 
                            'view' => 'emails/adding-patient-using-doctor-dashboard', 
                            'emailData' => $emailData, 'footerEmail' => $this->registrationInfo['emailaddress']
                            ), true)
                );
                $this->email->send();
            }
            # end of email sending
        }
        
        
        // $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        // $customJs = array('fullcalendar.min','calendar','tables-dynamic');
        
        
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Non Nambal Patient  - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'errorMessage' => $errorMessage,
            'errorEmailMessage' => $errorEmailMessage,
            'view' => 'register/register-nonnambal-doctor'
        ));
        return;
    }
}
?>