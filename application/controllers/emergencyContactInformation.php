<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class EmergencyContactInformation extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library("pagination");
		$this->load->model('ContactInformation_model', 'contactinfo');
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('Addresses_model', 'addresses');
		$this->load->model('Safeuseremail_model', 'safeuseremail');
		$this->load->model('Emruser_model', 'emrUser');
		$this->load->model('Safeuser_model', 'safeuser');
		$this->load->model('Safejournal_model','safejournal');
		$this->load->model('SafeuserLog_model','safeuserlog');
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->activeMainMenu = "wellness-profile";
        $this->navOpen = "Wellness profile";

		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

       	# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP     
        }
        # end of redirecting users to dashboard if logged in


        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);


         $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
         # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
	}

	public function index()
	{
		// $this->load->view(
		// 	'homepage.phtml', array('title' => 'Emeregency Contact Information', 
		// 	'view' => 'emergency-contact-information/index'));
		// $sampleData = $this->input->get('sampleData');
		$arrayGetContact = array();
        $customCss = array('custom-dashboard','patient-dashboard');
        $customJs = array('jquery.flot.min','jquery.flot.pie','jquery.flot.resize.min', 'jquery.stateselect', 'jquery.selectboxes.pack', 'custom-connect-doctor-search-short','../lib/select2', 'forms-elemets', 'custom-dashboard', 'countries');
        
    	$arrayGetContact = array();
		$dataRetrieveGetContact = array(
            'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
        );
		$arrayGetContact = $this->contactinfo->getAllContact($dataRetrieveGetContact);

		$this->load->view(
			//'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'My Emergency Contact Information',
			'view' => 'emergency-contact-information/index', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'arrayGetContact' => $arrayGetContact,
			'css' => 'patient-dashboard'));

		return;
	}

	public function insert()
	{
		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  
        }
        # end of redirecting users to dashboard if logged in
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$safeContact = array();
		$customCss = array('custom-dashboard');
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countryCode','Country code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('areaCode','Area code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('number','Phone number', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('fulladdress', 'Full Address', 'trim|xss_clean"required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}

		if ($error == TRUE) 
		{
			$errorMessage = validation_errors();
			
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$fullname = ucwords(filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING));
			$email = ucwords(filter_var($this->input->post('email'), FILTER_SANITIZE_STRING));
			$countryCode = filter_var($this->input->post('countryCode'), FILTER_SANITIZE_STRING);
			$areaCode = filter_var($this->input->post('areaCode'), FILTER_SANITIZE_STRING);
			$number = filter_var($this->input->post('number'), FILTER_SANITIZE_STRING);
			$countryCode = preg_replace('/[^A-Za-z0-9-]/', '', $countryCode);
			$fulladdress = filter_var($this->input->post('fulladdress'), FILTER_SANITIZE_STRING);

			$safeContact = array(
				'fullname' => $fullname,
				'email' => $email,
				'countrycode' => $countryCode,
				'areacode' => $areaCode,
				'number' => $number,
				'fulladdress' => $fulladdress
			);			
			$this->contactinfo->addContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $fullname, $email, $countryCode, $areaCode, $number, $fulladdress);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $safeContact)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Emergency Contacts',
			'view' => 'emergency-contact-information/insert', 
            'customCss' => $customCss,
            'safeContact' => $safeContact,
			'css' => 'dashboard-custom'));

			// $this->load->view(
			// 'homepage.phtml', array('title' => 'Insert contact information', 
			// 'view' => 'emergency-contact-information/insert',
			// 'customCss' => $customCss,
			// 'safeContact' => $safeContact));
			$sampleData = $this->input->get('sampleData');

		}

		return;
	}

	public function insertDoctor()
	{
		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  
        }
        # end of redirecting users to dashboard if logged in
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$safeContact = array();
		$customCss = array('custom-dashboard');
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countryCode','Country code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('areaCode','Area code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('number','Phone number', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('fulladdress', 'Full Address', 'trim|xss_clean"required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
		}
		else
		{
			$error = false;
		}

		if ($error == TRUE) 
		{
			$errorMessage = validation_errors();
			
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
			$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
			$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo
			# get the IDsafe_personalInfo
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			$fullname = ucwords(filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING));
			$email = ucwords(filter_var($this->input->post('email'), FILTER_SANITIZE_STRING));
			$countryCode = filter_var($this->input->post('countryCode'), FILTER_SANITIZE_STRING);
			$areaCode = filter_var($this->input->post('areaCode'), FILTER_SANITIZE_STRING);
			$number = filter_var($this->input->post('number'), FILTER_SANITIZE_STRING);
			$countryCode = preg_replace('/[^A-Za-z0-9-]/', '', $countryCode);
			$fulladdress = filter_var($this->input->post('fulladdress'), FILTER_SANITIZE_STRING);

			$safeContact = array(
				'fullname' => $fullname,
				'email' => $email,
				'countrycode' => $countryCode,
				'areacode' => $areaCode,
				'number' => $number,
				'fulladdress' => $fulladdress
			);			
			$this->contactinfo->addContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $fullname, $email, $countryCode, $areaCode, $number, $fulladdress);

			# check if patient ba ni siya sa doctor
			# if dili i add siya as one of the patient
			$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
			$IDemr_patient = @$IDemr_patient->IDemr_patient;
			if (empty($IDemr_patient))
				$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

			$safeContact['IDsafe_personalInfo'] = $IDsafe_personalInfo;
			$safeContact['IDemr_user'] = $doctorInformation->IDemr_user;
			$safeContact['IDemr_patient'] = $IDemr_patient;

			$patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($IDsafe_user));
			$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
			$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);


			$emailData = array(
				'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
				'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
			);
			$message = $this->load->view(
		            'for-email.phtml', array('title' => 'Notify patient', 
		            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
			
			$logdata = array(
				'IDsafe_user' => $IDsafe_user,
				'activity' => 'doctor added a contact information',
				'activityDescription' => 'IDemr_user='.$safeContact['IDemr_user'].'|Dr. '.$this->nagkamoritsing->ibalik($this->nambal_session['fullname']).' Added a contact'
			);
			$this->safeuserlog->logUser($logdata); // kailangan sila mag kuyog kung mahimo
			$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $safeContact)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'patient-dashboard-light-blue.phtml', array(
            'title' => 'Emergency Contacts',
			'view' => 'emergency-contact-information/insert', 
            'customCss' => $customCss,
            'safeContact' => $safeContact,
			'css' => 'dashboard-custom'));

			// $this->load->view(
			// 'homepage.phtml', array('title' => 'Insert contact information', 
			// 'view' => 'emergency-contact-information/insert',
			// 'customCss' => $customCss,
			// 'safeContact' => $safeContact));
			$sampleData = $this->input->get('sampleData');

		}

		return;
	}

	public function update()
	{
		$justtest = '';
		$this->checkSession();
		$errorMessage = '';
		$ajax = TRUE;
		$error = TRUE;
		$customCss = array('custom-dashboard');
		$safeContact = array();
		$this->form_validation->set_rules('email','Email', 'trim|xss_clean|required|valid_email');
		$this->form_validation->set_rules('fullname','Full name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countryCode','Country code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('areaCode','Area code', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('number','Phone number', 'trim|xss_clean|required|numeric');
		$this->form_validation->set_rules('fulladdress', 'Full Address', 'trim|xss_clean|required');
		$this->form_validation->set_rules('IDsafe_contactinformation', 'IDsafe_contactinformation', 'trim|xss_clean|required');
		$this->form_validation->set_rules('IDsafe_personalInfo', 'IDsafe_personalInfo', 'trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = FALSE;
		}

		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

		$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
		$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);

		if ($error == FALSE)
		{
			$fullname = filter_var($this->input->post('fullname'), FILTER_SANITIZE_STRING);
			$email = filter_var($this->input->post('email'), FILTER_SANITIZE_STRING);
			$countryCode = filter_var($this->input->post('countryCode'), FILTER_SANITIZE_STRING);
			$countryCode = preg_replace('/[^A-Za-z0-9-]/', '', $countryCode);
			$areaCode = filter_var($this->input->post('areaCode'), FILTER_SANITIZE_STRING);
			$number = filter_var($this->input->post('number'), FILTER_SANITIZE_STRING);
			$fulladdress = filter_var($this->input->post('fulladdress'), FILTER_SANITIZE_STRING);
			$IDsafe_contactinformation = filter_var($this->input->post('IDsafe_contactinformation'), FILTER_SANITIZE_STRING);
			$IDsafe_personalInfo = filter_var($this->input->post('IDsafe_personalInfo'), FILTER_SANITIZE_STRING);

			$safeContact = array(
				'fullname' => ucwords($fullname),
				'email' => ucwords($email),
				'countrycode' => $countryCode,
				'areacode' => $areaCode,
				'number' => $number,
				'fulladdress' => $fulladdress,
				'IDsafe_contactinformation' => $IDsafe_contactinformation,
				'IDsafe_personalInfo' => $IDsafe_personalInfo
			);
			$dataInDb = $this->contactinfo->getContact($safeContact, $IDsafe_contactinformation);
			if ((strcasecmp($dataInDb['fullname'], $fullname)==0) && 
				(strcasecmp($dataInDb['email'], $email)==0) && 
				(strcasecmp($dataInDb['countrycode'], $countryCode)==0) &&
				(strcasecmp($dataInDb['areacode'], $areaCode)==0) &&
				(strcasecmp($dataInDb['number'], $number)==0) &&
				(strcasecmp($dataInDb['fulladdress'], $fulladdress)==0))
			{
				# no update needed all the same.
			}
			else
			{
				if ($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']) == $IDsafe_personalInfo)
				{
					
					$this->contactinfo->modifyContact($IDsafe_contactinformation, $safeContact);
				}
				else
				{
					
					if (strcasecmp($isdoctor, 'TRUE')==0)
					{
						$this->contactinfo->modifyContact($IDsafe_contactinformation, $safeContact);
						$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
						$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
						$IDemr_patient = @$IDemr_patient->IDemr_patient;
						if (empty($IDemr_patient))
							$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

						$safeContact['IDemr_user'] = $doctorInformation->IDemr_user;
						$safeContact['IDemr_patient'] = $IDemr_patient;

						# doctor notes connection and for sending email
						$this->safejournal->addjournal($safeContact,'safe_contactinformation');
						$patientinfo = $this->safeuser->getSafeUserInfoUsingPersonalInfo(@$IDsafe_personalInfo);
						$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($patientinfo->IDsafe_user);
						$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

						$emailData = array(
						'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
						 'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
						 );
						$message = $this->load->view(
						 'for-email.phtml', array('title' => 'Notify patient', 
						 'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);

						 # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
						 $this->safeuserlog->notifypatientbyemail($patientinfo->IDsafe_user,$message);
					}
				}			

			}
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage, $safeContact)
			);
			
			# end of ajax ang display
		}
		else
		{
			// $this->load->view(
			// 	'homepage.phtml', array('title' => 'Update Contact Information', 
			// 	'view' => 'emergency-contact-information/update'));
			// $sampleData = $this->input->get('sampleData');
			// echo 'profileID:'.$sampleData;
		}

		
		
		return;
	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		#check session
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
		$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);


		$this->form_validation->set_rules('idsafecontactinformation', 'IDsafe_contactinformation',  'trim|required|xss_clean');
		$this->form_validation->set_rules('personalinfoid', 'personal info id', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);	

		if ($error == FALSE)
		{
			$personalinfoid = filter_var($this->input->post('personalinfoid'), FILTER_SANITIZE_STRING);
			$idsafecontactinformation = filter_var($this->input->post('idsafecontactinformation'), FILTER_SANITIZE_STRING);

			# check if primary contact ba ni ang gi delete
			if ($this->contactinfo->checkifprimary($idsafecontactinformation) >= 1)
			{
				$jsonDisplay['errorStatus'] = TRUE;
				$jsonDisplay['errorMessage'] = 'Contact Information is Primary contact';
			}
			else
			{
				# just to prevent non doctor input
				# just to prevent non owner input
				if ($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']) == $personalinfoid)
					$this->contactinfo->deleteContact($idsafecontactinformation, $personalinfoid);
				else
				{
					if (strcasecmp($isdoctor, 'TRUE')==0)
					{
						$this->contactinfo->deleteContact($idsafecontactinformation, $personalinfoid);
					}
				}
			}		
		}
	
		if ($ajax == TRUE)
		{
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Delete Emeregency Contact Information', 
				'view' => 'emergency-contact-information/delete'));
		}
		// $sampleData = $this->input->get('sampleData');
		// echo 'profileID:'.$sampleData;

		return;
	}

	public function setasprimary()
	{
		$ajax = TRUE;
		$error = FALSE;
		#check session
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		$isdoctor = filter_var($this->input->get('isdoctor'), FILTER_SANITIZE_STRING);
		$isdoctor = preg_replace('#[^a-z0-9_]#', '', $isdoctor);
		$this->form_validation->set_rules('idsafecontactinformation', 'IDsafe_contactinformation',  'trim|required|xss_clean');
		$this->form_validation->set_rules('personalinfoid', 'personal info id', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$personalinfoid = filter_var($this->input->post('personalinfoid'), FILTER_SANITIZE_STRING);
			$idsafecontactinformation = filter_var($this->input->post('idsafecontactinformation'), FILTER_SANITIZE_STRING);

			if ($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']) == $personalinfoid)
				$this->contactinfo->setasprimary($idsafecontactinformation, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
			else
			{
				if (strcasecmp($isdoctor, 'TRUE')==0)
				{
					$this->contactinfo->setasprimary($idsafecontactinformation, $personalinfoid);
				}
			}
			
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
				'homepage.phtml', array('title' => 'Delete Emeregency Contact Information', 
				'view' => 'emergency-contact-information/delete'));
		}
		// $sampleData = $this->input->get('sampleData');
		// echo 'profileID:'.$sampleData;

		return;
	}

	public function dashboardDisplay()
	{
		$this->checkSession();
		$arrayGetContact = array();

        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
		
		$dataRetrieveGetContact = array(
            'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
        );
		$arrayGetContact = $this->contactinfo->getAllContact($dataRetrieveGetContact);

		 # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);


		# for paging result 
        # will work with custom-dashboard-emergency.js pagination code
        # and display_emergency-information-profileviewer.phtml div emergency-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'emergencyContactInformation/dashboardDisplay?p=0';
        $config["total_rows"] = $this->contactinfo->record_count($IDsafe_personalInfo);
        $config["per_page"] = 2;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->contactinfo->getAllContact($dataRetrieveGetContact, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

        if (empty($data))
			 $data["results"] = $this->contactinfo->getAllContact($dataRetrieveGetContact);

		$this->load->view('emergency-contact-information/display_emergency-information.phtml',  $data, array('arrayGetContact' => $arrayGetContact));
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;

		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->safedrug = $this->medication->displayDashboardMedication($IDsafe_personalInfo);

		$dataRetrieveGetContact = array(
            'IDsafe_personalInfo' => $IDsafe_personalInfo
        );
		$arrayGetContact = $this->contactinfo->getAllContact($dataRetrieveGetContact);

		# for paging result 
        # will work with custom-dashboard-test-profileviewer.js pagination code
        # and display_test-information-profileviewer.phtml div tests-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'emergencyContactInformation/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->contactinfo->record_count($IDsafe_personalInfo);
        $config["per_page"] = 2;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->contactinfo->getAllContact($dataRetrieveGetContact, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('emergency-contact-information/display_emergency-information-profileviewer.phtml', $data, array('arrayGetContact' => $arrayGetContact));
	}

	public function insertContactDisplay()
	{
		$this->checkSession();
		$arrayGetContact = array();

		$IDsafe_personalInfo = filter_var($this->input->get('idsafepersonalinfo'), FILTER_SANITIZE_STRING);
		$IDsafe_contactinformation = filter_var($this->input->get('IDsafe_contactinformation'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);

		$safeContact = $this->contactinfo->getContact($retData, $IDsafe_contactinformation);

		$this->load->view('emergency-contact-information/display_emergency-insert-information.phtml', array(
			'arrayGetContact' => $arrayGetContact, 
			'safeContact' => $safeContact
		));
	}

	public function insertContactDisplayDoctor()
	{
		$this->checkSession();
		$arrayGetContact = array();

		$IDsafe_personalInfo = filter_var($this->input->get('idsafepersonalinfo'), FILTER_SANITIZE_STRING);
		$IDsafe_contactinformation = filter_var($this->input->get('IDsafe_contactinformation'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_personalInfo' => $IDsafe_personalInfo,
			'IDsafe_contactinformation' => $IDsafe_contactinformation
		);

		$safeContact = $this->contactinfo->getContact($retData, $IDsafe_contactinformation);
		$addresssession = $this->addresses->getPersonalInfoAddress($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));
        $emailsession = $this->safeuseremail->getuseremaildefault($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $doctorPhone = $this->emrUser->getDefaultNumber(@$doctorInformation->IDemr_user);

		# retrieve name
		# retrieve number
		# retrieve full address
		$safeContact['fullname'] = ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['firstname'])).' '.ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['lastname']));
		$safeContact['fulladdress'] = ucwords(@$addresssession->street.', '.@$addresssession->city.', '.@$addresssession->state.', '.@$addresssession->country.', '.@$addresssession->postcode);
		$safeContact['email'] = ucwords($this->nagkamoritsing->ibalik(@$emailsession->emailAddress));
		$safeContact['countrycode'] = @$doctorPhone[0]->countrycode;
		$safeContact['areacode'] = @$doctorPhone[0]->areacode;
		$safeContact['number'] = @$doctorPhone[0]->phonenumber;

		$this->load->view('emergency-contact-information/display_emergency-insert-information-profileviewer.phtml', array(
			'arrayGetContact' => $arrayGetContact, 
			'safeContact' => $safeContact
		));


	}

	public function forAjax()
	{
		
		$arrayGetContact = array();
		$dataRetrieveGetContact = array(
            'IDsafe_personalInfo' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])
        );
		$arrayGetContact = $this->contactinfo->getAllContact($dataRetrieveGetContact);
		
		$this->load->view('emergency-contact-information/display_emergency-information.phtml',
			array('arrayGetContact' => $arrayGetContact));


	}
	
	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }

            #
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }


}
?>