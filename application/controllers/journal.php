<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Journal extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Emrjournal_model', 'journal');
        $this->load->model('safejournal_model','safejournal');
        $this->load->model('Error_model', 'errordisplay');
        $this->load->model('personalprofile_model', 'personalprofile');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'), 
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
        $this->load->model('emruser_model', 'emruser');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 

	}

	public function index()
	{

		// $IDsafe_user = 3;
		$IDemr_personalinfo = 1;
        $IDemr_user = 2;
        
		$this->journals = $this->journal->getJournals($IDemr_user);

		if (empty($this->journals))
		{
			
		}

		

		print_r($this->journals);
	}
    

    public function dashboardDisplayProfileViewer()
    {
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            return;

        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
                
        $this->myemrsession = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));
        $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $IDemr_user = $doctorInformation->IDemr_user;
        $IDemr_patient = $this->journal->getidemrpatient($IDemr_user, $IDsafe_personalInfo);
        $IDemr_patient = @$IDemr_patient->IDemr_patient;
        $this->emrjournal = $this->journal->displayDashboardJournal($IDsafe_personalInfo, $this->myemrsession->IDemr_user);

        # for paging result
        # will work with custom-dashboard-journal.js pagination code
        # and display_jounal-information.phtml div journal-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'journal/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->journal->record_count($IDemr_user, @$IDemr_patient);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] = $this->journal->displayDashboardJournal($IDsafe_personalInfo, $this->myemrsession->IDemr_user, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
        
        if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo '';
        else
            $this->load->view('journal/display_journal-information.phtml', $data,array());
        // $this->output->enable_profiler(TRUE);

    }

    public function insertDoctor()
    {
        $this->checkSession();
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        $ajax = TRUE;
        $error = TRUE;
        $errorMessage = '';
        $journalInformation = array();

        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0){
            $error = TRUE;
            $errorMessage = 'please login as a doctor';
        }
        else if (strcasecmp($checkReturn['description'],'Session expired')==0){
            $error = TRUE;
            $errorMessage = 'session expired';
        }
            

        $sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
        $this->form_validation->set_rules('notes','notes', 'trim|xss_clean|required');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }

        if ($error == false)
        {
            $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
            $notes = ucwords(filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING));

            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $IDemr_user = $doctorInformation->IDemr_user;

            # get the IDsafe_personalInfo
            $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($sessionid));
            $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $IDemr_user = $doctorInformation->IDemr_user;
            $IDemr_patient = $this->journal->getidemrpatient($IDemr_user, $IDsafe_personalInfo);
            $IDemr_patient = @$IDemr_patient->IDemr_patient;

            if (empty($IDemr_patient))
                    $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

            $journalInformation = array(
                'notes' => $notes,
                'emr_user_IDemr_user' => $IDemr_user,
                'emr_patient_IDemr_patient' => $IDemr_patient
            );

            $this->safejournal->insertJournalOnly($journalInformation);
        }


        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');

            echo json_encode(
                array('errorStatus' => $error, 'errorMessage' => $errorMessage, @$journalInformation)
            );
            # end of ajax ang display
        }
        else
        {
            // $this->load->view(
            // 'homepage.phtml', array('title' => 'Update Journal Information', 
            // 'view' => 'test/update'));
        }
    }

    public function insertDisplayJournal()
    {
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');


        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            echo 'please login as a doctor';
        else if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo 'session expired';
        else
            $this->load->view('journal/display_journal-insert-journal.phtml', array());
    }

    public function insertDisplay()
    {
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        $this->load->model('Allergy_model', 'allergy');
        $this->load->model('Problem_model', 'problem');
        $this->load->model('Medication_model', 'medication');
        $this->load->model('Prescription_model', 'prescription');
        $this->load->model('Procedures_model', 'procedures');
        $this->load->model('Immunization_model', 'immunizations');
        $this->load->model('Test_model', 'test');

        $idemrjournal = filter_var($this->input->get('idemrjournal'), FILTER_SANITIZE_STRING);

        # get who owns the journal
        $this->whoownsjournal = $this->journal->whoOwnsTheJournal($idemrjournal);

        # get the journal
        $this->journalinformation = $this->safejournal->getjournal($idemrjournal);

        $this->whattableused = $this->journal->identifywhattableused($this->whoownsjournal);

        # para display sa right nga information
        // example Array ( [tableId] => IDsafe_allergen [tableName] => safe_allergen )
        if (strcasecmp($this->whattableused['tableName'], 'safe_allergen')==0)
            $this->theinformationfordisplay = $this->allergy->getToModify($this->whoownsjournal->IDsafe_allergen);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_problem')==0)
            $this->theinformationfordisplay = $this->problem->getToModifyProblem($this->whoownsjournal->IDsafe_problem);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_drug')==0)
            $this->theinformationfordisplay = $this->medication->getToModifyMedication($this->whoownsjournal->IDsafe_drug);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_prescription')==0)
            $this->theinformationfordisplay = $this->prescription->prescriptionInformation($this->whoownsjournal->IDsafe_prescription);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_procedure')==0)
            $this->theinformationfordisplay = $this->procedures->getToModifyB($this->whoownsjournal->IDsafe_procedure);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_immunization')==0)
            $this->theinformationfordisplay = $this->immunizations->getToModify($this->whoownsjournal->$IDsafe_immunization);
        elseif (strcasecmp($this->whattableused['tableName'], 'safe_test')==0)
            $this->theinformationfordisplay = $this->test->getToModify($this->whoownsjournal->IDsafe_test);
        // elseif (strcasecmp($this->whattableused['tableName'], 'safe_files')==0)
        //     $this->theinformationfordisplay = $this->test->getToModify($this->whoownsjournal->IDsafe_test);
        else
            $this->theinformationfordisplay = array();

        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            echo 'please login as a doctor';
        else if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo 'session expired';
        else
            $this->load->view('journal/display_journal-insert-information.phtml', array());
       
    }

    public function update()
    {
        $this->checkSession();
        $checkdoctor = $this->checkdoctor('DOM');
        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            return;
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
        $error = TRUE;
        $errorMessage = '';
        $journalinformation = array();

        $this->form_validation->set_rules('IDemr_journal','IDemr_journal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('notes','notes', 'trim|xss_clean|required');
        $this->form_validation->set_rules('datestarted', 'Date Started', 'xss_clean|required|valid_date');
        $this->form_validation->set_rules('timestarted', 'Time problem started', 'trim|min_length[3]|max_length[5]|validate_time|required');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }


        if ($error == FALSE)
        {
            $IDemrjournal = filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING);
            $notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
            $datestarted = filter_var($this->input->post('datestarted'), FILTER_SANITIZE_STRING);
            $timestarted = filter_var($this->input->post('timestarted'), FILTER_SANITIZE_STRING);

            // # check dates and time
            list($monthStart, $dayStart, $yearStart) = explode('/', $datestarted);
            $datestarted = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart.' '.$timestarted));

            $journalinformation = array(
                'IDemr_journal' => $IDemrjournal,
                'notes' => $notes,
                'datestarted' => $datestarted
            );

            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $IDemr_user = $doctorInformation->IDemr_user;

            if ($this->journal->doctorOwnsJournal($IDemrjournal, $IDemr_user))
            {
                #update the journal
                $journalinformation['IDemr_user'] = $IDemr_user;
                $this->journal->update($journalinformation);
            }
        }

        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');

            echo json_encode(
                array('errorStatus' => $error, 'errorMessage' => $errorMessage, @$journalinformation)
            );
            # end of ajax ang display
        }
        else
        {
            // $this->load->view(
            // 'homepage.phtml', array('title' => 'Update Journal Information', 
            // 'view' => 'test/update'));
        }
        return;
    }

    public function deletedoctor()
    {
        $ajax = TRUE;
        $error = TRUE;
        $this->checkSession();

        $sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

        $ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $this->form_validation->set_rules('idemrjournal', 'idemrjournal',  'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        }
        else
        {
            $error = FALSE;
        }

        $jsonDisplay = array(
            'errorStatus' => $error,
        );

        if ($error == FALSE)
        {
            $idemrjournal = filter_var($this->input->post('idemrjournal'), FILTER_SANITIZE_STRING);
            $idemrjournal = preg_replace('#[^a-z0-9_]#', '', $idemrjournal);

            $jsonDisplay['idemrjournal'] = $idemrjournal;

            $this->safejournal->deleteJournal($idemrjournal);
        }
    
        if ($ajax == TRUE)
        {           
            header('Content-Type: application/json');
            echo json_encode($jsonDisplay);
        }
        else
        {
            $this->load->view(
            'homepage.phtml', array('title' => 'Delete Medication Information', 
            'view' => 'medication/delete'));
        }
        return;
    }

    private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                $errorReturn = $this->errorHandlings($displaytype);
                return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                $this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                $this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }

            #
        }
        else
        {
            $errorReturn = $this->errorHandlings($displaytype);
            return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
        $errorD = $this->errordisplay->identifyError($errornum);
        if (strcasecmp('DOM', $displaytype)==0)
        {
            echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
            return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
            echo json_encode(
                array(
                    'errorStatus' => TRUE, 
                    'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
                    )); 
            return $errorD;
        }
        else
            redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
        $errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            if (strcasecmp('DOM', $displaytype)==0)
            {
                echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
                return $errorD;
            }
            else
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
}
?>