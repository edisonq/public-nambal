<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Insurance extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Insurance Information', 
			'view' => 'insurance/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Insurance Information', 
			'view' => 'insurance/insert'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function update(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Update Insurance Information', 
			'view' => 'insurance/update'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function delete(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Delete Insurance Information', 
			'view' => 'insurance/delete'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}
}
?>