<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
	}

	public function index()
	{
		// $this->load->view(
		// 	'homepage2014.phtml', array(
		// 		'view' => 'home/home2014'));
		// $this->load->view(
		// 	'homepage2014.phtml', array(''));
		// redirect(base_url().'dashboard', 'refresh');

		redirect(base_url("login"), 'refresh');

	}

	public function indexhtml()
	{
		redirect(base_url(), 'refresh');
	}

	public function thankyoupagehtml()
	{
		$this->load->view(
			'homepage2014.phtml', array(
				'title' => 'People that helped us', 
				'pageKeywords' => 'version 1, version 2, version 3, impressive consultants, thanks',
				'pageDescription' => 'Thanking GOD, families, friends and these amazing people.',
				'view' => 'home/thank-you'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */