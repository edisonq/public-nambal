<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Frequently Ask Question', 
			'view' => 'about/index'));
		return;
	}

	public function team(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Team', 
			'view' => 'about/team'));
		return;
	}

	public function healthCard()
	{
		$retUrl = ucwords(filter_var($this->input->post('retUrl'), FILTER_SANITIZE_URL));
		
		$this->load->view(
			'homepage.phtml', array('title' => 'Team', 
			'view' => 'about/card'));
		return;
	}
}

?>