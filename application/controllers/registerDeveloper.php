<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class RegisterDeveloper extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	}

	public function index()
	{
		$errorMessage = '';
		$customCss = array('custom-register');

		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Medication Information', 
			'view' => 'register/register-developer', 
			'customCss' => $customCss,
			'errorMessage' => $errorMessage  
		));
	}
}

?>