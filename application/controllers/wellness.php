<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Wellness extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Wellness List', 
			'view' => 'wellness/index'));
		return;
	}

	public function createWellnessTracker(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Create wellness tracker',
				'view' => 'wellness/create-wellness-tracker'));
		return;
	}

	public function insert(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Insert Wellness Information',
				'view' => 'wellness/insert'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;
	}

	public function displayData(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Display data of Wellness Information',
				'view' => 'wellness/display'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;
	}

	public function updateData(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Update data of Wellness Information',
				'view' => 'wellness/update'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;
	}

	public function deleteData(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Delete data of Wellness Information',
				'view' => 'wellness/delete'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;
	}
}
?>