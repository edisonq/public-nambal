<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class RegisterDoctorOnly extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'), 
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index()
	{
		$customJs = array(
           );
		$firstTime = false;
		$customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
		$errorMessage = '';

        
        $urlAdd = 'doctorDashboard';
        $navOpen = '';        
        
		//doctor-dashboard-light-payables.phtml # new UI
      $this->load->view(
				'homepage.phtml', array('title' => 'Register Doctor', 
				'customCss' => $customCss,
                'errorMessage' => $errorMessage,
				'view' => 'register-doctor/register-direct'));



	}
}

?>