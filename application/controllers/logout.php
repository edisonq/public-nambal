<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('SafeuserLog_model', 'safeuserlog');
        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
        $this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
	}

	public function index()
	{
		if((!empty($this->nambal_session['sessionName'])) )
		{

	        $data = array(
	            'IDsafe_user' => $this->nagkamoritsing->ibalik(@$this->nambal_session['IDsafe_user']),
	            'activity' => 'Properly logout',
	            'activityDescription' => 'Properly logout',
	            'IpAddress' => $this->nagkamoritsing->ibalik($this->ipAddress)
	         );
	        $this->safeuserlog->logUser($data);		 	
	    }
	    if (empty($data))
	    	redirect(base_url().'login', 'refresh');
	    $this->sessionModel->unsetSession($data);
	    $this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('fb_profile');
		$this->session->unset_userdata('barangay_login');
		$this->session->unset_userdata('urlredirect');
		// $this->facebook->destroySession();
		$_SESSION['fb_profile'] = '';
        $_SESSION['logoutURL'] =  '';
        $_SESSION['urldirect'] = '';

        // $this->session->sess_destroy();
	 	//$sess
	 	redirect(base_url().'login','refresh');	
	}

	 function auto()
	 {
	 	$this->session->unset_userdata('logged_in');
	 	// $this->facebook->destroySession();
	 	redirect(base_url().'login/successFacebook','refresh');
	 }

}