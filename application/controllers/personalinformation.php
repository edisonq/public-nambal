<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Personalinformation extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
        $this->load->library('form_validation');
        // $this->load->library('facebook', $fb_config);
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->model('Error_model', 'errordisplay');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->model('Personalprofile_model', 'personalinfo');
        $this->load->model('Addresses_model','addressinfo');
        $this->load->helper('s3');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // }

        # get the profile picture
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

	public function modifydisplay()
	{
		# check if login
		$this->checksession('DOM');

		# check if doctor
		# else modify own personal information
		# also display own personal information

		$this->IDsafe_user = filter_var($this->input->get('IDsafe_user'), FILTER_SANITIZE_STRING);
        $this->IDsafe_user = preg_replace('#[^a-z0-9_]#', '', $this->IDsafe_user);
		$this->patientInformation = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($this->IDsafe_user));
        $this->personalinformation = $this->personalinfo->getPersonalInfo($this->IDsafe_user);
        if (!empty($this->personalinformation))
            $this->personalinformationAddress = $this->addressinfo->getPersonalInfoAddress($this->personalinformation->IDsafe_personalInfo);


		$this->load->view('profile/insert-display.phtml', array());
	}

    public function updateDoctor()
    {
        $this->checkSession();
        $error = false;
        $errorMessage = '';
        $updateArray = array();

        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
        $sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);
        $sessionid = $this->nagkamoritsing->ibalik($sessionid);

        $this->form_validation->set_rules('firstname','Firstname', 'trim|xss_clean|required');
        $this->form_validation->set_rules('middlename','Middle name', 'trim|xss_clean');
        $this->form_validation->set_rules('lastname','Last or family name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('birthday','Birthday', 'trim|xss_clean|required|valid_date');
        $this->form_validation->set_rules('birthtime','birth time', 'trim|xss_clean|required');
        $this->form_validation->set_rules('gender','Gender', 'trim|xss_clean|required');
        $this->form_validation->set_rules('street','Street', 'trim|xss_clean|required');
        $this->form_validation->set_rules('state','State', 'trim|xss_clean|required');
        $this->form_validation->set_rules('city','City', 'trim|xss_clean|required');
        $this->form_validation->set_rules('country','Country', 'trim|xss_clean|required');
        $this->form_validation->set_rules('postcode','Post Code', 'trim|xss_clean|required');
        $this->form_validation->set_rules('bloodtype','Blood type', 'trim|xss_clean|required');
        $this->form_validation->set_rules('IDsafe_personalInfo','IDsafe_personalInfo', 'trim|xss_clean|required');
        $this->form_validation->set_rules('IDsafe_user','IDsafe_user', 'trim|xss_clean|required');

        if (empty($ajax))
            $ajax = TRUE;


        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }

        if ($error == FALSE)
        {
            $firstname = ucwords(filter_var($this->input->post('firstname'), FILTER_SANITIZE_STRING));
            $middlename = ucwords(filter_var($this->input->post('middlename'), FILTER_SANITIZE_STRING));
            $lastname = ucwords(filter_var($this->input->post('lastname'), FILTER_SANITIZE_STRING));
            $gender = ucwords(filter_var($this->input->post('gender'), FILTER_SANITIZE_STRING));
            $street = ucwords(filter_var($this->input->post('street'), FILTER_SANITIZE_STRING));
            $state = ucwords(filter_var($this->input->post('state'), FILTER_SANITIZE_STRING));
            $city = ucwords(filter_var($this->input->post('city'), FILTER_SANITIZE_STRING));
            $country = ucwords(filter_var($this->input->post('country'), FILTER_SANITIZE_STRING));
            $postcode = ucwords(filter_var($this->input->post('postcode'), FILTER_SANITIZE_STRING));
            $bloodtype = ucwords(filter_var($this->input->post('bloodtype'), FILTER_SANITIZE_STRING));
            $IDsafe_personalInfo = ucwords(filter_var($this->input->post('IDsafe_personalInfo'), FILTER_SANITIZE_STRING));
            $IDsafe_user = ucwords(filter_var($this->input->post('IDsafe_user'), FILTER_SANITIZE_STRING));
            $birthtime = $this->input->post('birthtime'); 
            $birthday = $this->input->post('birthday');
            $birthday = date('o-m-d', strtotime($birthday)).' '.date('H:i', strtotime($birthtime)).':00';

            $updateArray = array(
                'firstname' => $firstname,
                'middlename' => $middlename,
                'lastname' => $lastname,
                'gender' => $gender,
                'street' => $street,
                'state' => $state,
                'city' => $city,
                'country' => $country,
                'postcode' => $postcode,
                'bloodtype' => $bloodtype,
                'IDsafe_personalInfo' => $IDsafe_personalInfo,
                'IDsafe_user' => $IDsafe_user,
                'birthday' => $birthday
            );

            $this->personalinfo->update($updateArray);
        }


        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');
                
            echo json_encode(
                array('errorStatus' => $error, 'errorMessage' => $errorMessage, 'personalInformation' => $updateArray)
            );
        }   
        else
        {
            // $this->load->view(
            // 'homepage.phtml', array('title' => 'Update Medication Information', 
            // 'view' => 'medication/update'));
        }
    }

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$this->errorHandlings($displaytype);
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$this->errorHandlings($displaytype);
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor()
    {
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }

    public function updateView()
    {
        $this->load->helper('text');
        $this->IDsafe_user = filter_var($this->input->get('IDsafe_user'), FILTER_SANITIZE_STRING);
        $this->IDsafe_user = preg_replace('#[^a-z0-9_]#', '', $this->IDsafe_user);
        $this->patientInformation = $this->safeuser->getSafeUserInfo($this->IDsafe_user);

        $this->personalinformation = $this->personalinfo->getPersonalInfo($this->nagkamoritsing->ibalik($this->IDsafe_user));
        if (!empty($this->personalinformation))
            $this->personalinformationAddress = $this->addressinfo->getPersonalInfoAddress($this->personalinformation->IDsafe_personalInfo);

        $this->load->view('profile-viewer/personal-information-update.phtml', array());
    }
}

?>