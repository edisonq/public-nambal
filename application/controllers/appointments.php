<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Appointments extends CI_Controller 
{

	public function __construct() {
		parent::__construct();
        $this->load->library('email');
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->library('form_validation');
        $this->load->model('Personalprofile_model', 'personalProfile');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Messages_model', 'messagesModel');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Appointment_model', 'appointmentsmodel');
        $this->load->model('Location_model', 'locationmodel');
        $this->load->model('Pusher_model', 'pushery');
        $this->load->model('Clinicsettings_model', 'clinicsettings');
        $this->load->model('Error_model', 'errordisplay');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
    
        # check user's if properly logged-in
        if (empty($this->nambal_session['sessionName']))
        {
            redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        //     $fbUser = $this->facebook->getUser();
        //     if (!empty($fbUser))
        //     {
        //         if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        //         {
        //             redirect(base_url().'logout', 'refresh');
        //         }
        //     } else {
        //         redirect(base_url().'login/facebook', 'refresh');
        //     }
        // }
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

            # check if the user is a registered doctor in nambal
            if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
            }
            # end of checking if the user is a registered doctor in nambal 
        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in


        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

    public function setAppointmentFromPatient()
    {
        $customCss = array('custom-dashboard','patient-dashboard');
        $customJsTop = array('countries');
        $customJs = array('tour-patient-dashboard','../lib/jquery.autogrow-textarea','../lib/bootstrap-switch','../lib/bootstrap-colorpicker','forms-elemets','jquery.flot.min','jquery.flot.pie','jquery.flot.resize.min', 'jquery.stateselect', 'jquery.selectboxes.pack', 'custom-connect-doctor-search-short', 'jquery-ui-1.10.3.custom.min', '../lib/select2', 'forms-elemets','custom-dashboard');

        $this->load->view(
            //'dashboard-template.phtml', array('title' => 'Your Health Dashboard', 
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Your Health Dashboard',
            'view' => 'appointments/set-appointment', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'customJsTop' => $customJsTop,
            'css' => 'dashboard-custom'));
        return;
    }

    public function add()
    {

        $error = false;
        $urlAdd = 'doctorDashboard';
        $navOpen = 'doctor options';
        $nameWeek_timefromBoolean = false;
        $nameWeek_timetoBoolean = false;
        $customJs = array('fullcalendar.min','calendar','tables-dynamic','app');
        $customCss = array('custom-dashboard','zocial', 'custom-messaging');
        $errorMessage = '';
        $this->form_validation->set_rules('emr_user_IDemr_user', 'trim|xss_clean');
        $this->form_validation->set_rules('safe_user_IDsafe_user', 'trim|xss_clean');
        $this->form_validation->set_rules('emr_nonnambalinfo_IDemr_nonnambalinfo','trim|xss_clean');
        $this->form_validation->set_rules('timefrom','trim|required|xss_clean');
        $this->form_validation->set_rules('timeto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('allday', 'trim|required|xss_clean');
        $this->form_validation->set_rules('note', 'trim|required|xss_clean');
        $this->form_validation->set_rules('appointment_type_IDappointment_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('IDlocation_info', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
        }
        else
        {
            $error = false;
        }

        # sending message main area here
        if ($error != TRUE)
        {
            $emr_user_IDemr_user = filter_var($this->input->post('emr_user_IDemr_user'), FILTER_SANITIZE_STRING);
            $safe_user_IDsafe_user = filter_var($this->input->post('safe_user_IDsafe_user'), FILTER_SANITIZE_STRING);
            $emr_nonnambalinfo_IDemr_nonnambalinfo = filter_var($this->input->post('emr_nonnambalinfo_IDemr_nonnambalinfo'), FILTER_SANITIZE_STRING);
            $timefrom = filter_var($this->input->post('timefrom'), FILTER_SANITIZE_STRING);
            $timeto = filter_var($this->input->post('timeto'), FILTER_SANITIZE_STRING);
            $allday = filter_var($this->input->post('allday'), FILTER_SANITIZE_STRING);
            $note = filter_var($this->input->post('note'), FILTER_SANITIZE_STRING);
            $appointment_type_IDappointment_type = filter_var($this->input->post('appointment_type_IDappointment_type'), FILTER_SANITIZE_STRING);
            $IDlocation_info = filter_var($this->input->post('IDlocation_info'), FILTER_SANITIZE_STRING);


            if (empty($emr_user_IDemr_user))
            {
                # get the emr_user_IDemr_user
                $emr_user_IDemr_user = $this->emrUser->getDoctorId($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
                $emr_user_IDemr_user = $emr_user_IDemr_user->IDemr_user;
            }

            # check if the doctor already set the emr_actual_schedule
            $actualShedule = $this->appointmentsmodel->checkEASSet($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            if ($actualShedule['error'] == false)
            {
                $actualShedule = $actualShedule['theResult'];
            }
            else
            {
                $error = true;

                $actualShedule = $actualShedule['theResult'];
            }           
            # end of checking doctor already set the emr_actual_schedule.


            # check if the doctor already set a location info
            $locationInfo = $this->appointmentsmodel->getLocationInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            if (($locationInfo['error'] == false) && ($error != true))
            {
                $locationInfo = $locationInfo['theResult'];
            }
            else
            {
                $error = true;
                $locationInfo = $locationInfo['theResult'];
            }
            
            # end of checking if the doctor already set a location info

            # check if the appointment is in emr_actual_schedule
            # dont restrict but warn doctor that he/she set an appoinment outside his actual location schedule.
            if ($error != true)
            {
                $nameWeek_timefrom = date('l', strtotime($timefrom));
                $nameWeek_timeto = date('l', strtotime($timeto));
                $time_timeFrom = date('H:i:s',strtotime($timefrom));
                $time_timeTo = date('H:i:s', strtotime($timeto));

                if ($timefrom < $timeto)
                {
                    foreach ($actualShedule as $actual) 
                    {
                        if ((strcasecmp($nameWeek_timefrom, $actual->day)==0) && ($nameWeek_timefromBoolean != true))
                        {
                            $nameWeek_timefromBoolean = true;
                        }
                        if (( strcasecmp($nameWeek_timeto, $actual->day)==0) && ($nameWeek_timetoBoolean != true))
                        {
                            $nameWeek_timetoBoolean = true;
                        }
                        if (($actual->timefrom <= $time_timeFrom) && ($time_timeFrom <= $actual->timeto) && ($actual->timefrom <= $time_timeTo) && ($time_timeTo <= $actual->timeto))
                        {
                            # right timeto and right timefrom
                            $time_timeToAndFromBoolean = true;
                        }
                    }
                    if (($nameWeek_timetoBoolean == true) && ($nameWeek_timefromBoolean == true))
                    {
                        // echo 'this is ok for appointment';
                        $data = array(
                            'emr_user_IDemr_user' => $emr_user_IDemr_user,
                            'safe_user_IDsafe_user' => $safe_user_IDsafe_user,
                            'emr_nonnambalinfo_IDemr_nonnambalinfo' => $emr_nonnambalinfo_IDemr_nonnambalinfo,
                            'timefrom' => $timefrom,
                            'timeto' => $timeto,
                            'allday' => $allday,
                            'active' => true,
                            'note' => $note,
                            'approved' => false,
                            'appointment_type_IDappointment_type' => $appointment_type_IDappointment_type,
                            'IDlocation_info' => $IDlocation_info
                        );
                        $this->appointmentsmodel->setAppointment($data);
                    }
                    elseif(($nameWeek_timetoBoolean == true) || ($nameWeek_timefromBoolean == true))
                    {
                        echo 'this is ok for appointment but one of the day selected is not ok';
                    }
                    else
                    {
                        # not ok for appointment
                        echo 'not ok for appointment';
                    }
                }
                else
                {
                    # error in timefrom and timeto
                    # not ok for appointment
                    echo 'not ok for appointment, error in time from and time to';
                    
                }
            }
            # end of checking if the appointment is in emr_actual schedule
            echo '<Br>'.$safe_user_IDsafe_user.
            '<br>'.$timefrom.
            '<br>'.$timeto.
            '<br>'.$allday.
            '<br>'.$note.
            '<br>'.$appointment_type_IDappointment_type;
        }
        
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Appointments - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/add'
        ));
    
        $this->output->enable_profiler(TRUE);
    }

	public function index()
	{
        $this->activeMainMenu = "Appointments";
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
        
		$customJs = array(
            'fullcalendar.min',
            'calendar-appointment-doctor',
            'app',
            'tables-dynamic',
            'appointment',
            'customize-doctor-dashboard-appointment',
            '../lib/jquery.autogrow-textarea',
            '../lib/icheck.js/jquery.icheck'
        );
        // $customCss = array('custom-dashboard','zocial', 'custom-messaging');
        $json = '';
        $errorMessage = '';
        
        $urlAdd = 'appointments';
		$customCss = array('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = '';

        $this->userDoctorInfo = $this->emrUser->getDocInfoComplete($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->userDoctorAppointment = $this->appointmentsmodel->getDoctorAppointments($this->userDoctorInfo[0]->IDemr_user, NULL, FALSE, FALSE, TRUE, TRUE);
        $this->locationsArray = $this->locationmodel->getDoctorLocations($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        // print_r($this->userDoctorInfo);
        // echo '<br>';
        // echo '<br>';
        // print_r($this->userDoctorAppointment);

        // print_r($this->userDoctorAppointment);

		$this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Appointments - Doctor Dashboard', 
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/index'
        ));
        // $this->output->enable_profiler(TRUE);
	}

    public function appointmentTable()
    {
        $this->userDoctorInfo = $this->emrUser->getDocInfoComplete($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->userDoctorAppointment = $this->appointmentsmodel->getDoctorAppointments($this->userDoctorInfo[0]->IDemr_user);

        $this->load->view('appointments/list-table-appointment.phtml');
    }

    public function showAppointmentInformation()
    {
        $ajax = FALSE;
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        // $IDappointments = 0;
        $this->form_validation->set_rules('IDappointments', 'required|trim|xss_clean');
        
        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        }
        else
        {
            $error = FALSE;
        }

        # sending message main area here
        if ($error != TRUE)
        {            
            $IDappointments = filter_var($this->input->post('IDappointments'), FILTER_SANITIZE_STRING);
        }
        $select = "
            username, 
            firstname, 
            middlename, 
            lastname, 
            timefrom, 
            timeto, 
            allday, 
            appointments.active, 
            note, 
            approved, 
            forced, 
            public, 
            daterequested,
            dateapproved, 
            appointment_type_IDappointment_type
        ";
        $arrayJson = $this->appointmentsmodel->getSpecificAppointment($this->nagkamoritsing->ibalik($IDappointments), $select);
        if (empty($arrayJson))
        {
            $arrayJson = $this->appointmentsmodel->getSpecificAppointment($IDappointments, $select);
        }


        if ($ajax == TRUE)
        {
            header('Content-Type: application/json');
            echo json_encode($arrayJson);
            // $this->output->enable_profiler(TRUE);
        }
    }

    public function modify()
    {
        $this->checkSession();
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            return;


        $urlAdd = 'appointments/modify';
        $this->activeMainMenu = "Appointments";
        $customCss = array('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = '';
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);



        # retrive the appointment information
        $select = "
            username, 
            firstname, 
            middlename, 
            lastname, 
            timefrom, 
            timeto, 
            allday, 
            appointments.active, 
            note, 
            approved, 
            forced, 
            public, 
            daterequested,
            dateapproved, 
            appointment_type_IDappointment_type
        ";
        $appointmentID = filter_var($this->input->get('appointmentID'), FILTER_SANITIZE_STRING);
        $appointmentID = preg_replace('#[^a-z0-9_]#', '', $appointmentID);
        $appointmentInformation = $this->appointmentsmodel->getSpecificAppointment($this->nagkamoritsing->ibalik($appointmentID), $select);

        $arrayJson = array(
        );

        $this->form_validation->set_rules('dateappointment', 'Appointment Date', 'trim|xss_clean|required');
        $this->form_validation->set_rules('appointment-from', 'Appointment, start time', 'trim|xss_clean|required');
        $this->form_validation->set_rules('appointment-to', 'Appointment end time', 'trim|xss_clean|required');

        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        }
        else
        {
            $error = FALSE;
        }

        # sending message main area here

        if ($_POST)
        {
            # dawat sa post
            
            
            if ($error != TRUE)
            {

                $this->appointmentfrom = ucwords(filter_var($this->input->post('appointment-from'), FILTER_SANITIZE_STRING));
                $this->appointmentto = ucwords(filter_var($this->input->post('appointment-to'), FILTER_SANITIZE_STRING));
                $this->dateappointment = ucwords(filter_var($this->input->post('dateappointment'), FILTER_SANITIZE_STRING));
                
                # check if sakto ba ang input
                if (strtotime($this->appointmentto) > strtotime($this->appointmentfrom)) {
                    $this->dateappointment = explode('/', $this->dateappointment);
                    $this->appointmentfrom = date('Y-m-d H:i:s', strtotime($this->dateappointment[2].'-'.$this->dateappointment[0].'-'.$this->dateappointment[1].' '.$this->appointmentfrom));
                    $this->appointmentto = date('Y-m-d H:i:s', strtotime($this->dateappointment[2].'-'.$this->dateappointment[0].'-'.$this->dateappointment[1].' '.$this->appointmentto));
                    $this->appointmentsmodel->modifyAppointment($this->nagkamoritsing->ibalik($appointmentID), $this->appointmentfrom, $this->appointmentto);
                } else {
                    // echo strtotime($this->appointmentto);
                    // echo '<br>';
                    // echo strtotime($this->appointmentfrom);
                }
                # end of check if sakto ba ang input

                
    
                # retrieve the appointment and compute the changes
                $appointmentInformation = $this->appointmentsmodel->getSpecificAppointment($this->nagkamoritsing->ibalik($appointmentID));
                $timeto =  new DateTime($appointmentInformation->timeto);
                $timefrom =  new DateTime($appointmentInformation->timefrom);
                // if ((!empty($allEventDrag)) && ($allEventDrag == TRUE))
                // {
                //     $timefrom = date('Y-m-d H:i:s',strtotime($startDate));
                //     $timeto =  new DateTime($timefrom);

                //     $to_time = strtotime($appointmentInfo->timeto);
                //     $from_time = strtotime($appointmentInfo->timefrom);
                //     $minutesDiff = round(abs($to_time - $from_time) / 60,2);

                //     $timeto->modify("$minutesDiff minutes");
                //     $timeto = date_format($timeto, 'Y-m-d H:i:s');
                // }
                // else
                // {                
                //     $timeto->modify("$minuteDelta minutes");
                //     $timeto = date_format($timeto, 'Y-m-d H:i:s');
                //     $timefrom = date_format($timefrom, 'Y-m-d H:i:s');
                // }
               
                
                # end of retrieving the appointment and computation of changes         
                $this->settingsname = ucwords(filter_var($this->input->post('settingsname'), FILTER_SANITIZE_STRING));
                redirect(base_url('appointments/approve?redirect=true&IDappointments='.$appointmentID), 'refresh');


                
            }
        }

       

        $customJs = array(
            'fullcalendar.min',
            'calendar',
            'app',
            'tables-dynamic',
            'appointment',
            'customize-doctor-dashboard-appointment',
            '../lib/jquery.autogrow-textarea',
            '../lib/icheck.js/jquery.icheck'
        );



        if ($ajax == TRUE)
        {
            header('Content-Type: application/json');
            echo json_encode($arrayJson);
        }
        else
        {
            $this->load->view(
            'doctor-dashboard-light-blue.phtml', array( 
                'appointmentInformation' => $appointmentInformation,
                'title' => 'Appointments - Doctor Dashboard', 
                'customCss' => $customCss,
                'customJs' => $customJs,
                'urlAdd' => $urlAdd,
                'navOpen' => $navOpen,
                'view' => 'appointments/modify'
            ));
        }
        // $this->output->enable_profiler(TRUE);
    }

    public function approve()
    {
        $ajax = FALSE;
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $IDappointments = filter_var($this->input->get('IDappointments'), FILTER_SANITIZE_STRING);
        $redirect = filter_var($this->input->get('redirect'), FILTER_SANITIZE_STRING);
        $customCss = array();
        $customJs = array();
        $urlAdd = '';
        $navOpen = '';
        $arrayValue = array(
            'approve' => FALSE
        );

        $drname = $this->nagkamoritsing->ibalik($this->nambal_session['firstname']).' '.$this->nagkamoritsing->ibalik($this->nambal_session['lastname']);

        
            if (!empty($IDappointments))
            {
                # get appointment information
                $select = "
                    firstname,
                    lastname,
                    timefrom,
                    timeto,
                    allday,
                    note,
                    IDappointments,
                    daterequested,
                    safe_user_IDsafe_user
                ";
                $appointmentInfo = $this->appointmentsmodel->getSpecificAppointment($this->nagkamoritsing->ibalik($IDappointments), $select);
                # end of getting appointment information
                
                # retrieve patient email
                $patientInfo = $this->safeuser->getSafeUserEmailsInfo($appointmentInfo->safe_user_IDsafe_user);
                # end of retrieving patient email

                # email sending
                $this->email->from('no-reply@nambal.com', 'Nambal Information');
                $this->email->to($this->nagkamoritsing->ibalik($patientInfo->emailAddress));
                // // $this->email->cc('edisonq@yahoo.com');
                // // $this->email->bcc('edisonq@hotmail.com');
                 
                $this->email->subject('Appointment Approved');
                $emailData = array(
                    'firstname' => ucwords($this->nagkamoritsing->ibalik($appointmentInfo->firstname)),
                    'lastname' => ucwords($this->nagkamoritsing->ibalik($appointmentInfo->lastname)),
                    'drname' => ucwords($drname),
                    'timeto' => date('D, F d, o @ g:i A', strtotime($appointmentInfo->timeto)),
                    'timefrom' => date('D, F d, o @ g:i A', strtotime($appointmentInfo->timefrom)),
                    'IDappointments' => $appointmentInfo->IDappointments
                );

                $this->email->message(
                    $this->load->view(
                            'for-email.phtml', 
                            array('title' => 'Appointment approved', 
                            'view' => 'emails/appointment-approved', 
                            'emailData' => $emailData, 'footerEmail' => $this->nagkamoritsing->ibalik($patientInfo->emailAddress)
                            ), true)
                );
                $this->email->send();
                # end of email sending

                # send to pusher
                $this->pushery->appointmentApproveByDoctor(@$appointmentInfo->safe_user_IDsafe_user);
                # end of sending to pusher

                $this->appointmentsmodel->approveAppointment($this->nagkamoritsing->ibalik($IDappointments));
                $arrayValue = array(
                    'approve' => TRUE
                );
            }
            else
            {
                $arrayValue = array(
                    'approve' => FALSE
                );
            }
        if ($ajax == TRUE)
        {
            header('Content-Type: application/json');
            echo json_encode($arrayValue);
        }
        else
        {
            if (strcasecmp($redirect, 'true') == 0 ) {
                redirect(base_url('appointments/modify?appointmentID='.$IDappointments), 'refresh');
            } else {
                $this->load->view(
                'doctor-dashboard-light-blue.phtml', array(
                    'title' => 'Appointments - Doctor Dashboard', 
                    'customCss' => $customCss,
                    'customJs' => $customJs,
                    'urlAdd' => $urlAdd,
                    'navOpen' => $navOpen,
                    'view' => 'appointments/index'
                ));    
            }
        }
    }
    public function cancel()
    {
        $ajax = FALSE;
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $IDappointments = filter_var($this->input->get('IDappointments'), FILTER_SANITIZE_STRING);
        $customCss = array();
        $customJs = array();
        $urlAdd = '';
        $navOpen = '';
        $arrayValue = array(
            'approve' => FALSE
        );

        if ($ajax == TRUE)
        {
            if (!empty($IDappointments))
            {
                # get appointment information
                $select = "
                    safe_user_IDsafe_user
                ";
                $appointmentInfo = $this->appointmentsmodel->getSpecificAppointment($this->nagkamoritsing->ibalik($IDappointments), $select);

                # send to pusher
                $this->pushery->cancelAppointment(@$appointmentInfo->safe_user_IDsafe_user);
                # end of sending to pusher

                $this->appointmentsmodel->cancelAppointment($this->nagkamoritsing->ibalik($IDappointments));
                $arrayValue = array(
                    'approve' => TRUE
                );
            }
            else
            {
                $arrayValue = array(
                    'approve' => FALSE
                );
            }

            header('Content-Type: application/json');
            echo json_encode($arrayValue);
        }
        else
        {
            // $this->load->view(
            // 'doctor-dashboard-light-blue.phtml', array(
            //     'title' => 'Appointments - Doctor Dashboard', 
            //     'customCss' => $customCss,
            //     'customJs' => $customJs,
            //     'urlAdd' => $urlAdd,
            //     'navOpen' => $navOpen,
            //     'view' => 'appointments/index'
            // ));
            // $this->output->enable_profiler(TRUE);
        }
    }

    public function checkApprove()
    {
        $ajax = FALSE;
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $IDappointments = filter_var($this->input->get('IDappointments'), FILTER_SANITIZE_STRING);
        $customCss = array();
        $customJs = array();
        $urlAdd = '';
        $navOpen = '';
        $arrayValue = array(
            'approved' => FALSE
        );

        if ($ajax == TRUE)
        {
            if (!empty($IDappointments))
            {
                $ret = $this->appointmentsmodel->checkApprove($this->nagkamoritsing->ibalik($IDappointments));
                $arrayValue = array(
                    'approved' => $ret
                );
            }
            else
            {
                $arrayValue = array(
                    'approved' => FALSE
                );
            }

            header('Content-Type: application/json');
            echo json_encode($arrayValue);
        }
        else
        {
            // $this->load->view(
            // 'doctor-dashboard-light-blue.phtml', array(
            //     'title' => 'Appointments - Doctor Dashboard', 
            //     'customCss' => $customCss,
            //     'customJs' => $customJs,
            //     'urlAdd' => $urlAdd,
            //     'navOpen' => $navOpen,
            //     'view' => 'appointments/index'
            // ));
            // $this->output->enable_profiler(TRUE);
        }
    }

    public function allAppointments()
    {
        $start = filter_var($this->input->get('start'), FILTER_SANITIZE_STRING);
        $end = filter_var($this->input->get('end'), FILTER_SANITIZE_STRING);
        $start = date('o-m-d H:i:s', $start);
        $end = date('o-m-d H:i:s', $end);
        $arrayAppointment = array();
        $sameday = FALSE;
        $locationInformation = '';
        $this->userDoctorInfo = $this->emrUser->getDocInfoComplete($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->userDoctorAppointment = $this->appointmentsmodel->getDoctorAppointments($this->userDoctorInfo[0]->IDemr_user, NULL, FALSE, FALSE, TRUE, TRUE, $start, $end);
        $this->locationsArray = $this->locationmodel->getDoctorLocations($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        foreach ($this->userDoctorAppointment as $arr) 
        {
            $dateStart = date('o-m-d H:i:s', strtotime($arr->timefrom));
            $dateEnd = date('o-m-d H:i:s', strtotime($arr->timeto));

            $dateStartExpld = explode(' ', $dateStart);
            $dateEndExpld = explode(' ', $dateEnd);

            if ($dateEndExpld[0] == $dateStartExpld[0])
            {
                $sameday = TRUE;
            }

            if ($arr->approved == TRUE)
            {
                $color = $this->eventColor($arr->appointment_type_IDappointment_type, TRUE);
                $editable = FALSE;
            }
            else
            {
                $color = $this->eventColor($arr->appointment_type_IDappointment_type, FALSE);
                $editable = TRUE;
            }
            $patientName = ucwords($this->nagkamoritsing->ibalik($arr->firstname)).' '.ucwords($this->nagkamoritsing->ibalik($arr->lastname));

            if (strcasecmp($arr->allday, "TRUE")==0)
            {
                $allDay = TRUE;
            }
            elseif (empty($arr->allday))
            {
                $allDay = FALSE;
            }
            else
            {
                $allDay = FALSE;
            }

            if (empty($arr->location_info_IDlocation_info))
            {
                if (!empty($this->locationsArray))
                {
                    foreach ($this->locationsArray as $array) 
                    {
                        $locationInformation .= $array->street.', '.$array->state.', '. $array->city.', '.$array->country;
                        $locationInformation .= '<br>';
                    }
                }
                else
                {
                    $locationInformation = 'No clinic address yet';
                }
            }
            else
            {
                $locationInformation = $arr->location_info_IDlocation_info;

                foreach ($this->locationsArray as $return => $value) 
                {
                    // $locationInformation = 'please';
                    
                   if ($value->IDlocation_info === $locationInformation) 
                   {
                        $locationInformation = $value->locationname.'<BR>';
                        $locationInformation .= $value->street.', '.$value->state.', '.$value->city.', '.$value->country;
                        break;  
                   }
                   else
                   {
                        $locationInformation = 'No clinic address yet';
                   }
                }
            }


            $array = array(
                'id' => $this->nagkamoritsing->bungkag($arr->IDappointments),
                'title' => $patientName,
                'color' => $color,
                'start' => $dateStart,
                'end' => $dateEnd,
                'allDay' => $allDay,
                'editable' => $editable,
                'sameday' => $sameday,
                'note' => $arr->note,
                'approved' => $arr->approved,
                'appnum' => $arr->IDappointments,
                'timefrom' => date('g:i A', strtotime($arr->timefrom)),
                'timeto' => date('g:i A', strtotime($arr->timeto)),
                'dateonly' => date('D, F d, o', strtotime($arr->timefrom)),
                'firstname'=> $this->nagkamoritsing->ibalik($arr->firstname),
                'locationinformation' => $locationInformation,
                'lastname' => $this->nagkamoritsing->ibalik($arr->lastname)
            );
            array_push($arrayAppointment, $array);
        }

        // // echo date('D, F d, o @ g:i A', $start);

        // header('Content-Type: application/json');
        echo json_encode($arrayAppointment);
        // $this->output->enable_profiler(TRUE);
    }

    public function settings()
    {
        $this->breadcrumb[] = array(
            'link' => 'practiceSettings',
            'linkname' => 'Practice Settings'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settings',
            'linkname' => 'Appointment Settings'
        );
        $this->IDappointments_settings = filter_var($this->input->get('IDappointments_settings'), FILTER_SANITIZE_STRING);
        $this->modifyonly = filter_var($this->input->get('modifyonly'), FILTER_SANITIZE_STRING);

        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal 

        $error = FALSE;
        $this->activeMainMenu = "Doctor-options";
        $customJsTop = array('clinic-time','countries',
            '../lib/jquery.bootstrap.wizard',
            // 'phone-numbers'
        );
        $customJs = array();
        $urlAdd = 'doctorDashboard';
        $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';

        $this->form_validation->set_rules('settingsname', 'Settings name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('averageTimePerPatient', 'Average time per patient', 'trim|xss_clean|required|is_natural');
        $this->form_validation->set_rules('appointmentLimit', 'Appointment limit', 'trim|xss_clean|required|is_natural');
        $this->form_validation->set_rules('queueLimit', 'Queue limit', 'trim|xss_clean|required|is_natural');
        $this->form_validation->set_rules('allow-overlap', 'Allow overlap', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $this->errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }

        if (!empty($this->IDappointments_settings))
        {
            $this->appointmentSettingsDetail = $this->appointmentsmodel->getAppointmentSettingsDetail($this->IDappointments_settings);
            $this->settingsname = ucwords(@$this->appointmentSettingsDetail->settingsname);
            $this->averageTimePerPatient = @$this->appointmentSettingsDetail->averagetimeperpatient_min;
            $this->appointmentLimit = @$this->appointmentSettingsDetail->appointment_limit;
            $this->queueLimit = @$this->appointmentSettingsDetail->queue_limit;
            $this->allowOverlap = @$this->appointmentSettingsDetail->allowoverlap;
        }

        if ($_POST)
        {
            $this->settingsname = ucwords(filter_var($this->input->post('settingsname'), FILTER_SANITIZE_STRING));
            $this->averageTimePerPatient = filter_var($this->input->post('averageTimePerPatient'), FILTER_SANITIZE_STRING);
            $this->appointmentLimit = filter_var($this->input->post('appointmentLimit'), FILTER_SANITIZE_STRING);
            $this->queueLimit = filter_var($this->input->post('queueLimit'), FILTER_SANITIZE_STRING);
            $this->allowOverlap = filter_var($this->input->post('allow-overlap'), FILTER_SANITIZE_STRING);
        }

        if (($error == false) && ($_POST) )
        {
            $doctorInfo = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $dataArray = array(
                'settingsname' => $this->settingsname,
                'averageTimePerPatient' => $this->averageTimePerPatient,
                'appointmentLimit' => $this->appointmentLimit,
                'queueLimit' => $this->queueLimit,
                'allowOverlap' => @$this->allowOverlap,
                'IDemr_user' => $doctorInfo->IDemr_user
            );
            if (!empty($this->IDappointments_settings))
            {
                $dataArray['IDappointments_settings'] = $this->IDappointments_settings;
                $IDappointments_settings = $this->appointmentsmodel->updateAppointmentSettings($dataArray);
            }
            else
                $IDappointments_settings = $this->appointmentsmodel->appointmentSettings($dataArray);
            
            if (strcasecmp($this->modifyonly, 'TRUE')==0)
                redirect(base_url().'practiceSettings', 'refresh');
            else
                redirect(base_url().'appointments/settingsAppointmentType?IDappointments_settings='.@$IDappointments_settings, 'refresh');
        }

        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Clinic Settings - Doctor Dashboard', 
            'customJsTop' => $customJsTop,
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/settings'
        ));
    }

    public function settingsAppointmentType()
    {
        $this->breadcrumb[] = array(
            'link' => 'practiceSettings',
            'linkname' => 'Practice Settings'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settings',
            'linkname' => 'Appointment Settings'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settingsAppointmentType',
            'linkname' => 'Appointment type'
        );
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal 

        $error = FALSE;
        $this->activeMainMenu = "Doctor-options";
        $customJsTop = array('clinic-time','countries',
            '../lib/jquery.bootstrap.wizard',
            // 'phone-numbers'
        );
        $customJs = array(
            // 'numbersOnly',
            // 'clinic',
            'fullcalendar.min','calendar',
            'list-groups'
        );
        $urlAdd = 'doctorDashboard';
        $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';

        # get the appointment type
        $doctorInfo = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->appointmentSetting = $this->appointmentsmodel->getAppointmentSettings($doctorInfo->IDemr_user);
        $this->appointmentType = $this->appointmentsmodel->getAllAppointmentType();
        $this->IDappointments_settings = filter_var($this->input->get('IDappointments_settings'), FILTER_SANITIZE_STRING);
        $this->modifyonly = filter_var($this->input->get('modifyonly'), FILTER_SANITIZE_STRING);

        if (empty($this->IDappointments_settings))
        {
            redirect(base_url().'appointments/settings', 'refresh');
        }
       
        if (($_POST))
        {
            $this->appointmenttype = $this->input->post('appointmenttype');
            
            $this->appointmenttype = array_unique($this->appointmenttype);
            $dataInsert = array(
                'appointmenttypes' => $this->appointmenttype,
                'IDappointments_settings' => $this->IDappointments_settings
            );

            if (strcasecmp($this->modifyonly, 'TRUE')==0)
            {
                $this->appointmentsmodel->addAppointmentTypePriority($dataInsert);
                redirect(base_url().'practiceSettings', 'refresh');
            }
            else
            {
                if ($this->appointmentsmodel->checkIfOwnerSettings($this->IDappointments_settings, $doctorInfo->IDemr_user) >= 1)
                {
                    $this->appointmentsmodel->addAppointmentTypePriority($dataInsert);
                    redirect(base_url().'appointments/settingsLocation?IDappointments_settings='.@$this->IDappointments_settings, 'refresh');
                }
                else
                {
                    redirect(base_url().'appointments/settingsLocation', 'refresh');   
                }
            }
        }
        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Clinic Settings - Doctor Dashboard', 
            'customJsTop' => $customJsTop,
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/settings-appointment-type'
        ));
    }

    public function addAppointmentType()
    {
        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal
        
        if ($_POST)
        {
            $error = false;
            $this->errorMessage = '';

            $this->form_validation->set_rules('typename', 'Type name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('typedescription', 'Type description', 'trim|xss_clean|required');

            if ($this->form_validation->run() == FALSE)
            {
                $error = true;
                $this->errorMessage = validation_errors();
            }
            else
            {
                $error = false;
            }
            $this->typename = ucwords(filter_var($this->input->post('typename'), FILTER_SANITIZE_STRING));
            $this->typedescription = ucwords(filter_var($this->input->post('typedescription'), FILTER_SANITIZE_STRING));

            if ($error != true)
            {
                $data = array(
                    'typename' => $this->typename,
                    'typedescription' => $this->typedescription,
                );

                $IDappointment_type = $this->appointmentsmodel->addAppointmentType($data);
            }

            $arrayJson = array(
                'errorStatus' => $error,
                'errorMessage' => $this->errorMessage,
                'typename' => @$this->typename,
                'typedescription' => @$this->typedescription,
                'IDappointment_type' => @$IDappointment_type
            );

            header('Content-Type: application/json');
            echo json_encode($arrayJson);
        }
        else
        {

            $this->load->view('appointments/settings-appointment-type-add.phtml', array());
        }
    }

    public function settingsLocation()
    {
        $this->breadcrumb[] = array(
            'link' => 'practiceSettings',
            'linkname' => 'Practice Settings'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settings',
            'linkname' => 'Appointment Settings'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settingsAppointmentType',
            'linkname' => 'Appointment type'
        );
        $this->breadcrumb[] = array(
            'link' => 'appointments/settingsLocation',
            'linkname' => 'Location settings'
        );
        $this->errorMessage = '';
        $this->extraError = '';
        $validationCode = '';
        $validationUrl = '';
        $error = false;

        # check if the user is a registered doctor in nambal
        if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal 

        $error = FALSE;
        $this->activeMainMenu = "Doctor-options";
        $customJsTop = array('clinic-time','countries',
            '../lib/jquery.bootstrap.wizard',
            // 'phone-numbers'
        );
        $customJs = array(
            // 'numbersOnly',
            // 'clinic',
            'fullcalendar.min','calendar',
            'list-groups'
        );
        $urlAdd = 'doctorDashboard';
        $customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';
        $this->IDappointments_settings = filter_var($this->input->get('IDappointments_settings'), FILTER_SANITIZE_STRING);
        $this->IDlocation_info = filter_var($this->input->get('IDlocation_info'), FILTER_SANITIZE_STRING);
        $doctorInfo = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->appointmentSettings = $this->appointmentsmodel->getAppointmentSettings($doctorInfo->IDemr_user);
        $this->locationType = $this->locationmodel->getAllLocationType();
        
        if (!empty($this->IDlocation_info))
        {
            $this->locationDetail = $this->locationmodel->getLocationDetail($this->IDlocation_info);

            $this->appointmentsettings = $this->locationDetail->IDappointments_settings;
            $this->IDappointments_settings = $this->locationDetail->IDappointments_settings;
            $this->website = $this->locationDetail->website;
            $this->numberOfDoctors = $this->locationDetail->numberofdoctors;
            $this->locationname = ucwords($this->locationDetail->locationname);
            $this->locationTypes = ucwords($this->locationDetail->locationtype);

            $this->street = strtolower($this->locationDetail->street);
            $this->city = strtolower($this->locationDetail->city);
            $this->state = strtolower($this->locationDetail->state);
            $this->country = strtolower($this->locationDetail->country);
            $this->postalcode = $this->locationDetail->postcode;
            
            foreach ($this->locationDetail->getLocationEmail as $value) 
            {
                $this->emailaddress[] = @$value->emailaddress;
                $this->emaildescription[] = @$value->description;
            }

            foreach ($this->locationDetail->getLocationPhone as $value) 
            {
                $this->phoneNumber[] = @$value->phonenumber;
                $this->countryCode[] = @$value->countrycode;
                $this->areaCode[] = @$value->areacode;
            }
        }

        $this->specificAppointmentType = $this->appointmentsmodel->getDoctorAppointmentType($this->IDappointments_settings);
        $this->appointmentSettingsDetail = $this->appointmentsmodel->getAppointmentSettingsDetail($this->IDappointments_settings);
        if (empty($this->specificAppointmentType))
        {
            redirect(base_url().'appointments/settingsAppointmentType?IDappointments_settings='.$this->IDappointments_settings, 'refresh');
        }

        if (!empty($this->IDappointments_settings))
        {
            $getAppointmentActualSchedules = $this->appointmentsmodel->getAppointmentActualSchedules($this->IDappointments_settings);

            // print_r(count($getAppointmentActualSchedules));
            $this->countschedule = count(@$getAppointmentActualSchedules);

            foreach ($getAppointmentActualSchedules as $value) 
            {
                $this->dayoftheweek[] = @$value->day;
                $this->appointmentType[] = @$value->IDappointment_type;
                $this->timefrom[] = @$value->timefrom;
                $this->timeto[] = @$value->timeto;
                $this->appointmentdescription[] = @$value->description;
            }    
        }


        # data manipulation here
        $this->form_validation->set_rules('appointment-settings','Appointment settings', 'trim|required|xss_clean');
        $this->form_validation->set_rules('location-name','Location name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('website','website', 'trim|xss_clean|prep_url');
        $this->form_validation->set_rules('numberOfDoctors','Number of doctors', 'trim|is_natural_no_zero|required|xss_clean');
        $this->form_validation->set_rules('locationType','Location type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('street','Street', 'trim|required|xss_clean');
        $this->form_validation->set_rules('city','City', 'trim|required|xss_clean');
        $this->form_validation->set_rules('state','State', 'trim|required|xss_clean');
        $this->form_validation->set_rules('country','Country', 'trim|required|xss_clean');
        $this->form_validation->set_rules('postalcode','Postal code', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dayoftheweek','Day of the week', 'required');
        $this->form_validation->set_rules('appointmenttype','Appointment type', 'required');
        $this->form_validation->set_rules('timefrom','Time from', 'required');
        $this->form_validation->set_rules('timeto','Time to', 'required');
        $this->form_validation->set_rules('appointmentdescription','Appointment description', 'required');
        $this->form_validation->set_rules('count-schedule','count-schedule', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $this->errorMessage .= validation_errors();
        }
        else
        {
            $error = false;
        }       

        if ($_POST)
        {
            $this->appointmentsettings = ucwords(filter_var($this->input->post('appointment-settings'), FILTER_SANITIZE_STRING));
            $this->IDappointments_settings = $this->appointmentSettings;
            $this->website = filter_var($this->input->post('website'), FILTER_SANITIZE_STRING);
            $this->numberOfDoctors = filter_var($this->input->post('numberOfDoctors'), FILTER_SANITIZE_STRING);
            $this->locationname = ucwords(filter_var($this->input->post('location-name'), FILTER_SANITIZE_STRING));
            $this->locationTypes = ucwords(filter_var($this->input->post('locationType'), FILTER_SANITIZE_STRING));

            $this->street = strtolower(filter_var($this->input->post('street'), FILTER_SANITIZE_STRING));
            $this->city = strtolower(filter_var($this->input->post('city'), FILTER_SANITIZE_STRING));
            $this->state = strtolower(filter_var($this->input->post('state'), FILTER_SANITIZE_STRING));
            $this->country = strtolower(filter_var($this->input->post('country'), FILTER_SANITIZE_STRING));
            $this->postalcode = filter_var($this->input->post('postalcode'), FILTER_SANITIZE_STRING);
            
            $this->emailaddress = filter_var_array($this->input->post('emailaddress'), FILTER_SANITIZE_EMAIL);
            $this->emaildescription = filter_var_array($this->input->post('emaildescription'), FILTER_SANITIZE_STRING);
            $this->countryCode = filter_var_array($this->input->post('countryCode'), FILTER_SANITIZE_NUMBER_INT);
            $this->areaCode = filter_var_array($this->input->post('areaCode'), FILTER_SANITIZE_NUMBER_INT);
            $this->phoneNumber = filter_var_array($this->input->post('phoneNumber'), FILTER_SANITIZE_NUMBER_INT);

            $this->countschedule = filter_var($this->input->post('count-schedule'), FILTER_SANITIZE_NUMBER_INT);
            $this->dayoftheweek = filter_var_array($this->input->post('dayoftheweek'), FILTER_SANITIZE_STRING);
            $this->appointmentType = filter_var_array($this->input->post('appointmenttype'), FILTER_SANITIZE_NUMBER_INT);
            $this->timefrom = filter_var_array($this->input->post('timefrom'), FILTER_SANITIZE_STRING);
            $this->timeto = filter_var_array($this->input->post('timeto'), FILTER_SANITIZE_STRING);
            $this->appointmentdescription = filter_var_array($this->input->post('appointmentdescription'), FILTER_SANITIZE_STRING);


            # CHECK if timefrom, timeto, day of the week, appointment type is not empty
            for ($countsked=0; $countsked<$this->countschedule; $countsked++) 
            {
                if ((empty($this->dayoftheweek[$countsked])) || (empty($this->appointmentType[$countsked])))
                {
                    $error = TRUE;
                    $this->extraError .= '<p>Day of the week or Appointment type should not be empty.</p>';
                }
                
                if ((empty($this->timefrom[$countsked])) || empty($this->timeto[$countsked]))
                {
                    $error = TRUE;
                    $this->extraError .= '<p>Time from or to should not be empty.</p>';
                }

                if ((!$this->isTime($this->timefrom[$countsked])) || (!$this->isTime($this->timeto[$countsked])))
                {
                    $error = TRUE;
                    $this->extraError .= '<p>Time from or to is not a valid time.</p>';
                }

                $testdate = '2014/09/29 '; // choosed this date because it's the first 400 users, in just less than a week
                if (strtotime($this->timefrom[$countsked].':00') > strtotime($this->timeto[$countsked].':00'))
                {
                    $error = TRUE;
                    $this->extraError .= '<p>Time from is greater than time to.</p>';
                }
            }
            # end don't allow country code ...
            # end of CHECKing

            # end of data manipulation here
            # now saving to db
            if ($error == FALSE)
            {
                # must check if the user owns the appointment settings
                if ($this->appointmentsmodel->checkIfOwnerSettings($this->appointmentsettings, $doctorInfo->IDemr_user) != 0)
                {
                    $dataInsert = array(
                        'IDemr_user' => $doctorInfo->IDemr_user,
                        'IDsafe_user' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']),
                        'IDappointments_settings' => $this->appointmentsettings,
                        'locationname' => $this->locationname,
                        'website' => $this->website,
                        'numberofdoctors' => $this->numberOfDoctors,
                        'locationtype' => $this->locationTypes,
                        'street' => $this->street,
                        'state' => $this->state,
                        'city' => $this->city,
                        'country' => $this->country,
                        'postcode' => $this->postalcode,
                        'emailaddress' => $this->emailaddress,
                        'emaildescription' => $this->emaildescription,
                        'countryCode' => $this->countryCode,
                        'areaCode' => $this->areaCode,
                        'phoneNumber' => $this->phoneNumber,
                        'countschedule' => $this->countschedule,
                        'dayoftheweek' => $this->dayoftheweek,
                        'appointmenttype' => $this->appointmentType,
                        'timefrom' => $this->timefrom,
                        'timeto' => $this->timeto,
                        'appointmentdescription' => $this->appointmentdescription
                    );
                    if (!empty($this->IDlocation_info))
                    {
                        $dataInsert['IDlocation_info'] = $this->IDlocation_info;
                        $this->clinicsettings->updatestep3($dataInsert);
                    }
                    else
                    {
                        $this->clinicsettings->insertStep3($dataInsert);
                    }
                }
                # end of checking users owns ....
                // $this->output->enable_profiler(TRUE);
                redirect(base_url().'practiceSettings', 'refresh');
            }
        }

        $this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Clinic Settings - Doctor Dashboard', 
            'customJsTop' => $customJsTop,
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'appointments/settings-location'
        ));
    }

    private function isTime($time) 
    {
        $datetime = '2014/09/29 '.$time.':00';
        $unixtime = strtotime( $datetime );

        if ( ! is_bool( $unixtime ) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function displayAppointmentType()
    {
        $this->IDappointments_settings = filter_var($this->input->get('IDappointments_settings'), FILTER_SANITIZE_STRING);
        $this->specificAppointmentType = $this->appointmentsmodel->getDoctorAppointmentType($this->IDappointments_settings);
        $this->load->view('appointments/display-appointment-type.phtml', array());
    }

    public function displayClinicLocationInput()
    {
        $this->IDappointments_settings = filter_var($this->input->get('IDappointments_settings'), FILTER_SANITIZE_STRING);
        $this->countSchedule = filter_var($this->input->get('additional-section'), FILTER_SANITIZE_STRING);
        $this->specificAppointmentType = $this->appointmentsmodel->getDoctorAppointmentType($this->IDappointments_settings);
        $this->load->view('appointments/settings-appointment-schedule.phtml', array());
    }

    public function deleteSettings()
    {
        $ajax = TRUE;
        $error = FALSE;
        $errorMessage = '';
        $ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $this->form_validation->set_rules('IDappointments_settings', 'IDappointments_settings',  'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = TRUE;
        }
        else
        {
            $error = FALSE;
        }
        $IDappointments_settings = filter_var($this->input->post('IDappointments_settings'), FILTER_SANITIZE_STRING);

        if ($error == FALSE)
        {
            $this->appointmentsmodel->deleteAppointmentSettings($IDappointments_settings);
        }

        $jsonDisplay = array(
            'errorStatus' => $error
        );

        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');

            echo json_encode(
                array(
                    'errorStatus' => $error, 
                    'errorMessage' => $errorMessage
                )
            );
            # end of ajax ang display
        }
        else
        {
            $this->output->enable_profiler(TRUE);
        }
    }


    private function eventColor($eventID, $avail = false)
    {
        if ($eventID == 1)
        {
            if ($avail == true)
            return "rgba(255,0,0,.3)";
            else
            return "rgba(255,0,0,.1)";
        }
        elseif($eventID == 2)
        {
            if ($avail == true)
            return "rgba(0,255,0,.2)";    
            else
            return "rgba(0,255,0,.1)";
        }
        elseif($eventID == 3)
        {
            if ($avail == true)
            return "rgba(255, 182, 0, .3)";
            else
            return "rgba(255, 182, 0, .1)";
        }
        elseif($eventID == 5)
        {
            if ($avail == true)
            return "rgba(255, 0, 255, .3)";
            else
            return "rgba(255, 0, 255, .1)";
        }
        else
        {
            if ($avail == true)
            return "rgba(0,0,255,.5)";
            else
            return "rgba(0,0,255,.1)";
        }
    }

    private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                $errorReturn = $this->errorHandlings($displaytype);
                return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                $this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                $this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
            $errorReturn = $this->errorHandlings($displaytype);
            return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
        $errorD = $this->errordisplay->identifyError($errornum);
        if (strcasecmp('DOM', $displaytype)==0)
        {
            echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
            return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
            echo json_encode(
                array(
                    'errorStatus' => TRUE, 
                    'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
                    )); 
            return $errorD;
        }
        else
            redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
        $errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
            if (strcasecmp('DOM', $displaytype)==0)
            {
                echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
                return $errorD;
            }
            else
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
}
?>