<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class PracticeSettings extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
		$this->load->model('Login_model', 'login');
		$this->load->model('Appointment_model', 'appointmentsmodel');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Emruser_Clinic_model', 'emruser_clinic');
        $this->load->model('Emruser_model', 'emruser');
        $this->load->model('clinicsettings_model', 'clinicsettings');
        $this->load->model('location_model','locations');
		$this->load->library('form_validation');

		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('facebook', $fb_config);
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);
     


        $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // }


		# get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}

	public function index()
	{
		#check session
		$this->checkSession();

		$this->activeMainMenu = "Doctor-options";
		$customJsTop = array('clinic-time','countries',
			// 'phone-numbers'
		);
		$customJs = array(
			// 'numbersOnly',
			// 'clinic',
			'practice-settings'
			// 'tables-dynamic'
		);
		$this->breadcrumb[] = array(
            'link' => 'practiceSettings',
            'linkname' => 'Practice Settings'
        );
        $urlAdd = 'doctorDashboard';
		$customCss = array ('custom-dashboard','zocial', 'custom-profile-viewer');
        $navOpen = 'doctor options';

        $doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
        $this->IDemr_user = $doctorInfo->IDemr_user;
		$this->appointmentSettings = $this->appointmentsmodel->getAppointmentSettings($doctorInfo->IDemr_user);
		$this->getDoctorActualSchedule = $this->clinicsettings->getDoctorActualSchedule($doctorInfo->IDemr_user);
		$this->allLocations  = $this->locations->getAllLocation($doctorInfo->IDemr_user);

		$this->load->view(
        'doctor-dashboard-light-blue.phtml', array(
            'title' => 'Clinic Settings - Doctor Dashboard', 
			'customJsTop' => $customJsTop,
            'customCss' => $customCss,
            'customJs' => $customJs,
            'urlAdd' => $urlAdd,
            'navOpen' => $navOpen,
            'view' => 'doctor-dashboard-clinic/index'
        ));
	}

	public function displayLicenseSettings()
	{
		# get the doctors info
		$this->checkSession();
		$this->doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->licenseInfo = $this->emruser->getLicense($this->doctorInfo->IDemr_user);
		$this->specialty = $this->emruser->getDocSpecialty($this->doctorInfo->IDemr_user);
		$this->load->view('doctor-dashboard-clinic/license-practice-settings.phtml', array());

		 // $this->output->enable_profiler(TRUE);
	}

	public function saveLicenseSettings()
	{
		$this->checkSession();
		$ajax = FALSE;
		$error = TRUE;
		$errorMessage = '';
		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
		
		if ($_POST)
		{
			// $this->form_validation->set_rules('specialty', 'specialty',  'required|xss_clean');
			$this->form_validation->set_rules('licensenumber','licensenumber', 'trim|xss_clean|required');
			$this->form_validation->set_rules('dateregistered', 'dateregistered', 'xss_clean|valid_date|required');
			$this->form_validation->set_rules('dateexpire', 'dateexpire', 'xss_clean|valid_date|required');

			if ($this->form_validation->run() == FALSE)
			{
				$error = TRUE;
				$errorMessage = validation_errors();
			}
			else
			{
				$error = FALSE;
			}

			if ($error == FALSE)
			{
				$licensenumber = filter_var($this->input->post('licensenumber'), FILTER_SANITIZE_STRING);
				$dateregistered = filter_var($this->input->post('dateregistered'), FILTER_SANITIZE_STRING);
				$dateexpire = filter_var($this->input->post('dateexpire'), FILTER_SANITIZE_STRING);

				if (!empty($dateregistered)) 
				list($monthregister, $dayregister, $yearregister) = explode('/', $dateregistered);

				if (!empty($dateexpire)) 
				list($monthexpire, $dayexpire, $yearexpire) = explode('/', $dateexpire);

				$dateregistered = date('o-m-d h:i:s' ,strtotime(@$yearregister.'-'.@$monthregister.'-'.@$dayregister));
				$dateexpire = date('o-m-d h:i:s' ,strtotime(@$yearexpire.'-'.@$monthexpire.'-'.@$dayexpire));

				$docInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
				$dataInsert = array(
					'licensenumber' => $licensenumber,
					'dateregistered' => $dateregistered,
					'dateexpire' => $dateexpire,
					'IDemr_user' => $docInfo->IDemr_user,
					'active' => TRUE
				);
				$this->emruser->updateLicense($dataInsert);
			}
		}
		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
	}

	public function addAppointmentSettings()
	{
		$this->checkSession();
		$this->doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->allLocations  = $this->locations->getAllLocation($this->doctorInfo->IDemr_user);
		$this->load->view(
        'doctor-dashboard-clinic/practice-settings-add-appointment-settings.phtml', array());	
	}

	public function addAnotherSpecialty()
	{
		$this->load->model('Emrspecialty_model', 'emrspecialty');
		$this->checkSession();
		$ajax = FALSE;
		$error = TRUE;
		$errorMessage = '';

		if ($_POST)
		{
			$this->form_validation->set_rules('specialty', 'specialty',  'required|xss_clean');
			if ($this->form_validation->run() == FALSE)
			{
				$error = TRUE;
				$errorMessage = validation_errors();
			}
			else
			{
				$error = FALSE;
			}

			if ($error == FALSE)
			{
				$specialty = $this->input->post('specialty');
				$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
				$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

				$docInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
				$dataInsert = array(
					'specialty_id' => $specialty,
					'IDemr_user' => $docInfo->IDemr_user
				);
				$this->emruser->insertSpecialty($dataInsert);
			}			
		}
		
		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');
			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->allEmrSpecialty = $this->emrspecialty->getAll();
			$this->load->view(
	        'doctor-dashboard-clinic/add-specialty.phtml', array());
		}
		
	}
	
	public function deleteSpecialty()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('specialtyid', 'specialtyid',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);

		if ($error == FALSE)
		{
			$specialtyid = filter_var($this->input->post('specialtyid'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$docInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
			$this->emruser->deleteSpecialty($specialtyid, $docInfo->IDemr_user);
		}
	
		if ($ajax == TRUE)
		{			
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Medication Information', 
			'view' => 'doctor-dashboard-clinic/delete'));
		}
		return;
	}

	////// reate clinic
	public function createClinic()
	{
		$message = '';
		$position = '';
		$numberOfDoctors = 1;
		$admin = 0;
		$role = array();
		$countryCode = array();
		$areaCode = array();
		$number = array();
		$fromHour = array();
		$fromMinute = array();
		$meridianFrom = array();
		$toHour = array();
		$toMinute = array();
		$meridianTo = array();
		$description = array();
		$errorTime = array();
		
		#validate fields
		if(!empty($this->input->post('number-of-doctors')))
		{
			$this->form_validation->set_rules('number-of-doctors','Number of Doctors', 'trim|xss_clean|required|');
		}
		
		if(!empty($this->input->post('position')))
		{
			$this->form_validation->set_rules('position','Position', 'trim|xss_clean|required|');
		}
		
		$this->form_validation->set_rules('clinic-name','Clinic Name', 'trim|xss_clean|required');
		$this->form_validation->set_rules('email-address','Email Address', 'trim|xss_clean');
		$this->form_validation->set_rules('number-of-doctors','Number of Doctors', 'trim|xss_clean');
		$this->form_validation->set_rules('role[]','', 'trim|xss_clean');
		$this->form_validation->set_rules('country','Select Country', 'trim|xss_clean|required');
		$this->form_validation->set_rules('state','Select State', 'trim|xss_clean|required');
		$this->form_validation->set_rules('street','Street', 'trim|xss_clean|required');
		$this->form_validation->set_rules('city','City', 'trim|xss_clean|required');
		$this->form_validation->set_rules('postcode','Postal Code or Zip Code', 'trim|xss_clean|required');
		$this->form_validation->set_rules('countryCode[]','Select Country Code', 'trim|xss_clean');
		$this->form_validation->set_rules('areaCode[]','00', 'trim|xss_clean');
		$this->form_validation->set_rules('number[]','xxx-xxxx', 'trim|xss_clean');
		$this->form_validation->set_rules('fromHour[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('fromMinute[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('meridianFrom[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('toHour[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('toMinute[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('meridianTo[]','', 'trim|xss_clean|required');
		$this->form_validation->set_rules('description[]','', 'trim|xss_clean');
		
		#run form validation
		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		} 
		else 
		{
			$error = FALSE;
		}
		
		# sanitize datas
		
		# if number of doctors is set
		if(!empty($this->input->post('number-of-doctors')))
		{
			$numberOfDoctors = filter_var($this->input->post('number-of-doctors'), FILTER_SANITIZE_NUMBER_INT);
			$position = 'owner';
			$admin = 1;	
		}
		
		# if position is set
		if($this->input->post('position'))
		{
			if(!empty($this->input->post('number-of-doctors')))
			{
				$position = filter_var($this->input->post('position'), FILTER_SANITIZE_STRING).',owner';
			}
			else
			{
				$position = filter_var($this->input->post('position'), FILTER_SANITIZE_STRING);
			}
		}
		
		for($i=0;$i<sizeof($this->input->post('role'));$i++)
		{
			$role[$i] = filter_var($this->input->post('role')[$i], FILTER_SANITIZE_STRING);
		}
		
		for($i=0;$i<sizeof($this->input->post('countryCode'));$i++)
		{
			$countryCode[$i] = filter_var($this->input->post('countryCode')[$i], FILTER_SANITIZE_STRING);
		}
		
		for($i=0;$i<sizeof($this->input->post('areaCode'));$i++)
		{
			$areaCode[$i] = filter_var($this->input->post('areaCode')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('number'));$i++)
		{
			$number[$i] = filter_var($this->input->post('number')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('fromHour'));$i++)
		{
			$fromHour[$i] = filter_var($this->input->post('fromHour')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('fromMinute'));$i++)
		{
			$fromMinute[$i] = filter_var($this->input->post('fromMinute')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('meridianFrom'));$i++)
		{
			$meridianFrom[$i] = filter_var($this->input->post('meridianFrom')[$i], FILTER_SANITIZE_STRING);
		}
		
		for($i=0;$i<sizeof($this->input->post('toHour'));$i++)
		{
			$toHour[$i] = filter_var($this->input->post('toHour')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('toMinute'));$i++)
		{
			$toMinute[$i] = filter_var($this->input->post('toMinute')[$i], FILTER_SANITIZE_NUMBER_INT);
		}
		
		for($i=0;$i<sizeof($this->input->post('meridianTo'));$i++)
		{
			$meridianTo[$i] = filter_var($this->input->post('meridianTo')[$i], FILTER_SANITIZE_STRING);
		}
		
		for($i=0;$i<sizeof($this->input->post('description'));$i++)
		{
			$description[$i] = filter_var($this->input->post('description')[$i], FILTER_SANITIZE_STRING);
		}
		
		$clinicName = filter_var($this->input->post('clinic-name'), FILTER_SANITIZE_STRING);
		$emailAddress = filter_var($this->input->post('email-address'), FILTER_SANITIZE_EMAIL);
		$country = filter_var($this->input->post('country'), FILTER_SANITIZE_STRING);
		$state = filter_var($this->input->post('state'), FILTER_SANITIZE_STRING);
		$street = filter_var($this->input->post('street'), FILTER_SANITIZE_STRING);
		$city = filter_var($this->input->post('city'), FILTER_SANITIZE_STRING);
		$postcode = filter_var($this->input->post('postcode'), FILTER_SANITIZE_STRING);
		
		
		$createClinicInformation = array(
			'IDsafe_user' => $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']),
			'clinicname' => $clinicName,
			'email' => $emailAddress,
			'country' => $country,
			'state' => $state,
			'street' => $street,
			'city' => $city,
			'postcode' => $postcode,
			'countrycode' => $countryCode,
			'areacode' => $areaCode,
			'phonenumber' => $number,
			'numberOfDoctors' => $numberOfDoctors,
			'fromHour' => $fromHour,
			'fromMinute' => $fromMinute,
			'meridianFrom' => $meridianFrom,
			'toHour' => $toHour,
			'toMinute' => $toMinute,
			'meridianTo' => $meridianTo,
			'description' => $description			
		);
		
		$addClinicStaff = array(
			'IDsafe_user' => $createClinicInformation['IDsafe_user'],
			'admin' => $admin,
			'position' => $position
		);
		
		if(!$error)
		{	
			// check if for clinic hours input errors
			for($i=0;$i<7;$i++)
			{
				if(($createClinicInformation['fromHour'][$i] != '00' && $createClinicInformation['toHour'][$i] === '00') ||
					($createClinicInformation['fromHour'][$i] === '00' && $createClinicInformation['toHour'][$i] != '00'))
				{
					$errorTime[$i] = $i+1;
				}
				
			}
			
			if(!empty($errorTime))
			{
				$status = "error time"; 
				$message = "Please set clinic hours properly";
			}
			else
			{
				// create clinic information
				$createClinic = $this->emruser_clinic->createClinicInformation($createClinicInformation);
				$addStaff = $this->emruser_clinic->addClinicStaff($addClinicStaff);
				
				if($createClinic && $addStaff)
				{
					$status = "create clinic"; 
					$message = "Successfully added clinic";
				}
				else
				{
					$status = "create failed"; 
					$message = "There seems to be a problem. . resubmitting. .";
				}
			}
			
			
		}
		else
		{	
			$status = 'validation error';
			$message = "Some inputs are invalid";
		}
		
		$data = array(
			'status' => $status, 
			'message' => $message,
			'errorTime' => $errorTime,
			'sampleData' => "{$position},{$numberOfDoctors},{$admin}"
		);
		echo json_encode($data);
		exit;
	}

	public function appointmentSettingsDisplay()
	{
		$this->checkSession();
		$doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->appointmentSettings = $this->appointmentsmodel->getAppointmentSettings($doctorInfo->IDemr_user);
		
		$this->load->view('doctor-dashboard-clinic/practice-settings.phtml', array());

	}

	public function displayPracticeLocations()
	{
		$this->checkSession();
		$doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->allLocations  = $this->locations->getAllLocation($doctorInfo->IDemr_user);
		$this->load->view('doctor-dashboard-clinic/practice-settings-display-locations.phtml', array());
	}

	public function displayAppointmentSchedulesSettings()
	{
		$this->checkSession();
		$doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->getDoctorActualSchedule = $this->clinicsettings->getDoctorActualSchedule($doctorInfo->IDemr_user);
		$this->load->view('doctor-dashboard-clinic/practice-settings-display-schedules.phtml', array());
	}

	public function displayAllAppointmentList()
	{
		$this->checkSession();
		$doctorInfo = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
		$this->appointmentSettings = $this->appointmentsmodel->getAppointmentSettings($doctorInfo->IDemr_user);
		$this->load->view('doctor-dashboard-clinic/practice-settings-display-appointment-settings.phtml', array());
	}

	public function deleteLocation()
	{
		$this->checkSession();
		$ajax = FALSE;
		$error = FALSE;
		$errorMessage = '';

		$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

		if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');

            echo json_encode(
                array(
                    'errorStatus' => $error, 
                    'errorMessage' => $errorMessage
                )
            );
            # end of ajax ang display
        }
        else
        {
            $this->output->enable_profiler(TRUE);
        }
	}
	
	///// check session
	public function checkSession()
	{
		 # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   

            # check if the user is a registered doctor in nambal
            if (!$this->emrUser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
            {
                redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
            }
            # end of checking if the user is a registered doctor in nambal  
        }
        # end of redirecting users to dashboard if logged in
	}
}
?>