<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboardnew extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		
		$this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('ContactInformation_model', 'contactinformation');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );


        $this->load->library('facebook', $fb_config);

        if (empty($this->nambal_session['sessionName']))
        {
        	redirect(base_url().'login', 'refresh');
        }

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	// $fbUser = $this->facebook->getUser();
        //     $fbUser = $this->facebook->get_user();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // }

        # get the profile picture
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
    }

	public function index()
	{
        
	}
	
	public function tour()
	{
		$IDsafe_user = 0;
		$firsttime = 0;
		$status = false;
	
		#check session
		$this->checkSession();
		
		$IDsafe_user = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']);
		
		#update after tour
		if($this->login->updateAfterTour($IDsafe_user))
		{
			$status = true;
		}
		
		$data = array(
			'status' => $status
		);
		
		redirect(base_url().'dashboard', 'refresh');
        echo json_encode($data);
		
	}
	
	public function checkSession()
    {
		# redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        # end of redirecting users to dashboard if logged in
	}

	public function indexTest()
	{
		
	}
}
?>