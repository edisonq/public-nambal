<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barangay extends CI_Controller 
{
	
	public function __construct() 
	{
		parent::__construct();
		
		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('Barangaylist_model', 'barangay');
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}


	public function displayList()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->barangay->searchlist($q);

		$json = json_encode($arrayDisplay);
		// $json = $arrayDisplay;
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;

		// $this->output->enable_profiler(TRUE);
	}

	public function getInformation()
	{
		$barangayName = filter_var(@$this->input->post('formBarangay'), FILTER_SANITIZE_STRING);
		$arrayDisplay = $this->barangay->getBarangayInfoString($barangayName);


		$return = array();
		echo json_encode($arrayDisplay);
	}
}
?>