<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class CreateNambalAccount extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Blog Area', 
			'view' => 'create-nambal-account/index'));
		return;
	}
}

?>