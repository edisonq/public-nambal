<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Apps extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		/// this is the page that display the app container

		$this->load->view(
			'homepage.phtml', array('title' => 'Application List',
				'view' => 'apps/index'));
		$appID = $this->input->get('appID');
		echo 'profileID:'.$appID;

		return;
	}

	public function insideIframe(){
		$this->load->view(
			'homepage.phtml', array('title' => 'App Inside Frame',
				'view' => 'apps/iframe'));

		$appID = $this->input->get('appID');
		echo 'profileID:'.$appID;

		return;
	}


}
?>