<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Paraatora extends CI_Controller 
{
	public function __construct() {
		parent::__construct();
        $this->load->library('email');
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');
        $this->admin_session = $this->session->userdata('admin_in');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->library('form_validation');
        $this->load->model('Personalprofile_model', 'personalProfile');
        $this->load->model('Login_model', 'login');
        $this->load->model('Session_model', 'sessionModel');
        $this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
        $this->load->model('Messages_model', 'messagesModel');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Pusher_model', 'pushery');

        $fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );
       
        $this->load->library('facebook', $fb_config);
    
        # check user's if properly logged-in
        if (empty($this->nambal_session['sessionName']))
        {
            redirect(base_url().'login', 'refresh');
        }


        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        //     $fbUser = $this->facebook->getUser();
        //     if (!empty($fbUser))
        //     {
        //         if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        //         {
        //             redirect(base_url().'logout', 'refresh');
        //         }
        //     } else {
        //         redirect(base_url().'login/facebook', 'refresh');
        //     }
        // }
        
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               redirect(base_url().'login', 'refresh');
                // echo $this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress']);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                redirect(base_url().'login', 'refresh');
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
                redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
                redirect(base_url().'register/createAccountThird', 'refresh');
            }

        }
        # end of redirecting users to dashboard if logged in
        # end of checking if user's properly logged in


        # get the profile picture
        $this->load->model('profilepic_model', 'profilepic');
        $this->load->helper('s3');
        $this->profilepicInfo = $this->profilepic->getprofilepicinfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])); 
	}
	public function index()
	{
		$this->checkAdmin();				

		$this->load->view(
        'admin.phtml', array(
            'view' => 'paraatora/index'
        ));
    
        $this->output->enable_profiler(TRUE);
	}

	public function loginadmin()
	{
		if (!empty($this->admin_session['sessionName']))
		{
			redirect(base_url().'paraatora', 'refresh');
		}

		$error = FALSE;
		$ajax = TRUE;
		$this->username = '';
		$this->errorMessage = '';

		$this->form_validation->set_rules('username', 'username', 'trim|xss_clean|required');
		$this->form_validation->set_rules('password', 'password', 'trim|xss_clean|required');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$this->errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$this->username = filter_var($this->input->post('username'), FILTER_SANITIZE_STRING);
			$this->password = filter_var($this->input->post('password'), FILTER_SANITIZE_STRING);

			if (strcasecmp($this->username, 'jackyboy')==0)
			{
				if (strcasecmp($this->password, 'nigellito123!@#')==0)
				{
					$data = array(
						'sessionName' => $this->username
					);
					// $this->errorMessage = 'success';
					$this->session->set_userdata('admin_in', $data);
					redirect(base_url().'paraatora', 'refresh');
				}
			}
		}

		$this->load->view(
        'admin.phtml', array(
            'view' => 'paraatora/loginadmin'
        ));
	}

	public function showallusers()
	{
		$this->checkAdmin();
		
		$beginlimit = filter_var($this->input->get('beginlimit'), FILTER_SANITIZE_STRING);
		$endlimit = filter_var($this->input->get('endlimit'), FILTER_SANITIZE_STRING);

		if (empty($beginlimit) || (!isset($beginlimit)))
		{
			$beginLimit = 0;
		}

		if (empty($endlimit) || (!isset($endLimt)))
		{
			$endlimit = 1000;
		}

		$this->allusers = $this->safeuser->showAllUsers($beginLimit, $endlimit);

		$this->load->view(
        'admin.phtml', array(
            'view' => 'paraatora/show-all-users'
        ));
	}

	public function showalldoctors()
	{
		$this->checkAdmin();

		$beginlimit = filter_var($this->input->get('beginlimit'), FILTER_SANITIZE_STRING);
		$endlimit = filter_var($this->input->get('endlimit'), FILTER_SANITIZE_STRING);

		if (empty($beginlimit) || (!isset($beginlimit)))
		{
			$beginLimit = 0;
		}

		if (empty($endlimit) || (!isset($endLimt)))
		{
			$endlimit = 1000;
		}

		$this->alldoctors = $this->emrUser->showAllDoctors($beginLimit, $endlimit);

		$this->load->view(
        'admin.phtml', array(
            'view' => 'paraatora/show-all-doctors'
        ));
	}

	public function disableuser()
	{
		$this->checkAdmin();
		$IDsafe_user = filter_var($this->input->get('IDsafe_user'), FILTER_SANITIZE_STRING);

		$this->safeuser->disableuser($IDsafe_user);

		redirect(base_url().'paraatora/showallusers', 'refresh');
	}

	public function disabledoctor()
	{
		$this->checkAdmin();
		$IDemr_user = filter_var($this->input->get('IDemr_user'), FILTER_SANITIZE_STRING);

		$this->emrUser->disabledoctor($IDemr_user);

		redirect(base_url().'paraatora/showalldoctors', 'refresh');	
	}

	public function activateuser()
	{
		$this->checkAdmin();
		$IDsafe_user = filter_var($this->input->get('IDsafe_user'), FILTER_SANITIZE_STRING);

		$this->safeuser->activateuser($IDsafe_user);

		redirect(base_url().'paraatora/showallusers', 'refresh');
	}

	public function activatedoctor()
	{
		$this->checkAdmin();
		$IDemr_user = filter_var($this->input->get('IDemr_user'), FILTER_SANITIZE_STRING);

		$this->emrUser->activatedoctor($IDemr_user);

		redirect(base_url().'paraatora/showalldoctors', 'refresh');
	}

	public function logout()
	{
		$this->session->unset_userdata('admin_in');
		redirect(base_url().'paraatora', 'refresh');
	}

	private function checkAdmin()
	{
		if (empty($this->admin_session['sessionName']))
		{
			redirect(base_url().'paraatora/loginadmin', 'refresh');
		}
	}
}