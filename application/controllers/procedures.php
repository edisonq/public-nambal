<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Procedures extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
		$this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Procedures_model', 'procedures');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
		$this->load->model('Safejournal_model', 'safejournal');
		$this->load->model('safeuserlog_model', 'safeuserlog');
		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Procedures Information', 
			'view' => 'procedures/index'));
		$sampleData = $this->input->get('sampleData');
		echo 'profileID:'.$sampleData;

		return;
	}

	public function insert()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$procedurelist = array();

		$this->form_validation->set_rules('procedures','Procedures or Surgery', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$procedures = ucwords(filter_var($this->input->post('procedures'), FILTER_SANITIZE_STRING));
			$dateexecuted = $this->input->post('dateexperienced');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);

			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexecuted);
			$dateexecuted = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));
			
			$procedurelist = array(
				'procedures' => @$procedures,
				'dateexecuted' => @$dateexecuted,
				'notes' => @$notes
			);

			$this->procedures->insertProcedure($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']), $procedurelist);
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Procedures Information', 
			'view' => 'procedures/insert'));
		}		

		return;
	}

	public function insertDoctor()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$procedurelist = array();

		# pwede ra mogamit sa $IDsafe_user or $sessionid;
		$IDsafe_user = filter_var($this->input->get('IDsafe_user'), FILTER_SANITIZE_STRING);
		$sessionid = $IDsafe_user;

		$this->form_validation->set_rules('procedures','Procedures or Surgery', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('doctor-notes', 'Notes or journal entry', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$procedures = ucwords(filter_var($this->input->post('procedures'), FILTER_SANITIZE_STRING));
			$dateexecuted = $this->input->post('dateexperienced');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING);

			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexecuted);
			$dateexecuted = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));
			
			$procedurelist = array(
				'procedures' => @$procedures,
				'dateexecuted' => @$dateexecuted,
				'notes' => @$notes
			);

			# wala ko choice ani nga line kailangan mag dali
			# pwede ra ni siya kung 1 account 1 safe_personalinfo
			# get the IDsafe_personalInfo
			$IDsafe_user = $this->nagkamoritsing->ibalik($sessionid);
			$IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
			$doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

			$IDsafe_procedure = $this->procedures->insertProcedure($IDsafe_user, $procedurelist);

			# check if patient ba ni siya sa doctor
			# if dili i add siya as one of the patient
			$IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
			$IDemr_patient = @$IDemr_patient->IDemr_patient;
			if (empty($IDemr_patient))
				$IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

				
			$procedurelist['IDsafe_personalInfo'] = $IDsafe_personalInfo;
			$procedurelist['IDemr_user'] = $doctorInformation->IDemr_user;
			$procedurelist['doctor-notes'] = $doctorNotes;
			$procedurelist['IDsafe_procedure'] = $IDsafe_procedure;
			$procedurelist['IDemr_patient'] = $IDemr_patient;
				
			$this->safejournal->inserjournalprocedure($procedurelist);
			$patientinfo = $this->safeuser->getSafeUserInfo($IDsafe_user);
			$patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
			$footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

			$emailData = array(
				'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
				'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
			);
			$message = $this->load->view(
	            'for-email.phtml', array('title' => 'Notify patient', 
	            'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
	
			# dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
			$this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);

		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Insert Procedures Information', 
			'view' => 'procedures/insert'));
		}		

		return;
	}

	public function update()
	{
		$this->checkSession();
		$ajax = TRUE;
		$error = TRUE;
		$errorMessage = '';
		$procedurelist = array();

		$this->form_validation->set_rules('IDsafe_procedure','IDsafe_procedure', 'trim|xss_clean|required');
		$this->form_validation->set_rules('procedures','Procedures or Surgery', 'trim|xss_clean|required');
		$this->form_validation->set_rules('dateexperienced', 'Date experienced', 'xss_clean|required|valid_date'); 
		$this->form_validation->set_rules('notes', 'Notes or journal entry', 'trim|xss_clean');
		$this->form_validation->set_rules('isDoctor', 'isDoctor', 'trim|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = true;
			$errorMessage = validation_errors();
		}
		else
		{
			$error = false;
		}

		if ($error == FALSE)
		{
			$ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
			$ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

			$IDsafe_procedure = filter_var($this->input->post('IDsafe_procedure'), FILTER_SANITIZE_STRING);
			$procedures = ucwords(filter_var($this->input->post('procedures'), FILTER_SANITIZE_STRING));
			$dateexecuted = $this->input->post('dateexperienced');
			$notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING);
			$isDoctor = filter_var($this->input->get('isDoctor'), FILTER_SANITIZE_STRING);
			$isDoctor = preg_replace('#[^a-z0-9_]#', '', $isDoctor);
			$doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING); # notes for doctor
			$IDemr_journal = filter_var($this->input->post('IDemr_journal', FILTER_SANITIZE_STRING));

			list($monthStart, $dayStart, $yearStart) = explode('/', $dateexecuted);
			$dateexecuted = date('o-m-d h:i:s' ,strtotime(@$yearStart.'-'.@$monthStart.'-'.@$dayStart));
			
			$procedurelist = array(
				'IDsafe_procedure' => $IDsafe_procedure,
				'procedures' => @$procedures,
				'dateexecuted' => @$dateexecuted,
				'notes' => @$notes
			);

			

			$this->procedures->update($procedurelist);

			if (strcasecmp($isDoctor, 'TRUE')==0)
	        {
	        	$dataInDb = $this->procedures->getToModify($IDsafe_procedure);

				# added for doctor updates
	            # check if patient ba ni siya sa doctor
	            # if dili i add siya as one of the patient
	            // $IDsafe_user = $this->nagkamoritsing->ibalik($this->sessionid);
	            $IDsafe_personalInfo = $dataInDb['IDsafe_personalInfo'];
	            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
	            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
	            $IDemr_patient = @$IDemr_patient->IDemr_patient;
	            if (empty($IDemr_patient))
	                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);


	            $procedurelist['IDsafe_personalInfo'] = $IDsafe_personalInfo;
				$procedurelist['IDemr_user'] = $doctorInformation->IDemr_user;
				$procedurelist['doctor-notes'] = $doctorNotes;
				$procedurelist['IDemr_patient'] = $IDemr_patient;
				$procedurelist['IDemr_journal'] = $IDemr_journal;

	            # doctor notes connection and for sending email
	            # if empty ang IDemr_journal
	            # meaning patient input ni
	            if (!empty($IDemr_journal))
				{
					$procedurelist['IDemr_journal'] = $IDemr_journal;
					$this->safejournal->updateJounal($procedurelist);
				} elseif ((empty($IDemr_journal)) || ($IDemr_journal == 0)) {
	            	$this->safejournal->inserjournalprocedure($procedurelist);
	            } else {
	            	$this->safejournal->addjournal($procedurelist,'safe_procedure');
	            }


	            $patientinfo = $this->safeuser->getSafeUserInfoUsingPersonalInfo($IDsafe_personalInfo);
	            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($patientinfo->IDsafe_user);
	            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

	            $emailData = array(
	                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
	                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
	            );
	            $message = $this->load->view(
	                    'for-email.phtml', array('title' => 'Notify patient', 
	                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
	    
	            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
            	$this->safeuserlog->notifypatientbyemail($patientinfo->IDsafe_user,$message);
				
	           
	        }
		}

		if ($ajax ==TRUE)
		{
			# ajax ang display diri
			header('Content-Type: application/json');

			echo json_encode(
				array('errorStatus' => $error, 'errorMessage' => $errorMessage)
			);
			# end of ajax ang display
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Update Procedures Information', 
			'view' => 'procedures/update'));
		}		

		return;
	}

	public function delete()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('procedureid', 'IDsafe_procedure',  'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		
		if ($error == FALSE)
		{
			$IDsafe_procedure = filter_var($this->input->post('procedureid'), FILTER_SANITIZE_STRING);		
			$this->procedures->deleteSafeProcedure($IDsafe_procedure);
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);

	
		if ($ajax == TRUE)
		{
			
			header('Content-Type: application/json');
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Procedures Information', 
			'view' => 'procedures/delete'));
		}

		return;

	}

	public function deleteDoctor()
	{
		$ajax = TRUE;
		$error = TRUE;
		$this->checkSession();

		# check if doctor ba jud ang ga delete

		$ajax =  filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
		$this->form_validation->set_rules('procedureid', 'procedureid',  'trim|required|xss_clean');

		# know who is the patient
		$sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
		$sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);


		if ($this->form_validation->run() == FALSE)
		{
			$error = TRUE;
		}
		else
		{
			$error = FALSE;
		}

		$jsonDisplay = array(
			'errorStatus' => $error
		);
	
		if ($ajax == TRUE)
		{
			$procedureid = filter_var($this->input->post('procedureid'), FILTER_SANITIZE_STRING);
			$returnlang = $this->procedures->deleteSafeProcedure($procedureid, $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));  #butangan og sakto nga doctor ID

			header('Content-Type: application/json');
			$jsonDisplay['returnlang'] = $returnlang;
			echo json_encode($jsonDisplay);
		}
		else
		{
			$this->load->view(
			'homepage.phtml', array('title' => 'Delete Problem Information', 
			'view' => 'problem/delete'));
			$sampleData = $this->input->get('sampleData');
			echo 'profileID:'.$sampleData;
		}

		return;
	}

	public function dashboardDisplay()
	{
		$this->checkSession();
		// $this->procedureInformation = $this->procedures->displayDashboard($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));

		$this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);

		 # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);

		// $this->procedureInformation = $this->procedures->displayDashboard($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-procedure.js pagination code
        # and display_procedure-information.phtml div procedures-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'procedures/dashboardDisplay?q=0';
        $config["total_rows"] = $this->procedures->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->procedures->displayDashboard($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result

		$this->load->view('procedures/display_procedure-information.phtml', $data, array());
	}

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;
		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->procedureInformation = $this->procedures->displayDashboard($IDsafe_personalInfo);

		# for paging result 
        # will work with custom-dashboard-procedure-profileviewer.js pagination code
        # and display_procedure-information-profileviewer.phtml div procedures-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'procedures/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->procedures->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->procedures->displayDashboard($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('procedures/display_procedure-information-profileviewer.phtml', $data, array());
	}

	public function insertDisplay()
	{
		$this->checkSession();
		$arrayGetProcedure = array();

		$IDsafe_procedure = filter_var($this->input->get('IDsafe_procedure'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_procedure' => $IDsafe_procedure
		);

		# get the problem information
		// $problemInformation = $this->problem->getToModifyProblem($IDsafe_problem);
		$this->procedureInformation = $this->procedures->getToModify($IDsafe_procedure);

		// $this->problemInformation = array(
		// 	'problemName' => @$problemInformation->problemName,
		// 	'havingitnow' => (string)@$problemInformation->havingitnow,
		// 	'dateStart' => @$problemInformation->dateStart,
		// 	'dateEnd' => @$problemInformation->dateEnd,
		// 	'notes' => @$problemInformation->notes,
		// 	'IDsafe_problem' => @$problemInformation->IDsafe_problem
		// );

		$this->load->view('procedures/display_procedure-insert-information.phtml', array());
	}

	public function insertDisplayDoctor()
	{
		$this->checkSession();
		$arrayGetProcedure = array();

		$IDsafe_procedure = filter_var($this->input->get('IDsafe_procedure'), FILTER_SANITIZE_STRING);

		$retData = array(
			'IDsafe_procedure' => $IDsafe_procedure
		);

		# get the problem information
		// $problemInformation = $this->problem->getToModifyProblem($IDsafe_problem);
		$this->procedureInformation = $this->procedures->getToModify($IDsafe_procedure);

		if (!empty($this->procedureInformation['IDemr_journal']))
		{
			$this->journalinformation = $this->safejournal->getjournal($this->procedureInformation['IDemr_journal']);
			$this->procedureInformation['doctornotes'] = $this->journalinformation->notes;
		}



		// $this->problemInformation = array(
		// 	'problemName' => @$problemInformation->problemName,
		// 	'havingitnow' => (string)@$problemInformation->havingitnow,
		// 	'dateStart' => @$problemInformation->dateStart,
		// 	'dateEnd' => @$problemInformation->dateEnd,
		// 	'notes' => @$problemInformation->notes,
		// 	'IDsafe_problem' => @$problemInformation->IDsafe_problem
		// );


		$this->load->view('procedures/display_procedure-insert-information-profileviewer.phtml', array());
	}

	public function displayProcedures()
	{
		$callback = filter_var($this->input->get('callback'), FILTER_SANITIZE_STRING);
		$q = filter_var($this->input->get('q'), FILTER_SANITIZE_STRING);

		$arrayDisplay = $this->procedures->searchlist($q);

		$json = json_encode($arrayDisplay);
		header('Content-Type: application/json');
		echo isset($callback)
		    ? "$callback($json)"
		    : $json;
	}

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }   
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }
}
?>