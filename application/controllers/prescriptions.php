<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Prescriptions extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		
		# --
		$fb_config = array(
            'appId'  => config_item('facebook_app_id'),
            'secret' => config_item('facebook_secret_key')
        );

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('facebook', $fb_config);
        $this->load->library("pagination");
		$this->load->model('Session_model', 'sessionModel');
		$this->load->model('Nagkamoritsing_model', 'nagkamoritsing');
		$this->load->model('Login_model', 'login');
		$this->load->model('Medication_model', 'medication');
		$this->load->model('Prescription_model', 'prescription');
		$this->load->model('Error_model', 'errordisplay');
		$this->load->model('emruser_model', 'emruser');
		$this->load->model('personalprofile_model', 'personalprofile');
        $this->load->model('Safeuseremail_model', 'safeuseremail');
        $this->load->model('Emruser_model', 'emrUser');
        $this->load->model('Safejournal_model', 'safejournal');
        $this->load->model('Safeuser_model', 'safeuser');
        $this->load->model('safeuserlog_model', 'safeuserlog');

		$this->load->helper(array('form', 'url'));
		$this->logoutURL = $this->session->userdata('logoutURL');
		$this->ipAddress = $this->nagkamoritsing->bungkag(@$_SERVER['REMOTE_ADDR']);

		
       
        $this->nambal_session = $this->session->userdata('logged_in');
        $this->facebook_session = $this->session->userdata('fb_profile');
        $this->logoutURL = $this->session->userdata('logoutURL');

        # commented because of the new FB SDK
        // if (!empty($this->facebook_session['id']))
        // {
        // 	$fbUser = $this->facebook->getUser();
        // 	if (!empty($fbUser))
        // 	{
        // 		if (strcmp($this->facebook_session['id'], $fbUser)!=0)
        // 		{
        // 			redirect(base_url().'logout', 'refresh');
        // 		}
        // 	} else {
        // 		redirect(base_url().'login/facebook', 'refresh');
        // 	}
        // } 
	}

	public function index()
	{

	}

    public function dashboardDisplay()
    {
        $checkReturn = $this->checksession('DOM');
        // $this->safeprescription = $this->prescription->displayDashboardPrescription($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']));


        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
                

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo']);


        # for paging result 
        # will work with custom-dashboard-prescription-profileviewer.js pagination code
        # and display_prescription-information-profileviewer.phtml div prescription-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'prescriptions/dashboardDisplay?q=0';
        $config["total_rows"] = $this->prescription->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->prescription->displayDashboardPrescription($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result


        if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo '';
        else
            $this->load->view('prescriptions/display_prescription-information.phtml', $data, array());

    }

	public function dashboardDisplayProfileViewer()
	{
		$checkReturn = $this->checksession('DOM');
		$checkdoctor = $this->checkdoctor('DOM');
		if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
			return;

		$this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->per_page = filter_var($this->input->get('per_page'), FILTER_SANITIZE_STRING);
        $this->per_page = preg_replace('#[^a-z0-9_]#', '', $this->per_page);
				
		$this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

		// $this->safeprescription = $this->prescription->displayDashboardPrescription($IDsafe_personalInfo);

        # for paging result 
        # will work with custom-dashboard-prescription-profileviewer.js pagination code
        # and display_prescription-information-profileviewer.phtml div prescription-pagination-div-id
        $config = array();
        $config["base_url"] = base_url() .'prescriptions/dashboardDisplayProfileViewer?sessionid='.$this->sessionid;
        $config["total_rows"] = $this->prescription->record_count($IDsafe_personalInfo);
        $config["per_page"] = 7;
        $config["uri_segment"] = 'per_page';
        $config['num_links'] = 20;
        $config["use_page_numbers"] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['uri_protocol'] = "PATH_INFO";
        $page = @$this->per_page;
        $this->pagination->initialize($config);
        $data["results"] =  $this->prescription->displayDashboardPrescription($IDsafe_personalInfo, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        # end for paging result
		
		if (strcasecmp($checkReturn['description'],'Session expired')==0)
			echo '';
		else
			$this->load->view('prescriptions/display_prescription-information-profileviewer.phtml', $data, array());
	}

    public function insertPrescription()
    {
        $this->checkSession();
        $ajax = TRUE;
        $error = TRUE;
        $errorMessage = '';
        $prescriptionList = array();
        $IDsafe_prescription;

        $sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $sessionid = preg_replace('#[^a-z0-9_]#', '', $sessionid);

        $this->form_validation->set_rules('writtenon', 'Date started', 'xss_clean|required|valid_date'); 
        $this->form_validation->set_rules('expirydate', 'Date ended', 'xss_clean|valid_date'); 
        $this->form_validation->set_rules('includeAddress', 'Include address', 'trim|xss_clean');
        $this->form_validation->set_rules('includeAge', 'Include age', 'trim|xss_clean');
        $this->form_validation->set_rules('includeGender', 'Include age', 'trim|xss_clean');
        $this->form_validation->set_rules('includeDob', 'Include age', 'trim|xss_clean');
        $this->form_validation->set_rules('includeExpiry', 'Include expiry', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }


        if ($error == FALSE)
        {
            $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
            $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);

            $writtenon = $this->input->post('writtenon');
            $expirydate = $this->input->post('expirydate');
            $includeAddress = filter_var($this->input->post('includeAddress'), FILTER_SANITIZE_STRING);
            $includeAge = filter_var($this->input->post('includeAge'), FILTER_SANITIZE_STRING);
            $includeGender = filter_var($this->input->post('includeGender'), FILTER_SANITIZE_STRING);
            $includeDob = filter_var($this->input->post('includeDob'), FILTER_SANITIZE_STRING);
            $includeExpiry = filter_var($this->input->post('includeExpiry'), FILTER_SANITIZE_STRING);
            
            $writtenon = date('Y-m-d', strtotime($writtenon));
            $expirydate = date('Y-m-d', strtotime($expirydate));

            # insert here
            # getting the doctor information
            $drfullname = ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['firstname'])).' '.ucwords($this->nagkamoritsing->ibalik(@$this->nambal_session['lastname'])).' ,MD';
            $dremailaddress = $this->safeuseremail->getuseremaildefault($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $doctorInformation = $this->emrUser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $rxid = $doctorInformation->rxId;
            $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($sessionid));

            # if rxid is empty
            if (empty($rxid))
                $rxid = $doctorInformation->licenseNumber;

            # mag add sa prescription
            $prescriptionList = array(
                'prescriptiondate' => @$writtenon,
                'prescriptionend' => @$expirydate,
                'dr_name' => @$drfullname,
                'dr_rx' => @$rxid,
                'dr_email' => $this->nagkamoritsing->ibalik(@$dremailaddress->emailAddress),
                'approve' => TRUE,
                'active' => TRUE,
                'safe_personalinfo_IDsafe_personalInfo' => $IDsafe_personalInfo,
                'includeaddress' => (boolean)@$includeAddress,
                'includeage' => (boolean)@$includeAge,
                'includegender' => (boolean)@$includeGender,
                'includedob' => (boolean)@$includeDob,
                'includeexpiry' => (boolean)@$includeExpiry
            );
            $IDsafe_prescription = $this->prescription->createPrescription($prescriptionList);
            $IDsafe_prescription_bankag = $this->nagkamoritsing->bungkag($IDsafe_prescription);

            # now connecting to doctor nag prescribed
            $prescriptionList['IDemr_user'] = $doctorInformation->IDemr_user;
            $prescriptionList['safe_prescription_IDsafe_prescription'] = @$IDsafe_prescription;
            // $IDemr_prescription_safe = $this->prescription->connectPrescriptionDoctor($prescriptionList);

            # mag email sa patient if wala pa jud siya ma emailan sa changes sa iya health record.
            # check if patient ba ni siya sa doctor
            # if dili i add siya as one of the patient
            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
            $IDemr_patient = @$IDemr_patient->IDemr_patient;
            if (empty($IDemr_patient))
                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

            
            $prescriptionList['IDsafe_personalInfo'] = $IDsafe_personalInfo;
            $prescriptionList['IDemr_user'] = $doctorInformation->IDemr_user;
            $prescriptionList['IDemr_patient'] = $IDemr_patient;
            $sessionid = $this->nagkamoritsing->ibalik($sessionid);
            
            $this->safejournal->insertjournalprescription($prescriptionList);
            $patientinfo = $this->safeuser->getSafeUserInfo($this->nagkamoritsing->bungkag($sessionid));
            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($sessionid);
            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

            $emailData = array(
                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
            );
            $message = $this->load->view(
                    'for-email.phtml', array('title' => 'Notify patient', 
                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
    
            # mag email ni siya sa patient
            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
            $this->safeuserlog->notifypatientbyemail($sessionid,$message);
            #----
        }


        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');

            echo json_encode(
                array('errorStatus' => $error, 
                    'errorMessage' => $errorMessage, 
                    'IDsafeprescription' => $IDsafe_prescription_bankag,
                    'prescriptionlist' => $prescriptionList)
            );
            # end of ajax ang display
        }
        else
        {
            $this->load->view(
            'patient-dashboard-light-blue.phtml', array(
            'title' => 'Insert Problem Information',
            'view' => 'problem/insert', 
            'safeContact' => $prescriptionList
            ));
        }

        return;
    }

    public function insertDisplay()
    {
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            return;

        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
                
        $this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));

        $this->safeprescription = $this->prescription->displayDashboardPrescription($IDsafe_personalInfo);
        
        if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo '';
        else
            $this->load->view('prescriptions/display_prescription-insert-information.phtml', array());
    }

    public function insertDisplayDoctor()
    {
        $checkReturn = $this->checksession('DOM');
        $checkdoctor = $this->checkdoctor('DOM');
        if (strcasecmp($checkdoctor['description'],'user is not a doctor')==0)
            return;

        # sessionid is also IDsafe_user in session
        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
                
        $this->myemrsession = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));

        # get the IDsafe_personalInfo
        $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($this->nagkamoritsing->ibalik($this->sessionid));
        
        # get existing prescription
        // $this->safeprescription = $this->prescription->displayDashboardPrescription($IDsafe_personalInfo);

        $this->prescriptionInfo['writtenon'] = date('Y-m-d');
        
        if (strcasecmp($checkReturn['description'],'Session expired')==0)
            echo '';
        else
            $this->load->view('prescriptions/display_prescription-insert-information-profileviewer.phtml', array());
    }

    public function prescriptionInformation()
    {
        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->IDsafeprescription = filter_var($this->input->get('IDsafeprescription'), FILTER_SANITIZE_STRING);
        $this->IDsafeprescription = preg_replace('#[^a-z0-9_]#', '', $this->IDsafeprescription);

        $this->activeprescription = @$this->nagkamoritsing->ibalik($this->IDsafeprescription);
        $this->load->view('medication/display_medication-insert-information-prescription.phtml', array());
    }

    public function prescriptionFinish()
    {
        $this->load->model('Addresses_model', 'address');
        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->IDsafeprescription = filter_var($this->input->get('IDsafeprescription'), FILTER_SANITIZE_STRING);
        $this->IDsafeprescription = preg_replace('#[^a-z0-9_]#', '', $this->IDsafeprescription);

        # IDsafe_prescription
        $this->activeprescription = @$this->nagkamoritsing->ibalik($this->IDsafeprescription);
        # get the prescription information
        $this->prescriptionInformation = $this->prescription->prescriptionInformation($this->activeprescription);
        # get the medication list
        $this->medicationlist = $this->medication->getAllMedicationInPrescription(@$this->activeprescription);
        # get the address of the patient.
        # gi separate lang nako kay baisn walay address ang patient para dili ma daot ang query
        $this->patientAddress = $this->address->getPersonalInfoAddress($this->prescriptionInformation->IDsafe_personalInfo);

        # get the right journal
        $this->journalConnected = $this->safejournal->getjournal($this->prescriptionInformation->IDemr_journal);

        $this->load->view('medication/display_medication-information-prescription-final.phtml', array());
    }

    public function savePrescription()
    {
        $this->checkSession();
        $ajax = TRUE;
        $error = TRUE;
        $errorMessage = '';
        $ajax = filter_var($this->input->get('ajax'), FILTER_SANITIZE_STRING);
        $ajax = preg_replace('#[^a-z0-9_]#', '', $ajax);
        $this->sessionid = filter_var($this->input->get('sessionid'), FILTER_SANITIZE_STRING);
        $this->sessionid = preg_replace('#[^a-z0-9_]#', '', $this->sessionid);
        $this->IDsafeprescription = filter_var($this->input->get('IDsafeprescription'), FILTER_SANITIZE_STRING);
        $this->IDsafeprescription = preg_replace('#[^a-z0-9_]#', '', $this->IDsafeprescription);
        // IDsafeprescription


        $this->form_validation->set_rules('notes', 'notes', 'trim|xss_clean');
        $this->form_validation->set_rules('doctor-notes', 'doctor notes', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $error = true;
            $errorMessage = validation_errors();
        }
        else
        {
            $error = false;
        }


        if ($error == FALSE)
        {
            $notes = filter_var($this->input->post('notes'), FILTER_SANITIZE_STRING); # notes for patient
            $doctorNotes = filter_var($this->input->post('doctor-notes'), FILTER_SANITIZE_STRING); # notes for doctor
            $this->IDsafeprescription = filter_var($this->input->post('IDsafe_prescription'), FILTER_SANITIZE_STRING); # notes for doctor
            $this->IDemr_journal = filter_var($this->input->post('IDemr_journal'), FILTER_SANITIZE_STRING); # help us identify if it's modified
            $this->includeAddress = filter_var($this->input->post('includeAddress'), FILTER_SANITIZE_STRING);
            $this->includeAge = filter_var($this->input->post('includeAge'), FILTER_SANITIZE_STRING);
            $this->includeGender = filter_var($this->input->post('includeGender'), FILTER_SANITIZE_STRING);
            $this->includeDob = filter_var($this->input->post('includeDob'), FILTER_SANITIZE_STRING);
            
            $prescriptionArray = array(
                'notes' => $notes,
                'includeaddress' => (boolean)$this->includeAddress,
                'includeage' => (boolean)$this->includeAge,
                'includegender' => (boolean)$this->includeGender,
                'includedob' => (boolean)$this->includeDob,
                'IDemr_journal' => $this->IDemr_journal
            );
            
            #----
            # check if patient ba ni siya sa doctor
            # if dili i add siya as one of the patient
            $IDsafe_user = $this->nagkamoritsing->ibalik($this->sessionid);
            $IDsafe_personalInfo = $this->personalprofile->getIDSafePersonalInfo($IDsafe_user);
            $doctorInformation = $this->emruser->getDocInfo($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user']));
            $IDemr_patient = $this->safejournal->getpatientofdoctor($IDsafe_personalInfo, $doctorInformation->IDemr_user);
            $IDemr_patient = @$IDemr_patient->IDemr_patient;
            if (empty($IDemr_patient))
                $IDemr_patient = $this->safejournal->addthispatient($IDsafe_personalInfo, $doctorInformation->IDemr_user);

            
            $prescriptionArray['IDsafe_personalInfo'] = $IDsafe_personalInfo;
            $prescriptionArray['IDemr_user'] = $doctorInformation->IDemr_user;
            $prescriptionArray['doctor-notes'] = $doctorNotes;
            $prescriptionArray['IDemr_patient'] = $IDemr_patient;
            $prescriptionArray['safe_prescription_IDsafe_prescription'] = $this->IDsafeprescription;
            $prescriptionArray['IDsafe_prescription'] = $this->IDsafeprescription;
            
            $this->prescription->updatePrescriptionInformation($prescriptionArray);            
            
            # doctor notes connection and for sending email
            $this->safejournal->addjournal($prescriptionArray,'safe_prescription');
            $patientinfo = $this->safeuser->getSafeUserInfo($this->sessionid);
            $patientinfoemail = $this->safeuser->getSafeUserEmailsInfo($IDsafe_user);
            $footerEmail = $this->nagkamoritsing->ibalik(@$patientinfoemail->emailAddress);

            $emailData = array(
                'firstname' => $this->nagkamoritsing->ibalik(@$patientinfo->firstname),
                'lastname' => $this->nagkamoritsing->ibalik(@$patientinfo->lastname)
            );
            $message = $this->load->view(
                    'for-email.phtml', array('title' => 'Notify patient', 
                    'view' => 'emails/notify-patient', 'emailData' => $emailData, 'footerEmail' => $footerEmail), true);
    
            # dont forget to pair this sa $this->safeuserlog->logUser($data); kailangan sila mag kuyog kung mahimo
            $this->safeuserlog->notifypatientbyemail($IDsafe_user,$message);
            # ---
        }

        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');


            echo json_encode(
                array('errorStatus' => $error, 
                    'errorMessage' => $errorMessage,
                    'prescriptionArray' => $prescriptionArray
                    )
            );
            # end of ajax ang display
        }
        else
        {
            // $this->load->view(
            // 'patient-dashboard-light-blue.phtml', array(
            // 'title' => 'Insert Problem Information',
            // 'view' => 'problem/insert', 
            // 'safeContact' => $prescriptionList
            // ));
        }

        return;
    }

	private function checksession($displaytype = 'HTML')
    {
        # redirect users to login who are not properly logged in
        if (!empty($this->nambal_session['sessionName']))
        {
            # check if the Username, IDsafe_user and IDsafe_personalInfo matches
            if(!$this->login->checkIfSessionIsReal($this->nambal_session['sessionName'],$this->nambal_session['IDsafe_user'] ,$this->nambal_session['IDsafe_personalInfo']))
            {
            	$errorReturn = $this->errorHandlings($displaytype);
            	return $errorReturn;
            }
            # end of checking if the Username, IDsafe_user and IDsafe_personalInfo matches     
            # check if session and in the database are same
            if (!$this->sessionModel->compareSessionToDatabase($this->nambal_session['sessionAddress'], $this->nambal_session['IDsafe_user']))
            {
               $this->errorHandlings($displaytype);
            }
            # end of checking if the session and in the database are same   

            # check if the session IP address is same in users IP address
            # you can use this for quicker load in small CRUD request
            if (!$this->sessionModel->checkSessionIP($this->nambal_session['sessionAddress']))
            {
                $this->errorHandlings($displaytype);
            }
            # end of checiing if the usersIP is same with session IP  

            # check if the user has address
            if ((empty($this->nambal_session['street'])) || (empty($this->nambal_session['country'])) || (empty($this->nambal_session['city'])) )
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3002);
                // redirect(base_url().'register/createAccountSecond', 'refresh');
            }

            # check if the user has primary emergency contact person
            if (!$this->login->checkPrimaryContact($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_personalInfo'])))
            {
            	$this->errorHandlings($displaytype,'register/createAccountSecond', 3003);
                // redirect(base_url().'register/createAccountThird', 'refresh');
            }

            #
        }
        else
        {
        	$errorReturn = $this->errorHandlings($displaytype);
        	return $errorReturn;
        }
        # end of redirecting users to dashboard if logged in
    }

    private function errorHandlings($displaytype = 'HTML',$url = 'login', $errornum = 3001)
    {
    	$errorD = $this->errordisplay->identifyError($errornum);
    	if (strcasecmp('DOM', $displaytype)==0)
        {
        	echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
        	return $errorD;
        }
        elseif (strcasecmp('AJAX', $displaytype)==0) 
        {
        	echo json_encode(
				array(
					'errorStatus' => TRUE, 
					'errorMessage' => $errorD['userdisplaytext'].' '.$errorD['resolutiontext']
					));	
        	return $errorD;
        }
        else
        	redirect(base_url().$url, 'refresh');
    }

    private function checkdoctor($displaytype = 'HTML')
    {
    	$errorD = $this->errordisplay->identifyError(2001);
        # check if the user is a registered doctor in nambal
        if (!$this->emruser->checkIfDoctor($this->nagkamoritsing->ibalik($this->nambal_session['IDsafe_user'])))
        {
        	if (strcasecmp('DOM', $displaytype)==0)
        	{
  		        echo '<div class="alert alert-danger">'.$errorD['userdisplayhtml'].' '.$errorD['resolutionhtml'].'</div>';
  		        return $errorD;
        	}
        	else
            	redirect(base_url().'registerDoctor?errorNum=2001', 'refresh');
        }
        # end of checking if the user is a registered doctor in nambal  
    }

    public function prescriptionvisibility()
    {
        $error = FALSE;
        $errorMessage = '';
        $ajax = TRUE;
        
        if ($ajax ==TRUE)
        {
            # ajax ang display diri
            header('Content-Type: application/json');


            echo json_encode(
                array('errorStatus' => $error, 
                    'errorMessage' => $errorMessage,
                    )
            );
            # end of ajax ang display
        }
    }


}

?>