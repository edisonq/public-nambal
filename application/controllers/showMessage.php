<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ShowMessage extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view(
			'homepage.phtml', array('title' => 'Message List',
				'view' => 'show-message/index'));
		$msgID = $this->input->get('msgID');
		echo 'profileID:'.$msgID;

		return;
	}
}
?>