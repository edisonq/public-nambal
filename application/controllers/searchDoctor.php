<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SearchDoctor extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data = array(
			'view' => 'connection/search-doctor'
		);
		$this->load->view('templatev3.phtml', $data);
	}
}