<?php

class Dashboardv2 extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data = array(
			'view' => 'dashboard/dashboardv2'
		);
		$this->load->view('templatev2.phtml', $data);
	}

	public function doctor() {
		$data = array(
			'view' => 'dashboard/doctor'
		);
		$this->load->view('templatev2.phtml', $data);
	}

	public function pharmacy() {
		$data = array(
			'view' => 'dashboard/pharmacy'
		);
		$this->load->view('templatev2.phtml', $data);
	}

	public function laboratory() {
		$data = array(
			'view' => 'dashboard/laboratory'
		);
		$this->load->view('templatev2.phtml', $data);
	}

	public function patient() {
		$data = array(
			'view' => 'dashboard/patient'
		);
		$this->load->view('templatev2.phtml', $data);
	}
}

?>